(ns lv.demo)
(use 'lv.core)

(layout "grid" [wvid0 cline1] [ras3 ffb2])
(ipcam
       ; "http://192.168.2.64:8080/?action=stream"
       ; "http://212.142.228.68/axis-cgi/mjpg/video.cgi"
       "http://webkamera.overtornea.se/mjpg/video.mjpg?webcam.jpg"
       ; "http://79.141.146.81/mjpg/video.mjpg"
)
(render)

; read from a Motion JPEG IP camera and increase saturation
(hsv :wvid0 [c0 c1 c2 c3]
  (sx 1.8) (sy 2.6)
  (hsv (* (vec3 1 1.2 1) c1.hsv))
)

; find contours
(expr :cline1 [wvid0]
  (count 250)
  (max 5000)
  (thres 0.5)
  (fill 1)
  (paint -1)
  (holes 1)
)
; motion detection
(expr :ffb2 [wvid0]
  (reduce 4)
  (mode 3)
  (gauss -1)
  (levels 2)
  (win 8)
  (iter 4)
  (polyn 7)
  (polys 1.2)
  (r 20)
  (step 3)
)
; mixer
(rgb :ras3 [cline1 ffb2]
  (rgb (mix
          (* -1 cline1.rgb)
          ffb2.rgb
          0.7))
)
