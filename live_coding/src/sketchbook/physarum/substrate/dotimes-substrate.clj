(ns lv.demo)
(use 'lv.core)

(layout "grid" [ras0 clag1]
              [ras3])

(hsv :ras0 []
  (set vv 1)
  (dotimes [n 11]
    (set dir (even? n 1 -1))
    (set speed (* dir (+ n 1) 0.0011 f))
    (set! vv (* vv (distance  (* (vec2 x y) 2)
                         (vec2 (cos speed)
                               (sin speed))))))
  (set edge 6.0)
  (set border 2)
  (set val (smoothstep (- edge border) (+ edge border) (* vv 5)))
  ; (v (- 1 (smoothstep (- edge border) (+ edge border) (* vv 5))))
  (v (pos? x val (- 1 val)))
)

(expr :clag1 [ras0]
  "physarum/substrate/rotation-substrate.cl"
  (paint 1)

  (p0 (+ (* ks0 0.001) 0.00005))
  (p1 (* ks1 0.01))
  (p2 (* ks2 0.2))
  (p3 (* ks3 0.5))
  (p5 (sign (- (rand) 0.9)))
  (p6 (sqrt (/ ks6 16)))
  (p7 (+ ks7 0.1))
)

(rgb ras3 [clag1 ras0]
  (rgb (mix clag1.rgb (* 0.8 ras0.rgb) ks5))
)
