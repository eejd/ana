#pragma once

#include "ofMain.h"
#include "ofxOsc.h"

#include "Filter.h"
#include "OscListener.h"
#include "calc/ScalarLisp.h"

class FvPl: public Filter {
public:
    FvPl(ofGLWindowSettings settings, OscListener * oscParams);
    virtual ~FvPl();
    virtual void setup(const std::string id, int numSamples);
    virtual void reload(ofxOscMessage const &msg);
    virtual void update();
    virtual void draw(int li);

    ScalarLisp slMode;
    int draw_mode = 0;
    ScalarLisp slClear;

    ScalarLisp slCount;
    ScalarLisp slX;
    ScalarLisp slY;
    ScalarLisp slZ;
    ScalarLisp slW;
    ScalarLisp slH;
    ScalarLisp slS;
    ScalarLisp slV;
    ScalarLisp slA;
};
