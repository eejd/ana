__kernel void image_setup(
__global int* counters,
__write_only image2d_t image2
) 
{
    // dummy
}

__kernel void image_change(
__global int* counters,
__write_only image2d_t image2
) 
{
    // dummy
}

// --- noise ----------------------------------------------------------
// see: https://thebookofshaders.com/11/
// see: https://github.com/ashima/webgl-noise/blob/master/src/noise2D.glsl
//
// Description : Array and textureless GLSL 2D simplex noise function.
//      Author : Ian McEwan, Ashima Arts.
//  Maintainer : stegu
//     Lastmod : 20110822 (ijm)
//     License : Copyright (C) 2011 Ashima Arts. All rights reserved.
//               Distributed under the MIT License. See LICENSE file.
//               https://github.com/ashima/webgl-noise
//               https://github.com/stegu/webgl-noise
//

float3 mod289_3(float3 x) {
  return (float3)(x.x - floor(x.x * (1.0 / 289.0)) * 289.0,
                  x.y - floor(x.y * (1.0 / 289.0)) * 289.0,
                  x.z - floor(x.z * (1.0 / 289.0)) * 289.0);
}

float2 mod289_2(float2 x) {
  return (float2)(x.x - floor(x.x * (1.0 / 289.0)) * 289.0,
                  x.y - floor(x.y * (1.0 / 289.0)) * 289.0);
}

float3 permute(float3 x) {
  return mod289_3((float3)(((x.x*34.0)+1.0)*x.x,
                  ((x.y*34.0)+1.0)*x.y,
                  ((x.z*34.0)+1.0)*x.z));
}


float snoise(float2 v)
{
  const float4 C = (float4)(0.211324865405187,  // (3.0-sqrt(3.0))/6.0
                      0.366025403784439,  // 0.5*(sqrt(3.0)-1.0)
                     -0.577350269189626,  // -1.0 + 2.0 * C.x
                      0.024390243902439); // 1.0 / 41.0
// First corner
  float2 i  = floor(v + dot(v, C.yy) );
  float2 x0 = v -   i + dot(i, C.xx);

// Other corners
  float2 i1;
  i1 = (x0.x > x0.y) ? (float2)(1.0, 0.0) : (float2)(0.0, 1.0);
  float4 x12 = x0.xyxy + C.xxzz;
  x12.xy -= i1;

// Permutations
  i = mod289_2(i); // Avoid truncation effects in permutation
  float3 p = permute( permute( i.y + (float3)(0.0, i1.y, 1.0 ))
		+ i.x + (float3)(0.0, i1.x, 1.0 ));

  float3 m = max((float3)(0.5) - (float3)(dot(x0,x0), dot(x12.xy,x12.xy), dot(x12.zw,x12.zw)), (float3)(0.0));
  m = m*m ;
  m = m*m ;

// Gradients: 41 points uniformly over a line, mapped onto a diamond.
// The ring size 17*17 = 289 is close to a multiple of 41 (41*7 = 287)

  float3 dummy;
  float3 x = (float3)(2.0) * fract(p * C.www, &dummy) - (float3)(1.0);
  float3 h = fabs(x) - (float3)(0.5);
  float3 ox = floor(x + (float3)(0.5));
  float3 a0 = x - ox;

// Normalise gradients implicitly by scaling m
// Approximation of: m *= inversesqrt( a0*a0 + h*h );
  m *= (float3)(1.79284291400159) - (float3)(0.85373472095314) * ( a0*a0 + h*h );

// Compute final noise value at P
  float3 g;
  g.x  = a0.x  * x0.x  + h.x  * x0.y;
  g.yz = a0.yz * x12.xz + h.yz * x12.yw;
  return 130.0 * dot(m, g);
}


//-----------------------------
#define a_index(ix,iy) (SX*iy + ix)
#define g_index(ix,iy) (2*SX*iy + 2 * ix)
__kernel void set_zero(
__global int* countersR,
__global int* countersG,
__global int* countersB
) 
{
    const int ix = get_global_id(0);
    const int iy = get_global_id(1);
    const int SX = get_global_size(0);
    const int i_here = a_index(ix, iy);

    countersR[i_here] = 0;
    countersG[i_here] = 0;
    countersB[i_here] = 0;
}

__kernel void map_r2(
__global int* countersR,
__global int* countersG,
__global int* countersB,
__read_only image2d_t image_in,
__global float* gaussian,
__global float* uniform,
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7,
const float dt0,
const float dt1,
const float dt2,
const float dt3,
const float dt4,
const float dt5,
const float dt6,
const float dt7
) 
{
    const int ix = get_global_id(0);
    const int iy = get_global_id(1);
    const int SX = get_global_size(0);
    const int SY = get_global_size(1);

    sampler_t smp = CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_NEAREST;

    float px = (float)ix / (float)SX;
    float py = (float)iy / (float)SY;
    const float w0 = M_PI_F;
    float2 pos = (float2)((2.0f*px-1) * w0,
                          (2.0f*py-1) * w0);
    
    #define xspace    p0
    #define yspace    p1
    #define xnoise    p2
    #define ynoise    p3
    #define xscatter  p4
    #define yscatter  p5
    #define density   p6
    #define amplitude p7
    
    const int g_here = g_index(ix, iy);
    if (uniform[g_here] < density && iy % 4 == 0) {
        pos.y += amplitude * snoise((8.0f * pos) * (float2)(xnoise, ynoise) + (float2)(dt0, dt1));
        pos *= (float2)(xspace, yspace);
        pos += (float2)((gaussian[g_here] - 0.5) * xscatter,
                        (gaussian[g_here+1] - 0.5) * yscatter);

        float2 out_pos = 0.5f + pos / ( w0 * 2);
        int ox_pos = SX*out_pos.x;
        if(ox_pos >= 0 && ox_pos < SX) {
            int oy_pos = SY*out_pos.y;
            if(oy_pos >= 0 && oy_pos < SY) {
                const float4 color_in = read_imagef(image_in, smp, (int2)(ix, iy));
                const int red =   (int)(256*color_in.x);
                const int green = (int)(256*color_in.y);
                const int blue =  (int)(256*color_in.z);
                const int o_here = a_index(ox_pos, oy_pos);
                atomic_add(&countersR[o_here], red);
                atomic_add(&countersG[o_here], green);
                atomic_add(&countersB[o_here], blue);
            }
        }
    }
}

__kernel void write_image(
__global int* countersR,
__global int* countersG,
__global int* countersB,
__write_only image2d_t image2
) 
{
    const int ix = get_global_id(0);
    const int iy = get_global_id(1);
    const int SX = get_global_size(0);
    const int i_here = a_index(ix,iy);

    float red =   countersR[i_here] > 0 ? tanh((float)(countersR[i_here]/256)) : 0;
    float green = countersG[i_here] > 0 ? tanh((float)(countersG[i_here]/256)) : 0;
    float blue =  countersB[i_here] > 0 ? tanh((float)(countersB[i_here]/256)) : 0;
    write_imagef(image2, (int2)(ix, iy), (float4)(red, green, blue, 1));
}
