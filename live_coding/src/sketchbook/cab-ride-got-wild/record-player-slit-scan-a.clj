(ns lv.demo)
(use 'lv.core)

(layout "grid" [imgs0 imgs1] [imgs2 cline3])
; assume there are lots of JPEG images in the media folder.
; all imgaes have the same size 1080 x 1080
(imagesequ "rp-a-1080/*.png")
(render)

(hsv :imgs0 [c0]
  ; (rot (* -0.01 f))
  ; (k (mix (* 0.001 i j) sframe r))
  (k (mix 0 sframe (usin (* 0.1 r))))
  (hsv (* (vec3 1 1 2) c0.hsv))
)
(render 0)

(hsv :imgs1 [c0]
  (rot (* -0.01 f))
  (k (% (int (+ (* r sframe) f))
        slength))
  (hsv (* (vec3 1 1 2) c0.hsv))
)
(render 1)

(hsv :imgs2 [c0]
  ; (tx 0.25) (ty 0.5)
  ; (sx 2)    (sy 2)
  (k (+ (* 0.5 sframe)
        (* 0.5 (* slength
            (unoise (* 0.001 f)
                    (snoise (* 0.0001 f) x))))))
  (h c0.h)
  (s c0.s)
  (v (* 3 (- 1 (sqr r)) c0.v))
)
(render 2)

(expr :cline3 [imgs0]
  (count 100)
  (max 500)
  (thres 0.35)
  (paint 1)
  (fill 1)
  (holes 1)
)
(render 3)
