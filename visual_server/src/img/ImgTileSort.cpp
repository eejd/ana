#include "ofMain.h"
#include "Configuration.h"
#include "globalvars.h"
#include "OscListener.h"
#include "Filter.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispMng.h"
#include "img/ImgTileSort.h"

ImgTileSort::ImgTileSort(ofGLWindowSettings settings, OscListener * oscParams) :
        Filter(settings, oscParams) {
}

ImgTileSort::~ImgTileSort() {
}

void ImgTileSort::setup(const std::string id, int numSamples) {
    Filter::setup(id, numSamples);
}

void ImgTileSort::reload(ofxOscMessage const &msg) {
    ScalarLispMng mng;
    auto narg = msg.getNumArgs();
    if(narg > 1)
        Filter::setInputTexture(msg.getArgAsString(1));
    mng.compile(slMode, "mode", "", msg);
    mng.compile(slClutterLevel, "clutter", "0", msg);
    mng.compile(slCompare, "comp", "(+ h)", msg);
    mng.compile(slTileSizeX, "x", "60", msg);
    mng.compile(slTileSizeY, "y", "60", msg);
}

static bool comparePixel(Tpix i1, Tpix i2)
{
    return i1.comp < i2.comp;
}

inline float mix(float v1, float v2, float a) {
    if(a < 0) return v1;
    if(a >= 1) return v2;
    return v1 * (1-a) + v2 * a;
}

void ImgTileSort::update() {
    mode = slMode.rpn.execute0();
    tile_size_x = (size_t)std::max(1.0, slTileSizeX.rpn.execute0());
    tile_size_y = (size_t)std::max(1.0, slTileSizeY.rpn.execute0());
    hpix.clear();
    if(upstream_ids.size() >= 1) {
        auto in_id = upstream_ids[0];
        if(filterDict.find(in_id) != filterDict.end()) {
            auto f = filterDict[in_id];
            if(f) {
                f->getTexture().readToPixels(pixels);
                switch(mode) {
                case 1: { // permute
                    for(size_t tiy = 0; tiy < filterHeight / tile_size_y; ++tiy) {
                        for(size_t tix = 0; tix < filterWidth / tile_size_x; ++tix) {
                            const Tpix hx { tix, tiy, (float)slCompare.rpn.execute3d1i(tile_size_x*tix,
                                                                                       tile_size_y*tiy, 0.0, hpix.size()) };
                            hpix.push_back(hx);
                        }
                    }
                    float hpix_len = (float)hpix.size();
                    size_t hi = 0;
                    for(size_t tiy = 0; tiy < filterHeight / tile_size_y; ++tiy) {
                        for(size_t tix = 0; tix < filterWidth / tile_size_x; ++tix) {
                            float rnd = static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
                            hpix[hi].comp = mix((float)hi / hpix_len, rnd, (float)slClutterLevel.rpn.execute3d1i(tile_size_x*tix,
                                                                                                                 tile_size_y*tiy,
                                                                                                                 0.0, hi));
//                            float level = (float)slClutterLevel.rpn.execute3d1i(tix, tiy, 0.0, hi);
//                            hpix[hi].comp = (float)hi / (float)hpix_len + rnd * level;
//                            ofLogNotice() << level << " " << (float)hi / (float)hpix_len << " " << rnd;
                            ++hi;
                        }
                    }
                    break;
                }
                default: {
                    float* pd = pixels.getData();
                    for(size_t tiy = 0; tiy < filterHeight / tile_size_y; ++tiy) {
                        const size_t offset_y = tiy*tile_size_y;
                        for(size_t tix = 0; tix < filterWidth / tile_size_x; ++tix) {
                            const size_t offset_x = tix*tile_size_x;
                            double r_sum = 0.0;
                            double g_sum = 0.0;
                            double b_sum = 0.0;
                            for(size_t pyi=0; pyi<tile_size_y; ++pyi) {
                                for(size_t pxi=0; pxi<tile_size_x; ++pxi) {
                                    size_t pdi = ((offset_x+pxi) + (offset_y+pyi) * filterWidth ) * 4;
                                    r_sum += pd[pdi];
                                    g_sum += pd[pdi+1];
                                    b_sum += pd[pdi+2];
                                }
                            }
                            const float divisor = tile_size_x * tile_size_y;
                            r_sum /= divisor;
                            g_sum /= divisor;
                            b_sum /= divisor;
                            ofFloatColor f((float)r_sum, (float)g_sum, (float)b_sum);
                            f.getHsb(h0, s0, v0);
                            const Tpix hx { tix, tiy, (float)slCompare.rpn.execute3d1i(tix, tiy, 0.0, hpix.size()) };
                            hpix.push_back(hx);
                        }
                    }
                    break;
                }
                }
            }
        }
    }
    stable_sort(hpix.begin(), hpix.end(), comparePixel);
}

void ImgTileSort::draw(int li) {
    fbo.begin();
    ofClear(0, 0, 0, 255);

    ofFloatImage img;
    img.setFromPixels(pixels);
    size_t cols = filterWidth  / tile_size_x;
    size_t hlen = hpix.size();
    size_t col = 0;
    size_t row = 0;
    for(size_t ih = 0; ih < hlen; ++ih) {
        img.drawSubsection(tile_size_x * col,
                           tile_size_y * row,
                           tile_size_x,
                           tile_size_y,
                           hpix[ih].tile_ix * tile_size_x,
                           hpix[ih].tile_iy * tile_size_y);
        ++col;
        if(col >= cols) {
            ++row;
            col = 0;
        }
    }

    fbo.end();
}
