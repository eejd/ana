#include <string>

#include "util/stringhelper.h"
#include "globalvars.h"
#include "Configuration.h"
#include "Filter.h"
//#include "Pool.h"
#include "ofApp.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispMng.h"
#include "OscListener.h"

OscListener::OscListener() {
    if(!apptarget) {
        ofLog(OF_LOG_FATAL_ERROR) << "OscListener: apptraget not set.";
        std::exit(1); // should not return from this call
    }
}

OscListener::~OscListener() {
    receiver.stop();
}

void OscListener::setup() {
    dump_osc_input = asBool(DUMP_OSC_INPUT);
    dump_frag_src = asBool(DUMP_FRAG_SRC);
    receiver.setup(asInt(ANALOG_PORT));
}

void OscListener::reloadByID(std::string const &id, ofxOscMessage const &msg) {
    std::string id0 = id.find(":") == 0 ? id.substr(1) : id;
    if(filterDict.find(id0) != filterDict.end()) {
        auto f = filterDict[id0];
        if(f) {
            f->reload(msg);
        }
    }
}

std::string OscListener::msg_toString(ofxOscMessage &msg) {
    std::stringstream info;
    info << "### " << msg.getAddress() << "\n";
    info << "| index | type | content | comment |\n";
    bool isFragSrc = msg.getAddress() == "/ana_frag";

    auto numArgs = msg.getNumArgs();
    for(size_t n = 0; n < numArgs; ++n) {
        if(isFragSrc && n == 3) {
            std::string param = msg.getArgAsString(n);
            if(dump_frag_src) {
                param = msg.getArgAsString(n);
                info << "| " << n << " | " << (char) msg.getArgType(n) << "\n" << param << "\n";
            }
            else {
                param = msg.getArgAsString(n).substr(0, 32);
                findAndReplaceAll(param, "\n", "<br>");
                info << "| " << n << " | " << (char) msg.getArgType(n) << " | " << param << "... |  |\n";
            }
        }
        else {
//            info << n << " " << (char) msg.getArgType(n) << " " << msg.getArgAsString(n) << "\n";
            info << "| " << n << " | " << (char) msg.getArgType(n) << " | " << msg.getArgAsString(n) << " |  |\n";
        }
    }
    return info.str();
}

void OscListener::processCommand(const std::string &path, const ofxOscMessage &msg) {
    waiting_osc = false;
    size_t numArgs = 0;
    try {
        if(path.find("/ana_u") == 0) {
            if(path == "/ana_uniform") {
                numArgs = std::min(msg.getNumArgs(), (size_t)8);
                for(size_t ax=0; ax<numArgs; ++ax) {
                    switch(ax) {
                    case 0:
                        u0 = msg.getArgAsFloat(0);
                        break;
                    case 1:
                        u1 = msg.getArgAsFloat(1);
                        break;
                    case 2:
                        u2 = msg.getArgAsFloat(2);
                        break;
                    case 3:
                        u3 = msg.getArgAsFloat(3);
                        break;
                    case 4:
                        u4 = msg.getArgAsFloat(4);
                        break;
                    case 5:
                        u5 = msg.getArgAsFloat(5);
                        break;
                    case 6:
                        u6 = msg.getArgAsFloat(6);
                        break;
                    case 7:
                        u7 = msg.getArgAsFloat(7);
                        break;
                    }
                }
                return;
            }
            if(path == "/ana_u0") {
                u0 = msg.getArgAsFloat(0);
                return;
            }
            if(path == "/ana_u1") {
                u1 = msg.getArgAsFloat(0);
                return;
            }
            if(path == "/ana_u2") {
                u2 = msg.getArgAsFloat(0);
                return;
            }
            if(path == "/ana_u3") {
                u3 = msg.getArgAsFloat(0);
                return;
            }
            if(path == "/ana_u4") {
                u4 = msg.getArgAsFloat(0);
                return;
            }
            if(path == "/ana_u5") {
                u5 = msg.getArgAsFloat(0);
                return;
            }
            if(path == "/ana_u6") {
                u6 = msg.getArgAsFloat(0);
                return;
            }
            if(path == "/ana_u7") {
                u7 = msg.getArgAsFloat(0);
                return;
            }
        }
        if(path == "/ana_quit") {
            ofLog(OF_LOG_NOTICE) << "-> " << path;
            apptarget->stopsrv = true;
            receiver.stop();
            apptarget->exit("OSC message /ana_quit from " + msg.getRemoteHost() + " arrived.");
            std::exit(0); // should not return from this call
            return;
        }
        if(path == "/ana_f_clear") {
            ofLog(OF_LOG_NOTICE) << "-> " << path;
            frame = 0;
            apptarget->preShot.reset();
            return;
        }
        if(path == "/ana_sync") {
            ofLog(OF_LOG_NOTICE) << "-> " << path;
            frame = 0;
            beat.setPositionMS(0);
            apptarget->preShot.reset();
            return;
        }

        numArgs = msg.getNumArgs();
        if(numArgs >= 1) {
            if(path == "/ana_render") {
                std::string ma0 = msg.getArgAsString(0);
                ofLog(OF_LOG_NOTICE) << "-> " << path << " " << ma0;
                apptarget->layoutMng.setRender(ma0);
            } else if(path == "/ana_expr"
                   || path == "/ana_fx"
                   || path == "/ana_frag"
                   || path == "/ana_matrix"
                   || path == "/ana_patch") {
                std::string ma0 = msg.getArgAsString(0);
                ofLog(OF_LOG_NOTICE) << "-> " << path << " " << ma0;
                reloadByID(ma0, msg);
            } else if(path == "/ana_layout") {
                pipe0 = msg.getArgAsString(0);
                ofLog(OF_LOG_NOTICE) << "-> " << path << " " << pipe0;
                apptarget->loadPipeline();
            } else if(path == "/ana_sound") {
                sound0 = msg.getArgAsString(0);
                ofLog(OF_LOG_NOTICE) << "-> " << path << " " << sound0;
                apptarget->loadSound();
            } else if(path == "/ana_image") {
                image0 = msg.getArgAsString(0);
                ofLog(OF_LOG_NOTICE) << "-> " << path << " " << image0;
                apptarget->loadImage();
            } else if(path == "/ana_imagesequ") {
                imagesequ0 = msg.getArgAsString(0);
                ofLog(OF_LOG_NOTICE) << "-> " << path << " " << imagesequ0;
                apptarget->loadImageSequence();
            } else if(path == "/ana_movie") {
                movie0 = msg.getArgAsString(0);
                ofLog(OF_LOG_NOTICE) << "-> " << path << " " << movie0;
                apptarget->loadMovie();
            } else if(path == "/ana_ipmjpeg") {
                ofLog(OF_LOG_NOTICE) << "-> " << path << " " << msg.getArgAsString(0);
                apptarget->loadIpVideoGrabber(msg);
            } else if(path == "/ana_texpr") {
                ofLog(OF_LOG_NOTICE) << "-> " << path << " " << msg.getArgAsString(0);
                ScalarLispMng mng;
                mng.compile(slT0, "t0", "", msg, 0);
                mng.compile(slT1, "t1", "", msg, 0);
                mng.compile(slT2, "t2", "", msg, 0);
                mng.compile(slT3, "t3", "", msg, 0);
                mng.compile(slT4, "t4", "", msg, 0);
                mng.compile(slT5, "t5", "", msg, 0);
                mng.compile(slT6, "t6", "", msg, 0);
                mng.compile(slT7, "t7", "", msg, 0);
            } else if(path == "/ana_inspect" && numArgs >= 2) {
                ofLog(OF_LOG_NOTICE) << "-> " << path << " " << msg.getArgAsString(0);
#ifdef WITH_openCL
                inspector.setInputFilter(msg.getArgAsString(0));
                inspector.clAnalysisProgName = asString(MEDIA_PATH) + "/" + msg.getArgAsString(1);
                inspector.pathClAnalysisProg = inspector.clAnalysisProgName;
                inspector.reloadClAnalysisProg(true);
#endif // WITH_openCL

                ScalarLispMng mng;
                mng.compile(slInspectChannels, "channels", "2", msg);
                mng.compile(slInspectX0, "x0", "-0.8", msg);
                mng.compile(slInspectY0, "y0", "-0.8", msg);
                mng.compile(slInspectR0, "r0", "0.01", msg);
                mng.compile(slInspectX1, "x1", "0.8", msg);
                mng.compile(slInspectY1, "y1", "0.8", msg);
                mng.compile(slInspectR1, "r1", "0.01", msg);
                mng.compile(slInspectX2, "x2", "-0.8", msg);
                mng.compile(slInspectY2, "y2", "0.8", msg);
                mng.compile(slInspectR2, "r2", "0.01", msg);
                mng.compile(slInspectX3, "x3", "0.8", msg);
                mng.compile(slInspectY3, "y3", "-0.8", msg);
                mng.compile(slInspectR3, "r3", "0.01", msg);
            }
        }
    } catch(const std::exception &ex) {
        ofLog(OF_LOG_ERROR) << "-> " << path << " #" << numArgs << " exception " << ex.what();
    }
}

void OscListener::update() {
    // check for waiting OSC messages
    while(receiver.hasWaitingMessages() && !apptarget->stopsrv) {
        ofxOscMessage msg;
        receiver.getNextMessage(msg);
        if(dump_osc_input) {
            ofLog(OF_LOG_NOTICE) << "-> OSC from " << msg.getRemoteHost() << "\n" << msg_toString(msg);
        }
        std::string path = msg.getAddress();
        if(path.find("/ana_") == 0) {
            processCommand(path, msg);
        }
        else if(path.find("/bespoke/") == 0) {
            if(path == "/bespoke/slider0") {
                u0 = msg.getArgAsFloat(0);
            }
            else if(path == "/bespoke/slider1") {
                u1 = msg.getArgAsFloat(0);
            }
            else if(path == "/bespoke/slider2") {
                u2 = msg.getArgAsFloat(0);
            }
            else if(path == "/bespoke/slider3") {
                u3 = msg.getArgAsFloat(0);
            }
            else if(path == "/bespoke/slider4") {
                u4 = msg.getArgAsFloat(0);
            }
            else if(path == "/bespoke/slider5") {
                u5 = msg.getArgAsFloat(0);
            }
            else if(path == "/bespoke/slider6") {
                u6 = msg.getArgAsFloat(0);
            }
            else if(path == "/bespoke/slider7") {
                u7 = msg.getArgAsFloat(0);
            }
        }
// var pool is removed yet
//        else {
//            auto numArgs = msg.getNumArgs();
//            try {
//                if(numArgs == 1)
//                    Pool::add(msg);
//            } catch(const std::exception &ex) {
//                ofLog(OF_LOG_ERROR) << "-> " << path << " #" << numArgs << " exception " << ex.what();
//            }
//        }
    }
}
