#pragma once

#include "ana_features.h"

#include "ofMain.h"
#ifdef WITH_openCL
#include "MSAOpenCL.h"
#endif

#include "Filter.h"
#include "Layout.h"
#include "CursorManager.h"
#include "OscListener.h"
#include "PreShot.h"
#include "MermaidGraph.h"
#include "layout/LayoutMng.h"
#include "input/Joystick_dev_input_js.h"
#include "input/NanoKontrol2_midi_input.h"
#include "calc/ScalarLisp.h"

#ifdef WITH_openCL
extern msa::OpenCL openCL;
#endif

class ofApp: public ofBaseApp {
public:

    static const int infoPosX = 5;
    static const int infoPosY = 35;

    ofApp(ofGLWindowSettings settings);
    void setup();
    void loadPipeline();
    void loadSound();
    void loadImage();
    void loadImageSequence();
    void loadImageSequence_per_pixel();
    void loadMovie();
    void loadGrabber();
    void loadIpVideoGrabber(ofxOscMessage const &msg);
    void update();
    void draw();
    void closeVideoGrabber();
    void closeIpVideoGrabber();
    void exit(std::string msg);
    void keyPressed(int key);
    void mouseMoved(int x, int y);
    void audioIn(ofSoundBuffer &input);
    void exportImage();
    void startPreshotExport();

    bool stopsrv = false;
    PreShot preShot;
    ofxOscSender talkToSClang;
    LayoutMng layoutMng;

private:
    void toggleCsvExport();
    void updateMovies();
    void updateVideoGrabber();
    void updateJoystick();
    void updateExpressions();
    void updateSound();

    ofGLWindowSettings window_settings;

    CursorManager cursor;

    string exportPreshotPath = "";
    string exportCsvPath = "";
    bool isCsvExpActive = false;
    std::ofstream export_stream;
    unsigned int epoch = 0;
    string exportContinuousPath = "/dev/shm/"; //TODO
    bool isContinuousActive = false;

    std::vector<std::shared_ptr<Filter>> filterLine;
    MermaidGraph mermaidGraph;

    OscListener * oscParams = nullptr;

    ofSoundStream soundStream;
    float raw_onset = 0;
    float fft_smooth = 0.75;

    ofTrueTypeFont infofont;
    int viewstate = 0;
    ofColor viewcolor = ofColor::white;
    bool viewred = false;

    bool draw_inspector = false;

#ifdef WITH_js_dev_input
    JoystickDevice js0;
#endif // WITH_js_dev_input

#ifdef WITH_nanoKontrol2_midi_input
    NanoKontrol2 nk0;
#endif // WITH_js_dev_input

    bool send_vital_signs = false;
    ofxOscSender vital_signs;
    float oscMsg[8];

    std::string ipv4_list = "";
};

