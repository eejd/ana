#include "../ana_features.h"
#ifdef WITH_openCL

#include "ofMain.h"
#include "MSAOpenCL.h"
#include "ofApp.h"
#include "globalvars.h"
#include "calc/FloatArrays.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispMng.h"
#include "cl/ClAgentPingPong.h"

// OK
// #undef BASE_VELOCITY
// ana/live_coding/src/sketchbook/physarum/substrate/vel-substrate.clj
// "physarum/substrate/vel-substrate.cl"

//#define BASE_VELOCITY
#undef BASE_VELOCITY

ClAgentPingPong::ClAgentPingPong(ofGLWindowSettings settings, OscListener * oscParams) :
ClFilter(settings, oscParams) {
}

ClAgentPingPong::~ClAgentPingPong() {
    delete[] position;
    delete[] velocity;
    delete[] heading;
}

void ClAgentPingPong::setup(const std::string id, int numSamples) {
    Filter::setup(id, numSamples);

    clImageInput.initWithoutTexture(filterWidth, filterHeight);

    pingPongIndex = 0;
    clOutFbo.allocate(filterWidth, filterHeight, GL_RGBA32F_ARB);
    clOutFbo.begin();
    ofClear(0, 0, 0, 255);
    clOutFbo.end();
    clOutImage.initFromTexture(clOutFbo.getTexture());

    clPositionBuffers[0].initBuffer(agentlength * 2 * sizeof(float), CL_MEM_READ_WRITE);
    clPositionBuffers[1].initBuffer(agentlength * 2 * sizeof(float), CL_MEM_READ_WRITE);
    clVelocityBuffers[0].initBuffer(agentlength * sizeof(float), CL_MEM_READ_WRITE);
    clVelocityBuffers[1].initBuffer(agentlength * sizeof(float), CL_MEM_READ_WRITE);

    clHeadingBuffers[0].initBuffer(agentlength * sizeof(float), CL_MEM_READ_WRITE);
    clHeadingBuffers[1].initBuffer(agentlength * sizeof(float), CL_MEM_READ_WRITE);

    clTrailBuffers[0].initBuffer(filterWidth * filterHeight * sizeof(cl_int), CL_MEM_READ_WRITE);
    clTrailBuffers[1].initBuffer(filterWidth * filterHeight * sizeof(cl_int), CL_MEM_READ_WRITE);
}

void ClAgentPingPong::reload(ofxOscMessage const &msg) {
    ClFilter::reload(msg);

    ScalarLispMng mng;
    mng.compile(slUpstream, "paint", "-1", msg);
    mng.compile(slP0, "p0", "1.0", msg);
    mng.compile(slP1, "p1", "1.0", msg);
    mng.compile(slP2, "p2", "1.0", msg);
    mng.compile(slP3, "p3", "1.0", msg);
    mng.compile(slP4, "p4", "1.0", msg);
    mng.compile(slP5, "p5", "1.0", msg);
    mng.compile(slP6, "p6", "1.0", msg);
    mng.compile(slP7, "p7", "1.0", msg);

    pingPongIndex = 0;
    if(position == nullptr)
        position = floats.uniform(0.0f, 1.0f, 2 * agentlength);
    clPositionBuffers[0].write(position, 0, agentlength * 2 * sizeof(float));

    if(velocity == nullptr)
        velocity = floats.uniform(0.001f, 0.02f, agentlength);
    clVelocityBuffers[0].write(velocity, 0, agentlength * sizeof(float));

    if(heading == nullptr)
        heading = floats.uniform(-M_PI, M_PI, agentlength);
    clHeadingBuffers[0].write(heading, 0, agentlength * sizeof(float));

    clOutFbo.begin();
    ofClear(0, 0, 0, 255);
    clOutFbo.end();
    glFinish();

    hasImageInput = false;

    pingPongIndex = 0;
    run_ag_setup_kernel(ag_setup_kernel);
    run_trail_kernel(trail_setup_kernel);
    pingPongIndex = 1;
    openCL.finish();
}

bool ClAgentPingPong::reloadClProg(const bool force) {
    if(fatalClProg)
        return false;

    bool reloaded = ClFilter::reloadClProg(force);
    if(reloaded) {
        ag_setup_kernel = pcl->loadKernel("ag_setup");
        trail_setup_kernel = pcl->loadKernel("trail_setup");
        ag_move_kernel = pcl->loadKernel("ag_move");
        trail_disipate_kernel = pcl->loadKernel("trail_disipate");
//        ag_clear_kernel = pcl->loadKernel("ag_clear");
        ag_draw_kernel = pcl->loadKernel("ag_draw");
//        trail_setup_kernel = pcl->loadKernel("trail_setup");
    }
    return reloaded;
}

ofTexture& ClAgentPingPong::getTexture() {
    openCL.finish();
    return fbo.getTexture();
}

void ClAgentPingPong::run_ag_setup_kernel(msa::OpenCLKernelPtr kernel) {
//    printf("%d run_ag_setup_kernel\n", pingPongIndex);
    int arg = 0;
    kernel->setArg(arg++, clImageInput);
    kernel->setArg(arg++, (cl_int)filterWidth);
    kernel->setArg(arg++, (cl_int)filterHeight);

    kernel->setArg(arg++, clPositionBuffers[pingPongIndex]);
    kernel->setArg(arg++, clPositionBuffers[1 - pingPongIndex]);
#ifdef BASE_VELOCITY
    kernel->setArg(arg++, clVelocityBuffers[0]); // base velocity
    kernel->setArg(arg++, clVelocityBuffers[1]); // calculated velocity
#else
    kernel->setArg(arg++, clVelocityBuffers[pingPongIndex]);
    kernel->setArg(arg++, clVelocityBuffers[1 - pingPongIndex]);
#endif
    kernel->setArg(arg++, clHeadingBuffers[pingPongIndex]);
    kernel->setArg(arg++, clHeadingBuffers[1 - pingPongIndex]);

    kernel->setArg(arg++, (float)p0);
    kernel->setArg(arg++, (float)p1);
    kernel->setArg(arg++, (float)p2);
    kernel->setArg(arg++, (float)p3);
    kernel->setArg(arg++, (float)p4);
    kernel->setArg(arg++, (float)p5);
    kernel->setArg(arg++, (float)p6);
    kernel->setArg(arg++, (float)p7);
    kernel->run1D(agentlength);
}

void ClAgentPingPong::run_trail_kernel(msa::OpenCLKernelPtr kernel) {
//    printf("%d run_trail_kernel\n", pingPongIndex);
    int arg = 0;
    kernel->setArg(arg++, clImageInput);
    kernel->setArg(arg++, (cl_int)filterWidth);
    kernel->setArg(arg++, (cl_int)filterHeight);

    kernel->setArg(arg++, clTrailBuffers[pingPongIndex]);
    kernel->setArg(arg++, clTrailBuffers[1 - pingPongIndex]);

    kernel->setArg(arg++, (float)p0);
    kernel->setArg(arg++, (float)p1);
    kernel->setArg(arg++, (float)p2);
    kernel->setArg(arg++, (float)p3);
    kernel->setArg(arg++, (float)p4);
    kernel->setArg(arg++, (float)p5);
    kernel->setArg(arg++, (float)p6);
    kernel->setArg(arg++, (float)p7);
    kernel->run1D(filterWidth * filterHeight);
}

void ClAgentPingPong::run_ag_move_kernel(msa::OpenCLKernelPtr kernel) {
//    printf("%d run_ag_move_kernel\n", pingPongIndex);
    int arg = 0;
    kernel->setArg(arg++, clImageInput);
    kernel->setArg(arg++, (cl_int)filterWidth);
    kernel->setArg(arg++, (cl_int)filterHeight);

    kernel->setArg(arg++, clPositionBuffers[pingPongIndex]);
    kernel->setArg(arg++, clPositionBuffers[1 - pingPongIndex]);
#ifdef BASE_VELOCITY
    kernel->setArg(arg++, clVelocityBuffers[0]); // base velocity
    kernel->setArg(arg++, clVelocityBuffers[1]); // calculated velocity
#else
    kernel->setArg(arg++, clVelocityBuffers[pingPongIndex]);
    kernel->setArg(arg++, clVelocityBuffers[1 - pingPongIndex]);
#endif
    kernel->setArg(arg++, clHeadingBuffers[pingPongIndex]);
    kernel->setArg(arg++, clHeadingBuffers[1 - pingPongIndex]);
    kernel->setArg(arg++, clTrailBuffers[pingPongIndex]);
    kernel->setArg(arg++, clTrailBuffers[1 - pingPongIndex]);

    kernel->setArg(arg++, (float)p0);
    kernel->setArg(arg++, (float)p1);
    kernel->setArg(arg++, (float)p2);
    kernel->setArg(arg++, (float)p3);
    kernel->setArg(arg++, (float)p4);
    kernel->setArg(arg++, (float)p5);
    kernel->setArg(arg++, (float)p6);
    kernel->setArg(arg++, (float)p7);
    kernel->run1D(agentlength);
}

void ClAgentPingPong::run_ag_clear_kernel(msa::OpenCLKernelPtr kernel) {
//    printf("%d run_ag_clear_kernel\n", pingPongIndex);
//    int arg = 0;
//    kernel->setArg(arg++, clImageInput);
//    kernel->setArg(arg++, (cl_int)filterWidth);
//    kernel->setArg(arg++, (cl_int)filterHeight);
//
//    kernel->setArg(arg++, clOutImage);
//    kernel->run2D(filterWidth, filterHeight);
}

void ClAgentPingPong::run_ag_draw_kernel(msa::OpenCLKernelPtr kernel) {
//    printf("%d run_ag_draw_kernel\n", pingPongIndex);
    int arg = 0;
    kernel->setArg(arg++, clImageInput);
    kernel->setArg(arg++, (cl_int)filterWidth);
    kernel->setArg(arg++, (cl_int)filterHeight);

    kernel->setArg(arg++, clTrailBuffers[pingPongIndex]);

    kernel->setArg(arg++, (float)p0);
    kernel->setArg(arg++, (float)p1);
    kernel->setArg(arg++, (float)p2);
    kernel->setArg(arg++, (float)p3);
    kernel->setArg(arg++, (float)p4);
    kernel->setArg(arg++, (float)p5);
    kernel->setArg(arg++, (float)p6);
    kernel->setArg(arg++, (float)p7);
    kernel->setArg(arg++, clOutImage);
    kernel->run2D(filterWidth, filterHeight);
}

void ClAgentPingPong::update(){
    if(fatalClProg || clProgName.empty())
        return;

    if(upstream_ids.size() >= 1 && slUpstream.rpn.execute0() > 0) {
        auto in_id = upstream_ids[0];
        if(filterDict.find(in_id) != filterDict.end()) {
            auto f = filterDict[in_id];
            if(f && !hasImageInput) {
                clImageInput.initFromTexture(f->getTexture());
                hasImageInput = true;
            }
        }
    }

    p0 = slP0.rpn.execute0();
    p1 = slP1.rpn.execute0();
    p2 = slP2.rpn.execute0();
    p3 = slP3.rpn.execute0();
    p4 = slP4.rpn.execute0();
    p5 = slP5.rpn.execute0();
    p6 = slP6.rpn.execute0();
    p7 = slP7.rpn.execute0();

    if(reloadClProg(false)) {
        clOutFbo.begin();
        ofClear(0, 0, 0, 255);
        clOutFbo.end();
        glFinish();

        pingPongIndex = 0;
        run_ag_setup_kernel(ag_setup_kernel);
        run_trail_kernel(trail_setup_kernel);
        pingPongIndex = 1;
    }
    else {
        glFinish();
    }

    run_ag_move_kernel(ag_move_kernel);
    pingPongIndex = 1 - pingPongIndex;
    run_trail_kernel(trail_disipate_kernel);
    pingPongIndex = 1 - pingPongIndex;
//    run_ag_clear_kernel(ag_clear_kernel);
    run_ag_draw_kernel(ag_draw_kernel);
    pingPongIndex = 1 - pingPongIndex;
}

std::string ClAgentPingPong::dumpState() {
    std::stringstream info;
    info.precision(5);
    info.width(9);
    info.flags(std::ios::fixed);
    info << "\np0  vel         " << p0
         << "\np1  sense dist  " << p1
         << "\np2  sense angl  " << p2
         << "\np3  agent angl  " << p3;
    return info.str();
}
#endif // WITH_openCL
