# Raspberry Pi SenseHAT

A 8x8 LED matrix as output device.

![](./ledmatrix.jpg)

![](./sensehat-camera-to-led.jpg)

```mermaid
graph LR
camera((camera)) -- c0 --> :vid0
:vid0 -- vid0 --> :ras1
:ras1 -- ras1 --> :ras2
:ras2 -- ras2 --> :ras2
:ras2 -- ras2 --> :pihat3
:pihat3 --> senseHAT((LED8x8))
```

```clojure
(ns lv.demo)
(use 'lv.core)

(layout "grid" [vid0 ras1] [ras2 pihat3])
(render)

(rgb :vid0 [c0]
  (sx 0.6)
  (rgb c0.rgb)
)

(hsv :ras1 [vid0]
  ; (h vid0.h)
  (h (+ 0.5 vid0.h))
  (s vid0.s)
  (v (+ (+ 0.15 (sin (* 0.05 f)))
        vid0.v))
)

(rgb :ras2 [ras1 ras2]
  (rgb (mix ras1.rgb ras2.rgb 0.9))
)

(expr :pihat3 [ras2]
  (mode 0) ; draw mode 0:
           ; use the color from the upstream filter node
)
```
