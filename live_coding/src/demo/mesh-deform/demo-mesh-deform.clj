(ns lv.demo)
(use 'lv.core)

(layout "grid" [dmesh0 dmesh1] [mopr2 ras3])
(render)

(expr :dmesh0 []
  (mesh "mesh-deform/noise-scale+3-triangulated.ply")
  (scale 25)
  (ambient 0x0f0f0f)
  (diffuse 0x39bdcc)
  (specular 0xa8eaf1)

  (amp 5)
  (speed 0.5)
  (liqu 0.5)
  (rx (* 0.001 f))
)
(render 0)

(expr :dmesh1 []
  (mesh "mesh-deform/noise-scale+3-triangulated.ply")
  (scale 25)
  (wire -1)
  (ambient 0x0f0f0f)
  (diffuse 0xe5a039)
  (specular 0xe0cdb0)

  (speed 0.5)
  (liqu 0.5)
  (rz (* 0.002 f))
)
(render 1)

(expr :mopr2 [dmesh0] "d")
(render 2)

(rgb :ras3 [dmesh1 mopr2 ras3]
  (rgb (mix (+ (* 0.7 dmesh1.rgb)
               (* 0.3 mopr2.rgb))
            ras3.rgb
            0.98))
)
(render 3)
