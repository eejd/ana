#version 330
// see: https://michaelwalczyk.com/blog-ray-marching.html
// original code from Michael Walczyk
uniform float a = 0.5;
uniform float b = 0.5;
uniform float c = 0.5;
uniform float mix = 0.9;
uniform float rx = 0.0;
uniform float ry = 0.0;
uniform float rz = 0.0;
uniform sampler2DRect tex0;
uniform ivec2 panel;

out vec4 o_color;

mat4 rotateX(float theta) {
    float c = cos(theta);
    float s = sin(theta);

    return mat4(
        vec4( 1,  0,  0,  0),
        vec4( 0,  c, -s,  0),
        vec4( 0,  s,  c,  0),
        vec4( 0,  0,  0,  1)
    );
}

mat4 rotateY(float theta) {
    float c = cos(theta);
    float s = sin(theta);

    return mat4(
        vec4( c,  0,  s,  0),
        vec4( 0,  1,  0,  0),
        vec4(-s,  0,  c,  0),
        vec4( 0,  0,  0,  1)
    );
}

mat4 rotateZ(float theta) {
    float c = cos(theta);
    float s = sin(theta);

    return mat4(
        vec4( c, -s,  0,  0),
        vec4( s,  c,  0,  0),
        vec4( 0,  0,  1,  0),
        vec4( 0,  0,  0,  1)
    );
}

float distance_from_sphere(in vec3 p, in vec3 c, float r)
{
    return length(p - c) - r;
}

float distance_from_box(in vec3 p)
{
  vec3 b = texture(tex0, gl_FragCoord.xy).xyz;
  vec3 q = abs(p) - b;
  return length(max(q,0.0)) + min(max(q.x,max(q.y,q.z)),0.0);
}

float distance_from_ellipsoid(in vec3 p)
{
  vec3 r = texture(tex0, gl_FragCoord.xy).xyz;
  float k0 = length(p/r);
  float k1 = length(p/(r*r));
  return k0*(k0-1.0)/k1;
}

float map_the_world(in vec3 p)
{
    float r = length(texture(tex0, gl_FragCoord.xy).xyz);
    float dist = distance_from_sphere(p, vec3(0.0), r);
//     float dist = distance_from_box(p);
//     float dist = distance_from_ellipsoid(p);
    return dist;
}

vec3 calculate_normal(in vec3 p)
{
    const vec3 small_step = vec3(0.001, 0.0, 0.0);

    float gradient_x = map_the_world(p + small_step.xyy) - map_the_world(p - small_step.xyy);
    float gradient_y = map_the_world(p + small_step.yxy) - map_the_world(p - small_step.yxy);
    float gradient_z = map_the_world(p + small_step.yyx) - map_the_world(p - small_step.yyx);

    vec3 normal = vec3(gradient_x, gradient_y, gradient_z);

    return normalize(normal);
}

vec3 ray_march(in vec3 ro, in vec3 rd)
{
    float total_distance_traveled = 0.0;
    const int NUMBER_OF_STEPS = 32;
    const float MINIMUM_HIT_DISTANCE = 0.01;
    const float MAXIMUM_TRACE_DISTANCE = 1000.0;
    const vec3 ambientColor  = vec3(0.05, 0.05, 0.10);
//     const vec3 diffuseColor  = vec3(0.8, 0.4, 0.1);
    const vec3 specularColor = vec3(0.7, 0.7, 0.75);

    for (int i = 0; i < NUMBER_OF_STEPS; ++i)
    {
        vec3 current_position = ro + total_distance_traveled * rd;

        float distance_to_closest = map_the_world(current_position);

        if (distance_to_closest < MINIMUM_HIT_DISTANCE) 
        {
            vec3 normal = calculate_normal(current_position);
            vec3 light_position = vec3(3.0, 5.0, 3.0);
            vec3 direction_to_light = normalize(current_position - light_position);
            float diffuse_intensity = dot(normal, direction_to_light);
            if(diffuse_intensity > 0.0) {
                vec3 reflectionDirection = normalize(reflect(-normalize(direction_to_light), normalize(normal)));
                vec3 diffuseColor = texture(tex0, panel.x*(reflectionDirection.xy)).xyz;
                float specular = dot(normalize(normal), reflectionDirection);
                if(specular > 0.0) {
                    return vec3(ambientColor + diffuse_intensity * diffuseColor + pow(specular, 128.0) * specularColor);
                }
                else {
                    return vec3(ambientColor + diffuse_intensity * diffuseColor);
                }
            }
            else {
                // dark side of the object, only ambient color
                return ambientColor;
            }
        }

        if (total_distance_traveled > MAXIMUM_TRACE_DISTANCE)
        {
            break;
        }
        total_distance_traveled += distance_to_closest;
    }
    return vec3(0.0);
}

void main()
{
    vec2 uv = (gl_FragCoord.xy / panel.x) * 2.0 - 1.0;

    vec3 camera_position = vec3(0.0, 0.0, -1.25);
    vec3 ro = camera_position;
    vec3 rd = vec3(uv, 1.0);

    vec3 shaded_color = ray_march(ro, rd);

    o_color = vec4(shaded_color, 1.0);
}
 
