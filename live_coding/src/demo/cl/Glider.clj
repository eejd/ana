; internaly calculate on a 64x64 grid
; but width and height of the canvas can have arbitary siz
(ns lv.demo)
(use 'lv.core)

(layout "grid" [clspp0 clspp1]
               [ras2   mopr3])

(expr :clspp0 []
 "cl/Glider.cl"
 (loops 113)
)

(expr :clspp1 []
 "cl/Glider.cl"
 (loops 151)
)

(rgb :ras2 [clspp0 clspp1]
  (rgb (max (- 1 clspp0.hsv) clspp1.rgb))
)

(expr :mopr3 [ras2]
  "bbb"
)

(render 3)
