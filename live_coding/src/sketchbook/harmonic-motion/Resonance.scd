s.waitForBoot({

  SynthDef.new(\CombRing, { | channel = 0, freq = 0.5, pan = 0,
    a1 = 0.50, a2 = 0.25, a3 = 0.10, a4 = 0.10 |
    var exciter = GrayNoise.ar(0.002);
    var base = 2.5;
    var f    = 100 * freq;
    var r1 = Ringz.ar(exciter, base.pow(1)*f, 0.5, a1);
    var r2 = Ringz.ar(exciter, base.pow(2)*f, 0.5, a2);
    var r3 = Ringz.ar(exciter, base.pow(3)*f, 0.5, a3);
    var r4 = Ringz.ar(exciter, base.pow(4)*f, 0.5, a4);
    var snd = Mix.new([r1, r2, r3, r4]);
    Out.ar(channel, snd);
  }, \ar ! 7).add;

  SynthDef.new(\CombSin, { | freq = 100, pan = 0,
    a1 = 0.25, a2 = 0.25, a3 = 0.25, a4 = 0.25 |
    var base = 2.5;
    var r1 = SinOsc.ar(base.pow(1)*freq, 0,        a1);
    var r2 = SinOsc.ar(base.pow(2)*freq, 2pi*0.25, a2);
    var r3 = SinOsc.ar(base.pow(3)*freq, 2pi*0.50, a3);
    var r4 = SinOsc.ar(base.pow(4)*freq, 2pi*0.75, a4);
    var snd = 0.1 * Mix.new([r1, r2, r3, r4]);
    Out.ar(0, Pan2.ar(snd, pan));
  }, \ar ! 6).add;

  h.free;
	// h = OSCFunc({ |msg, time, addr, recvPort|
	// 	[time, msg].postln;
	// 	~pl.set(\freq, msg[1]);
	// 	~pl.set(\a2, msg[2] + msg[3] );
	// 	~pl.set(\a3, msg[2]);
	// 	~pl.set(\a4, msg[3]);
	// 	~pr.set(\freq, msg[4]);
	// 	~pr.set(\a2, msg[5] + msg[6] );
	// 	~pr.set(\a3, msg[5]);
	// 	~pr.set(\a4, msg[6]);
	// }, '/f_analyze');
  h = OSCFunc({ |msg, time, addr, recvPort|
    [time, msg].postln;
		// ~pl.set(\freq, (msg[1] * 0.2) + 960 + 50);
		~pl.set(\freq, (msg[1]) + 50);
  }, '/ana/hit/enter');

  q.free;
  q = OSCFunc({ |msg, time, addr, recvPort|
    [time, msg].postln;
    s.quit;
    0.exit;
  }, '/f_collider/quit');

  0.1.wait;

	~pl = Synth(\CombRing, [ \channel, 0 ]);
	~pr = Synth(\CombRing, [ \channel, 1 ]);
	// ~pl = Synth(\CombSin, [ \channel, 0 ]);
	// ~pr = Synth(\CombSin, [ \channel, 1 ]);
});
