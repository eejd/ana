(ns lv.demo)
(use 'lv.core)

(layout "grid" [cline2 mov0 clipp1]
               [mopr3 ras4 ras5])
               ; [cline2])
(movie "4movies/*.mp4")

(texpr
  ; use nanoKONTROL2 als input
  (t0 (* 10 ks7))
)

; two movies, but useing only the first
(rgb :mov0 [c0 c1 c2 c3]
  (sy 2.5)
  (ty 0.1)
  ; (hsv (min c1.hsv c3.hsv))
  (rgb (mix c1.rgb c3.rgb kp1))
)

; read from nanoKONTROL2
; kp0    0.29134
;
; ks0    1
; ks1    0
; ks2    0.41732
; ks3    0
;
; ks7    0.54331
(expr :clipp1 [mov0]
  "cl/image-color-drift.cl"
  (paint 1)
  ; kp0 = mix upstren image
  (p0 (* kp0 0.1))
  ; ks0 .. ks3 = direction of the drift
  ; t0 = damping the feedback loop
  (p1 (* t0 (- ks0 0.5) 8))
  (p2 (* t0 (- ks1 0.5) 8))
  (p3 (* t0 (- ks2 0.5) 8))
  (p4 (* t0 (- ks3 0.5) 8))
)

(expr :cline2 [clipp1]
  (count 10)
  (max 2500)
  (thres kp6)
  (fill 1)
  (names -1)
  ; (clear (zero? (% f 30) 1 -1))
  (paint -1)
  (holes 1)
)

(expr :mopr3 [cline2] "ddb")

(rgb :ras4 [mov0 clipp1 mopr3]
   (rgb (+ (* kp4 mov0.rgb)
           (/ clipp1.rgb mopr3.rgb)))
   ; (rgb (mix clipp1.rgb
   ;           mopr3.rgb
   ;           (/ kp5 mov0.rgb)))
)

(rgb :ras5 [mov0 clipp1 mopr3]
   ; (rgb (+ (* kp5 mov0.rgb)
   ;         (+ clipp1.rgb mopr3.rgb)))
   (rgb (mix clipp1.rgb
             mopr3.rgb
             (/ kp5 mov0.rgb)))
)

(render )
