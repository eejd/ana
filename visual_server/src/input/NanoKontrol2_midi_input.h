#pragma once
#include "ana_features.h"
#ifdef WITH_nanoKontrol2_midi_input

#include "ofMain.h"
#include "ofxMidi.h"
#include "ofxMidiMessage.h"

class NanoKontrol2 : public ofxMidiListener {

public:
    NanoKontrol2();
    ~NanoKontrol2();

    void setup();
    std::string getName();

    void newMidiMessage(ofxMidiMessage& message);

private:
    std::string name;
    ofxMidiIn midiIn;
    std::vector<ofxMidiMessage> midiMessages;
};
#endif // WITH_nanoKontrol2_midi_input
