#version 330
// see: https://michaelwalczyk.com/blog-ray-marching.html
// original code from Michael Walczyk
uniform float a = 0.5;
uniform float b = 0.5;
uniform float c = 0.5;
uniform float mix = 0.9;
uniform float rx = 0.0;
uniform float ry = 0.0;
uniform float rz = 0.0;
uniform sampler2DRect tex0;
uniform ivec2 panel;

out vec4 o_color;

mat4 rotateX(float theta) {
    float c = cos(theta);
    float s = sin(theta);

    return mat4(
        vec4( 1,  0,  0,  0),
        vec4( 0,  c, -s,  0),
        vec4( 0,  s,  c,  0),
        vec4( 0,  0,  0,  1)
    );
}

mat4 rotateY(float theta) {
    float c = cos(theta);
    float s = sin(theta);

    return mat4(
        vec4( c,  0,  s,  0),
        vec4( 0,  1,  0,  0),
        vec4(-s,  0,  c,  0),
        vec4( 0,  0,  0,  1)
    );
}

mat4 rotateZ(float theta) {
    float c = cos(theta);
    float s = sin(theta);

    return mat4(
        vec4( c, -s,  0,  0),
        vec4( s,  c,  0,  0),
        vec4( 0,  0,  1,  0),
        vec4( 0,  0,  0,  1)
    );
}

float distance_from_sphere(in vec3 p0, in vec3 c0, float r0)
{
//     return min(abs(p0.x - c0.x), min(abs(p0.y - c0.y), abs(p0.z - c0.z))) - r0;
//     return min(p0.x - c0.x, min(p0.y - c0.y, p0.z - c0.z)) - r0;
//     return length(p0 - c0) - r0;
    return length(p0 - c0) - 1.5 * a * r0;
}

float distance_from_box(in vec3 p)
{
  vec3 b = vec3(0.1, 2.0, 2.0);
  vec3 q = abs(p) - b;
  return length(max(q,0.0)) + min(max(q.x,max(q.y,q.z)),0.0);
}

float distance_from_ellipsoid(in vec3 p)
{
  vec3 r = texture(tex0, gl_FragCoord.xy).xyz;
  float k0 = length(p/r);
  float k1 = length(p/(r*r));
  return k0*(k0-1.0)/k1;
}

float map_the_world(in vec3 p)
{
    vec3 px  = (rotateX(-rx) * vec4(p, 1.0)).xyz;
    vec3 py  = (rotateY(-ry) * vec4(px, 1.0)).xyz;
    vec3 pz  = (rotateZ(-rz) * vec4(py, 1.0)).xyz;
    vec3 pxz = (rotateX(-rz) * vec4(px, 1.0)).xyz;
    vec3 pyz = (rotateY(-rz) * vec4(py, 1.0)).xyz;
    
    float u1 = distance_from_sphere(px, vec3( 0.7832039464265976,  0.15907697882313565, -0.03586214034754383), 0.45);
    float v1 = distance_from_sphere(px, vec3( 1.1748059196398966,  0.2386154682347035,  -0.05379321052131575), 0.2);
    u1 = max(u1, -v1);

    float u2 = distance_from_sphere(py, vec3(-0.32456565840007773, -0.15779369862412906, -0.7139735863894697), 0.45);
    float v2 = distance_from_sphere(py, vec3(-0.4868484876001166,  -0.2366905479361936,  -1.0709603795842046), 0.2);
    u2 = max(u2, -v2);

    float u3 = distance_from_sphere(pz, vec3( 0.22934068399306176, -0.4453614932732368,  -0.6237435298061434), 0.45);
    float v3 = distance_from_sphere(pz, vec3( 0.3440110259895927,  -0.6680422399098552,  -0.9356152947092152), 0.2);
    u3 = max(u3, -v3);

    float u4 = distance_from_sphere(pxz, vec3(-0.44403046328764106,  0.5056111476051409,  -0.4326595833793298),  0.45);
    float v4 = distance_from_sphere(pxz, vec3(-0.6660456949314616,   0.7584167214077115,  -0.6489893750689948), 0.2);
    u4 = max(u4, -v4);

    float u5 = distance_from_sphere(pyz, vec3(-0.6513783615436248,  -0.45136049185112476, -0.10945289630089101), 0.45);
    float v5 = distance_from_sphere(pyz, vec3(-0.9770675423154374,  -0.6770407377766873,  -0.16417934445133653), 0.2);
    u5 = max(u5, -v5);
    
    float u0 = distance_from_sphere(p, vec3(0.0), 1.0);
    u0 = min(u0, u1);
    u0 = min(u0, u2);
    u0 = min(u0, u3);
    u0 = min(u0, u4);
    u0 = min(u0, u5);
    
    float b0 = distance_from_box(pyz);
    u0 = max(u0, -b0);
    
//     return u0 - 0.1 * b;

//     float red = length(texture(tex0, gl_FragCoord.xy).xyz);
    float red = texture(tex0, gl_FragCoord.xy).x;
    return abs(u0) - 0.1 * red * b;
//     return abs(u0) - 0.0001;
}

vec3 calculate_normal(in vec3 p)
{
    const vec3 small_step = vec3(0.01, 0.0, 0.0);

    float gradient_x = map_the_world(p + small_step.xyy) - map_the_world(p - small_step.xyy);
    float gradient_y = map_the_world(p + small_step.yxy) - map_the_world(p - small_step.yxy);
    float gradient_z = map_the_world(p + small_step.yyx) - map_the_world(p - small_step.yyx);

    vec3 normal = vec3(gradient_x, gradient_y, gradient_z);

    return normalize(normal);
}

vec3 ray_march(in vec3 ro, in vec3 rd)
{
    float total_distance_traveled = 0.0;
    const int NUMBER_OF_STEPS = 32;
    const float MINIMUM_HIT_DISTANCE = 0.01;
    const float MAXIMUM_TRACE_DISTANCE = 1000.0;
    const vec3 ambientColor  = vec3(0.05, 0.05, 0.10);
//     const vec3 diffuseColor  = vec3(0.8, 0.4, 0.1);
    const vec3 specularColor = vec3(0.7, 0.7, 0.75);

    for (int i = 0; i < NUMBER_OF_STEPS; ++i)
    {
        vec3 current_position = ro + total_distance_traveled * rd;

        float distance_to_closest = map_the_world(current_position);

        if (distance_to_closest < MINIMUM_HIT_DISTANCE) 
        {
            vec3 normal = calculate_normal(current_position);
            vec3 light_position = vec3(3.0, 5.0, 3.0);
            vec3 direction_to_light = normalize(current_position - light_position);
            float diffuse_intensity = dot(normal, direction_to_light);
            if(diffuse_intensity > 0.0) {
                vec3 reflectionDirection = normalize(reflect(-normalize(direction_to_light), normalize(normal)));
                vec3 diffuseColor = texture(tex0, panel.x*(reflectionDirection.xy)).xyz;
                float specular = dot(normalize(normal), reflectionDirection);
                if(specular > 0.0) {
                    return vec3(ambientColor + diffuse_intensity * diffuseColor + pow(specular, 128.0) * specularColor);
                }
                else {
                    return vec3(ambientColor + diffuse_intensity * diffuseColor);
                }
            }
            else {
                // dark side of the object, only ambient color
                return ambientColor;
            }
        }

        if (total_distance_traveled > MAXIMUM_TRACE_DISTANCE)
        {
            break;
        }
        total_distance_traveled += distance_to_closest;
    }
    return vec3(0.0);
}

void main()
{
    vec2 uv = (gl_FragCoord.xy / panel.x) * 2.0 - 1.0;

    vec3 camera_position = vec3(0.0, 0.0, -1.8);
    vec3 ro = camera_position;
    vec3 rd = vec3(uv, 1.0);

    vec3 shaded_color = ray_march(ro, rd);

    o_color = vec4(shaded_color, 1.0);
}
 
