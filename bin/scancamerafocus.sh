#!/bin/bash
mkdir /dev/shm/focusscan
#echo -x
uvcdynctrl -s "Power Line Frequency" 1
uvcdynctrl -s  Sharpness 7
uvcdynctrl -s "Focus, Auto" 0
sleep 1
for i in `seq 255`
do
uvcdynctrl -s "Focus (absolute)" $i
fn=$(printf "%05d" $i)
fswebcam --no-banner -d /dev/video0 -r 3264x2448 /dev/shm/focusscan/focus_$fn.jpg
#sleep 1
done
ls -Srl /dev/shm/focusscan/*.jpg |tail
#echo +x
