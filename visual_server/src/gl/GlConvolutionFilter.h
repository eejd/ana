#pragma once

#include "ofMain.h"
#include "Filter.h"
#include "OscListener.h"

class GlConvolutionFilter : public Filter{
	public:
		GlConvolutionFilter(ofGLWindowSettings settings, OscListener * oscParams);
		virtual ~GlConvolutionFilter();
		virtual void setup(const std::string id, int numSamples);
		virtual void reload(ofxOscMessage const & msg);
		virtual void draw(int li);

	private:
		std::vector<float> convolutionKernel;
};
