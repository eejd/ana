(ns lv.demo)
(use 'lv.core)

(layout "grid" [win0 win1]
               [ras2 ras3])
(render)

; read from a visible X11 window
(expr :win0 [] "A.mp3")
(expr :win1 []
  (i 7)
)
; mix input streams
(hsv :ras2 [win0 win1]
  (h (+ u0 win1.h))
  (s win0.s)
  (v (+ win0.v win1.v))
)
(hsv :ras3 [win0 win1]
  (mirror)
  (h (+ u0 win0.h))
  (s win0.s)
  (v (+ win0.v win1.v))
)
