#pragma once

#include "ofMain.h"

class CursorManager {
public:
    virtual ~CursorManager();
    void setup();
    void nextFrame();
    void moved();
    void show();

private:
    bool hideCursor = false;
    int hideCursorCounter = 0;
    bool isCursorVisible = true;
};
