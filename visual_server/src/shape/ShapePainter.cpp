#include "ofMain.h"
#include "ofxOsc.h"

#include "Configuration.h"
#include "globalvars.h"
#include "OscListener.h"
#include "Filter.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispMng.h"
#include "shape/ShapePainter.h"

ShapePainter::ShapePainter(ofGLWindowSettings settings, OscListener * oscParams, int draw) :
        Filter(settings, oscParams) {
    draw_mode = draw;
}

ShapePainter::~ShapePainter() {
}

void ShapePainter::setup(const std::string id, int numSamples) {
    Filter::setup(id, numSamples);
}

void ShapePainter::reload(ofxOscMessage const &msg) {
    ScalarLispMng mng;
    mng.compile(slCount, "count", "0", msg);
    mng.compile(slX, "x", "0", msg);
    mng.compile(slY, "y", "0", msg);
    mng.compile(slRotate, "rot", "0", msg);
    mng.compile(slSize, "size", "0", msg);
    mng.compile(slH, "h", "0", msg);
    mng.compile(slS, "s", "0", msg);
    mng.compile(slV, "v", "1", msg);
    mng.compile(slResolution, "res", "22", msg);
    mng.compile(slFill, "fill", "-1", msg);
    mng.compile(slClear, "clear", "1", msg);
}

void ShapePainter::draw(int li) {
    fbo.begin();
    float clear = slClear.rpn.execute0();
    if(clear >= 0.0) {
        glClearColor(clear, clear, clear, 1);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    ofPushStyle();
    size_t count = std::round(std::max(0.0, slCount.rpn.execute0()));
    if(count > vertices_capacity) {
        clearVertices();
        reallocateVertices(count);
    }
    switch(draw_mode) {
    case 1: {
        if(usedImages > 0) {
            for(float i = 0; i < count; ++i) {
                float x = slX.rpn.execute3d1i(0.0, 0.0, 0.0, i);
                *FVx(i) = x;
                float y = slY.rpn.execute3d1i(x, 0.0, 0.0, i);
                *FVy(i) = y;
                float sizexy = slSize.rpn.execute3d1i(x, y, 0.0, i);
                *FVz(i) = sizexy;
                int ix = sizexy < 0 ? -sizexy : sizexy;
                ofPushMatrix();
                ofTranslate(x, y, 0);
                ofRotateDeg(slRotate.rpn.execute3d1i(x, y, sizexy, i), 0, 0, 1);
                images[ix % usedImages].draw(x, y);
                ofPopMatrix();
            }
        }
        break;
    }
    default: {
        for(float i = 0; i < count; ++i) {
            float x = slX.rpn.execute3d1i(0.0, 0.0, 0.0, i);
            *FVx(i) = x;
            float y = slY.rpn.execute3d1i(x, 0.0, 0.0, i);
            *FVy(i) = y;
            float z = slSize.rpn.execute3d1i(x, y, 0.0, i);
            *FVz(i) = z;
            float rot = slRotate.rpn.execute3d1i(x, y, z, i);
            *FVw(i) = rot;

            float h = slH.rpn.execute3d1i(x, y, z, i);
            *FVh(i) = h;
            float s = slS.rpn.execute3d1i(x, y, z, i);
            *FVs(i) = s;
            float v = slV.rpn.execute3d1i(x, y, z, i);
            *FVv(i) = v;
            ofFloatColor rgb = ofFloatColor::fromHsb(h, s, v);
            ofSetColor(rgb);

            if(slFill.rpn.execute3d1i(x, y, z, i) >= 0.0)
                ofFill();
            else
                ofNoFill();

            int resolution = std::round(std::max(0.0, slResolution.rpn.execute3d1i(x, y, z, i)));
            ofSetCircleResolution(resolution);

            ofPushMatrix();
            ofTranslate(x, y, 0);
            ofRotateDeg(rot);
            ofDrawCircle(0, 0, z);
            ofPopMatrix();
        }
    }
    }
    vertices_size = count;
    ofPopStyle();

    fbo.end();
}
