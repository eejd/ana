#include <limits>
#include <math.h>
#include <string>

#include "ofMain.h"

#include "ana_features.h"
#include "globalvars.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispTest.h"

#define TRACE_MONOTONIC

ScalarLispTest::ScalarLispTest() {
}

ScalarLispTest::~ScalarLispTest() {
}

#ifdef WITH_testcases
ScalarLisp ScalarLispTest::sl;
int ScalarLispTest::test_exec_failed = 0;
int ScalarLispTest::test_syntax_failed = 0;

bool ScalarLispTest::double_equals(double a, double b) {
    // positive infinity
    if(a == std::numeric_limits<double>::infinity() && b == std::numeric_limits<double>::infinity())
        return true;
    // negative infinity
    if(a == -1 * std::numeric_limits<double>::infinity() && b == -1 * std::numeric_limits<double>::infinity())
        return true;
    return std::fabs(a - b) < 100000.0 * rpn_epsilon;
}

void ScalarLispTest::check(double expected, std::string input, double x, double y, double z, int i) {
//    ofLog(OF_LOG_NOTICE) << input;
    int compile_err = sl.compile(input);
    if(compile_err) {
        ++test_syntax_failed;
        ofLog(OF_LOG_ERROR) << "TESTERROR " << input << " compile failed " << compile_err;
    } else {
#ifdef TRACE_MONOTONIC
        timespec time1, time2;
        clock_gettime(CLOCK_MONOTONIC, &time1);
#endif // TRACE_MONOTONIC
        double res1 = sl.rpn.execute3d1i(x, y, z, i); //TODO test parameter w
#ifdef TRACE_MONOTONIC
        clock_gettime(CLOCK_MONOTONIC, &time2);
#endif // TRACE_MONOTONIC
        bool hit = double_equals(res1, expected);
        if(hit) {
#ifdef TRACE_MONOTONIC
            ofLog(OF_LOG_NOTICE) << input << "\t->\t" << res1
                                 << "\t CLOCK\t" << (time2.tv_sec * 1000000000l + time2.tv_nsec) - (time1.tv_sec * 1000000000l + time1.tv_nsec);
#else
            ofLog(OF_LOG_NOTICE) << input << " -> " << res1;
#endif // TRACE_MONOTONIC
//			ofLog(OF_LOG_NOTICE) << '\n' << sl.rpn.dump();
        } else {
            ++test_exec_failed;
            ofLog(OF_LOG_ERROR) << "TESTERROR " << input << " -> " << res1;
            ofLog(OF_LOG_ERROR) << "TESTERROR " << expected << " != " << res1;
            std::cout << res1 << '\n';
            ofLog(OF_LOG_ERROR) << '\n' << sl.rpn.dump();
        }
    }
}

void ScalarLispTest::errcheck(std::string input, double x, double y) {
    ScalarRPN rpn;
    int compile_err = sl.compile(input);
    if(compile_err) {
        ofLog(OF_LOG_NOTICE) << input << " syntax error found " << compile_err;
    } else {
        ++test_syntax_failed;
        ofLog(OF_LOG_ERROR) << "TESTERROR " << input << " missed a syntax error " << compile_err;
    }
}


void ScalarLispTest::runTestCases() {
    ofLog(OF_LOG_NOTICE) << "--- begin ScalarLispTest::runTestCases ---";
    test_exec_failed = 0;
    test_syntax_failed = 0;
    ScalarLispTest l1;
    l1.check(0.351702, "(rand)", 1.23, 4.5, 6.7, 101);
    l1.check(0.611450, "(rand)", 1.23, 4.5, 6.7, 101);
    l1.check(0.662142, "(rand)", 1.23, 4.5, 6.7, 101);
    l1.check(1.23, "x", 1.23, 4.5, 6.7, 101);
    l1.check(4.5, "y", 1.23, 4.5, 6.7, 101);
    l1.check(6.7, "z", 1.23, 4.5, 6.7, 101);
    l1.check(101, "i", 1.23, 4.5, 6.7, 101);
    l1.check(113.43, "(+ x y z i)", 1.23, 4.5, 6.7, 101);

    l1.check(3.0, "(+ 3)", 0, 0, 0, 0);
    l1.check(1.6, "(+ 0.6 1)", 0, 0, 0, 0);
    l1.check(3.6, "(+ 0.6 1 2)", 0, 0, 0, 0);
    l1.check(1.7, "(+ 0.7 x)", 1, 0, 0, 0);
    l1.check(1.7, "(+ y x)", 1, 0.7, 0, 0);
    l1.check(36.5, "(+ 0.5 (* 12 3))", 0, 0, 0, 0);
    l1.check(14.5, "(+ 0.5 (- 12 3) 5)", 0, 0, 0, 0);
    l1.check(6.5, "(+ 0.5 (% 11 3) (- 5 1))", 0, 0, 0, 0);

    l1.check(3.0, "(* 3)", 0, 0, 0, 0);
    l1.check(-12, "(* 3 -4)", 0, 0, 0, 0);
    l1.check(60, "(* 3 -4 -5)", 0, 0, 0, 0);
    l1.check(60, "(* 3 (* -4 (* -5)))", 0, 0, 0, 0);
    l1.check(12, "(* 3 (* -4 (* -5)) 0.2)", 0, 0, 0, 0);

    l1.check(-3.0, "(- 3)", 0, 0, 0, 0);
    l1.check(-0.4, "(- 0.6 1)", 0, 0, 0, 0);
    l1.check(-2.4, "(- 0.6 1 2)", 0, 0, 0, 0);
    l1.check(-0.3, "(- 0.7 x)", 1, 0, 0, 0);
    l1.check(-0.3, "(- y x)", 1, 0.7, 0, 0);
    l1.check(-3.5, "(- 0.5 (/ 12 3))", 0, 0, 0, 0);
    l1.check(-8.5, "(- 0.5 (/ 12 3) 5)", 0, 0, 0, 0);
    l1.check(-7.5, "(- 0.5 (/ 12 3) (- 5 1))", 0, 0, 0, 0);

    l1.check(1.0/3.0, "(/ 3)", 0, 0, 0, 0);
    l1.check(0.6, "(/ 0.6 1)", 0, 0, 0, 0);
    l1.check(-0.1, "(/ 0.6 -3 2)", 0, 0, 0, 0);
    l1.check(0.7, "(/ 0.7 x)", 1, 0, 0, 0);
    l1.check(0.7/1.1, "(/ y x)", 1.1, 0.7, 0, 0);
    l1.check(0.125, "(/ 0.5 (/ 12 3))", 0, 0, 0, 0);
    l1.check(0.025, "(/ 0.5 (/ 12 3) 5)", 0, 0, 0, 0);
    l1.check(0.03125, "(/ 0.5 (/ 12 3) (- 5 1))", 0, 0, 0, 0);

    l1.check(1, "(pos? 0 1 2)", 1, 0, 0, 0);
    l1.check(2, "(pos? -0.00000001 1 2)", 1, 0, 0, 0);
    l1.errcheck("(pos? 0 1 2 3)", 1, 0);
    l1.errcheck("(pos? -10 5)", 1, 0);
    l1.errcheck("(pos? 10)", 1, 0);
    l1.check(-4, "(pos? (pos? -1 2 -3) 1 -4)", 1, 0, 0, 0);
    l1.check(5, "(pos? (pos? -1 2 -3) 1 (pos? 1 5 -3))", 1, 0, 0, 0);
    l1.errcheck("(pos? (pos? -1 2 -3) 1 (pos? -10 5))", 1, 0);
    l1.errcheck("(pos? (pos? -1 2 -3) 1 (pos? -10))", 1, 0);
    l1.check(-0.5, "(- 0 0.5)", 1, 0, 0, 0);
    l1.check(-1, "(pos? (- 0 0.5) 1 -1)", 1, 0, 0, 0);
    l1.check(0.5, "(- 1 0.5)", 1, 0, 0, 0);
    l1.check(1, "(pos? (- 1 0.5) 1 -1)", 1, 0, 0, 0);

    l1.check(1.3, "(min 1.3)", 0.1, 1.2, 0, 0);
    l1.check(-3, "(min -1 -2 -3)", 0.1, 1.2, 0, 0);
    l1.check(0.1, "(min y x 1.3)", 0.1, 1.2, 0, 0);
    l1.check(1.0, "(min y 1.0)", 0.1, 1.2, 0, 0);
    l1.check(0.1, "(min x)", 0.1, 1.2, 0, 0);
    l1.check(0.1, "(min x y)", 0.1, 1.2, 0, 0);
    l1.check(-1.2, "(min y x)", -0.1, -1.2, 0, 0);
    l1.check(0.1, "(min x y)", 0.1, 1.2, 0, 0);
    l1.check(0.1, "(min y x)", 0.1, 1.2, 0, 0);
    l1.check(0, "(min y x 0)", 0.1, 1.2, 0, 0);
    l1.check(-0.1, "(min y x 0)", -0.1, std::numeric_limits<double>::infinity(), 0, 0);
    l1.check(1.3, "(max 1.3)", 0.1, 1.2, 0, 0);
    l1.check(-1, "(max -1 -2 -3)", 0.1, 1.2, 0, 0);
    l1.check(1.3, "(max y x 1.3)", 0.1, 1.2, 0, 0);
    l1.check(1.2, "(max y 1.0)", 0.1, 1.2, 0, 0);
    l1.check(0.1, "(max x)", 0.1, 1.2, 0, 0);
    l1.check(1.2, "(max x y)", 0.1, 1.2, 0, 0);
    l1.check(-0.1, "(max y x)", -0.1, -1.2, 0, 0);
    l1.check(10, "(max y x 10)", 0.1, 1.2, 0, 0);

    l1.check(17, "17", 1.23, 4.5, 0, 0);
    l1.check(0, "", 1.23, 4.5, 0, 0);
    l1.check(-7, "-7", 1.23, 4.5, 0, 0);
    l1.check(1.23, "x", 1.23, 4.5, 0, 0);
    l1.check(4.5, "y", 1.23, 4.5, 0, 0);

    l1.check(12.3, "(/ x 0.1)", 1.23, 0, 0, 0);
    l1.check(1.36667, "( / x (- 1.0 0.1))", 1.23, 0, 0, 0);
    l1.check(1.36667, "(/  x  (-  1.0 0.1))", 1.23, 0, 0, 0);
    l1.check(1.36667, "(/ x (- 1.0 0.1 ) )", 1.23, 0, 0, 0);
    l1.check(1.36667, "(/ x (- 1.0 0.1   )) ", 1.23, 0, 0, 0);
    l1.check(1.36667, " (/ x(- 1.0  0.1))", 1.23, 0, 0, 0);

    l1.check(std::numeric_limits<double>::infinity(), "(/ x y)", 1, 0, 0, 0);
    l1.check(1, "(pos? x 1 2)", std::numeric_limits<double>::infinity(), 0, 0, 0);
    l1.check(-1 * std::numeric_limits<double>::infinity(), "(- 0 x)", std::numeric_limits<double>::infinity(), 0, 0, 0);
//TODO    l1.check(Double.doubleToLongBits(Double.NaN),  Double.doubleToLongBits("(- 1 x)", Double.NaN, 0)), 0, 0);
//TODO    l1.check(Double.doubleToLongBits(Double.NaN),  Double.doubleToLongBits("(turbulence x 0.2 0.3 x)", std::numeric_limits<double>::infinity(), 0)), 0, 0);

    l1.check(0.67561462439055364, "(unoise 0.1 0.2 0.3)", 1, 0, 0, 0);
    l1.check(0.57117857993727994, "(unoise 0.1 0.2 0.1)", 1, 0, 0, 0);
    l1.check(0.59308035717724161, "(unoise 0.1 0.1 0.1)", 1, 0, 0, 0);
    l1.check(0.35122924878110723, "(snoise 0.1 0.2 0.3)", 1, 0, 0, 0);
    l1.check(0.14235715987455999, "(snoise 0.1 0.2 0.1)", 1, 0, 0, 0);
    l1.check(0.18616071435448320, "(snoise 0.1 0.1 0.1)", 1, 0, 0, 0);
    l1.errcheck("(snoise 0.1 0.2 0.3 1.0)", 1, 0);
    l1.errcheck("(unoise 0.1 0.2)", 1, 0);
    l1.errcheck("(unoise 0.1)", 1, 0);
    l1.errcheck("(snoise 0.1)", 1, 0);
    l1.check(0.38679732719329274,   "(turb 0.1 0.2 0.3 3)", 1, 0, 0, 0);
    l1.check(0.42515722670161921,   "(turb 0.1 0.2 0.3 x)", std::numeric_limits<double>::infinity(), 0, 0, 0);

    l1.check(0.5, "(sin x)", M_PI / 6, 0, 0, 0);
    l1.check(0.75, "(usin x)", M_PI / 6, 0, 0, 0);
    l1.check(0.5235987755982989, "(asin x)", 0.5, 0, 0, 0);
    l1.check(0.5, "(cos y)", 0, M_PI / 3, 0, 0);
    l1.check(0.75, "(ucos y)", 0, M_PI / 3, 0, 0);
    l1.check(1.0471975511965979, "(acos y)", 0, 0.5, 0, 0);
    l1.check(8, "(pow 2 3)", 1, 0, 0, 0);
    l1.check(0.125, "(pow 2 -3)", 1, 0, 0, 0);
    l1.check(1, "(log x)", M_E, 0, 0, 0);
    l1.check(10, "(log2 x)", 1024, 0, 0, 0);
    l1.check(1.7, "(+ 0.7 x)", 1, 0, 0, 0);
    l1.check(10, "(+ 1 2 x y)", 3, 4, 0, 0);
    l1.check(120, "(* 1 2 x y 5)", 3, 4, 0, 0);
    l1.check(120, "(* (+ 0 1) (+ 1 1) (/ x 1 1 2) (* (+ 1 1) y) (- 15 5 3 2))", 6, 2, 0, 0);
    l1.check(-0.3, "(- 0.7 x)", 1, 0, 0, 0);
    l1.check(-1, "(- 0.7 x y (/ 2 10))", 1, 0.5, 0, 0);
    l1.check(-1, "(- 0.7 x y (/ 2 10))", 1, 0.5, 0, 0);
    l1.check(0.99, "(mix x y 0.99)", 0, 1, 0, 0);
    l1.check(0, "(mix -1 1 0.5)", 0, 1, 0, 0);
    l1.check(0, "(step 1 1)", 0, 1, 0, 0);
    l1.check(1, "(step y -0.99)", 0, -1, 0, 0);
    l1.check(1, "(puls -1 1 x)", 0, 1, 0, 0);
    l1.check(0, "(puls 0 0.1 x)", 0 - rpn_epsilon, 1, 0, 0);
    l1.check(0, "(puls 0 0.1 x)", 0, 0.1 + rpn_epsilon, 0, 0);
    l1.check(1, "(clamp -1 1 x)", 10, 1, 0, 0);
    l1.check(-1, "(clamp -1 1 x)", -(1 + rpn_epsilon), 1, 0, 0);
    l1.check(0.55, "(clamp 0.5 0.6 x)", 0.55, 1, 0, 0);
    l1.check(1, "(sign 0.7)", 1, 0, 0, 0);
    l1.check(-1, "(sign y)", 1, -1, 1, 1);
    l1.check(0, "(sign i)", -1, -1, -1, 0);
    l1.check(0.7, "(abs 0.7)", 1, 0, 0, 0);
    l1.check(1, "(abs x)", -1, 0, 0, 0);
    l1.check(1, "(trunc 1.3)", 1, 0, 0, 0);
    l1.check(-1, "(trunc x)", -1.7, 0, 0, 0);
    l1.check(2, "(round y)", 0, 1.5001, 0, 0);
    l1.check(1, "(round 1.3)", 1, 0, 0, 0);
    l1.check(-1, "(round x)", -1.3, 0, 0, 0);
    l1.check(1, "(smooth -1 1 x)", 1 + rpn_epsilon, 1, 0, 0);
    l1.check(0, "(smooth -1 1 x)", -(1 + rpn_epsilon), 1, 0, 0);
    l1.check(0.5, "(smooth -1 1 x)", 0, 0, 0, 0);
    l1.check(1, "(box -1 1 x)", 1 + rpn_epsilon, 1, 0, 0);
    l1.check(0, "(box -1 1 x)", -(1 + rpn_epsilon), 1, 0, 0);
    l1.check(0.5, "(box -1 1 x)", 0, 0, 0, 0);
    l1.check(2, "(% 11 3)", 0, 0, 0, 0);
    l1.check(1, "(% -11 3)", 0, 0, 0, 0);
    l1.check(std::numeric_limits<double>::infinity(), "(% -11 0)", 0, 0, 0, 0);
    l1.check(2, "(sqrt x)", 4, 0, 0, 0);
    l1.check(2, "(sqrt x)", -4, 0, 0, 0);
    l1.check(0, "(sqrt 0)", 0, 0, 0, 0);
    l1.check(16, "(sqr x)", 4, 0, 0, 0);
    l1.check(0.01, "(sqr x)", -0.1, 0, 0, 0);
    l1.check(0, "(sqr 0)", 0, 0, 0, 0);
    l1.check(-M_PI / 2, "(atan y 1)", 0, -1 * std::numeric_limits<double>::infinity(), 0, 0);
    l1.check(M_PI / 4, "(atan x y)", 1, 1, 0, 0);
    l1.check(-M_PI / 2, "(atan x y)", -1, 0, 0, 0);
    l1.check(1, "(floor x)", 1, 0, 0, 0);
    l1.check(3, "(floor x)", M_PI, 0, 0, 0);
    l1.check(1, "(ceil x)", 1, 0, 0, 0);
    l1.check(4, "(ceil x)", M_PI, 0, 0, 0);

//TODO    l1.check(38.832975677895, "(edist 11 12 x y)", 33, 44, 0, 0);
    l1.check(54, "(mdist 11 12 x y)", 33, 44, 0, 0);

    l1.check(0.0, "(gamma 0.7 x)", 0, 1, 0, 0);
    l1.check(1.0, "(gamma 0.7 y)", 0, 1, 0, 0);
    l1.check(0.0, "(gamma 1.7 x)", 0, 1, 0, 0);
    l1.check(1.0, "(gamma 1.7 y)", 0, 1, 0, 0);
    l1.check(0.5, "(gamma 1.0 0.5)", 0, 1, 0, 0);
    l1.check(0.0, "(bias 0.7 x)", 0, 1, 0, 0);
    l1.check(1.0, "(bias 0.7 y)", 0, 1, 0, 0);
    l1.check(0.0, "(gain 0.7 x)", 0, 1, 0, 0);
    l1.check(1.0, "(gain 0.7 y)", 0, 1, 0, 0);
    l1.check(0.5, "(gain 0.8 0.5)", 0, 1, 0, 0);
    l1.check(0.0, "(gain 1.7 x)", 0, 1, 0, 0);

    h0 = 3; s0 = 0.5; v0 = 0.7;
    l1.check(3 * 0.5 * 0.7, "(* h s v)", 1.23, 4.5, 6.7, 101);

    if(test_exec_failed || test_syntax_failed) {
        ofLog(OF_LOG_ERROR) << test_syntax_failed << " " << test_exec_failed << " some test cases failed";
    }
    ofLog(OF_LOG_NOTICE) << "--- end ScalarLispTest::runTestCases ---";
}

inline bool zero(double v) { return std::fabs(v) < rpn_epsilon; }
static double fn0(double x, double y, double z, int i) {
    return (zero(x) ? (zero(y) ? 1 : 2) : (zero(z) ? 3 : 4));
}

void ScalarLispTest::prof(std::string input, double x, double y, double z, int i) {
    int compile_err = sl.compile(input);
    if(compile_err) {
        ++test_syntax_failed;
        ofLog(OF_LOG_ERROR) << input << " compile failed " << compile_err;
    } else {
#ifdef TRACE_MONOTONIC
        timespec time1, time2;
        clock_gettime(CLOCK_MONOTONIC, &time1);
#endif // TRACE_MONOTONIC
        double res0 = sl.rpn.execute4d3i(x, y, z, 0.0, i, 0, 0); // 0
        double res1 = sl.rpn.execute4d3i(x, y, z, 0.0, i, 0, 0); // 1
        double res2 = sl.rpn.execute4d3i(x, y, z, 0.0, i, 0 ,0); // 2
        double res3 = sl.rpn.execute4d3i(x, y, z, 0.0, i, 0 ,0); // 3
        double res4 = sl.rpn.execute4d3i(x, y, z, 0.0, i, 0 ,0); // 4
        double res5 = sl.rpn.execute4d3i(x, y, z, 0.0, i, 0 ,0); // 5
        double res6 = sl.rpn.execute4d3i(x, y, z, 0.0, i, 0 ,0); // 6
        double res7 = sl.rpn.execute4d3i(x, y, z, 0.0, i, 0 ,0); // 7
        double res8 = sl.rpn.execute4d3i(x, y, z, 0.0, i, 0 ,0); // 8
        double res9 = sl.rpn.execute4d3i(x, y, z, 0.0, i, 0 ,0); // 9
#ifdef TRACE_MONOTONIC
        clock_gettime(CLOCK_MONOTONIC, &time2);
        ofLog(OF_LOG_NOTICE) << "inter\t" << (time2.tv_sec * 1000000000l + time2.tv_nsec) - (time1.tv_sec * 1000000000l + time1.tv_nsec)
                             << "\t" << input << "\t->\t" << res0;
#endif // TRACE_MONOTONIC
        if(res0 != res1
            || res0 != res2
            || res0 != res3
            || res0 != res4
            || res0 != res5
            || res0 != res6
            || res0 != res7
            || res0 != res8
            || res0 != res9) {
            ++test_exec_failed;
            ofLog(OF_LOG_ERROR) << "TESTERROR " << "different " << input << "\t->\t" << res0;
        }
        ofLog(OF_LOG_NOTICE) << '\n' << sl.rpn.dump();
    }
}

void ScalarLispTest::runProfile() {
    ofLog(OF_LOG_NOTICE) << "--- begin ScalarLispTest::runProfile ---";

#ifdef TRACE_MONOTONIC
        timespec time1, time2;
        clock_gettime(CLOCK_MONOTONIC, &time1);
#endif // TRACE_MONOTONIC
        double res0 = fn0(1.23, 4.5, 6.7, 101);
        double res1 = fn0(1.23, 4.5, 6.7, 101);
        double res2 = fn0(1.23, 4.5, 6.7, 101);
        double res3 = fn0(1.23, 4.5, 6.7, 101);
        double res4 = fn0(1.23, 4.5, 6.7, 101);
        double res5 = fn0(1.23, 4.5, 6.7, 101);
        double res6 = fn0(1.23, 4.5, 6.7, 101);
        double res7 = fn0(1.23, 4.5, 6.7, 101);
        double res8 = fn0(1.23, 4.5, 6.7, 101);
        double res9 = fn0(1.23, 4.5, 6.7, 101);
#ifdef TRACE_MONOTONIC
        clock_gettime(CLOCK_MONOTONIC, &time2);
        ofLog(OF_LOG_NOTICE)  << "compc\t" << (time2.tv_sec * 1000000000l + time2.tv_nsec) - (time1.tv_sec * 1000000000l + time1.tv_nsec)
                              << "\t" << res0;
#endif // TRACE_MONOTONIC
        if(res0 != res1
            || res0 != res2
            || res0 != res3
            || res0 != res4
            || res0 != res5
            || res0 != res6
            || res0 != res7
            || res0 != res8
            || res0 != res9) {
            ++test_exec_failed;
            ofLog(OF_LOG_ERROR) << "TESTERROR " << "different fn0\t->\t" << res0;
        }

    test_exec_failed = 0;
    test_syntax_failed = 0;
    ScalarLispTest l1;
    l1.prof("", 1.23, 4.5, 6.7, 101);
    l1.prof("", 1.23, 4.5, 6.7, 101);
    l1.prof("3.1415", 1.23, 4.5, 6.7, 101);
    l1.prof("(+ 50 (* 34 (% i 12)))", 1.23, 4.5, 6.7, 101);
    l1.prof("(+ 50 (* 34 (% i (+ 50 (* 34 (% i (+ 50 (* 34 (% i 12)))))))))", 1.23, 4.5, 6.7, 101);
    l1.prof("(* 2 i (sin (* (+ f i) 2.3998)))", 1.23, 4.5, 6.7, 101);
    l1.prof("(* 7 (pow (unoise (* 0.0051 x) (* 0.0051 y) (* 0.0053 f)) 1.75))", 1.23, 4.5, 6.7, 101);
    l1.prof("(+ 45 (* 0.6 (- y 940) (sin (* 0.031 f)) (snoise x y 0.1)))", 1.23, 4.5, 6.7, 101);
    l1.prof("(zero? x (zero? y 1 2) (zero? z 3 4))", 0, 1, 2, 101);
    l1.prof("(zero? x (zero? y 1 2) (zero? z 3 4))", 1.23, 4.5, 6.7, 101);
    l1.prof("(zero? x 1 2)", 0, 1, 2, 101);
    l1.prof("(zero? y (+ 1 2) 3)", 1, 1, 0, 101);
    l1.prof("(zero? x (zero? y (+ 1 2) 3) (zero? z 4 (+ 5 z)))", 1, 1, 0, 101);
//    l1.prof("(even? (* 181 y) i (sqr (sin i))", 1.23, 4.5, 6.7, 101);
    l1.prof("(+ 7 (* (+ 3 4 5 6) 1 2))", 1.23, 4.5, 6.7, 101);

    if(test_exec_failed || test_syntax_failed) {
        ofLog(OF_LOG_ERROR) << "TESTERROR " << test_syntax_failed << " " << test_exec_failed << " some test cases failed";
    }
    ofLog(OF_LOG_NOTICE) << "--- end ScalarLispTest::runProfile ---";
}
#endif // WITH_testcases
