#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

#include "ofMain.h"

#include "globalvars.h"
#include "Configuration.h"
#include "OscListener.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispMng.h"
#include "gl/GlWindowFilter.h"


GlWindowFilter::GlWindowFilter(ofGLWindowSettings settings, OscListener *oscParams) :
        Filter(settings, oscParams) {
    display = XOpenDisplay(NULL);
}

GlWindowFilter::~GlWindowFilter() {
    clearPixelBuffer();
}

#define ACTIVE_WINDOWS "_NET_CLIENT_LIST"

void GlWindowFilter::findForeignWindow_by_title(bool verbose)
{
    Window rootWindow = RootWindow(display, DefaultScreen(display));
    Atom atom = XInternAtom(display, ACTIVE_WINDOWS, true);
    Atom actualType;
    int format = 0;
    unsigned long numItems = 0;
    unsigned long bytesAfter = 0;
    unsigned char *data = nullptr;

    int status_get = XGetWindowProperty(display, rootWindow, atom, 0L, (~0L), false,
        AnyPropertyType, &actualType, &format, &numItems, &bytesAfter, &data);

    if (status_get == Success && numItems) {
        Window *list = (Window *)data;
        if(verbose)
            printf("search  '%s' num items %ld\n", foreign_title.c_str(), numItems);
        for (unsigned long i = 0; i < numItems; ++i) {
            XTextProperty prop;
            if(XGetWMName(display, list[i], &prop)) {
                int count = 0;
                char **wmlist = NULL;
                int result = XmbTextPropertyToTextList(display, &prop, &wmlist, &count);
                if(result == Success && count > 0 && wmlist && wmlist[0]){
                    if(verbose)
                        printf("title %lu  '%s'\n", i, wmlist[0]);
                    if(strcmp(wmlist[0], foreign_title.c_str()) == 0) {
                        foreign_window = list[i];
                        hasWindow = true;
                        if(verbose)
                            printf("found %lu '%s'\n", i, wmlist[0]);
                        break;
                    }
                }
                else {
                    if(verbose)
                        printf("wmli %lu  ?\n", i);
                }
            }
            else {
                if(verbose)
                    printf("wmli %lu  !\n", i);
            }
        }
    }
    XFree(data);
}

void GlWindowFilter::findForeignWindow_by_num(bool verbose, int window_num)
{
    Window rootWindow = RootWindow(display, DefaultScreen(display));
    Atom atom = XInternAtom(display, ACTIVE_WINDOWS, true);
    Atom actualType;
    int format = 0;
    unsigned long numItems = 0;
    unsigned long bytesAfter = 0;
    unsigned char *data = nullptr;

    int status_get = XGetWindowProperty(display, rootWindow, atom, 0L, (~0L), false,
        AnyPropertyType, &actualType, &format, &numItems, &bytesAfter, &data);

    if (status_get == Success && numItems > 0) {
        Window *list = (Window *)data;
        foreign_window = list[window_num % numItems];
        hasWindow = true;
    }

    XFree(data);
}

void GlWindowFilter::searchFromRoot(bool verbose)
{
    hasWindow = false;
    foreign_width = 0;
    foreign_height = 0;

    int window_num = slI.rpn.execute0();
    if(window_num >= 0) {
        findForeignWindow_by_num(verbose, window_num);
    }
    else {
        if(foreign_title.empty())
            return;
        findForeignWindow_by_title(verbose);
    }

    if (hasWindow) {
        XWindowAttributes foreignWindowAttributes;
        XGetWindowAttributes(display, foreign_window, &foreignWindowAttributes);

        foreign_width = foreignWindowAttributes.width;
        foreign_height = foreignWindowAttributes.height;
    }
    else {
        if(verbose)
            ofLogWarning() << "Cannot find window with title '" << foreign_title.c_str() << "'";
    }

}

void GlWindowFilter::setup(const std::string id, int numSamples) {
    Filter::setup(id, numSamples);

    allocatePixelBuffer();
    searchFromRoot(true);
}

void GlWindowFilter::clearPixelBuffer() {
    if(hasPixelBuffer) {
        pixelBuffer.clear();
        pixelImage.clear();
        hasPixelBuffer = false;
    }
}

void GlWindowFilter::allocatePixelBuffer() {
    pixelBuffer.allocate(filterWidth, filterHeight, OF_PIXELS_RGB);
    hasPixelBuffer = true;
    ofLogNotice() << "allocatePixelBuffer " << filterWidth << "x" << filterHeight;
}

void GlWindowFilter::reload(ofxOscMessage const &msg) {
    ScalarLispMng mng;
    mng.compile(slI, "i", "-1", msg);

    if(msg.getNumArgs() >= 3) {
        foreign_title = msg.getArgAsString(2);
    }
    searchFromRoot(true);
}

void GlWindowFilter::update() {
    searchFromRoot(false);

    if (!hasWindow)
        return;

    XWindowAttributes foreignWindowAttributes;
    XGetWindowAttributes(display, foreign_window, &foreignWindowAttributes);

    foreign_width = foreignWindowAttributes.width;
    foreign_height = foreignWindowAttributes.height;
    XImage *image = XGetImage(display, foreign_window,
                              0, 0, foreign_width, foreign_height,
                              AllPlanes, ZPixmap);

    if(image) {
        XColor colors;
        unsigned long red_mask = image->red_mask;
        unsigned long green_mask = image->green_mask;
        unsigned long blue_mask = image->blue_mask;

        for (size_t j = 0; j < foreign_width && j < filterWidth; ++j) {
            for (size_t i = 0; i < foreign_height && i < filterHeight; ++i) {
                colors.pixel = XGetPixel(image, j, i);
                pixelBuffer.setColor(j, i, ofColor(
                                     (colors.pixel & red_mask) >> 16,
                                     (colors.pixel & green_mask) >> 8,
                                     colors.pixel & blue_mask));
            }
        }
        pixelImage.setFromPixels(pixelBuffer);

        XDestroyImage(image);
    }
}

void GlWindowFilter::draw(int li) {
    fbo.begin();

    if(hasPixelBuffer && hasWindow) {
        pixelImage.draw(0, 0);
    }

    fbo.end();
}

void GlWindowFilter::toMermaid(std::stringstream & info) {
    info << "window((window))" << " -- c0 --> " << id << "\n";
    Filter::toMermaid(info);
}
