(ns lv.demo)
(use 'lv.core)

(layout "grid" [img0 cline1] [cline2 ras3])
; assume there are 4 PNG images in the media folder.
(image "flower-sampler/*.png")
(render)

; channels
(rgb :img0 [c0 c1 c2 c3]
  (rot (* f 0.0053))
  (rgb (mix c2.rgb
            c3.rgb
            (unoise x y)))
)

; find contours center points
; and display color names
(expr :cline1 [img0]
  (count 100)
  (max 1000)
  (thres (+ 0.05
            (* 0.2
               (usin (* 0.011 f)))))
  (fill 1)
  (names 1)
  (clear -1)
  (paint -1)
  (holes 1)
)

; find contours center points
; and display color names
(expr :cline2 [img0]
  (count 100)
  (max 1000)
  (thres (+ 0.05
            (* 0.2
               (usin (* 0.011 f)))))
  (fill 1)
  (names 1)
  (clear 1)
  (paint 1)
  (holes 1)
)

(rgb :ras3 [ras3 cline2]
  (rgb (mix ras3.rgb
            cline2.rgb
            (unoise y x)))
)
; (render 3)
