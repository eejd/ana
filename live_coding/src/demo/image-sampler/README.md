# Animation with 4 static images

Over time the frame counter `f` will deform the geometry of the input PNG images.
A wobbling effect. Used a Perlin noise to animate mix 2 static images.

![](./image-sampler.jpg)

```mermaid
graph LR
c0>P1000177-1080x1080.png] -- c0 --> :img0
c0>P1000177-1080x1080.png] -. c0 .-> :img1
c1>P1080408-1080x1080.png] -. c1 .-> :img1
c2>P1080896-1080x1080.png] -- c2 --> :img1
c3>P1120404-1080x1080.png] -. c3 .-> :img1
:img0 -- img0 --> :ras2
:img1 -- img1 --> :ras2
:img0 -- img0 --> :ras3
:ras2 -- ras2 --> :ras3
```

```clojure
(ns lv.demo)
(use 'lv.core)

(layout "grid" [img0 img1] [ras2 ras3])
; assume there are 4 PNG images in the media folder.
(image "flower-sampler/*.png")
(render)

; deform channel c0
(hsv :img0 [c0]
  (rot (* f 0.005))
  (sx (+ 2 (snoise (* f 0.01) y)))
  (sy (+ 2 (snoise x (* f 0.01))))
  (hsv c0.hsv)
)
(render 0)

; deform channel c2
(hsv :img1 [c0 c1 c2 c3]
  (tx (* 0.093 (sin (* 0.1 f)) c2.v))
  (ty (* 0.015 (cos (* 0.1 f)) c2.v))
  (hsv c2.hsv)
)
(render 1)

; mix the two upsteam filters
; the green channel of filter img0 is used as mixing factor
(rgb :ras2 [img0 img1]
  (rgb (mix img0.rgb
            img1.rgb
            img0.g))
)
(render 2)

; a kaleidoscope effect
(hsv :ras3 [img0 ras2]
  (kaleid 2)
  (mirror)
  (h img0.h)
  (s img0.s)
  (v (* img0.v ras2.v))
)
(render 3)

(render)
```
