#include <filesystem>
#include <chrono>

#include "ofMain.h"

#include "Configuration.h"
#include "stringhelper.h"
#include "globalvars.h"
#include "util/pathhelper.h"
#include "calc/MsgLisp.h"
#include "OscListener.h"
#include "Filter.h"
#include "frag/FragMultipassFilter.h"

FragMultipassFilter::FragMultipassFilter(ofGLWindowSettings settings, OscListener * oscParams) : Filter(settings, oscParams){
}

FragMultipassFilter::~FragMultipassFilter(){
    fxPipeline.clear();
	passThroughShader.unload();
    for(auto glsl : glslProgPool) {
        glsl.shader.unload();
        glsl.uniforms.clear();
        glsl.inject.clear();
    }
    glslProgPool.clear();
	tmpFbo0.clear();
	tmpFbo1.clear();
}

static std::string builtinFrag = STRINGIFY(#version 330\n
uniform sampler2DRect tex0;\n
out vec4 outputColor;\n
void main()\n
{\n
    outputColor = texture(tex0, gl_FragCoord.xy);\n
\n
    outputColor.a = 1.0;\n
}\n
);

//static std::string mainDefFragS = std::string("\n") + std::string("\n");

static std::string mainDefFrag =
std::string("\n")
+ std::string("    float f = float(frame);\n")
+ std::string("    vec2 _xy = 2.0 * gl_FragCoord.xy / panel - 1.0;\n")
+ std::string("    float x = _xy.x;\n")
+ std::string("    float y = _xy.y;\n")
+ std::string("    float r = length(_xy);\n")
+ std::string("    float a = atan(_xy.x, _xy.y);\n")
+ std::string("    vec2 _ij = gl_FragCoord.xy;\n")
+ std::string("    float i = _ij.x;\n")
+ std::string("    float j = _ij.y;\n")
+ std::string("\n")
+ std::string("#define vol volume\n")
+ std::string("#define f0 fft03.x\n")
+ std::string("#define f1 fft03.y\n")
+ std::string("#define f2 fft03.z\n")
+ std::string("#define f3 fft03.w\n")
+ std::string("#define f4 fft47.x\n")
+ std::string("#define f5 fft47.y\n")
+ std::string("#define f6 fft47.z\n")
+ std::string("#define f7 fft47.w\n")
+ std::string("\n")
+ std::string("#define u0 u0123.x\n")
+ std::string("#define u1 u0123.y\n")
+ std::string("#define u2 u0123.z\n")
+ std::string("#define u3 u0123.w\n")
+ std::string("#define u4 u4567.x\n")
+ std::string("#define u5 u4567.y\n")
+ std::string("#define u6 u4567.z\n")
+ std::string("#define u7 u4567.w\n")
+ std::string("\n")
+ std::string("#define t0 t0123.x\n")
+ std::string("#define t1 t0123.y\n")
+ std::string("#define t2 t0123.z\n")
+ std::string("#define t3 t0123.w\n")
+ std::string("#define t4 t4567.x\n")
+ std::string("#define t5 t4567.y\n")
+ std::string("#define t6 t4567.z\n")
+ std::string("#define t7 t4567.w\n")
+ std::string("\n")
+ std::string("#define kp0 kp0123.x\n")
+ std::string("#define kp1 kp0123.y\n")
+ std::string("#define kp2 kp0123.z\n")
+ std::string("#define kp3 kp0123.w\n")
+ std::string("#define kp4 kp4567.x\n")
+ std::string("#define kp5 kp4567.y\n")
+ std::string("#define kp6 kp4567.z\n")
+ std::string("#define kp7 kp4567.w\n")
+ std::string("\n")
+ std::string("#define ks0 ks0123.x\n")
+ std::string("#define ks1 ks0123.y\n")
+ std::string("#define ks2 ks0123.z\n")
+ std::string("#define ks3 ks0123.w\n")
+ std::string("#define ks4 ks4567.x\n")
+ std::string("#define ks5 ks4567.y\n")
+ std::string("#define ks6 ks4567.z\n")
+ std::string("#define ks7 ks4567.w\n")
+ std::string("\n")
+ std::string("#define kb0 kb0123.x\n")
+ std::string("#define kb1 kb0123.y\n")
+ std::string("#define kb2 kb0123.z\n")
+ std::string("#define kb3 kb0123.w\n")
+ std::string("#define kb4 kb4567.x\n")
+ std::string("#define kb5 kb4567.y\n")
+ std::string("#define kb6 kb4567.z\n")
+ std::string("#define kb7 kb4567.w\n")
+ std::string("\n")
+ std::string("#define lx js0xy.x\n")
+ std::string("#define ly js0xy.y\n")
+ std::string("#define rx js0xy.z\n")
+ std::string("#define ry js0xy.w\n")
+ std::string("#define l2 js0lr.x\n")
+ std::string("#define r2 js0lr.y\n")
+ std::string("\n");


void FragMultipassFilter::insertFallback() {
    if(fxPipeline.empty()) {
        GlslProgMng passThroughProg;
        memset(&passThroughProg, 0, sizeof(passThroughProg));
        passThroughProg.shader = passThroughShader;
        fxPipeline.push_back(passThroughProg);
    }
}

void FragMultipassFilter::updatePipeline() {
    fxPipeline.clear();
    for(auto glsl : glslProgPool) {
        fxPipeline.push_back(glsl);
    }
    insertFallback();
}

void FragMultipassFilter::setup(const std::string id, int numSamples) {
	Filter::setup(id, numSamples);

	tmpFbo0.allocate(filterWidth, filterHeight, GL_RGBA32F_ARB, numSamples);
	tmpFbo1.allocate(filterWidth, filterHeight, GL_RGBA32F_ARB, numSamples);

	ofLog(OF_LOG_NOTICE) << "compile build in shaders " << id;
	passThroughShader.setupShaderFromSource(GL_VERTEX_SHADER, fallbackVert);
	passThroughShader.setupShaderFromSource(GL_FRAGMENT_SHADER, builtinFrag);
	passThroughShader.linkProgram();

    std::vector<std::string> frag_file_list = file_names(asString(MEDIA_PATH) + "/fx/*.frag", false);
    for(auto fn : frag_file_list) {
        GlslProgMng glslProg;
        glslProg.fragProgName = fn;
        glslProg.pathFragProg = glslProg.fragProgName;
        if(!std::filesystem::exists(glslProg.pathFragProg)) {
            glslProg.fatalFragProg = true;
            ofLog(OF_LOG_FATAL_ERROR) << "Cannot access '" << glslProg.fragProgName << "'";
        }
        else {
            glslProg.maindef = mainDefFrag;
            reloadFragProg(true, glslProg);
            glslProgPool.push_back(glslProg);
        }
    }

    updatePipeline();
}

void FragMultipassFilter::reload(ofxOscMessage const &msg){
    auto narg = msg.getNumArgs();
    fxPipeline.clear();
    if(narg >= 2) {
        Filter::setInputTexture(msg.getArgAsString(1));
    }
    for(size_t nx = 2; nx < narg; ++nx) {
        std::string fx_src = msg.getArgAsString(nx);
//        ofLog(OF_LOG_NOTICE) << "FragMultipassFilter::reload '" << fx_src << "'";
        MsgLisp l1;
        ast_m* res = l1.compile(fx_src);
        bool ok = res
                && res->count() >= 1;
        if(ok) {
//            ofLog(OF_LOG_NOTICE) << "res->count()      '" << res->count() << "'";
//            ofLog(OF_LOG_NOTICE) << "res->at(0)->tok   '" << res->at(0)->tok << "'";
//            ofLog(OF_LOG_NOTICE) << "res->childCount() '" << res->childCount() << "'";
//            ofLog(OF_LOG_NOTICE) << l1.dump_ast(res, 0);
//            for(int px=0; px+1<res->count(); ++px) {
//                ofLog(OF_LOG_NOTICE) << "\n" << l1.ast2frag(res->at(px+1), 0) << "\n";
//            }

            for(auto glsl : glslProgPool) {
                bool match = hasEnding(glsl.fragProgName, res->at(0)->tok + ".frag");
//                ofLog(OF_LOG_NOTICE) << match << " match '" << glsl.fragProgName << "'";
                if(match) {
                    glsl.uniforms.clear();
                    glsl.inject.clear();
                    for(int px=0; px+1<res->count(); ++px) {
                        std::string pname = "p" + to_string(px);
                        UnifomExpression ue;
                        ue.name = pname;
                        std::string frag_term = l1.ast2frag(res->at(px+1), 0);
                        ue.term = frag_term;
                        std::string single_pi_inject = "\n#undef _" + pname + "\n"
                                                     + "#define _" + pname + " float " + pname + " = " + frag_term + "\n";
//                        ofLog(OF_LOG_NOTICE) << match << " single '" << single_pi_inject << "'";
                        glsl.inject.push_back(single_pi_inject);
                        glsl.uniforms.push_back(ue);
                    }
                    glsl.maindef = mainDefFrag;
                    reloadFragProg(true, glsl);
                    fxPipeline.push_back(glsl);
                }
            }
        }
//        ofLog(OF_LOG_NOTICE) << "----";
    }
    insertFallback();
}

void FragMultipassFilter::update() {
    size_t psize = glslProgPool.size();
    bool need = false;
    for(size_t px=0; px<psize; ++px) {
        need = reloadFragProg(false, glslProgPool.at(px)) || need;
    }
    if(need) {
        updatePipeline();
    }
}

void FragMultipassFilter::draw(int li) {
	size_t len = fxPipeline.size();
	size_t uix = 0;
	bool toggle = false;
	for(auto currentFxProg : fxPipeline){
		bool last = uix+1 >= len;
		bool first = uix == 0;
		if(last) fbo.begin(); else (toggle ? tmpFbo0 : tmpFbo1).begin();
//		ofClear(0, 0, 0, 255);

		currentFxProg.shader.begin();

		if(first) {
			if(upstream_ids.size() >= 1) {
				auto in_id = upstream_ids[0];
				if(filterDict.find(in_id) != filterDict.end()) {
					auto f = filterDict[in_id];
					if(f) {
						currentFxProg.shader.setUniformTexture("tex0", f->getTexture(), 0);
					}
				}
			}
		}
		else {
			currentFxProg.shader.setUniformTexture("tex0", (toggle ? tmpFbo1 : tmpFbo0).getTexture(), 0);
		}
		sendUniforms(currentFxProg.shader, li);
		for(auto ue : currentFxProg.uniforms) {
            float pvalue = ue.slFloat.rpn.execute0();
		    currentFxProg.shader.setUniform1f(ue.name, pvalue);
		}

		ofDrawRectangle(0, 0, filterWidth, filterHeight);

		currentFxProg.shader.end();
		if(last) fbo.end(); else (toggle ? tmpFbo0 : tmpFbo1).end();
		++uix;
		toggle = !toggle;
	}
}
