#pragma once
#include <string>
#include <vector>

#include "ana_features.h"
#include "color/Palette.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "lsys/LsysMachine.h"
#include "lsys/LsysIntepretation.h"

#define maxDepthLeafStack 100
#define maxPolyPoints 100

#define maxDepthBranchStack 1000
#define maxDepthLeafStack 100

class LsysCurve : public LsysIntepretation {
public:
    LsysCurve();
    virtual ~LsysCurve();

    virtual void init(size_t length0, char* string0, ScalarLisp** sl0);
    virtual void draw();
    virtual void expandBoundigBox();

    double start_direction = 1.5*M_PI;
    double turtle_direction = 0;
    float turtle_x = 0.0;
    float turtle_y = 0.0;

private:
    int drawBranch(size_t readix, float px, float py, float dir, int colorix, int rd);
    int boundsBranch(size_t readix, float px, float py, float dir, int rd);

    typedef struct point2D { float x; float y; } point2D_t;
    void drawHermiteCubicCurve(point2D_t* vert_list, int vert_len, int coli);

    bool stackMessageSend = false;
};
