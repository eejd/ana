(ns lv.demo)
(use 'lv.core)

(layout "horizontal" [clag1])

; kp0    0.33
;
; ks0    0.32283
; ks1    0.64567
; ks2    0.59055
; ks3    0.66142
;
; kb0    1
; kb1    1

(expr :clag1 []
  "physarum/avoid/avoid.cl"
  (paint 1)

  (p0 (+ (* ks0 0.005) 0.00005))
  (p1 (* ks1 0.01))
  (p2 (* ks2 0.2))
  (p3 (* ks3 0.2))
  (p5 kb0)
  (p6 kb1)
  (p7 (+ kp0 0.1))
)
