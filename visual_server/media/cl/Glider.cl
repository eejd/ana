// this OpenCL code is published with Ready 0.7
// see: https://github.com/GollyGang/ready
// This sample ist based on 
// C. P. Schenk, A. W. Liehr, M. Bode, and H.-G. Purwins (1999) "Quasi-Particles in a Three-Dimensional Three-Component Reaction-Diffusion System" High Performance Computing in Science and Engineering, pp. 354-364.
// http://www.uni-muenster.de/Physik.AP/Purwins/RD/Literatur/hlrs1999.pdf
// Dissipative solitons in a three-chemical system, by the Purwins group at Munster.
// http://www.uni-muenster.de/Physik.AP/Purwins/Gruppe/Purwins-en.html


// width and height must be power of 2

#define XY(x,y) ((x) & ((X)-1)) * (Y) + ((y) & ((Y)-1))

__constant float value_min = -1.25;
__constant float value_max =  1.25;
__constant float value_range =  2.5; // value_max-value_min

#define NORMALIZE_COLOR(v) (((v)-value_min) / value_range)

bool inside(float x, float y, float mx, float my) {
    float dx = mx-x;
    float dy = my-y;
    return (dx*dx + dy * dy) < (0.05 * 0.05);
}

__kernel void image_setup(
__global float* a_in,
__global float* a_out,
__global float* b_in,
__global float* b_out,
__global float* c_in,
__global float* c_out,
__global float* d_in,
__global float* d_out,
__global float* e_in,
__global float* e_out,
__global float* f_in,
__global float* f_out,
__global float* g_in,
__global float* g_out,
__global float* h_in,
__global float* h_out,
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7,
__write_only image2d_t image2
) 
{

    const int x = get_global_id(0);
    const int y = get_global_id(1);
    const int X = get_global_size(0);
    const int Y = get_global_size(1);
    const int ixy = XY(x, y);
    
    if(        inside((float)x/(float)X, (float)y/(float)Y, 0.50, 0.50)
            || inside((float)x/(float)X, (float)y/(float)Y, 0.30, 0.70)
            || inside((float)x/(float)X, (float)y/(float)Y, 0.50, 0.82)
            || inside((float)x/(float)X, (float)y/(float)Y, 0.50, 0.20)
            || inside((float)x/(float)X, (float)y/(float)Y, 0.20, 0.20)
            || inside((float)x/(float)X, (float)y/(float)Y, 0.80, 0.80)
            || inside((float)x/(float)X, (float)y/(float)Y, 0.82, 0.30))
        a_out[ixy] = 1.3;
    else
        a_out[ixy] = -0.82;
    if(        inside((float)x/(float)X, (float)y/(float)Y, 0.47, 0.50)
            || inside((float)x/(float)X, (float)y/(float)Y, 0.33, 0.70)
            || inside((float)x/(float)X, (float)y/(float)Y, 0.53, 0.80)
            || inside((float)x/(float)X, (float)y/(float)Y, 0.50, 0.23)
            || inside((float)x/(float)X, (float)y/(float)Y, 0.20, 0.23)
            || inside((float)x/(float)X, (float)y/(float)Y, 0.80, 0.83)
            || inside((float)x/(float)X, (float)y/(float)Y, 0.80, 0.36))
        b_out[ixy] = 1.3;
    else
        b_out[ixy] = -0.87;
    c_out[ixy] = -0.843297;
    
    
    float cr = NORMALIZE_COLOR(a_out[ixy]);
    float cg = NORMALIZE_COLOR(b_out[ixy]);
    float cb = NORMALIZE_COLOR(c_out[ixy]);
    const int2 pixelcoord = (int2)(x, y);
    write_imagef(image2, pixelcoord, (float4)(cr, cg, cb, 1));
}

#include <media/cl/mask64x64.h>

__kernel void image_change(
__global float* a_in,
__global float* a_out,
__global float* b_in,
__global float* b_out,
__global float* c_in,
__global float* c_out,
__global float* d_in,
__global float* d_out,
__global float* e_in,
__global float* e_out,
__global float* f_in,
__global float* f_out,
__global float* g_in,
__global float* g_out,
__global float* h_in,
__global float* h_out,
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7,
__write_only image2d_t image2
) 
{
    
    const int x = get_global_id(0);
    const int y = get_global_id(1);
    const int X = get_global_size(0);
    const int Y = get_global_size(1);
    const int ixy = XY(x, y);
    const int i_n  = XY(x  , y+1);
    const int i_ne = XY(x+1, y+1);
    const int i_e  = XY(x+1, y  );
    const int i_se = XY(x+1, y-1);
    const int i_s  = XY(x  , y-1);
    const int i_sw = XY(x-1, y-1);
    const int i_w  = XY(x-1, y  );
    const int i_nw = XY(x-1, y+1);

    const float a = a_in[ixy];
    const float b = b_in[ixy];
    const float c = c_in[ixy];

    const float a_n  = a_in[i_n];
    const float a_ne = a_in[i_ne];
    const float a_e  = a_in[i_e];
    const float a_se = a_in[i_se];
    const float a_s  = a_in[i_s];
    const float a_sw = a_in[i_sw];
    const float a_w  = a_in[i_w];
    const float a_nw = a_in[i_nw];
    const float b_n  = b_in[i_n];
    const float b_ne = b_in[i_ne];
    const float b_e  = b_in[i_e];
    const float b_se = b_in[i_se];
    const float b_s  = b_in[i_s];
    const float b_sw = b_in[i_sw];
    const float b_w  = b_in[i_w];
    const float b_nw = b_in[i_nw];
    const float c_n  = c_in[i_n];
    const float c_ne = c_in[i_ne];
    const float c_e  = c_in[i_e];
    const float c_se = c_in[i_se];
    const float c_s  = c_in[i_s];
    const float c_sw = c_in[i_sw];
    const float c_w  = c_in[i_w];
    const float c_nw = c_in[i_nw];

    // 2D standard 9-point stencil: [ 1,4,1; 4,-20,4; 1,4,1 ] / 6
    const float _K0 = -20.0 / 6.0; // center weight
    const float _K1 =   4.0 / 6.0; // edge-neighbors
    const float _K2 =   1.0 / 6.0; // vertex-neighbors

    // compute the Laplacians of each chemical
    const float laplacian_a = a_n*_K1 + a_s*_K1 + a_w*_K1 + a_e*_K1
                            + a_ne*_K2 + a_nw*_K2 + a_se*_K2 + a_sw*_K2
                            + a*_K0;
    const float laplacian_b = b_n*_K1 + b_s*_K1 + b_w*_K1 + b_e*_K1
                            + b_ne*_K2 + b_nw*_K2 + b_se*_K2 + b_sw*_K2
                            + b*_K0;
    const float laplacian_c = c_n*_K1 + c_s*_K1 + c_w*_K1 + c_e*_K1
                            + c_ne*_K2 + c_nw*_K2 + c_se*_K2 + c_sw*_K2
                            + c*_K0;

    const float D_a = 0.00015;
    const float D_b = 0.00015;
    const float D_c = 0.0096;
    const float k3 = 8.5;
    const float lambda = 2;
    const float k1 = -6.92;
    const float theta = 1;
    const float tau = 48;
    const float dx = 0.009;
    const float timestep = 0.002;
    
    const float delta_a = D_a * laplacian_a / (dx*dx) - b - k3*c + lambda*a - a*a*a + k1 + (mask[ixy] - 0.5) * 2.5;
    const float delta_b = ( D_b * laplacian_b / (dx*dx) + a - b ) / tau;
    const float delta_c = ( D_c * laplacian_c / (dx*dx) + a - c ) / theta;

    a_out[ixy] = a + timestep * delta_a;
    b_out[ixy] = b + timestep * delta_b;
    c_out[ixy] = c + timestep * delta_c;
    
    const float cr = NORMALIZE_COLOR(a_out[ixy]);
    const float cg = NORMALIZE_COLOR(b_out[ixy]);
    const float cb = NORMALIZE_COLOR(c_out[ixy]);
    const int2 pixelcoord = (int2)(x, y);
    write_imagef(image2, pixelcoord, (float4)(cr, cg, cb, 1));
}

#define IRANGE(v) (int)(65536*(((v)-value_min) / value_range))

__kernel void image_analyze(
__global float* a_in,
__global float* b_in,
__global float* c_in,
__global int * inparams,
__global int * result
) 
{
    const int x = get_global_id(0);
    const int y = get_global_id(1);
    const int X = get_global_size(0);
    const int Y = get_global_size(1);
    const int ixy = XY(x, y);

    #define DIST 1
    if(    (x >= inparams[0]-DIST)
        && (x < inparams[0]+DIST)
        && (y >= inparams[1]-DIST)
        && (y < inparams[1]+DIST)) {
        const float a = a_in[ixy];
        const float b = b_in[ixy];
        const float c = c_in[ixy];
        atomic_add(&result[0], IRANGE(a));
        atomic_add(&result[1], IRANGE(b));
        atomic_add(&result[2], IRANGE(c));
        atom_inc(&result[3]);
    }
    if(    (x >= inparams[2]-DIST)
        && (x < inparams[2]+DIST)
        && (y >= inparams[3]-DIST)
        && (y < inparams[3]+DIST)) {
        const float a = a_in[ixy];
        const float b = b_in[ixy];
        const float c = c_in[ixy];
        atomic_add(&result[4], IRANGE(a));
        atomic_add(&result[5], IRANGE(b));
        atomic_add(&result[6], IRANGE(c));
        atom_inc(&result[7]);
    }
    if(    (x >= inparams[4]-DIST)
        && (x < inparams[4]+DIST)
        && (y >= inparams[5]-DIST)
        && (y < inparams[5]+DIST)) {
        const float a = a_in[ixy];
        const float b = b_in[ixy];
        const float c = c_in[ixy];
        atomic_add(&result[8], IRANGE(a));
        atomic_add(&result[9], IRANGE(b));
        atomic_add(&result[10], IRANGE(c));
        atom_inc(&result[11]);
    }
    if(    (x >= inparams[6]-DIST)
        && (x < inparams[6]+DIST)
        && (y >= inparams[7]-DIST)
        && (y < inparams[7]+DIST)) {
        const float a = a_in[ixy];
        const float b = b_in[ixy];
        const float c = c_in[ixy];
        atomic_add(&result[12], IRANGE(a));
        atomic_add(&result[13], IRANGE(b));
        atomic_add(&result[14], IRANGE(c));
        atom_inc(&result[15]);
    }
}

