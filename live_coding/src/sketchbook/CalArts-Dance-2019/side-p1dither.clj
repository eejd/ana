(ns lv.demo)
(use 'lv.core)

(layout "grid" [imgs0 imgs1]
               [imgs2 ras3])

(imagesequ "CalArts-Dance-2019/side-p1/*.png")
(render)
(rgb :imgs0 [c0]
  (k (% sframe slength))
  (rgb c0.rgb)
)
(rgb :imgs1 [c0]
  (k (% (int (- slength sframe)) slength))
  (rgb c0.rgb)
)
(rgb :imgs2 [c0]
  (k (% (int (+ sframe 35)) slength))
  (rgb c0.rgb)
)

(rgb :ras3 [imgs0 imgs1 imgs2]
  (rgb (even? (int i)
              imgs0.rgb
              (even? (int j)
                     imgs1.rgb
                     imgs2.rgb)))
)
(render 3)
