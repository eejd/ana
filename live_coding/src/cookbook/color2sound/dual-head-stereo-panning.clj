(ns lv.demo)
(use 'lv.core)

(layout "grid" [ras0 mov1]
               [ras2 ras3])

(movie "../../../timelapse/series/20210401-hirsch-1/hirsch-1.mkv")
(texpr
  ; use nanoKONTROL2 als input
  ; ks0    0.81102
  ; ks1    0.32283
  (t0 (- 1 (* 2 ks0)))
  (t1 (- 1 (* 2 ks1)))
)

(hsv :ras0 []
  (v (+ (pow (- 1 (abs (+ x (snoise y (* 0.011 f))))) 8)
        (pow (- 1 (abs (+ x (snoise y (* 0.023 f))))) 2)))
)

(hsv :mov1 [c0]
  (sy 1.42)
  (s 0.2)
  (h (+ c0.h 0.5))
  (v (- 1 (- c0.v r)))
)

(hsv :ras2 [ras0 ras2]
  (v (mix (max ras2.v 0) (sqr ras0.v) kp7))
)

(hsv :ras3 [mov1 ras2]
  (s (max 0 (- 0.7 r)))
  (h mov1.h 0.5)
  (v (* (sqr mov1.v) ras2.v))
)
(render 3)

(inspect [ras3]
  "color2sound/analyse_dualhead_color.cl"
  (channels 2)
  (x0 (* 1.7 (snoise (* 0.0051 f) 0.1 0.2)))
  (y0 t0)
  (r0 ks7)

  (x1 (* 1.7 (snoise (* 0.0053 f) 0.5 0.6)))
  (y1 t1)
  (r1 ks7)
)
