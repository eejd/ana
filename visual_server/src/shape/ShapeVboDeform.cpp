#include "ofMain.h"

#include "stringhelper.h"
#include "pathhelper.h"
#include "globalvars.h"
#include "Configuration.h"
#include "Filter.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispMng.h"
#include "shape/ShapeVboDeform.h"

ShapeVboDeform::ShapeVboDeform(ofGLWindowSettings settings, OscListener * oscParams) : Filter(settings, oscParams){
}


ShapeVboDeform::~ShapeVboDeform() {
	phong_shader.unload();
	glDeleteBuffers(1, &vertexbufferID);
	glDeleteBuffers(1, &normalbufferID);
	glDeleteVertexArrays(1, &vertexArrayID);
	vertices.clear();
	normals.clear();
	transformed_vertices.clear();
}

static std::string phongVert = STRINGIFY(#version 330\n
uniform mat4 modelViewMatrix;\n
uniform mat4 projectionMatrix;\n
uniform mat4 modelViewProjectionMatrix;\n
uniform mat4 normalMatrix;\n
in vec4 position;\n
in vec4 normal;\n
smooth out vec3 lightDirectionVarying;\n
smooth out vec3 normalVarying;\n
void main()\n
{\n
	vec3 lightPosition = vec3(-100.0, 100.0, 100.0);\n
    normalVarying = normalize(normalMatrix * normal).xyz;\n
	vec4 position4 = modelViewMatrix * position;\n
	vec3 position3 = position4.xyz / position4.w;\n
	lightDirectionVarying = normalize(lightPosition - position3);\n
	gl_Position = modelViewProjectionMatrix * position;\n
}\n
);

static std::string phongFrag = STRINGIFY(#version 330\n
uniform vec3 ambientColor  = vec3(0.05, 0.05, 0.10);\n
uniform vec3 diffuseColor  = vec3(0.8, 0.4, 0.1);\n
uniform vec3 specularColor = vec3(0.7, 0.7, 0.75);\n
in vec3 lightDirectionVarying;\n
in vec3 normalVarying;\n
out vec4 outputColor;\n
void main()\n
{\n
	vec3 nn = normalize(normalVarying);\n
	float diffuse = dot(nn, normalize(lightDirectionVarying));\n
	if(diffuse > 0.0) {\n
		vec3 reflectionDirection = normalize(reflect(-normalize(lightDirectionVarying), nn));\n
		float specular = dot(nn, reflectionDirection);\n
		if(specular > 0.0) {\n
			outputColor = vec4(ambientColor + diffuse * diffuseColor + pow(specular, 128.0) * specularColor, 1);\n
		} else {\n
			outputColor = vec4(ambientColor + diffuse * diffuseColor, 1);\n
		}\n
	} else {\n
		/* dark side of the object, only ambient color */\n
		outputColor = vec4(ambientColor, 1);\n
	}\n
}\n
);

void ShapeVboDeform::setup(const std::string id, int numSamples) {
	Filter::setup(id, asInt(SHAPEVBODEFORM_MAX_SAMPLES));

//	ambientColor = ofFloatColor(0.05, 0.05, 0.10);
//	diffuseColor = ofFloatColor(0.8, 0.4, 0.1);
//	specularColor = ofFloatColor(0.7, 0.7, 0.75);

	sz = sy = sx = 1.0;
}

void ShapeVboDeform::loadMesh(const string &modelName) {
    if(modelName.empty())
        return;

    std::string pmn = asString(MEDIA_PATH) + "/" + modelName;
	if(file_exists(pmn)) {
		ofMesh model;
		model.load(pmn);
		vertices.clear();
		normals.clear();
		transformed_vertices.clear();
		auto f = model.getUniqueFaces();
		auto fsize = f.size();

		auto v = model.getVertices();
		auto vsize = v.size();
		auto n = model.getNormals();
		auto nsize = n.size();
		if(vsize > 0 || nsize == vsize) {
			glm::vec3 scale = glm::vec3(sx, sy, sz);
			if(wireframe) {
				vertices.resize(6*fsize);
				normals.resize(6*fsize);
				transformed_vertices.resize(6*fsize);
				size_t ni = 0u;
				for (auto fi = 0u; fi < fsize; ++fi) {
					ofMeshFace triangle = f[fi];

					glm::vec3 va = triangle.getVertex(0) * scale;
					glm::vec3 na = glm::normalize(triangle.getNormal(0));
					glm::vec3 vb = triangle.getVertex(1) * scale;
					glm::vec3 nb = glm::normalize(triangle.getNormal(1));
					glm::vec3 vc = triangle.getVertex(2) * scale;
					glm::vec3 nc = glm::normalize(triangle.getNormal(2));

					vertices[ni] = va;
					normals[ni]  = na;
					++ni;
					vertices[ni] = vb;
					normals[ni]  = nb;
					++ni;

					vertices[ni] = vb;
					normals[ni]  = nb;
					++ni;
					vertices[ni] = vc;
					normals[ni]  = nc;
					++ni;

					vertices[ni] = vc;
					normals[ni]  = nc;
					++ni;
					vertices[ni] = va;
					normals[ni]  = na;
					++ni;
				}
			}
			else {
				vertices.resize(3*fsize);
				normals.resize(3*fsize);
				transformed_vertices.resize(3*fsize);
				size_t ni = 0u;
				for (auto fi = 0u; fi < fsize; ++fi) {
					ofMeshFace triangle = f[fi];

					auto vt = triangle.getVertex(0);
					vertices[ni].x = sx * vt.x;
					vertices[ni].y = sy * vt.y;
					vertices[ni].z = sz * vt.z;
					auto nt = triangle.getNormal(0);
					normals[ni] = glm::normalize(nt);
					++ni;

					vt = triangle.getVertex(1);
					vertices[ni].x = sx * vt.x;
					vertices[ni].y = sy * vt.y;
					vertices[ni].z = sz * vt.z;
					nt = triangle.getNormal(1);
					normals[ni] = glm::normalize(nt);
					++ni;

					vt = triangle.getVertex(2);
					vertices[ni].x = sx * vt.x;
					vertices[ni].y = sy * vt.y;
					vertices[ni].z = sz * vt.z;
					nt = triangle.getNormal(2);
					normals[ni] = glm::normalize(nt);
					++ni;
				}
			}
		}
		else {
			ofLog(OF_LOG_ERROR) << vsize << "" << nsize;
		}

		glGenVertexArrays(1, &vertexArrayID);
		glBindVertexArray(vertexArrayID);

		glGenBuffers(1, &vertexbufferID);
		glBindBuffer(GL_ARRAY_BUFFER, vertexbufferID);
		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), 0, GL_DYNAMIC_DRAW);

		glGenBuffers(1, &normalbufferID);
		glBindBuffer(GL_ARRAY_BUFFER, normalbufferID);
		glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0], GL_STATIC_DRAW);

		glBindVertexArray(0);
	}
}

void ShapeVboDeform::reload(ofxOscMessage const &msg) {
    ScalarLispMng mng;
    mng.compile(slScale, "scale", "1", msg);
    mng.compile(slWireframe, "wire", "1", msg);

    mng.compile(slAmbientColor,  "ambient",    "986895", msg);
    mng.compile(slDiffuseColor,  "diffuse",   "8355711", msg);
    mng.compile(slSpecularColor, "specular", "15790320", msg);

    mng.compile(slAmplitude, "amp", "0", msg);
    mng.compile(slLiquidness, "liqu", "1", msg);
    mng.compile(slSpeed, "speed", "0", msg);
    mng.compile(slRx, "rx", "0", msg);
    mng.compile(slRy, "ry", "0", msg);
    mng.compile(slRz, "rz", "0", msg);

    sz = sy = sx = slScale.rpn.execute0();
    wireframe = slWireframe.rpn.execute0() >= 0.0;
    std::string modelName = mng.scanForString("mesh", msg);
	loadMesh(modelName);

	phong_shader.setupShaderFromSource(GL_VERTEX_SHADER, phongVert);
	phong_shader.setupShaderFromSource(GL_FRAGMENT_SHADER, phongFrag);
	phong_shader.linkProgram();
}

ofTexture & ShapeVboDeform::getTexture() {
    return fbo.getTexture();
}

void ShapeVboDeform::update() {
	elapsedTime = (float)frame / (float)fps;
}

void ShapeVboDeform::deformMesh() {
	auto vsize = vertices.size();
	if(vsize == transformed_vertices.size()) {
        float liq = std::max(0.001f, (float)slLiquidness.rpn.execute0());
        float amplitude = slAmplitude.rpn.execute0();
        float speed = slSpeed.rpn.execute0();
		for (auto vn = 0u; vn < vsize; ++vn) {
			transformed_vertices[vn] = vertices[vn]
				+ normals[vn] *  amplitude
					* ofSignedNoise(vertices[vn].x / liq,
									vertices[vn].y / liq,
									vertices[vn].z / liq,
									elapsedTime * speed);
		}
		glBindVertexArray(vertexArrayID);
		glBindBuffer(GL_ARRAY_BUFFER, vertexbufferID);
		glBufferSubData(GL_ARRAY_BUFFER, 0, vsize * sizeof(glm::vec3), &transformed_vertices[0]);
		glBindVertexArray(0);
	}
}

void ShapeVboDeform::setColorUniform(int progID, ScalarLisp &sl, const char *uname) {
    int icolor = (int)std::round(sl.rpn.execute0());
    float red   = ((icolor >> 16) & 0xff) / 255.0f;
    float green = ((icolor >>  8) & 0xff) / 255.0f;
    float blue  = (icolor & 0xff) / 255.0f;
    GLuint colorID = glGetUniformLocation(progID, uname);
    glUniform3f(colorID, red, green, blue);
}

void ShapeVboDeform::drawMesh(ofEasyCam * cam) {
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_CULL_FACE);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	int progID = phong_shader.getProgram();
	glUseProgram(progID);

    setColorUniform(progID, slAmbientColor,  "ambientColor");
    setColorUniform(progID, slDiffuseColor,  "diffuseColor");
    setColorUniform(progID, slSpecularColor, "specularColor");

	glm::mat4 modelViewMatrix = cam->getModelViewMatrix();
	GLuint modelViewMatrixID = glGetUniformLocation(progID, "modelViewMatrix");
	glUniformMatrix4fv(modelViewMatrixID, 1, GL_FALSE, &modelViewMatrix[0][0]);

	glm::mat4 projectionMatrix = cam->getProjectionMatrix();
	GLuint projectionMatrixID = glGetUniformLocation(progID, "projectionMatrix");
	glUniformMatrix4fv(projectionMatrixID, 1, GL_FALSE, &projectionMatrix[0][0]);

	glm::mat4 modelViewProjectionMatrix = cam->getModelViewProjectionMatrix();
	GLuint modelViewProjectionMatrixID = glGetUniformLocation(progID, "modelViewProjectionMatrix");
	glUniformMatrix4fv(modelViewProjectionMatrixID, 1, GL_FALSE, &modelViewProjectionMatrix[0][0]);

	glm::mat4 normalMatrix = ofGetCurrentNormalMatrix();
	GLuint normalMatrixID = glGetUniformLocation(progID, "normalMatrix");
	glUniformMatrix4fv(normalMatrixID, 1, GL_FALSE, &normalMatrix[0][0]);

	glBindVertexArray(vertexArrayID);
	// 1rst attribute buffer : vertices
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbufferID);
	glVertexAttribPointer(
		0,                  // attribute
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
	);

	// 2rd attribute buffer : normals
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, normalbufferID);
	glVertexAttribPointer(
		1,                                // attribute
		3,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
	);

	if(wireframe)
		glDrawArrays(GL_LINES, 0, vertices.size() );
	else
		glDrawArrays(GL_TRIANGLES, 0, vertices.size() );

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glBindVertexArray(0);

	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
    glUseProgram(0);
}

void ShapeVboDeform::draw(int li) {
	if(cam && !vertices.empty()) {
        float cam_rx = slRx.rpn.execute0();
        float cam_ry = slRy.rpn.execute0();
        float cam_rz = slRz.rpn.execute0();
		fbo.begin();
		const float r = 100;
		auto gp0 = (glm::rotateX(glm::vec3(r, r, r), cam_rx));
		auto gp1 = (glm::rotateY(gp0, cam_ry));
		auto gp2 = (glm::rotateY(gp1, cam_rz));
		cam->setGlobalPosition(gp2);
		cam->lookAt(glm::vec3(0.0));
		cam->begin();
		deformMesh();
		drawMesh(cam);
		cam->end();
		fbo.end();
	}
}
