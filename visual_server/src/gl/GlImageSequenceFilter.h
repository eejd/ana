#pragma once

#include "Filter.h"
#include "OscListener.h"

class GlImageSequenceFilter : public Filter {
	public:
		GlImageSequenceFilter(ofGLWindowSettings settings, OscListener * oscParams);
		virtual ~GlImageSequenceFilter();
        virtual void reload(ofxOscMessage const &msg);
		virtual void setup(const std::string id, int numSamples);
		virtual const std::string& differentiation() const;
		virtual void draw(int li);
	    virtual void finish();
	    virtual void toMermaid(std::stringstream & info);
	private:
	    unsigned int sframe = 0;
};
