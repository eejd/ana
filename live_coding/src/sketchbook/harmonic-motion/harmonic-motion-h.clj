(ns lv.demo)
(use 'lv.core)

(layout "grid" [bill0 ffb1] [mopr2 ras3])
(render)

(expr :bill0 []
  (count 113)
  (x (mix
    (* 1000 (snoise 0 (* i 3.14159) (* 0.013 f)))
    (* 1000 (sin (/ f 200)) 0.450 (cos (* 0.3 i)))
    (+ -0.02 (usin (* 0.053 f)))
  ))
  (y (mix
    (* 1000 (snoise (* i 3.14159) (* 0.017 f) 0))
    (* 1000 (sin (/ f 200)) 0.450 (sin (* 0.3 i)))
    (+ -0.02 (usin (* 0.053 f)))
  ))
  (size 240)
)
(expr :ffb1 [bill0]
  (reduce 1)
  (paint 1)
  (clear -1)
  (step 8)
  (mode 1)
)
(expr :mopr2 [ffb1]
  "ddbb"
)
(rgb :ras3 [mopr2 bill0 ras3]
  (rgb (+ (* 0.2 mopr2.rgb)
          (* 0.3 bill0.rgb)
          (* 0.7 ras3.rgb)
            ))
)

(render 3)
