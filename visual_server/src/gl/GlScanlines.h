#pragma once

#include "ofMain.h"
#include "Filter.h"
#include "OscListener.h"
#include "calc/ScalarLisp.h"

class GlScanlines : public Filter {
	public:
		GlScanlines(ofGLWindowSettings settings, OscListener * oscParams);
		virtual ~GlScanlines();
		virtual void setup(const std::string id, int numSamples);
	    virtual void reload(ofxOscMessage const &msg);
		virtual void update();
		virtual void draw(int li);

	private:
        const size_t max_vert_color = 2048;
        const size_t per_vert_color_stride = 5;
        const size_t max_vert_color_index = per_vert_color_stride * max_vert_color;
		GLfloat* vert_color = nullptr;
		GLuint vertexArrayID = -1;
		GLuint vertexbufferID = -1;
        ofShader buildin_shader;
        int progID = -1;
        GLint posAttrib = -1;
        GLint colAttrib = -1;

        ofFloatPixels src_pixels;
        float* src_pd = nullptr;
        size_t src_pixel_width = 0;
        size_t src_pixel_height = 0;
        ofFloatPixels ctrl_pixels;
        float* ctrl_pd = nullptr;

        ScalarLisp slMode;
        int mode = 0;
        ScalarLisp slDx;
        ScalarLisp slDy;
        ScalarLisp slDrift;
        ScalarLisp slJitter;
        ScalarLisp slX;
        ScalarLisp slY;
        ScalarLisp slClear;
};
