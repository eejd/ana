#include "../ana_features.h"
#ifdef WITH_openCL

#include "ofMain.h"
#include "MSAOpenCL.h"
#include "ofApp.h"
#include "globalvars.h"
#include "ClScaledPingPong.h"

#define SQUARE_SIZE 64

ClScaledPingPong::ClScaledPingPong(ofGLWindowSettings settings, OscListener * oscParams) :
ClPingPong(settings, oscParams) {
}

ClScaledPingPong::~ClScaledPingPong() {
}

size_t ClScaledPingPong::getKernelWidth() {
    return SQUARE_SIZE;
}

size_t ClScaledPingPong::getKernelHeight() {
    return SQUARE_SIZE;
}

void ClScaledPingPong::draw(int li) {
    // make sure all OpenCL kernels have finished executing before drawing
    usleep(1000);
    openCL.finish();

    fbo.begin();
    if(fatalClProg || clProgName.empty()) {
        ofClear(0, 0, 0, 255);
    } else {
        ofPushMatrix();
        ofScale((float)filterWidth  / (float)SQUARE_SIZE,
                (float)filterHeight / (float)SQUARE_SIZE);
        clOutImage.draw(0, 0);
        ofPopMatrix();
    }
    fbo.end();
}
#endif // WITH_openCL
