(ns lv.demo)
(use 'lv.core)

(layout "grid" [mov0 img0]
               [frag1 ras2])
(image "mesh/palais-bulles-dots.png")
(movie "4movies/*.mp4")
(render)

; movie as input stream
(rgb :mov0 [c0 c1 c2 c3]
 (sy 5)
 (rgb (- 1 c3.rgb))
)

(rgb :img0 [c0]
  (rot (* 0.01 f))
 (rgb c0.rgb)
)

(expr :frag1 [mov0]
 "frag/sdf-ray-3.frag"
 (rx (* 31 (snoise (* 0.0011 f)  0.4 -0.3)))
 (ry (* 30 (snoise (* 0.0013 f)  0.2 -0.1)))
 (rz (* 39 (snoise (* 0.0017 f)  0.3 -0.2)))
)

(rgb :ras2 [ras2 frag1 img0]
  (rgb (+ (* 0.1 ks1 img0.rgb)
          (mix ras2.rgb
               frag1.rgb
               ks0)))
)
