//#pragma OPENCL EXTENSION all : enable

const sampler_t SAMPLER = CLK_NORMALIZED_COORDS_TRUE | CLK_ADDRESS_REPEAT | CLK_FILTER_LINEAR;

typedef struct {
    float mass;
    float rad;
    int group;
    float d3;
} AgentAttributes;

typedef struct {
    float4 pos;
    float4 vel;
} AgentState;


// This is a simple pseudo random number generator as used for example in shadertoys.
// It is based on scrambling the bits of the coordinates of the currently sampled point.
float rand_float(int n) {
  n = (n << 13) ^ n; 
  return (float)((n * (n*n*15731+789221) + 1376312589) & 0x7fffffff) / (float)(0x7fffffff);
}

// convert from hsv color to rgb color
float3 hsv2rgb(float3 c)
{
    float4 K = (float4)(1.0f, 2.0f / 3.0f, 1.0f / 3.0f, 3.0f);
    float3 dummy;
    float3 p = fabs(fract(c.xxx + K.xyz, &dummy) * (float3)(6.0, 6.0, 6.0) - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, (float3)(0.0, 0.0, 0.0), (float3)(1.0, 1.0, 1.0)), c.y);
}

// --- noise ----------------------------------------------------------
// see: https://thebookofshaders.com/11/
// see: https://github.com/ashima/webgl-noise/blob/master/src/noise2D.glsl
//
// Description : Array and textureless GLSL 2D simplex noise function.
//      Author : Ian McEwan, Ashima Arts.
//  Maintainer : stegu
//     Lastmod : 20110822 (ijm)
//     License : Copyright (C) 2011 Ashima Arts. All rights reserved.
//               Distributed under the MIT License. See LICENSE file.
//               https://github.com/ashima/webgl-noise
//               https://github.com/stegu/webgl-noise
//

float3 mod289_3(float3 x) {
  return (float3)(x.x - floor(x.x * (1.0 / 289.0)) * 289.0,
                  x.y - floor(x.y * (1.0 / 289.0)) * 289.0,
                  x.z - floor(x.z * (1.0 / 289.0)) * 289.0);
}

float2 mod289_2(float2 x) {
  return (float2)(x.x - floor(x.x * (1.0 / 289.0)) * 289.0,
                  x.y - floor(x.y * (1.0 / 289.0)) * 289.0);
}

float3 permute(float3 x) {
  return mod289_3((float3)(((x.x*34.0)+1.0)*x.x,
                           ((x.y*34.0)+1.0)*x.y,
                           ((x.z*34.0)+1.0)*x.z));
}

float snoise(float2 v)
{
//  float2 v = float2(_s, _t);
  const float4 C = (float4)(0.211324865405187,  // (3.0-sqrt(3.0))/6.0
                            0.366025403784439,  // 0.5*(sqrt(3.0)-1.0)
                           -0.577350269189626,  // -1.0 + 2.0 * C.x
                            0.024390243902439); // 1.0 / 41.0
// First corner
  float2 i  = floor(v + dot(v, C.yy) );
  float2 x0 = v -   i + dot(i, C.xx);

// Other corners
  float2 i1;
  i1 = (x0.x > x0.y) ? (float2)(1.0, 0.0) : (float2)(0.0, 1.0);
  float4 x12 = x0.xyxy + C.xxzz;
  x12.xy -= i1;

// Permutations
  i = mod289_2(i); // Avoid truncation effects in permutation
  float3 p = permute( permute( i.y + (float3)(0.0, i1.y, 1.0 ))
		+ i.x + (float3)(0.0, i1.x, 1.0 ));

  float3 m = max((float3)(0.5) - (float3)(dot(x0,x0), dot(x12.xy,x12.xy), dot(x12.zw,x12.zw)), (float3)(0.0));
  m = m*m ;
  m = m*m ;

// Gradients: 41 points uniformly over a line, mapped onto a diamond.
// The ring size 17*17 = 289 is close to a multiple of 41 (41*7 = 287)

  float3 dummy;
  float3 x = (float3)(2.0) * fract(p * C.www, &dummy) - (float3)(1.0);
  float3 h = fabs(x) - (float3)(0.5);
  float3 ox = floor(x + (float3)(0.5));
  float3 a0 = x - ox;

// Normalise gradients implicitly by scaling m
// Approximation of: m *= inversesqrt( a0*a0 + h*h );
  m *= (float3)(1.79284291400159) - (float3)(0.85373472095314) * ( a0*a0 + h*h );

// Compute final noise value at P
  float3 g;
  g.x  = a0.x  * x0.x  + h.x  * x0.y;
  g.yz = a0.yz * x12.xz + h.yz * x12.yw;
  return 130.0 * dot(m, g);
}

float turb(float2 v, float octaves) {
  float t = 0;
  float f = 1;
  int on = (int)(fabs(octaves));
  for (int io = 0; io < on; io++) {
      float value = snoise(v);
      t += value / f;
      f *= 2;
  }
  return t;
}

__kernel void image_setup() {}
__kernel void image_change() {}

#define DAMPING         0.99f
#define FOCUS_FORCE     1.0f
#define CENTER_FORCE    2.0f
#define AVOID_FORCE     25.0f
#define VISC            6.0f
#define DRAG            0.6f
#define MAX_VELOCITY    0.1f
    
// ---------------------------------------------------------
__kernel void ag_setup(
const int img_x_max,
const int img_y_max,
const int agent_length,
const int trace_length,
const int trace_index,
__global AgentState* astate_in,
__global AgentState* astate_out,
__global AgentAttributes* aattrib,
__global float16* traceBuffer, 
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7
) 
{
    const int ai = get_global_id(0);

    __global AgentState *s_out = &astate_out[ai];
    s_out->pos = (float4)(10*(rand_float(ai+17)-0.5),
                          10*(rand_float(ai+31)-0.5),
                          10*(rand_float(ai+53)-0.5),
                          1);
    int limit = 10;
    while(limit > 0 && length(s_out->pos.xyz) > 5) {
        s_out->pos = (float4)(10*(rand_float(ai+limit+17)-0.5),
                              10*(rand_float(ai+limit+31)-0.5),
                              10*(rand_float(ai+limit+53)-0.5),
                              1);
        --limit;
    }
    
    s_out->vel = (float4)(rand_float(ai+23) - 0.5,
                          rand_float(ai+13) - 0.5,
                          rand_float(ai+29) - 0.5,
                          0);
    s_out->vel *= MAX_VELOCITY;
    
    __global AgentAttributes *at = &aattrib[ai];
    at->mass = 0.1+10*rand_float(ai+71);
    at->rad = 0.5;
    at->group = ai % 10;
    
    int ti = ai*trace_length;
    for(int pi = trace_length-1; pi >= 0; --pi) {
        traceBuffer[ti+pi].xyzw  = s_out->pos;
        traceBuffer[ti+pi].s4567 = (float4)(1, 1, 0, 0); // normal
        traceBuffer[ti+pi].s89ab = (float4)(1, 0.5, 0, 1); // color
    }
}

// ---------------------------------------------------------
__kernel void ag_move(
const int img_x_max,
const int img_y_max,
const int agent_length,
const int trace_length,
const int trace_index,
__global AgentState* astate_in,
__global AgentState* astate_out,
__global AgentAttributes* aattrib,
__global float16* traceBuffer, 
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7
)
{
    const int ai = get_global_id(0);
    __global AgentAttributes *at = &aattrib[ai];
    __global AgentState *s_in  = &astate_in[ai];
    __global AgentState *s_out = &astate_out[ai];
    
    float3 focusL = (float3)(5*p0, 5*p1, 5);
    float3 focusR = (float3)(5*p2, 5*p3, -5);

    float3 f = (float3)(0);
    
    float3 focus = (float3)(0);

    
    const float3 diff = focus - s_in->pos.xyz;
    const float invDistSQL = 1.0f / dot(diff, diff);
    f += diff * (FOCUS_FORCE * invDistSQL * p7);

    float factor = p6 * max(0.0,  (4.0 - length(s_in->pos.xyz)));
//     factor = p6;
//     f += (float3)(factor*turb(s_in->pos.yz*p5, 1+p4*7),
//                   factor*turb(s_in->pos.xz*p5, 1+p4*7),
//                   factor*turb(s_in->pos.xy*p5, 1+p4*7));
//     f += (float3)(factor*snoise(s_in->pos.yz*p5),
//                   factor*snoise(s_in->pos.xz*p5),
//                   factor*snoise(s_in->pos.xy*p5));
//     f += (float3)(0.1)*((float3)(rand_float((int)(1001.0*s_in->pos.y)),
//                                  rand_float((int)(1002.0*s_in->pos.z)),
//                                  rand_float((int)(1003.0*s_in->pos.x)))
//                         - (float3)(0.5));
    f -= (float3)(factor)*((float3)(rand_float((int)(1000.0*s_in->pos.x*p5)),
                                  rand_float((int)(1000.0*s_in->pos.y)),
                                  rand_float((int)(1000.0*s_in->pos.z)))
                     -(float3)(0.5));
    
    const float dt = 1.0 / 25.0;
    float3 a = f / at->mass;

    float3 vvel = a * dt;
    float3 nvel = normalize(vvel);
    float  vel = min(MAX_VELOCITY, length(vvel));
    s_out->vel.xyz = s_in->vel.xyz + nvel * vel * DAMPING;
    
    s_out->pos.xyz = s_in->pos.xyz + s_out->vel.xyz * dt;
//     s_out->pos.xyz = (float3)(factor*snoise(s_in->pos.yz*p5),
//                   factor*snoise(s_in->pos.xz*p5),
//                   factor*snoise(s_in->pos.xy*p5));
//     s_out->pos.xyz = s_in->pos.xyz
//                      +(float3)(0.1)*((float3)(rand_float((int)(1000.0*s_in->pos.y)),
//                               rand_float((int)(1000.0*s_in->pos.z)),
//                               rand_float((int)(1000.0*s_in->pos.x)))
//                      -(float3)(0.5));

    int ti = ai*trace_length;
    for(int pi= trace_length - 2; pi >= 0; --pi) {
        traceBuffer[ti+pi+1] = traceBuffer[ti+pi];
    }
    traceBuffer[ti].xyzw  = (float4)(s_out->pos.xyz, 1);
    
//     traceBuffer[ti].s456 =normalize(cross(s_out->vel.xyz, (float3)(0, 0, 1)));
    traceBuffer[ti].s456 =normalize(cross(s_out->vel.xyz, (float3)(1, 0, 1)));
//      traceBuffer[ti].s456 =normalize(cross(traceBuffer[ti].xyz, traceBuffer[ti+1].xyz));
    traceBuffer[ti].s7 = 1;
    
    traceBuffer[ti].s89ab = (float4)(hsv2rgb((float3)(0.04*at->group-0.01, 0.8, 0.15+250*vel*vel)), 1);
 }
