#include "ofMain.h"

#include "globalvars.h"
#include "calc/ScalarLispMng.h"
#include "shape/ShapeNumber.h"

ShapeNumber::ShapeNumber(ofGLWindowSettings settings, OscListener * oscParams) : Filter(settings, oscParams){
}

ShapeNumber::~ShapeNumber(){
}

void ShapeNumber::setup(const std::string id, int numSamples){
    Filter::setup(id, numSamples);
}


void ShapeNumber::reload(ofxOscMessage const &msg) {
    auto narg = msg.getNumArgs();
    if(narg >= 2) {
        format = msg.getArgAsString(2);
    }

    ScalarLispMng mng;
    mng.compile(slSize,  "size", "80", msg);
    mng.compile(slClear, "clear", "0", msg);
    mng.compile(slValue, "value", "f", msg);

    int font_size = slSize.rpn.execute0();
    bool loaded = digit_font.load("NotoSansMono-CondensedSemiBold.ttf", font_size, true, true);
    if(!loaded) {
        loaded = digit_font.load("verdana.ttf", font_size, true, true);
    }
    if(!loaded)
        ofLog(OF_LOG_ERROR) << "can't load font '" << "verdana.ttf" << "'";
    else
        bb = digit_font.getStringBoundingBox("M", 0, 0);
}

void ShapeNumber::draw(int li){
	fbo.begin();
    float clear = slClear.rpn.execute0();
    if(clear >= 0.0) {
        glClearColor(clear, clear, clear, 1);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    double value = slValue.rpn.execute0();
    char buffer[32];
    snprintf(buffer, 31, format.c_str(), value);
//    std::string number = to_string(value);
    size_t num_chars = strlen(buffer);

    ofSetColor(ofColor::white);
    ofPushMatrix();
    ofTranslate(0.5*filterWidth, 0.5*filterHeight);
//    ofRectangle bb = digit_font.getStringBoundingBox(number, 0, 0);
//    ofRectangle bb = digit_font.getStringBoundingBox("M", 0, 0);
//    digit_font.drawString(number, -0.5*bb.width, -0.5*bb.height);
    digit_font.drawString(buffer, -0.5*num_chars*bb.width, 0.5*bb.height);
    ofPopMatrix();

	fbo.end();
}
