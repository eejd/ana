#include <FvPl.h>
#include "ofMain.h"
#include "ofxOsc.h"

#include "Configuration.h"
#include "globalvars.h"
#include "OscListener.h"
#include "Filter.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispMng.h"
#include "fv/FvHit.h"

FvHit::FvHit(ofGLWindowSettings settings, OscListener * oscParams) :
        Filter(settings, oscParams) {
//    hit_state_size = 1024;
//    hit_state = new int[hit_state_size];
//    memset(hit_state, 0, hit_state_size*sizeof(int));
}

FvHit::~FvHit() {
    if(hit_state) {
        delete [] hit_state;
        hit_state = nullptr;
    }
}

void FvHit::setup(const std::string id, int numSamples) {
    Filter::setup(id, numSamples);
}

void FvHit::reload(ofxOscMessage const &msg) {
    auto narg = msg.getNumArgs();
    if(narg > 1) {
        Filter::setInputTexture(msg.getArgAsString(1));
    }
    ScalarLispMng mng;
    mng.compile(slMode, "mode", "0", msg);
    mng.compile(slThreshold, "thres", "0.5", msg);
    mng.compile(slClear, "clear", "1", msg);
    mng.compile(slUpstream, "paint", "1", msg);
}

void FvHit::update() {
    draw_mode = slMode.rpn.execute0();

    upf = nullptr;
    maskf = nullptr;
    if(upstream_ids.size() >= 1) {
        auto in_id = upstream_ids[0];
        if(filterDict.find(in_id) != filterDict.end()) {
            auto f = filterDict[in_id];
            if(f) {
                upf = f;
            }
        }
        if(upstream_ids.size() >= 2) {
            auto mask_id = upstream_ids[1];
            if(filterDict.find(mask_id) != filterDict.end()) {
                auto f = filterDict[mask_id];
                if(f) {
                    maskf = f;
                }
            }
        }
    }

    if(upf && hit_state_size != upf->vertices_size) {
        if(hit_state)
            delete [] hit_state;
        hit_state_size = upf->vertices_size;
        hit_state = new int[hit_state_size];
        memset(hit_state, 0, hit_state_size*sizeof(int));
    }
}

void FvHit::talk(std::string path, size_t i, float fx, float fy, float fz) {
    ofxOscMessage s;
    s.setAddress(path);
    s.addIntArg(i);
    s.addFloatArg(fx);
    s.addFloatArg(fy);
    s.addFloatArg(fz);
    apptarget->talkToSClang.sendMessage(s, false);
}

inline size_t clampfu(float v, float hi) {
    if(v < 0.0f) return 0u;
    if(v > hi) return (size_t)hi;
    return (size_t)v;
}

void FvHit::draw(int li) {
    if(hit_state == nullptr)
        return;

    fbo.begin();
    if(slClear.rpn.execute0() >= 0.0)
        ofClear(0, 0, 0, 255);

    if(upf) {
        int paint = std::round(slUpstream.rpn.execute0());
        switch(paint) {
        case -1:
            break;
        case 2:
            maskf->getTexture().draw(0, 0);
            break;
        default:
            upf->getTexture().draw(0, 0);
            break;
        }

        if(upf->vertices_size > 0) {
            ofPushStyle();
            ofPushMatrix();
            ofTranslate(filterWidth / 2, filterHeight / 2, 0);

            switch(draw_mode) {
            default: {
                if(maskf) {
                    maskf->getTexture().readToPixels(grabbedPixels);
                    for(size_t i = 0; i < upf->vertices_size; ++i) {
                        float fx = upf->vertices[i*vertices_patch];
                        float fy = upf->vertices[i*vertices_patch+1];
                        float fz = upf->vertices[i*vertices_patch+2];
                        ofFloatColor rgb = grabbedPixels.getColor(clampfu(fx + filterWidth  / 2.0f, filterWidth-1),
                                                   clampfu(fy + filterHeight / 2.0f, filterHeight-1));
                        float gray = (rgb.r + rgb.g + rgb.b) / 3.0f;
//                        ofLog(OF_LOG_NOTICE) << "   " << i
//                                << " " << fx
//                                << "/" << fy
//                                << "   " << clampfu(fx + filterWidth  / 2.0f, filterWidth)
//                                << "/" << clampfu(fy + filterHeight / 2.0f, filterHeight)
//                                << " " << rgb.r
//                                << ":" << rgb.g
//                                << ":" << rgb.b
//                                << "   " << gray
//                                ;
                        if(gray > slThreshold.rpn.execute3d1i(fx, fy, fz, i)) {
                            if(hit_state[i] == 0) {
                                hit_state[i] = 1;
//                                ofLog(OF_LOG_NOTICE) << "enter " << i;
                                talk("/ana/hit/enter", i, fx, fy, fz);
                            }
                            ofSetColor(255, 0, 0);
                            ofFill();
                            ofDrawCircle(fx, fy, fz, 10);
                        }
                        else {
                            if(hit_state[i] == 1) {
                                hit_state[i] = 0;
//                                ofLog(OF_LOG_NOTICE) << "leave " << i;
                                talk("/ana/hit/leave", i, fx, fy, fz);
                            }
                            ofSetColor(127);
                            ofNoFill();
                            ofDrawCircle(fx, fy, fz, 10);
                        }
                    }
                }
            }
            }
            ofPopMatrix();
            ofPopStyle();
        }
    }

    fbo.end();
}
