#pragma once

#include "ofMain.h"
#include "Filter.h"
#include "calc/ScalarLisp.h"

struct Hpix {
    Hpix(float r0, float g0, float b0, float c0) {
        comp = c0;
        r = r0;
        g = g0;
        b = b0;
    };
    float comp;
    float r, g, b;
};

class ImgPixelSort: public Filter {
public:
    ImgPixelSort(ofGLWindowSettings settings, OscListener *oscParams);
    virtual ~ImgPixelSort();
    virtual void setup(const std::string id, int numSamples);
    virtual void reload(ofxOscMessage const &msg);
    virtual void update();
    virtual void draw(int li);

    ScalarLisp slCompare;
    ScalarLisp slThresholdBand;
    ScalarLisp slThresholdWidth;

private:
//    float threshold_min   = 0.5;
//    float threshold_width = 0.1;
    ofFloatPixels pixels;
    std::vector<Hpix> hpix;

    void horizontal(const float threshold_min, const float threshold_max, float *pd);
    void veritcal(const float threshold_min, const float threshold_max, float *pd);
};
