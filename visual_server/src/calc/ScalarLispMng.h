#pragma once

#include <string>

#include "ofxOsc.h"
#include "calc/ScalarLisp.h"

class ScalarLispMng {
public:
    ScalarLispMng();
    ~ScalarLispMng();
    int compile(ScalarLisp &sl, std::string key, std::string fallback, ofxOscMessage const &msg, size_t first = 2);
    std::string scanForString(std::string key, ofxOscMessage const &msg, size_t first = 2);
};
