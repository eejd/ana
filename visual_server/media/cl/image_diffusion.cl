#define XY(x,y) ((x) & ((X)-1)) * (Y) + ((y) & ((Y)-1))

__kernel void image_setup(
__read_only image2d_t image_in,
__read_only image2d_t image_src,
__write_only image2d_t image_dst,
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7
) 
{
    const int x = get_global_id(0);
    const int y = get_global_id(1);
    const int X = get_global_size(0);
    const int Y = get_global_size(1);
    const int ixy = XY(x, y);
}

__kernel void image_change(
__read_only image2d_t image_in,
__read_only image2d_t image_src,
__write_only image2d_t image_dst,
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7
)
{

    const int x = get_global_id(0);
    const int y = get_global_id(1);
    const int X = get_global_size(0);
    const int Y = get_global_size(1);
    const int ixy = XY(x, y);
    const int i_n  = XY(x  , y+1);
    const int i_ne = XY(x+1, y+1);
    const int i_e  = XY(x+1, y  );
    const int i_se = XY(x+1, y-1);
    const int i_s  = XY(x  , y-1);
    const int i_sw = XY(x-1, y-1);
    const int i_w  = XY(x-1, y  );
    const int i_nw = XY(x-1, y+1);

	sampler_t smp = CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_NEAREST;
    const int2 drift1   = (int2)(x+p1, y+p2);
    const int2 center   = (int2)(x, y);
    const int2 drift2   = (int2)(x+p3, y+p4);
	float4 color_in = read_imagef(image_in,  smp, center);
	float4 color_bc = read_imagef(image_src, smp, center);
	float4 color_d1 = read_imagef(image_src, smp, drift1);
	float4 color_d2 = read_imagef(image_src, smp, drift2);

    float a = p0;
    float b = 1.0 - a;
    float c = b / 2.975;
    write_imagef(image_dst, center, (float4)(
    a*color_in.x + c*color_d1.x + c*color_bc.x + c*color_d2.x,
    a*color_in.y + c*color_d1.y + c*color_bc.y + c*color_d2.y,
    a*color_in.z + c*color_d1.z + c*color_bc.z + c*color_d2.z,
    1.0
    ));
}

