#pragma once
#include "Filter.h"
#include "Layout.h"

class VerticalLayout: public Layout {
public:
    VerticalLayout(int c);
    virtual ~VerticalLayout();
    virtual int numFilters();
    virtual void draw(int render, std::vector<std::shared_ptr<Filter>> &filterLine);

private:
    int cols = 3;
};
