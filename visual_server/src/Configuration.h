#pragma once

#include <string>
#include <unordered_map>

#define FULLSCREEN	    "fullscreen"
#define FPS	            "fps"
#define VERTICAL_SYNC   "vertical_sync"
#define GL_MALOR	    "gl_major"
#define GL_MINOR	    "gl_minor"
#define LAYOUT  	    "layout"
#define CANVAS_WIDTH    "canvas_width"
#define CANVAS_HEIGHT   "canvas_height"
//#define FILTER_ROWS     "filter_rows"
//#define FILTER_COLS     "filter_cols"
#define FILTER_WIDTH    "filter_width"
#define FILTER_HEIGHT   "filter_height"
#define VIDEO_WIDTH     "video_width"
#define VIDEO_HEIGHT    "video_height"
#define MEDIA_PATH      "media_path"
#define PRESHOT	        "preshot"
#define PRESHOT_PATH    "preshot_path"
#define CSV_EXPORT_PATH "csv_export_path"
#define INFOFONT        "infofont"
#define INFOFONT_SIZE   "infofont_size"
#define FONT            "font"
#define FONT_SIZE       "font_size"
#define FONT_DPI        "font_dpi"
#define CAMERA          "camera"
#define HIDE_CURSOR     "hide_cursor"
#define SC_LANG_HOST         "sc_lang_host"
#define SC_LANG_PORT         "sc_lang_port"
#define ANALOG_PORT          "analog_port"
#define SATELLITE_SENSEHAT_HOST  "satellite_senshat_host"
#define SATELLITE_SENSEHAT_PORT  "satellite_senshat_port"
#define HARDWARE_CONCURRENCY "hardware_concurrency"
#define SOUND_LOOP      "sound_loop"
#define FFT_SMOOTH      "fft_smooth"

#define JOYSTICK_DEV_INPUT    "joystick_dev_input"

#define SHAPEVBODEFORM_MAX_SAMPLES "ShapeVboDeform_max_samples"
#define GLVIDEOFILTER_VERBOSE_GRABBER "GlVideoFilter_verbose_grabber"
#define GLVIDEOFILTER_ROTATE "GlVideoFilter_rotate"

#define DUMP_OSC_INPUT   "dump_osc_input"
#define DUMP_FRAG_SRC    "dump_frag_src"
#define SEND_VITAL_SIGNS "send_vital_signs"
#define VITAL_SIGNS_HOST "send_vital_host"
#define VITAL_SIGNS_PORT "send_vital_port"

void load_configuration(char *conf);
bool asBool(const std::string key);
int asInt(const std::string key);
float asFloat(const std::string key);
std::string asString(const std::string key);
