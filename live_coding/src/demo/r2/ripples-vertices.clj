(ns lv.demo)
(use 'lv.core)

(layout "grid" [clfv0 ras1])

(expr :clfv0 []
  "cl/ripples-vertices.cl"
  (lines 20)
  (rows 500)
  (mode 1) ; 0 = hsv, 1 = rgb
  (p0 (* ks0 1))    ; xspace
  (p1 (* ks1 10))   ; yspace
  (p2 (* ks2 0.1))  ; xnoise
  (p3 (* ks3 0.1))  ; ynoise
  (p4 (* ks4 2))    ; xscatter
  (p5 (* ks5 2))    ; yscatter
  (p6 (+ ks6 0.01)) ; density
  (p7 (* ks7 4))    ; amplitude
)

(rgb :ras1 [clfv0 ras1]
  (rgb (mix clfv0.rgb ras1.rgb 0.95))
)

(render 1)
