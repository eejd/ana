# Organizing media and programs

## All files on a single computer
Live coding, visualization and debugging the C++ code with only a single computer.

    ana
    ├── bin                        helper bash scripts for video processing
    ├── live_coding                live coding client
    │   └── src
    │       ├── lv                 lv name-space. The required core.clj file.
    │       └── demo
    │           ├── 4movies        DSL code for the movie example
    │           ├── flower-sampler DSL code for the flower-sampler animation
    │           ├── pattern        DSL code for the pattern animation
    │           └── video          DSL code for the WEB cam example
    ├── visual_server              visualization sever
    │    ├── bin                   configuration files and binaries
    │    ├── media                 media files used by the server
    │    │   ├── 4movies           symbolic links to movie files elsewhere
    │    │   ├── flower-sampler    4 PNGs used in the flower-sampler animation
    │    │   └── pattern           PNG files used in the pattern animation
    │    └── src                   C++ source code for aNa
    └── satellite                  Python code for aNa satellites
        └── rpi                    Raspberry Pi specific source code


    ana_exp                         exported image sequences
    ├── exp_2020-11-29-16-01-51-272 recorded in 11/29/2020
    └── exp_2020-12-01-20-00-09-289

    /ssd/bin/of_v0.11.0_linux64gcc6_release/
    ├── addons                     somewhere on your machine openFrameworks
        ....                       set OF_ROOT to this directory
        ├── ofxJoystick
        ....
    ....

## Using two or more computers
Using different machines to separate live coding environment from the server.

### Server side
The aNa visualization server needs run-time libraries. Mostly the same as openFrameworks.
Best practice to install: First build openFrameworks on the server machine and check the oF examples. Then compile the visualization server on this server machine. [see](./build_visualization_server.md)

C++ / openFrameworks binaries are compiled against the current version of your operating systems libraries. openFrameworks applications are portable as source code. But moving binaries between different computers can directly lead to hell.

    ana
    ├── bin                        (optional) helper bash scripts
    └── visual_server              visualization sever
        ├── bin                    configuration files and binaries
        └── media                  media files used by the server
            ├── 4movies            symbolic links to movie files elsewhere
            ├── flower-sampler     4 PNGs used in the flower-sampler animation
            └── pattern            PNG files used in the pattern animation

    ana_exp                         exported image sequences
    ├── exp_2020-11-29-16-01-51-272 recorded in 11/29/2020
    └── exp_2020-12-01-20-00-09-289

### Client side
You can use one or more Laptops for live editing.

    ana
    └── live_coding                live coding client
        └── src
            ├── lv                 lv name-space. A required core.clj file.
            └── demo
                ├── 4movies        DSL code for the movie example
                ├── flower-sampler DSL code for the flower-sampler animation
                ├── pattern        DSL code for the pattern animation
                └── video          DSL code for the WEB cam example

## Satellite
Satellites are optional. If you want to use your Raspberry Pi in conjunction with aNa copy the Python source code to your `pi` home directory.

    /home/pi
    └── satellite                   
        └── rpi                    Raspberry Pi specific source code
            └── senseHatLed.py     control the 8x8 LED matrix
