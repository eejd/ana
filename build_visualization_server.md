# How to build the visualization server
## Step 1: Build openFrameworks from source.
openFrameworks is used for creative coding. They publish source code, not precompiled binaries.

- Download openFrameworks for linux gcc6 or later. `of_v0.11.0_linux64gcc6_release.tar.gz`
https://openframeworks.cc/download/

- Follow the [Linux install](https://openframeworks.cc/setup/linux-install/) tutorial to build openFrameworks core functions.

- Test some of openFrameworks OpenGL shader based examples. see: [06_multiTexture](https://github.com/openframeworks/openFrameworks/tree/patch-release/examples/shader/06_multiTexture)

## Step 2: Clone this Git repository.
```bash
git clone https://gitlab.com/metagrowing/ana.git
```
## Step 3: Download required ofx addons for aNa
| addon | link | description |
| --- | --- | --- |
| ofxCv | https://github.com/kylemcdonald/ofxCv.git | ofxCv represents an alternative approach to wrapping OpenCV for openFrameworks. |
| ofxIpVideoGrabber | https://github.com/bakercp/ofxIpVideoGrabber | ofxIpVideoGrabber is an Open Frameworks addon used to capture video streams from IP Cameras that use the mjpeg streaming protocol. |
| ofxMidi | https://github.com/danomatika/ofxMidi | ofxMidi is a MIDI input and output addon for openFrameworks |

cd to your addons folder und clone these repositories:
```bash
cd ~/of_v0.11.0_linux64gcc6_release/addons/
git clone https://github.com/kylemcdonald/ofxCv.git
git clone https://github.com/bakercp/ofxIpVideoGrabber
git clone https://github.com/danomatika/ofxMidi
```

## Step 4: Download optional ofx addons for aNa
> :information_source: Step 4.2 is in a reduced version not required.

### OpenCL
OpenCL support is not required for the base system. Following the instructions in Step 4.1 avoids trouble with the OpenCL installation.

#### Step 4.1: I will build without the filters based on OpenCL
You can disable OpenCL:
- To disable OpenCL support edit `visual_server/src/ana_features.h`. Switch from `#define WITH_openCL` to `#undef WITH_openCL`.
- And remove the line containing `ofxMSAOpenCL` from the `addons.make` file.

```c
// to disable OpenCL remove ofxMSAOpenCL from addons.make too
//#define WITH_openCL
#undef WITH_openCL
```

#### Step 4.2: I need filters based on OpenCL

##### Step 4.2.1: cd to your addons folder und clone these repositories:

| addon | link | description |
| --- | --- | --- |
| ofxMSAOpenCL | https://github.com/memo/ofxMSAOpenCL |  wrapper for OpenCL |
| ofxMSAPingPong | https://github.com/memo/ofxMSAPingPong | needed by ofxMSAOpenCL |

```bash
cd ~/of_v0.11.0_linux64gcc6_release/addons/
git clone https://github.com/memo/ofxMSAOpenCL
git clone https://github.com/memo/ofxMSAPingPong
```

##### Step 4.2.2: Install OpenCL:
As root user install OpenCL support suitable for you operating system and your graphics card.

> :warning: This is only tested with the NVIDIA proprietary driver. Package nvidia-opencl-icd provides the NVIDIA installable client driver (ICD) for OpenCL.

```bash
apt install ocl-icd-opencl-dev
apt install nvidia-opencl-icd
```

##### Trouble shooting OpenCL
| error | when | description |
| --- | --- | --- |
| make[1]: *** No rule to make target of_v0.11.0_linux64gcc6_release/addons/obj/linux64/Release/ofxMSAOpenCL/./src/ | build | addons.make missed ofxMSAOpenCL line |
| fatal error: CL/opencl.h: No such file or directory #include <CL/opencl.h> | build | OpenCL headers are missing.<br>Install ocl-icd-opencl-dev package. |
| /usr/bin/ld.gold: error: cannot find -lOpenCL | build | OpenCL libraries are missing<br>Install ocl-icd-opencl-dev package. |
| [ error ] Error finding clDevices. | runtime | aNa needs OpenCL support on the GPU because OpenGL buffers and OpenCL memory must be shared on the GPU. |

## Step 5: Build the visualization server from source.

> :information_source: First openFrameworks must be compiled successfully. See step 1 above. `of_v0.11.0_linux64gcc6_release/scripts/linux/compileOF.sh`

### Enabling / disabling features
[visual_server/src/ana_features.h](./visual_server/src/ana_features.h) is a header file containing defines to enable or disable some dependencies. Use this to `#undef` features if you encounter compiler or linker errors from rarely used components.

### Using make
Two points must be adjusted.
1. Set `OF_ROOT` to the folder of your openFrameworks installation.
1. Configure how many parallel jobs make can use to speed up the compilation. On a 16 core CPU using `make -j 16` is ok.

> :warning: *Change these commands suitable for your system. Too much parallel build processes spawned by make can straight lead to hell.*

1. On a terminal `cd` to `ana/visual_server`
1. `export OF_ROOT=/_YOUR_OF_/of_v0.11.0_linux64gcc6_release/`
1. Now you can start the build script in this folder: `./clean_build_release`

```bash
strom@ana:~/ana/visual_server$ cd ana/visual_server/
strom@ana:~/ana/visual_server$ export OF_ROOT=~/of_v0.11.0_linux64gcc6_release/
strom@ana:~/ana/visual_server$ ./clean_build_release
```
#### Build script `clean_build_release`
```bash
#!/bin/bash
echo \#define VERSION_BUILD_DATE \"$(date +%Y%m%d_%H%M)\" > src/ana_build_date.h
make clean
if test -f "/usr/bin/nproc"; then
    echo number of available processors $(/usr/bin/nproc)
    make -j $(/usr/bin/nproc)
else
    make -j 4
fi
echo "---------------------------------------"
echo "    to launch the visual_server use:   "
echo "    ./run_release                      "
echo "---------------------------------------"
```

> :information_source: Remove the `make clean` line temporarily in case you need several attempts to compile this application.

## Using Eclipse for debugging
TODO
