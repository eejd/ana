#pragma OPENCL EXTENSION cl_khr_global_int32_base_atomics : enable

const sampler_t SAMPLER = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_LINEAR;

__kernel void image_analysis(__read_only image2d_t image1,
                             __global int * inparams,
                             __global int * result)
{
    int px = get_global_id(0);
    int py = get_global_id(1);
    int dist0 = inparams[2];
    if(   (px >= inparams[0]-dist0)
       && (px <= inparams[0]+dist0)
       && (py >= inparams[1]-dist0)
       && (py <= inparams[1]+dist0)) {  
        int2 pos = (int2)(px, py);
        float4 color = read_imagef(image1, SAMPLER, pos);
        atomic_add(&result[0], 32768*fmax(0.0f, color.x));
        atomic_add(&result[1], 32768*fmax(0.0f, color.y));
        atomic_add(&result[2], 32768*fmax(0.0f, color.z));
        atom_inc(&result[3]);
    }
    int dist1 = inparams[5];
    if(   (px >= inparams[3]-dist1)
       && (px <= inparams[3]+dist1)
       && (py >= inparams[4]-dist1)
       && (py <= inparams[4]+dist1)) {  
        int2 pos = (int2)(px, py);
        float4 color = read_imagef(image1, SAMPLER, pos);
        atomic_add(&result[4], 32768*fmax(0.0f, color.x));
        atomic_add(&result[5], 32768*fmax(0.0f, color.y));
        atomic_add(&result[6], 32768*fmax(0.0f, color.z));
        atom_inc(&result[7]);
    }
} 
