(ns lv.demo)
(use 'lv.core)

(layout "grid" [bill0 ras1] [hit2 ras3])
(render)

(expr :bill0 []
  (count 38)
  (x (+ -960 (* 50 i)))
  (y (* (- (+ i -19))
        30
        (usin (* 0.01 f))
        (sin (+ (* 0.05 f) i))))
  (size 120)
)
; (render 0)

(hsv :ras1 []
  ; (rot 2)
  ; (ty -0.05)
  (ty (sin (* 0.013 f)))
  (v (sin (* 30 y)))
)

(expr :hit2 [bill0 ras1]
  (thres 0.5)
  (paint 2)
)
; (render 2)

(rgb :ras3 [bill0 ras2]
  (rgb (mix (* 3 bill0.rgb)
            ras2.rgb
            0.98))
)
; (render 3)
