(ns lv.demo)
(use 'lv.core)

(layout "grid" [img0 igen0]
               [ras1 mopr3])

(image "wave/wave*.png")

(rgb :img0 [c0 c1 c2 c3]
  (rgb (mix c0.rgb c1.rgb (usin (* 0.031 f))))
)

; calculate raw image
(expr :igen0 [img0]
  "cl/ripples-image.cl"
  (p0 (* ks0 1))    ; xspace
  (p1 (* ks1 1))    ; yspace
  (p2 (* ks2 0.1))  ; xnoise
  (p3 (* ks3 0.1))  ; ynoise
  (p4 (* ks4 0.01)) ; xscatter
  (p5 (* ks5 0.01)) ; yscatter
  (p6 (+ ks6 0.0))  ; density
  (p7 (* ks7 4))    ; amplitude
)

; "temporal blur"
(rgb :ras1 [igen0 ras1]
  (rgb (mix igen0.rgb ras1.rgb 0.95))
)

; two blur passes
(expr :mopr3 [igen0] "bb")

(render)
