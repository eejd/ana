#include "../ana_features.h"
#ifdef WITH_openCL
#include "ofMain.h"
#include "MSAOpenCL.h"
#include "globalvars.h"
#include "ofApp.h"
#include "Configuration.h"
#include "cl/ClFilter.h"

ClFilter::ClFilter(ofGLWindowSettings settings, OscListener * oscParams) :
        Filter(settings, oscParams) {
}

ClFilter::~ClFilter() {
}

bool ClFilter::reloadClProg(const bool force) {
    if(fatalClProg)
        return false;

    bool need = force;
    if(!need) {
        long sourceModTime = std::filesystem::last_write_time(pathClProg);
        need = sourceModTime != clProgModTime;
    }

    if(need) {
        clProgModTime = std::filesystem::last_write_time(pathClProg);
        openCL.clearPrograms();
        ofLog(OF_LOG_NOTICE) << "compiling " << clProgName;
        pcl = openCL.loadProgramFromFile(clProgName);
        setup_kernel = openCL.loadKernel("image_setup");
        update_kernel = openCL.loadKernel("image_change");
        std::string kn = setup_kernel->getName();
        ofLog(OF_LOG_NOTICE) << "setup  kernel: " << (kn.empty() ? "N/A" : kn);
        kn = update_kernel->getName();
        ofLog(OF_LOG_NOTICE) << "update kernel: " << (kn.empty() ? "N/A" : kn);
    }
    return need;
}

void ClFilter::reload(ofxOscMessage const &msg) {
    fatalClProg = false;
    auto numArgs = msg.getNumArgs();
    for(size_t n = 0; n < numArgs; ++n) {
//        ofLog(OF_LOG_NOTICE) << n << " " << (char) msg.getArgType(n) << " " << msg.getArgAsString(n) << "\n";
        auto fn = msg.getArgAsString(n);
        switch(n) {
        case 0:
            break;
        case 1:
            Filter::setInputTexture(msg.getArgAsString(n));
            break;
        case 2: {
            auto fn = msg.getArgAsString(n);
            if(!fn.empty()) {
                clProgName = asString(MEDIA_PATH) + "/" + fn;
                pathClProg = clProgName;
                if(!std::filesystem::exists(pathClProg)) {
                    fatalClProg = true;
                    ofLog(OF_LOG_FATAL_ERROR) << "Cannot access '" << clProgName << "'";
                }
                else {
                    reloadClProg(true);
                }
            }
            break;
        }
        }
    }
}

void ClFilter::draw(int li) {
    // make sure all OpenCL kernels have finished executing before drawing
    openCL.finish();

    fbo.begin();
    if(fatalClProg || clProgName.empty()) {
        ofClear(0, 0, 0, 255);
    } else {
        clOutImage.draw(0, 0);
    }
    fbo.end();
}
#endif // WITH_openCL
