# Virtual machine support for aNa
aNa can be compiled and used inside Virtual Box. Even the visualization server is running on a virtual PC. But maybe significant slower.

You DON'T need hardware and software equipment identical to the original high end gaming computer i used for development. The virtual machine uses Mesa as 3.3 OpenGL software solution.

![](./stuff/doc/vbox-ubuntu-ana.jpg)

## What differs from a physical PC
- Oracle VM VirtualBox version virtualbox-6.1 instead of physical hardware.
- Virtual Box **must NOT** enable 3D Acceleration. (That sounds strange to me. But was my experience.)
- Two guest operating system are tested successfully
    - 64-Bit Ubuntu 20.04.
    - 64-Bit Debian Buster 10.7
- On the guest operating system use Mesa.

![](./stuff/doc/do_NOT_enable_3D_acceleration_in_vbox.jpg)
> :warning: Do **NOT** enable 3D acceleration in VirtualBox

## Whats the same as on a physical PC
Follow the same build process as described for the physical hardware.
- [System requirements](./system_requirements.md)
- [How to build the visualization server](./build_visualization_server.md)
- [An editor for live coding](./editor_for_live_coding.md)
