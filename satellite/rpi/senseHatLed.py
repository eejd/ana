from osc4py3.as_eventloop import *
from osc4py3 import oscmethod as osm
#from sense_emu import SenseHat
from sense_hat import SenseHat

satellite_port = 57510
hat = SenseHat()

def scale(v):
    return min(255, max(0, int(v * 255)))

def h_clear(r, g, b):
    #print(r, g, b)
    hat.clear(scale(r), scale(g), scale(b))

def h_set(ix, iy, r, g, b):
    hat.set_pixel(ix, iy, scale(r), scale(g), scale(b))

def h_setrow(iy, r0, g0, b0, r1, g1, b1, r2, g2, b2, r3, g3, b3, r4, g4, b4, r5, g5, b5, r6, g6, b6, r7, g7, b7):
    hat.set_pixel(0, iy, scale(r0), scale(g0), scale(b0))
    hat.set_pixel(1, iy, scale(r1), scale(g1), scale(b1))
    hat.set_pixel(2, iy, scale(r2), scale(g2), scale(b2))
    hat.set_pixel(3, iy, scale(r3), scale(g3), scale(b3))
    hat.set_pixel(4, iy, scale(r4), scale(g4), scale(b4))
    hat.set_pixel(5, iy, scale(r5), scale(g5), scale(b5))
    hat.set_pixel(6, iy, scale(r6), scale(g6), scale(b6))
    hat.set_pixel(7, iy, scale(r7), scale(g7), scale(b7))

osc_startup()

osc_udp_server("0.0.0.0", satellite_port, "satellite")

osc_method("/satellite/sensehat/clear", h_clear)
osc_method("/satellite/sensehat/set",  h_set)
osc_method("/satellite/sensehat/setrow",  h_setrow)

hat.clear(63, 127, 255)
print(satellite_port)
finished = False
while not finished:
    osc_process()

osc_terminate()
