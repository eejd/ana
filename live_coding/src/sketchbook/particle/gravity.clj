(ns lv.demo)
(use 'lv.core)

(layout "grid" [clvb0 mopr1]
               [ras2])

(expr :clvb0 []
  "particle/gravity.cl"
  (al 32)
  (tl 4096)

  (rx (* 0.0017 f))
  (ry (* 0.0013 f))
  (rz (* 0.00073 f))
  ; (rx (* 6.28 kp0))
  ; (ry (* 6.28 kp1))
  ; (rz (* 6.28 kp2))

  ; mutual GRAVITY force
  (p7 (* 0.1 ks7))
)

(expr :mopr1 [clvb0]
  "dbb"
)

(rgb ras2 [mopr1 ras2]
  (rgb (mix ras2.rgb (* 2.5 mopr1.rgb) 0.1))
)
; (render 2)
(render 2)
