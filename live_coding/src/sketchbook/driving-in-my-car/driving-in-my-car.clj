(ns lv.demo)
(use 'lv.core)

(layout "grid"
  [imgs0 ffb1] [mopr2 ras3])

; (imagesequ "../../../timelapse/series/20191217-Karussell-4/split/*.png")
; (imagesequ "../../../timelapse/series/20150428-1-drehteller-licht/split-2525/*.png")
(imagesequ "../../../timelapse/series/20201231-1-plattenspieler/split/*.png")
(render)

(hsv :imgs0 [c0]
  (k sframe)
  (hsv c0.hsv)
)

(expr :ffb1 [imgs0]
  (reduce 3)
  (mode 2)
  (gauss 1)
  (levels 4)
  (win 16)
  (iter 4)
  (polyn 7)
  (polys 1.2)
  (r (* 0.3 z))
  (a 0.5)
  (s 0.6)
  (clear -1)
  (paint 1)

  (step 2)
)
(render 1)

(expr :mopr2 [ffb1]
  "dddbb"
)
(rgb :ras3 [imgs0 mopr2]
  (rgb (* imgs0.b
          (* 2.8 imgs0.rgb)
          (* 1.0 mopr2.rgb)))
)
; (render 3)
