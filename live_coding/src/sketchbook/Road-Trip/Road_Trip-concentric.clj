(ns lv.demo)
(use 'lv.core)

(layout "grid" [sline1 mov0 cline1]
               [mopr3 ras4 ras6]
               [fdiff5 ])
(movie "emilysoddities/ALo-FiRoadTriptoArizonaandNewMexico-720p-hls.mov")
(render)

; ks0    0.48031
; ks2    1
;
; kb0    0

(hsv :mov0 [c0]
  (sx 1) (sy 1)
  ; (hsv (* (vec3 1 1.2 1) c0.hsv))
  (hsv c0.hsv)
)

; find contours
(expr :cline1 [mov0]
  (count 250)
  (max 5000)
  (thres ks0)
  (fill kb0)
  (paint -1)
  (holes 1)
)
; concentric scan lines
(expr :sline1 [mov0]
	(mode 2)
	(clear kb7)
  (dy (/ 3.14149 300))
  (dx 0.045)
	(jitter (* -0.05 (+ r g b)))
)
; horizontal scan lines
(expr :sline2 [mov0]
	(mode 0)
	(drift (* 0.2 (snoise (* 5.1 y) 0.1 (sin (* 0.1 f)))))
	(clear 1)
  (dy 0.023)
  (dx 0.030)
	(jitter (* -0.02 (+ r g b)))
)
; morphological operator
; dilate blur blur
(expr :mopr3 [sline1] "dddbb")

(expr :fdiff5 [mov0])

(rgb :ras4 [mov0 mopr3]
  ; (rgb (mix mov0.rgb mopr3.rgb 0.3))
  (rgb (* 2 mov0.rgb mopr3.rgb))
)

(rgb :ras6 [ras6 fdiff5]
  (rgb (mix ras6.rgb (* ks2 16 fdiff5.rgb) 0.1))
)
