(ns lv.demo)
(use 'lv.core)

(layout "grid" [img0 img1]
               [img2 ras3])
; assume there are 4 PNG images in the media folder.
(image "flower-sampler/*.png")
(render)

; Do not modify the image.
; Copies hue, saturation and brightness without modification from image c0.
(hsv :img0 [c0 c1 c2 c3]
  (hsv c0.hsv)
)

; Invert the hue channel.
(hsv :img1 [c0 c1 c2 c3]
  (h (+ 0.5 c0.h))
  (s c0.s)
  (v c0.v)
)

; Exchange brightness and saturation channel.
(hsv :img2 [c0 c1 c2 c3]
  (h c2.h)
  (s c2.v)
  (v c2.s)
)

; Mix the output of node img0, img1 and img2.
; Playing with the equations.
(hsv :ras3 [img0 img1 img2]
  (h (* img0.h img1.h img2.h))
  (s (- 1 img1.s))
  (v img1.v)
)
