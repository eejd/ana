(ns lv.demo)
(use 'lv.core)

(layout "grid"
 [win0 mopr3 mt5]
 [cline2 ras3 win1])
(render)

; https://hydra.ojack.xyz/?sketch_id=rangga_0
; https://hydra.ojack.xyz/?sketch_id=example_4
(expr :win0 [] (i 5))
(expr :win1 [] (i 9))

(expr :cline2 [win0]
  (count 250)
  (max 2000)
  (thres ks0)
  (fill -1)
  (names -1)
  (clear (zero? (% f 30) 1 -1))
  (paint -1)
  (holes 1)
)

(expr :mopr3 [cline2] "dbb")

(rgb :ras3 [win0 win1 mopr3 ras3]
  (kaleid 3)
  (rot (* f ks2 0.005))
  (rgb (mix ras3.rgb
            (* win0.rgb win0.rgb mopr3.rgb)
            ks1))
)

(expr :mt5 [win0]
  (mode 0)
  (count 100)
  (trace 32)
  (qual 0.02)
  (paint -1)
)
