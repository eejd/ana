#include <math.h>
#include "util/p3circle.h"

// see: https://www.geeksforgeeks.org/equation-of-circle-when-three-points-on-the-circle-are-given/
// Function to find the circle on which the given three points lie
void findCircle(float x1, float y1, float x2, float y2, float x3, float y3, float& cx, float& cy, float& r)
{
    float x12 = x1 - x2;
    float x13 = x1 - x3;

    float y12 = y1 - y2;
    float y13 = y1 - y3;

    float y31 = y3 - y1;
    float y21 = y2 - y1;

    float x31 = x3 - x1;
    float x21 = x2 - x1;

    // x1^2 - x3^2
    float sx13 = std::pow(x1, 2) - std::pow(x3, 2);

    // y1^2 - y3^2
    float sy13 = std::pow(y1, 2) - std::pow(y3, 2);

    float sx21 = std::pow(x2, 2) - std::pow(x1, 2);
    float sy21 = std::pow(y2, 2) - std::pow(y1, 2);

    float f = (sx13 * x12
             + sy13 * x12
             + sx21 * x13
             + sy21 * x13)
            / (2 * (y31 * x12 - y21 * x13));
    float g = (sx13 * y12
             + sy13 * y12
             + sx21 * y13
             + sy21 * y13)
            / (2 * (x31 * y12 - x21 * y13));

    float c = -std::pow(x1, 2) - std::pow(y1, 2) - 2 * g * x1 - 2 * f * y1;

    // eqn of circle be x^2 + y^2 + 2*g*x + 2*f*y + c = 0
    // where centre is (h = -g, k = -f) and radius r
    // as r^2 = h^2 + k^2 - c
    cx = -g;
    cy = -f;

    // r is the radius
    r = std::sqrt(cx * cx + cy * cy - c);
}
