#include <limits>
#include <math.h>
#include <string>

#include "ofMain.h"

#include "ana_features.h"
#include "calc/MsgLisp.h"
#include "calc/MsgLispTest.h"

#define TRACE_MONOTONIC

MsgLispTest::MsgLispTest() {
}

MsgLispTest::~MsgLispTest() {
}

#ifdef WITH_testcases
int MsgLispTest::test_exec_failed = 0;
int MsgLispTest::test_syntax_failed = 0;

void MsgLispTest::check(std::string expected, std::string input) {
    ofLog(OF_LOG_NOTICE) << "in  " << input;
    MsgLisp l1;
    ast_m* res = l1.compile(input);
    std::string frag = l1.ast2frag(res, 0);
    if(expected == frag) {
        ofLog(OF_LOG_NOTICE) << "ast2frag '" << frag << "'";
    }
    else {
        l1.dump_ast(res, 0);
        ofLog(OF_LOG_ERROR) << "expected '" << expected << "' ast2frag '" << frag << "'";
        ++test_syntax_failed;
    }

}

void MsgLispTest::errcheck(std::string input) {
    //TODO
}


void MsgLispTest::runAstTestCases() {
    ofLog(OF_LOG_NOTICE) << "--- begin MsgLispTest::runTestCases ---";
    test_exec_failed = 0;
    test_syntax_failed = 0;
    MsgLispTest t1;
    t1.check("(2 * 3)",          "(* 2 3)");
    t1.check("(2 * 3 * 5)",      "(* 2 3 5)");
    t1.check("(1 + (f * 0.01))", "(+ 1(* f 0.01))");
    t1.check("20", "20");
    t1.check("f", "f");

    if(test_exec_failed || test_syntax_failed) {
        ofLog(OF_LOG_ERROR) << test_syntax_failed << " " << test_exec_failed << " some test cases failed";
    }
    ofLog(OF_LOG_NOTICE) << "--- end MsgLispTest::runTestCases ---";
}

void MsgLispTest::runLispTestCases() {
    test_exec_failed = 0;
    MsgLisp l1;

    {
        std::string input = "(render -1)";
        ofLog(OF_LOG_NOTICE) << input;
        ast_m* res = l1.compile(input);
        bool ok = res
                && res->count() == 2
                && res->at(0)->tok == "render"
                && res->at(1)->tok == "-1";
        if(!ok) {
            ++test_exec_failed;
            ofLog(OF_LOG_ERROR) << input << " -> failed";
            ofLog(OF_LOG_ERROR) << "\n" << l1.dump_ast(res, 0);
        }
    }


    {
        std::string input = "(layout \"grid\" [img0 img1] [ras2 ras3])";
        ast_m* res = l1.compile(input);
        ofLog(OF_LOG_NOTICE) << input;
        bool ok = res
                && res->count() == 4
                && res->at(2)->count() == 2
                && res->at(3)->count() == 2;
        if(!ok) {
            ++test_exec_failed;
            ofLog(OF_LOG_ERROR) << input << " -> failed";
            ofLog(OF_LOG_ERROR) << "\n" << l1.dump_ast(res, 0);
        }
    }

    if(test_exec_failed) {
        ofLog(OF_LOG_ERROR) << "MsgLisp " << test_exec_failed << " some test cases failed";
    }
}
#endif // WITH_testcases
