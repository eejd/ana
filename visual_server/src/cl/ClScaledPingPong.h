#pragma once
#include "../ana_features.h"
#ifdef WITH_openCL

#include "ClPingPong.h"

class ClScaledPingPong: public ClPingPong {
public:
    ClScaledPingPong(ofGLWindowSettings settings, OscListener * oscParams);
    virtual ~ClScaledPingPong();
    virtual void draw(int li);

protected:
    virtual size_t getKernelWidth();
    virtual size_t getKernelHeight();
};
#endif // WITH_openCL

