#pragma once

#include "ofMain.h"
#include "Filter.h"
#include "OscListener.h"

class GlMovieFilter : public Filter {
	public:
        GlMovieFilter(ofGLWindowSettings settings, OscListener * oscParams);
		virtual ~GlMovieFilter();
        virtual void setInputTexture(std::string cmd);
		virtual void reload(ofxOscMessage const &msg);
        virtual void draw(int li);

	protected:
		std::vector<int> channel_ids;
};
