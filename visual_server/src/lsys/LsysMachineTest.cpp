#include "LsysMachineTest.h"

#include <limits>
#include <math.h>
#include <string>

#include "ofMain.h"

#include "ana_features.h"
#include "lsys/LsysMachine.h"
#include "lsys/LsysMachineTest.h"

#define TRACE_MONOTONIC

LsysMachineTest::LsysMachineTest() {
}

LsysMachineTest::~LsysMachineTest() {
}

#ifdef WITH_testcases
int LsysMachineTest::test_exec_failed = 0;
int LsysMachineTest::test_syntax_failed = 0;

void LsysMachineTest::check(const char *expected, bool expected_post,
                            std::string axiom, std::vector<std::string> predecessor, std::vector<std::string> successor, std::vector<std::string> leftContext, std::vector<std::string> rightContext, int depth,
                            const bool deterministic) {
    LsysMachine l1;
    l1.setLsys("+-", axiom, predecessor, successor, leftContext, rightContext);
    l1.expand(depth);
    if(deterministic) {
        if(strcmp(expected, l1.charInterpretation()) != 0) {
            ++test_exec_failed;
            ofLog(OF_LOG_ERROR) << "TESTERROR " << "expand failed\n" << l1.charInterpretation();
        }
        else {
            char * s1 = strdup(l1.charInterpretation());
            ofLog(OF_LOG_NOTICE) << "expand OK\n" << s1;
            l1.postProcess();
            char * s2 = strdup(l1.charInterpretation());
            ofLog(OF_LOG_NOTICE) << "post\n" << s2;
            bool post_eq = strcmp(s1, s2) == 0;
            if(post_eq == expected_post)
                ofLog(OF_LOG_NOTICE) << "processing OK\n" << s2;
            else {
                ++test_exec_failed;
                ofLog(OF_LOG_ERROR) << "TESTERROR " << "processing failed\n" << s2;
            }
            std::free(s1);
        }
    }
}

void LsysMachineTest::lContext(bool expected, int at, char testChar, std::string pattern) {
    LsysMachine l1;
    l1.setIgnore("+-");
    l1.src_length = pattern.size();
    int ci = l1.src_length;
    while(ci-- > 0) {
        l1.src_string[ci] = pattern[ci];
    }
    if(expected != l1.testLeftContext(at, testChar)) {
        ++test_exec_failed;
        ofLog(OF_LOG_ERROR) << "TESTERROR " << "lContext failed " << at << " " << testChar << " " << pattern;
    }
    else {
        ofLog(OF_LOG_NOTICE) << "lContext OK " << at << " " << testChar << " " << pattern;
    }
}

void LsysMachineTest::rContext(bool expected, int at, char testChar, std::string pattern) {
    LsysMachine l1;
    l1.setIgnore("+-");
    l1.src_length = pattern.size();
    int ci = l1.src_length;
    while(ci-- > 0) {
        l1.src_string[ci] = pattern[ci];
    }
    if(expected != l1.testRightContext(at, testChar)) {
        ++test_exec_failed;
        ofLog(OF_LOG_ERROR) << "TESTERROR " << "rContext failed " << at << " " << testChar << " " << pattern;
    }
    else {
        ofLog(OF_LOG_NOTICE) << "rContext OK " << at << " " << testChar << " " << pattern;
    }
}

void LsysMachineTest::runTestCases() {
    ofLog(OF_LOG_NOTICE) << "--- begin LsysMachineTest::runTestCases ---";
    test_exec_failed = 0;
    test_syntax_failed = 0;
    LsysMachineTest t1;

    {
        //                          012345678901234567890123
        t1.lContext(false, 0, 'A', "ABC[DE][SG[HI[JK]L]MNO]P");
        t1.lContext(true, 19, 'G', "ABC[DE][SG[HI[JK]L]MNO]P");
        t1.lContext(true,  1, 'A', "ABC[DE][SG[HI[JK]L]MNO]P");
        t1.lContext(true,  8, 'C', "ABC[DE][SG[HI[JK]L]MNO]P");
        t1.lContext(true, 14, 'I', "ABC[DE][SG[HI[JK]L]MNO]P");
        t1.lContext(true, 23, 'C', "ABC[DE][SG[HI[JK]L]MNO]P");

        //                         012345678901234567890123
        t1.lContext(true, 3, 'F', "F[[A");
        t1.lContext(true, 3, 'F', "F[]A");
        t1.lContext(true, 4, 'F', "F[B]A");
        t1.lContext(true, 5, 'F', "F[B][A");
        t1.lContext(true, 6, 'C', "F[B]C[A");
        t1.lContext(true, 5, 'B', "F[B[[A");
        t1.lContext(true, 7, 'F', "F[[B]][A");
        t1.lContext(true, 9, 'F', "F[[B]+][-A");
        t1.lContext(true, 9, 'F', "F-[[B]]+[A");

        //                         012345678901234567890123
        t1.rContext(true, 0, 'B', "F[A]B");
        t1.rContext(true, 0, 'C', "F[AB]C");
        t1.rContext(true, 2, 'B', "F[AB]C");
        t1.rContext(false,3, 'C', "F[AB]C");
        t1.rContext(true, 0, 'B', "F[]B");
        t1.rContext(false,0, 'A', "F[[A");
        t1.rContext(false,3, 'A', "F[[A");
        t1.rContext(true, 0, 'B', "F[][A]B");
        t1.rContext(true, 0, 'G', "F[AB[CD]E]G");
        t1.rContext(true, 3, 'E', "F[AB[CD]E]G");
        t1.rContext(false,3, 'C', "F[AB[CD]E]G");
        t1.rContext(false,6, 'E', "F[AB[CD]E]G");
        //                         012345678901234567890123
        t1.rContext(true, 3, 'E', "F[AB+[+C+D+]+E+]+G+");
        t1.rContext(true, 3, 'E', "F[AB[CD]--E]G");
        t1.rContext(true, 3, 'E', "F[AB[CD---]E]G");
    }

    {
        std::string axiom = "F";
        std::vector<std::string> predecessor = { "F"};
        std::vector<std::string> successor =   { "FF-[-F+F+F]+[+F-F-F]" };
        std::vector<std::string> leftContext;
        std::vector<std::string> rightContext;
        t1.check("F", true,
                axiom, predecessor, successor, leftContext, rightContext, 0);
        t1.check("FF-[-F+F+F]+[+F-F-F]", true,
                axiom, predecessor, successor, leftContext, rightContext, 1);
        t1.check("FF-[-F+F+F]+[+F-F-F]FF-[-F+F+F]+[+F-F-F]-[-FF-[-F+F+F]+[+F-F-F]+FF-[-F+F+F]+[+F-F-F]+FF-[-F+F+F]+[+F-F-F]]+[+FF-[-F+F+F]+[+F-F-F]-FF-[-F+F+F]+[+F-F-F]-FF-[-F+F+F]+[+F-F-F]]",
                true,
                axiom, predecessor, successor, leftContext, rightContext, 2);
    }

    { //TODO
//        std::string axiom = "F";
//        std::vector<std::string> predecessor = { "F"};
//        std::vector<std::string> successor =   { "F[-f+][+f-][]" };
//        std::vector<std::string> leftContext;
//        std::vector<std::string> rightContext;
//        t1.check("F", true,
//                axiom, predecessor, successor, leftContext, rightContext, 0);
//        t1.check("F[-f][+f]", true,
//                axiom, predecessor, successor, leftContext, rightContext, 1);
//        t1.check("FF-[-F+F+F]+[+F-F-F]FF-[-F+F+F]+[+F-F-F]-[-FF-[-F+F+F]+[+F-F-F]+FF-[-F+F+F]+[+F-F-F]+FF-[-F+F+F]+[+F-F-F]]+[+FF-[-F+F+F]+[+F-F-F]-FF-[-F+F+F]+[+F-F-F]-FF-[-F+F+F]+[+F-F-F]]",
//                true,
//                axiom, predecessor, successor, leftContext, rightContext, 2);
    }

    {
        std::string axiom = "F";
        std::vector<std::string> predecessor = { "F",     "A" };
        std::vector<std::string> successor =   { "[-FA]", "F" };
        std::vector<std::string> leftContext;
        std::vector<std::string> rightContext;
        t1.check("F", true,
                axiom, predecessor, successor, leftContext, rightContext, 0);
        t1.check("[-FA]", false,
                axiom, predecessor, successor, leftContext, rightContext, 1);
        t1.check("[-[-FA]F]", false,
                axiom, predecessor, successor, leftContext, rightContext, 2);
    }

    {
        std::string axiom = "F";
        std::vector<std::string> predecessor = { "F", "F" };
        std::vector<std::string> successor =   { "-", "FF-[-F+F+F]+[+F-F-F]" };
        std::vector<std::string> leftContext;
        std::vector<std::string> rightContext;
        t1.check("FF-[-F+F+F]+[+F-F-F]", true,
                axiom, predecessor, successor, leftContext, rightContext, 1,
                false);
        t1.check("FF-[-F+F+F]+[+F-F-F]--[-FF-[-F+F+F]+[+F-F-F]+-+FF-[-F+F+F]+[+F-F-F]]+[+--FF-[-F+F+F]+[+F-F-F]-FF-[-F+F+F]+[+F-F-F]]",
                true,
                axiom, predecessor, successor, leftContext, rightContext, 2,
                false);
    }

    {
        std::string axiom = "BAAAAAAAA";
        std::vector<std::string> predecessor =  { "A",      "B" };
        std::vector<std::string> successor =    { "B(2)", "A(+ 0.5)" };
        std::vector<std::string> leftContext =  { "B", "" };
        std::vector<std::string> rightContext = { "", "" };
        t1.check("ABAAAAAAA", false, axiom, predecessor, successor, leftContext, rightContext, 1);
        t1.check("AAAAAAAAB", false, axiom, predecessor, successor, leftContext, rightContext, 8);
    }

    {
    }

    if(test_exec_failed || test_syntax_failed) {
        ofLog(OF_LOG_ERROR) << "TESTERROR " << test_syntax_failed << " " << test_exec_failed << " some test cases failed";
    }
    ofLog(OF_LOG_NOTICE) << "--- end LsysMachineTest::runTestCases ---";
}

void LsysMachineTest::prof() {
}

void LsysMachineTest::runProfile() {
}
#endif
