(ns lv.demo)
(use 'lv.core)

(layout "vertical" [img0])
; assume there are up to 4 PNG images in the media folder.
(image "flower-sampler/*.png")

; mix colors in rgb space from image c1, c2 and c3
; ignore the already loaded image c0
(rgb :img0 [c0 c1 c2 c3]
  (rgb (mix c1.rgb
            (mix c2.rgb
                 c3.rgb
                 (sin (* c1.g f)))
            (snoise x (sin (* 0.05 f)))))
)
