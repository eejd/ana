#include <filesystem>
#include <algorithm>
#include <chrono>

#include "ofMain.h"

#include "stringhelper.h"
#include "util/pathhelper.h"
#include "globalvars.h"
#include "OscListener.h"
#include "Filter.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispMng.h"
#include "gl/GlLoadFilter.h"

GlLoadFilter::GlLoadFilter(ofGLWindowSettings settings, OscListener * oscParams) : Filter(settings, oscParams){
}

GlLoadFilter::~GlLoadFilter(){
}

void GlLoadFilter::setup(const std::string _id, int numSamples) {
    Filter::setup(id, numSamples);
}

void GlLoadFilter::reload(ofxOscMessage const &msg) {
    list_size = 0;
    no_input = true;
    auto narg = msg.getNumArgs();
    if(narg > 2) {
        img_pattern = msg.getArgAsString(2);
        img_list = file_names(img_pattern);
        list_size = img_list.size();
    }
    ScalarLispMng mng;
    mng.compile(slKf, "k", "f", msg);
    frame = 0; //TODO
}

void GlLoadFilter::update() {
    no_input = true;
    k = slKf.rpn.execute0();
    if(list_size > 0 && k >= 0 && (k+1) < list_size) {
        if(file_exists(img_list[k])) {
            img.clear();
            no_input = !img.load(img_list[k]);
        }
    }
}

void GlLoadFilter::draw(int li){
	fbo.begin();
//	ofClear(0, 0, 0, 255);
//
//	shader.begin();
//	sendUniforms(shader, li);
//
//	shader.setUniformTexture("tex0", no_input ? no_img : img, 0);
//    shader.setUniform1i("sframe", k);
//    shader.setUniform1i("slength", list_size);
//
//	ofDrawRectangle(0, 0, filterWidth, filterHeight);
//
//	shader.end();
	if(no_input) {
	    ofClear(0, 0, 0, 255);
	}
	else {
        img.draw(0,0);
	}
	fbo.end();
}

void GlLoadFilter::toMermaid(std::stringstream & info) {
    info << "c0" << ">" << img_pattern << "]" << " -- c0"<< " --> " << id << "\n"; //TODO quote [ ]
    Filter::toMermaid(info);
}
