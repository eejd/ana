# An editor for live coding

You need an editor that provides an interactive REPL (read evaluate print loop) for Clojure.
I am using Atom. The hardware requirements are small. A Laptop can be used for editing. Or the editor can run together with the visualization server on the same computer.

[[_TOC_]]

## Installing Atom as live coding editor for aNa.
### Install Clojure
Use your distributions package manger to install Clojure and its dependencies.
I am using `clojure 1.10.0`
```bash
apt install clojure
```

### Install Leiningen
Use your distributions package manger to install Leiningen.
I am using `leiningen 2.9.0`
```bash
apt install leiningen
```

### Install Atom
#### Installation on Debian
* Download `atom-amd64.deb` from https://atom.io/
* As root user install package `atom-amd64.deb` from your download folder.
```bash
apt install ./atom-amd64.deb
```
Installing `atom-amd64.deb` will install `apm` the Atom Package Manager too.

### Install required plugins for Atom
#### Add the proto-repl plugin
![](./stuff/doc/install-proto-repl.png)

* start Atom
* inside Atom open `Preferences->Install Packages`
  * search for proto-repl
* install `proto-repl 1.4.24`

Optionally `proto-repl` can use `ink 0.12.2`.

> :warning: But I got troubles when ink is updated. Do **not** update ink.
Upgrading ink to version 0.11.0 breaks proto-repl #327
https://github.com/jasongilman/proto-repl/issues/327

As a non privileged user you can install the specific version of ink on the command line.
```bash
apm install ink@0.12.2
```

#### Add overtone, test.check, ...  etc
Additional dependencies are listed in  `live_coding/project.clj`. Leiningen can download these packages. No need to install them manually.

The following commands assume that aNa is cloned to folder `/home/<YOUR_USER_NAME>/ana`

* `cd` to folder `ana/live_coding`
* start Atom
  * These commands will open file `live_coding/src/lv/core.cjl`
```bash
cd ~/ana/live_coding
atom --clear-window-state -a . src/lv/core.clj
```
* From the menu start proto-repl 'from open project' `Packages-->proto-repl-->Start REPL from open project`
  ![](./stuff/doc/download-dependecies-ana.png)
* And wait until the all dependencies are downloaded.

## Starting the live coding editor.
#### Open and run an example
* For starting Atom you can use this `run_atom` bash script.
```bash
#!/bin/bash
cd ~/ana/live_coding
atom --clear-window-state -a . src/lv/core.clj
```
* From the menu start proto-repl 'from open project' `Packages-->proto-repl-->Start REPL from open project`
  ![](./stuff/doc/start-atom-editor.png)

* Select the whole text in buffer `core.clj` and execute the all buffer contend at once. This will load the required `lv.core` name space.
![](./stuff/doc/start-lv-core.png)
* Open a demo file. For example  `live_coding/src/demo/raster-fbo-loop/demo_loop_gray.cjl`
* Move the cursor inside the first code block and execute this expression. You can do this inside the source file in block by block manner. It is not necessary to execute the whole stuff at once.

![](./stuff/doc/execute-code-block.jpg)

* You can edit the content of a block and execute it again.

* It is only necessary
  * to execute the whole `lv/core.clj` file before continuing with one of the example files.
  * to execute the `(ns` and `(use` lines at the top of a example file before working on the rest of the example.

> :information_source:
Most examples can be run as a whole by selecting all lines and send them in one bunch. But live coding is more about changing the code block often and send them in different order over and over again.

> :information_source: Configure and use your own short keys for **execute block** and **execute selected text**. Using the mouse is too slow for live coding.

## Software used on Debian 10
- Clojure 1.10.0
- Leiningen 2.9.0
- Atom 1.47.0 or 1.53.0
- required Atom package: proto-repl 1.4.24
- optional Atom package: ink 0.12.2
