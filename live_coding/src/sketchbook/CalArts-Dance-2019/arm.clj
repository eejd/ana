(ns lv.demo)
(use 'lv.core)

(layout "grid" [imgs0 bkg1]
               [cline2 mopr3]
               [ras4 ras5])

(imagesequ "CalArts-Dance-2019/arm/*.png")
(render)
(hsv :imgs0 [c0]
  (k sframe)
  (hsv (* (vec3 1 1 1) c0.hsv))
)

(expr :bkg1 [imgs0]
  (thres 0.25)
  (learn 2)
)

(expr :cline2 [bkg1]
  (count 50)
  (max 5000)
  (thres 0.5)
  (fill -1)
  (paint -1)
  (holes 1)
)

(expr mopr3 [cline2]
  "ddbbb"
)

(rgb ras4[imgs0 mopr3]
  (rgb (* imgs0.rgb
          mopr3.rgb))
)
(rgb ras5[ras4 ras5 imgs0]
  (rot (* 0.01 f))
  (rgb (+ (mix (* 2 ras4.rgb)
               ras5.rgb
               0.4)
          (* 0.5 imgs0.rgb)))
)
(render 5)
