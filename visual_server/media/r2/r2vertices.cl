// This is a simple pseudo random number generator as used for example in shadertoys.
// It is based on scrambling the bits of the coordinates of the currently sampled point.
float rand_float(int n) {
  n = (n << 13) ^ n; 
  return (float)((n * (n*n*15731+789221) + 1376312589) & 0x7fffffff) / (float)(0x7fffffff);
}

//-----------------------------
float2 sinusoidal(float2 v, float amount) {
    return amount * sin(v);
}

float2 spherical(float2 v, float amount) {
    float r = length(v);
    return (amount * 1.0f / (r * r)) * v;
}

float2 expotential(float2 v, float amount) {
    return (amount * exp(v.x-1)) * (float2)(cos(M_PI_F*v.y), sin(M_PI_F*v.y));
}

float2 hyperbolic(float2 v, float amount) {
    float r = length(v) + 1.0e-10;
    float theta = atan2(v.x, v.y);
    return amount * (float2)(sin(theta) / r, cos(theta) * r);
}

// parametrization P={p1, p2, p3, p4}
float2 pdj(float2 v, float amount, float p1, float p2, float p3, float p4) {
    return amount * (float2)(sin(p1 * v.y) - cos(p2 * v.x),
                            sin(p3 * v.x) - cos(p4 * v.y));
}

float2 rect(float2 v, float amount, float p1, float p2) {
    float2 pp = (float2)(M_PI_F*p1, M_PI_F*p2);
    return amount * sign(v) * (((2.0f * fabs(v / pp) + 1.0f) * pp) - v);
}

float2 sech(float2 v, float amount) {
  float d = cos(2.0f * v.y) + cosh(2.0f * v.x);
  if (fabs(d) > 0.0001)
    d = amount * 2.0f / d;
  return (float2)(d * cos(v.y) * cosh(v.x), -d * sin(v.y) * sinh(v.x));
}

//-----------------------------
float2 remap_sin(float2 v) {
    return M_PI_F * sin(v);
}

float2 remap_torus(float2 v) {
    return fmod(v, (float2)(M_PI_F, M_PI_F));
}

// ---------------------------------------------------------
__kernel void map_r2(
__global float * result,
const int lines,
const int rows,
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7,
const float dt0,
const float dt1,
const float dt2,
const float dt3,
const float dt4,
const float dt5,
const float dt6,
const float dt7
) 
{
    const float size = sqrt((float)(lines * rows));
    const float size2 = size/2.0;
    const int vi = get_global_id(0);
    float2 pos = (float2)(fmod(vi, size)-size2,
                          (vi / size)-size2);
    
    pos.x += 0.5 * rand_float(vi);
    pos.y += 0.5 * rand_float(vi+1);
//     pos *= M_PI_F;
    pos *= 0.01f * M_PI_F;

#define A2

#ifdef A1
    for(uint l=0; l<3; ++l) {
        pos = expotential(pos, p2);
        pos = sinusoidal(pos, p0);
        pos = spherical(pos, p1);
        pos = hyperbolic(pos, p3);
    }
#endif
    
#ifdef A2
    for(uint l=0; l<3; ++l) pos = sinusoidal(pos, p0);
    for(uint l=0; l<5; ++l) pos = spherical(pos, p1);
    for(uint l=0; l<7; ++l) pos = expotential(pos, p2);
#endif
    
#ifdef A3
    pos = sinusoidal(pos, p0) * spherical(pos, p1) - expotential(pos, p2);
#endif
    
#ifdef A4
    pos = rect(pos, p0, p1, p2);
#endif

#ifdef A5
    for(uint l=0; l<3; ++l) {
        pos = pdj(pos, p0, p1, p2, p3, p4);
        pos = sech(pos, p0);
    }
#endif
    
    pos = remap_torus(pos);
//     pos = remap_sin(pos);
    
    
/* x */    result[vi*8+0] = 550.0 * pos.x;
/* y */    result[vi*8+1] = 550.0 * pos.y;
/* z */    result[vi*8+2] = 0.0;
/* w */    result[vi*8+3] = 35.0;
/* h */    result[vi*8+4] = vi / (size * size);
/* s */    result[vi*8+5] = 0.5;
/* v */    result[vi*8+6] = 0.5;
/* a */    result[vi*8+7] = 0.2;
}
