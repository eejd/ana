#!/bin/bash
mkdir /dev/shm/burst
#echo -x
uvcdynctrl -s "Power Line Frequency" 1
uvcdynctrl -s  Sharpness 7
#uvcdynctrl -s "Focus, Auto" 1
#sleep 2
uvcdynctrl -s "Focus, Auto" 0
uvcdynctrl -s "Focus (absolute)" 127
uvcdynctrl -s "Brightness" 0
sleep 1
for i in `seq 10`
do
fn=$(printf "%05d" $i)
fswebcam --no-banner -d /dev/video0 -r 3264x2448 /dev/shm/burst/burst_$fn.jpg
#sleep 1
done
#echo +x
