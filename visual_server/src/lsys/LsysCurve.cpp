#include <math.h>
#include <string>

#include "ofMain.h"

#include "ana_features.h"
#include "color/Palette.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "lsys/LsysCurve.h"

//#define TRACE_MONOTONIC
#undef  TRACE_MONOTONIC

LsysCurve::LsysCurve() : LsysIntepretation() {
}

LsysCurve::~LsysCurve() {
}

void LsysCurve::init(size_t length0, char* string0, ScalarLisp** sl0) {
    turtle_length = length0;
    turtle_string = string0;
    turtle_sl = sl0;
    turtle_xmax = turtle_ymax = 0.1f;
    turtle_xmin = turtle_ymin = -0.1f;
    canvas_width = 1;
    canvas_height = 1;
    borderFactor = 0.01;
    initTransformation();
    stackMessageSend = false;
}

inline int GetIndexClamped(int index, int len)
{
    if(index < 0)    return 0;
    if(index >= len) return len-1;
    return index;
}

// see: https://blog.demofox.org/2015/08/08/cubic-hermite-interpolation/
// t is a value that goes from 0 to 1 to interpolate in a C1 continuous way across uniformly sampled data points.
// when t is 0, this will return B.  When t is 1, this will return C.
static float CubicHermite (float A, float B, float C, float D, float t)
{
    float a = -A/2.0f + (3.0f*B)/2.0f - (3.0f*C)/2.0f + D/2.0f;
    float b = A - (5.0f*B)/2.0f + 2.0f*C - D / 2.0f;
    float c = -A/2.0f + C/2.0f;
    float d = B;
    float t2 = t*t;
    return a*t*t2 + b*t2 + c*t + d;
}

void LsysCurve::drawHermiteCubicCurve(point2D_t *vert_list, int vert_len, int coli) {
    ofSetColor(lut.at(coli));
    if(vert_len == 2) {
        ofDrawLine(vert_list[0].x, vert_list[0].y, vert_list[1].x, vert_list[1].y);
    } else if(vert_len == 3) {
        ofDrawLine(vert_list[0].x, vert_list[0].y, vert_list[1].x, vert_list[1].y);
        ofDrawLine(vert_list[1].x, vert_list[1].y, vert_list[2].x, vert_list[2].y);
    } else if(vert_len >= 4) {
        ofPolyline pl;
        int c_numSamples = vert_len * 4;
        point2D_t A, B, C, D;
        for(int i = 0; i < c_numSamples; ++i) {
            float percent = (float)i / (float)(c_numSamples - 1);
            float tx = (vert_len - 1) * percent;
            int index = int(tx);
            float t = tx - floor(tx);

            A = vert_list[GetIndexClamped(index - 1, vert_len)];
            B = vert_list[GetIndexClamped(index, vert_len)];
            C = vert_list[GetIndexClamped(index + 1, vert_len)];
            D = vert_list[GetIndexClamped(index + 2, vert_len)];
            float x = CubicHermite(A.x, B.x, C.x, D.x, t);
            float y = CubicHermite(A.y, B.y, C.y, D.y, t);
            pl.addVertex(x, y);
        }
        pl.draw();
    }
}

int LsysCurve::drawBranch(size_t readix, float px, float py, float dir, int colorix, int rd) {
    const int max_vert_len = 16*1024;
    point2D_t* vert_list = new point2D_t[max_vert_len];
    vert_list[0].x = px; vert_list[0].y = py;
    int vert_len = 1;

    while(readix < turtle_length) {
        int character = turtle_string[readix];
        if(character == '[') {
            readix = 1 + drawBranch(readix+1, px, py, dir, colorix, rd+1);
            continue;
        }
        else if(character == ']') {
            drawHermiteCubicCurve(vert_list, vert_len, colorix);
            delete[] vert_list;
            return readix;
        }

        switch(character) {
        case 'F':
        case 'f': {
            if(turtle_sl[readix] == nullptr) {
                float r = slForward.rpn.execute3d1i(px, py, 0.0, rd);
                px += r * std::cos(dir);
                py += r * std::sin(dir);
            }
            else {
                float r = turtle_sl[readix]->rpn.execute3d1i(px, py, 0.0, rd);
                px += r * std::cos(dir);
                py += r * std::sin(dir);
            }
            vert_list[vert_len].x = px; vert_list[vert_len].y = py;
            if( vert_len < max_vert_len)
                vert_len += 1;
            break;
        }
        case '+': {
            if(turtle_sl[readix] == nullptr) {
                dir += slAngel.rpn.execute3d1i(px, py, 0.0, rd);;
            }
            else {
                dir += turtle_sl[readix]->rpn.execute3d1i(px, py, 0.0, rd);
            }
            break;
        }
        case '-':
            if(turtle_sl[readix] == nullptr) {
                dir -= slAngel.rpn.execute3d1i(px, py, 0.0, rd);;
            }
            else {
                dir -= turtle_sl[readix]->rpn.execute3d1i(px, py, 0.0, rd);
            }
            break;
        case '|':
            dir += M_PI;
            break;
        case '\'':
            if(turtle_sl[readix])
                colorix += turtle_sl[readix]->rpn.execute3d1i(px, py, 0.0, rd);
            else
                ++colorix;
            break;
        case ',':
            if(turtle_sl[readix])
                colorix -= turtle_sl[readix]->rpn.execute3d1i(px, py, 0.0, rd);
            else
                --colorix;
            break;
        }

        ++readix;
    }

    drawHermiteCubicCurve(vert_list, vert_len, colorix);
    delete[] vert_list;
    return readix;
}


void LsysCurve::draw() {
#ifdef TRACE_MONOTONIC
    timespec time1, time2;
    clock_gettime(CLOCK_MONOTONIC, &time1);
#endif // TRACE_MONOTONIC
    ofPushMatrix();
    ofTranslate(0.5f*canvas_width - center_x, 0.5f*canvas_height - center_y);
    ofScale(scale_to_canvas);

    ofClear(0, 255);
    drawBranch(0, 0.0, 0.0, start_direction, 0, 0);
    ofPopMatrix();
#ifdef TRACE_MONOTONIC
    clock_gettime(CLOCK_MONOTONIC, &time2);
    ofLog(OF_LOG_NOTICE) << "LsysCurve::draw " << (time2.tv_sec * 1000000000l + time2.tv_nsec) - (time1.tv_sec * 1000000000l + time1.tv_nsec);
#endif // TRACE_MONOTONIC
}

/**
 * Calculates the bounds.
 */
int LsysCurve::boundsBranch(size_t readix, float px, float py, float dir, int rd) {
    turtle_xmax = std::max(turtle_xmax, px);
    turtle_xmin = std::min(turtle_xmin, px);
    turtle_ymax = std::max(turtle_ymax, py);
    turtle_ymin = std::min(turtle_ymin, py);

    const int max_vert_len = 16*1024;
    point2D_t* vert_list = new point2D_t[max_vert_len];
    vert_list[0].x = px; vert_list[0].y = py;
    int vert_len = 1;

    while(readix < turtle_length) {
        int character = turtle_string[readix];

        if(character == '[') {
            readix = 1 + boundsBranch(readix+1, px, py, dir, rd+1);
            continue;
        }
        else if(character == ']') {
            delete[] vert_list;
            return readix;
        }

        switch(character) {
        case 'F':
        case 'f': {
            if(turtle_sl[readix] == nullptr) {
                float r = slForward.rpn.execute3d1i(px, py, 0.0, rd);
                px += r * std::cos(dir);
                py += r * std::sin(dir);
            }
            else {
                float r = turtle_sl[readix]->rpn.execute3d1i(px, py, 0.0, rd);
                px += r * std::cos(dir);
                py += r * std::sin(dir);
            }
            turtle_xmax = std::max(turtle_xmax, px);
            turtle_xmin = std::min(turtle_xmin, px);
            turtle_ymax = std::max(turtle_ymax, py);
            turtle_ymin = std::min(turtle_ymin, py);
            vert_list[vert_len].x = px; vert_list[vert_len].y = py;
            if( vert_len < max_vert_len)
                vert_len += 1;
            break;
        }
        case '+': {
            if(turtle_sl[readix] == nullptr) {
                dir += slAngel.rpn.execute3d1i(px, py, 0.0, rd);;
            }
            else {
                dir += turtle_sl[readix]->rpn.execute3d1i(px, py, 0.0, rd);
            }
            break;
        }
        case '-':
            if(turtle_sl[readix] == nullptr) {
                dir -= slAngel.rpn.execute3d1i(px, py, 0.0, rd);;
            }
            else {
                dir -= turtle_sl[readix]->rpn.execute3d1i(px, py, 0.0, rd);
            }
            break;
        case '|':
            dir += M_PI;
            break;
        }

        ++readix;
    }

    delete[] vert_list;
    return readix;
}

void LsysCurve::expandBoundigBox() {
#ifdef TRACE_MONOTONIC
    timespec time1, time2;
    clock_gettime(CLOCK_MONOTONIC, &time1);
#endif // TRACE_MONOTONIC
    reset_minmax();
    boundsBranch(0, 0.0, 0.0, start_direction, 0);
    initTransformation();
#ifdef TRACE_MONOTONIC
    clock_gettime(CLOCK_MONOTONIC, &time2);
    ofLog(OF_LOG_NOTICE) << "LsysCurve::expandBoundigBox " << (time2.tv_sec * 1000000000l + time2.tv_nsec) - (time1.tv_sec * 1000000000l + time1.tv_nsec);
#endif // TRACE_MONOTONIC
}
