(ns lv.demo)
(use 'lv.core)

(layout "grid" [vid0 ras1] [ras2 ras3 pihat3])
(render)

(rgb :vid0 [c0]
  (rgb c0.rgb)
)

(hsv :ras1 [vid0]
  (rot (* 0.001 f))
  (h vid0.h)
  (s vid0.s)
  (v (* (+ 1 (sin (* 0.05 f)))
        vid0.v))
)

(rgb :ras2 [ras1 ras2]
  (rgb (mix ras1.rgb ras2.rgb 0.95))
)

(rgb :ras3 [ras2 pihat3]
  (rgb (mix ras2.rgb pihat3.rgb 0.75))
)
(render 3)

(expr :pihat3 [ras2]
  (mode 0) ; draw mode 0:
           ; use the color from the upstream filter node
)
