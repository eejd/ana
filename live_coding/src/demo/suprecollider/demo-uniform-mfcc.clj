(ns lv.demo)
(use 'lv.core)
; see: http://doc.sccode.org/Classes/MFCC.html

(layout "grid" [img0 cline1] [mopr2 ras4])

; assume there are 4 PNG images in the media folder.
(image "flower-sampler/*.png")
(render)

; rotate input image
(hsv :img0 [c0 c1 c2 c3]
  (rot (* 0.01 f))
  (hsv c2.hsv)
)

; find contours
(expr :cline1 [img0]
  (count 250)
  (max 5000)
  (thres (+ 0.35 u6))
  (fill (* -1 u5))
  (paint (- u1 0.1))
  (holes -1)
)

; use dilation to expand 1 pixel wide lines
(expr :mopr2 [cline1]
  "dddb"
)

; a kaleidoscope effect
(rgb :ras4 [mopr2]
  (kaleid (* 27 (- u4 0.3)))
  (rgb mopr2.rgb)
)
; (render 3)
