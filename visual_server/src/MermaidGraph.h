#pragma once
#include <string>

#include "Filter.h"

class MermaidGraph {
public:
    MermaidGraph();
    std::string toGraphString(std::vector<std::shared_ptr<Filter>> & filterLine);
};
