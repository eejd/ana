#include "../ana_features.h"
#include "ofMain.h"
#ifdef WITH_openCL
#include "MSAOpenCL.h"
#endif // WITH_openCL
#include "util/stringhelper.h"
#include "OscListener.h"
#include "globalvars.h"
#include "Configuration.h"
#include "Filter.h"

Filter::Filter(ofGLWindowSettings settings, OscListener * _oscParams) :
        oscParams(_oscParams) {
    filterWidth = asInt(FILTER_WIDTH);
    filterHeight = asInt(FILTER_HEIGHT);
}

Filter::~Filter() {
    shader.unload();
    fbo.clear();
    if(cam) {
        delete cam;
        cam = nullptr;
    }
}

void Filter::reallocateVertices(size_t capacity) {
    const size_t block = 256;
    if(capacity == 0) {
        capacity = std::max(capacity, block);
        vertices_capacity = capacity;
        vertices = new float[capacity * 8];
        memset(vertices, 0, capacity * 8 * sizeof(float));
        vertices_size = 0;
    }
    else if(capacity < vertices_capacity) {
        // did not shrink
    }
    else {
        size_t nc = block * ((capacity / block) + 1);
        float * nv = new float[nc * 8];
        memset(nv, 0, nc * 8 * sizeof(float));
        std::memcpy(nv, vertices, vertices_capacity * 8 * sizeof(float));
        delete[] vertices;
        vertices_capacity = nc;
        vertices = nv;
    }
}

void Filter::setup(const std::string _id, int numSamples) {
    id = _id;
    int camera = asInt(CAMERA);
    switch(camera) {
    case 1:
        cam = new ofEasyCam();
        cam->enableMouseInput();
        break;
    case 2:
//		anaglyphCam = new ofxCameraAnaglyph();
//		anaglyphCam->enableMouseInput();
//		anaglyphCam->enableStereo();
//		anaglyphCam->disableColorMask();
        break;
    case 3:
//		jsCam = new JoystickCamera();
//		jsCam->setup();
        break;
    default:
        break;
    }

    fbo.allocate(filterWidth, filterHeight, GL_RGBA32F_ARB, numSamples);
    fbo.begin();
    ofClear(0, 0, 0, 255);
    fbo.end();
#ifdef WITH_openCL
    clAnalysisImage.initFromTexture(fbo.getTexture());
#endif // WITH_openCL
}

const std::string& Filter::differentiation() const {
    static std::string defs = "#version 330\n#undef TEXTUREARRAY\n#define TEXTURECHANNELS\n";
    return defs;
}

std::string* Filter::getDefaultFrag() {
    return nullptr;
}

void Filter::update() {
}

ofTexture& Filter::getTexture() {
    return fbo.getTexture();
}

ofFbo& Filter::getFbo() {
    return fbo;
}

#ifdef WITH_openCL
msa::OpenCLImage& Filter::getAnalysisImage() {
    return clAnalysisImage;
}
#endif // WITH_openCL

void Filter::setInputTexture(std::string inputs) {
    upstream_ids.clear();
    split(trim(inputs, '[', ']'), ' ', upstream_ids);
}

void Filter::compileShader(std::string msg_id, const std::string src) {
    std::string id0 = msg_id.find(":") == 0 ? msg_id.substr(1) : msg_id;
    if(!(id0 == id)) {
        ofLog(OF_LOG_ERROR) << "mismatched id's " << id0 << " != " << id;
    }
    fragDict[id] = src;
    std::string okV = shader.setupShaderFromSource(GL_VERTEX_SHADER, fallbackVert) ? "OK" : "failed";
    std::string okF = shader.setupShaderFromSource(GL_FRAGMENT_SHADER, frag(id, getDefaultFrag()).insert(0, differentiation())) ? "OK" : "failed";
    std::string okP = shader.linkProgram() ? "OK" : "failed";
// for printing generated shader source
//#define PRINT_SHADER_SRC
#undef PRINT_SHADER_SRC
#ifdef PRINT_SHADER_SRC
    ofLog(OF_LOG_NOTICE) << shader.getShaderSource(GL_VERTEX_SHADER) << "\n";
    ofLog(OF_LOG_NOTICE) << shader.getShaderSource(GL_FRAGMENT_SHADER) << "\n";
#endif // PRINT_SHADER_SRC
    ofLog(OF_LOG_NOTICE) << "shader compiler " << id << ": vertex=" << okV << " fragment=" << okF <<  " link=" << okP;
}

void Filter::draw(int li) {
}

void sendUniforms(const ofShader & shader, int li) {
    // size of the canvas in pixel units
    shader.setUniform2i("panel", filterWidth, filterHeight);
    // frame counter
    shader.setUniform1i("frame", frame);
    // number of the rendering buffer 0, 1, 2, 3 ...
    shader.setUniform1i("li", li);

    // position / time of a playing mp3 file
    shader.setUniform1f("b0", beat_pos);
    // sound volume detection
    shader.setUniform1f("volume", smoothedVol);
    // simple sound onset detection
    shader.setUniform1f("onset", onset);
    // fft bins from sound
    shader.setUniform4f("fft03", fftSmoothed[0], fftSmoothed[1], fftSmoothed[2], fftSmoothed[3]);
    shader.setUniform4f("fft47", fftSmoothed[4], fftSmoothed[5], fftSmoothed[6], fftSmoothed[7]);

    // Received Open Sound Control (OSC) packages.
    // /ana_u0, /ana_u1, /ana_u2, /ana_u3
    shader.setUniform4f("u0123", u0, u1, u2, u3);
    // /ana_u4, /ana_u5, /ana_u6, /ana_u7
    shader.setUniform4f("u4567", u4, u5, u6, u7);

    // Global variables from the live coding Lisp environment.
    // These global variable are calculated once per frame.
    // NOT per pixel
    //    (texpr
    //      (t0 (usin (* 0.2 f)))
    //      (t1 (turb (* 0.23 f) (* 0.11 f) y 3))
    //      (t2 (* 0.05 (snoise (* 0.13 f) (* 0.071 f)  (* 0.05 f))))
    //    )
    shader.setUniform4f("t0123", t0, t1, t2, t3);
    shader.setUniform4f("t4567", t4, t5, t6, t7);

    shader.setUniform4f("dt0123", dt0, dt1, dt2, dt3);
    shader.setUniform4f("dt4567", dt4, dt5, dt6, dt7);

    // nanoKONTROL2 potentiometers
    shader.setUniform4f("kp0123", kp0, kp1, kp2, kp3);
    shader.setUniform4f("kp4567", kp4, kp5, kp6, kp7);
    // nanoKONTROL2 sliders
    shader.setUniform4f("ks0123", ks0, ks1, ks2, ks3);
    shader.setUniform4f("ks4567", ks4, ks5, ks6, ks7);
    // nanoKONTROL2 buttons
    shader.setUniform4f("kb0123", kb0, kb1, kb2, kb3);
    shader.setUniform4f("kb4567", kb4, kb5, kb6, kb7);

    // Game controller. For example Dualshock 3
    shader.setUniform4f("js0xy", lx, ly, rx, ry);
    shader.setUniform2f("js0lr", l2, r2);
}

void Filter::finish() {
}

void Filter::exportCsv(int li, std::ofstream &ostrm) {
}

void Filter::toMermaid(std::stringstream & info) {
    for(std::vector<std::string>::iterator it = upstream_ids.begin(); it != upstream_ids.end(); ++it) {
        std::string in_id = *it;
        info << in_id << " -- " << *it << " --> " << id << "\n";
    }
}

std::string Filter::dumpState() {
    static std::string e = "";
    return e;
}
