(ns lv.demo)
(use 'lv.core)

(layout "horizontal" [img0 imgs1 cline3 ras2])
; assume there are lots of JPEG images in the media folder.
; all imgaes have the same size 1080 x 1080
(imagesequ "20191217-Karussell-4/*.png")
(image "20191217-Karussell-4/*399.png")
(render)

(rgb :img0 [c0 c1 c2]
  (rgb (+
    (* 0.7 (usin (* 0.11 f)) c0.rgb)
    (* 0.7 (ucos (* 0.13 f)) c1.rgb)
    (* 0.7 (usin (* 0.17 f)) c2.rgb)
       ))
)
(render 0)

(hsv :imgs1 [c0]
  (rot (* -0.01 f))
  (k (even? (* 181 y)
           sframe
           (- slength sframe)))
  (h c0.h)
  (s c0.s)
  (v (* 1 (- 1 (sqr r)) c0.v))
)
(render 1)

(rgb :ras2 [img0 imgs1 cline3]
  (rgb (+ (* 2 cline3.rgb)
          (* img0.rgb imgs1.rgb)))
)
(render 2)

(expr :cline3 [imgs1]
  (count 250)
  (max 500)
  (thres 0.35)
  (paint -1)
  (fill -1)
  (holes 1)
)
(render 3)
