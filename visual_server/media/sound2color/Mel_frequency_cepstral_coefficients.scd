//s.sampleRate;
(
b = Buffer.alloc(s,1024,1); //for sampling rates 44100 and 48000
//b = Buffer.alloc(s,2048,1); //for sampling rates 88200 and 96000

//d=Buffer.read(s,"/usr/share/SuperCollider/sounds/a11wlk01.wav");
//d=Buffer.read(s,"/home/strom/tmp/01-Intro_I_Switched_My_Robot_Off.aiff");
// d = Buffer.read(s,"/home/strom/mat/sound/numbersstation/Numbers_Station_2014-10-23T04-51-24Z_5846.0kHz-user388337858.aiff");
//d = Buffer.read(s,"/home/strom/mat/sound/onSets/Skeptical-Nebula.wav");

d = Buffer.read(s,"/home/strom/ana_0.1-workspace/visual_server/media/sound/jine-LED_screen-Elektrosluch-reverese-reverb-mono.wav");

f = 7;
c = Bus.new('control', 0, f);
)

(
x= {
	var in, fft, array;

	in= PlayBuf.ar(1,d.bufnum,BufRateScale.kr(d.bufnum),1,0,1);

	//in = SoundIn.ar(0);;

	fft = FFT(b.bufnum, in);

	array = MFCC.kr(fft, numcoeff: f);

	array.size.postln;

	Out.kr(0,array);

	Out.ar(0,Pan2.ar(in));
}.play
)


//poll coefficients
c.getn(f,{arg val; {val.plot;}.defer});


//Continuous graphical display of MFCC values; free routine before closing window

(
var ms;

n = NetAddr("127.0.0.1", 57220); // 57220 is the default port number for analog server

w = GUI.window.new("MFCC coefficients", Rect(200, 400, f * 20 + 40, 600));

ms = GUI.multiSliderView.new(w, Rect(10, 10, f * 20, 580));

ms.value_(Array.fill(f, 0.0));
ms.valueThumbSize_(20.0);
ms.indexThumbSize_(20.0);
ms.gap_(0);

w.front;

r = {

	inf.do{
    c.get({arg val; {
      if(f == 13,
        {
          n.sendMsg("/a"
            , val.at(0)
            , val.at(1)
            , val.at(2)
            , val.at(3)
            , val.at(4)
            , val.at(5)
            , val.at(6)
            , val.at(7)
            , val.at(8)
            , val.at(9)
            , val.at(10)
            , val.at(11)
            , val.at(12));
        },
        {
          n.sendMsg("/a"
            , val.at(0)
            , val.at(1)
            , val.at(2)
            , val.at(3)
            , val.at(4)
            , val.at(5)
            , val.at(6));
        }
      );
      ms.value_(val);
    }.defer});

    (1.0 / 60.0).wait; //animation running at 60 frames per second
	};

}.fork;

)


//tidy up
(
r.stop;
b.free;
c.free;
x.free;
w.close;
)
