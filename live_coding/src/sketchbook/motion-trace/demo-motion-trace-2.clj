(ns lv.demo)
(use 'lv.core)

(layout "grid" [mov0 mt1] [mopr2 ras3])
(movie "4movies/*.mp4")

; movie as input stream
(rgb :mov0 [c0 c1 c2 c3]
  (sy 1.5)
  (rgb (pos? (sin (* 0.13 f))
             c1.rgb
             c2.rgb))
)
(render 0)

(expr :mt1 [mov0]
  (mode (* (usin (* 0.031 f)) 2.1))
  (trace 75)
  (paint -1)
  (count 30)
  (qual 0.01)
  (min 10)
)
(render 1)

(expr :mopr2 [mt1]
  "ddeeb"
)
(render 2)

(rgb :ras3 [mopr2 ras3]
  (rgb (mix (* 8 mopr2.rgb)
            ras3.rgb
            0.97))
)
(render 3)

(render)
