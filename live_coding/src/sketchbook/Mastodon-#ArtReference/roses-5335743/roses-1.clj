(ns lv.demo)
(use 'lv.core)

(layout "grid" [img0 img1 ras2] [cline3 ras4 ras5])
(image "Mastodon-#ArtReference/roses-5335743/*.png")
(render)

(rgb :img0 [c0 c1 c2]
  (rgb (mix c0.rgb c1.rgb (snoise (* f 0.013) y)))
)

(rgb :img1 [c0 c1 c2]
  (rgb (mix c1.rgb c2.rgb (snoise x (* f 0.011))))
)

(rgb :ras2 [img0 img1]
  (rgb (mix img0.rgb img1.rgb (unoise 0.5 (* f 0.023))))
)

; find contours center points
; and display color names
(expr :cline3 [ras2]
  (count 100)
  (max 250)
  (thres 0.6)
  (fill 1)
  (names -1)
  (clear 1)
  (paint 1)
  (holes 1)
)

(rgb :ras4 [cline3 ras4]
  (rgb (mix ras4.rgb cline3.rgb 0.1))
)
(rgb :ras5 [ras2 ras4]
  (rgb (* ras2.rgb ras4.rgb))
)
