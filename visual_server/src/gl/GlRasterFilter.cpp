#include "ofMain.h"
#include "globalvars.h"
#include "OscListener.h"
#include "GlRasterFilter.h"

GlRasterFilter::GlRasterFilter(ofGLWindowSettings settings, OscListener * oscParams) :
        Filter(settings, oscParams) {
}

GlRasterFilter::~GlRasterFilter() {
    fbo0.clear();
    fbo1.clear();
}

void GlRasterFilter::setup(const std::string id, int numSamples) {
    Filter::setup(id, numSamples);
    fbo0.allocate(filterWidth, filterHeight, GL_RGBA32F_ARB, 0); //TODO use numSamples instead of 0
    fbo1.allocate(filterWidth, filterHeight, GL_RGBA32F_ARB, 0); //TODO use numSamples instead of 0
#ifdef WITH_openCL
    clAnalysisImage0.initFromTexture(fbo0.getTexture());
    clAnalysisImage1.initFromTexture(fbo1.getTexture());
#endif // WITH_openCL
}

void GlRasterFilter::reload(ofxOscMessage const &msg) {
    auto narg = msg.getNumArgs();
    if(narg > 1) {
        Filter::setInputTexture(msg.getArgAsString(1));
        if(narg > 2) {
            Filter::compileShader(msg.getArgAsString(0), msg.getArgAsString(3));
        }
    }
}

ofTexture& GlRasterFilter::getTexture() {
    return getFbo().getTexture();
}

ofFbo& GlRasterFilter::getFbo() {
    return ringIndex == 0 ? fbo0 : fbo1;
}

#ifdef WITH_openCL
msa::OpenCLImage& GlRasterFilter::getAnalysisImage() {
    return ringIndex == 0 ? clAnalysisImage1 : clAnalysisImage0;
}
#endif // WITH_openCL

void GlRasterFilter::draw(int li) {
    (ringIndex == 0 ? fbo1 : fbo0).begin();
    ofClear(0, 0, 0, 255);

    shader.begin();
    sendUniforms(shader, li);

    auto tx = 0;
    for(std::vector<std::string>::iterator it = upstream_ids.begin(); it != upstream_ids.end(); ++it) {
        std::string in_id = *it;
        if(filterDict.find(in_id) != filterDict.end()) {
            auto f = filterDict[in_id];
            if(f)
                shader.setUniformTexture("tex" + to_string(tx), f->getTexture(), tx);
        }
        ++tx;
    }

    ofDrawRectangle(0, 0, filterWidth, filterHeight);

    shader.end();
    (ringIndex == 0 ? fbo1 : fbo0).end();
}

void GlRasterFilter::finish() {
    ringIndex = (ringIndex == 0 ? 1 : 0);
}
