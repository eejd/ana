# CPU executed functions
This will be translated to byte running on the CPU

[[_TOC_]]

## Parameter for filters
TODO

## Expressions
### Simple arithmetic
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| + | 2 .. n | sum of all parameters | (+ 0.1 x)<br><br>(+ 1 2 3) |
| - | 2 .. n | subtract from first parameter all following parameters | (- 0.1 x)<br><br>(- 1 2 3) |
| * | 2 .. n | multiply all parameters | (* 0.1 x)<br><br>(* 0.05 f x) |
| / | 2 .. n | dived the first parameter by all following parameters | (/ x 2)<br><br>(+ 16 4 2) |
| % | 2 | modulo | (% i 4) |
| sqr | 1 | square of x<br>(* x x) | (sqr x) |
| sqrt | 1 | square root | (sqrt x) |

### Conditionals
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| zero? | 3 | If the first term evaluates to 0 the second term is evaluated. If not the third term is evaluated. | (zero?<br>(/ x 1000)<br>1<br>(+ 1 x)) |
| pos? | 3 | If the first term evaluates to an positive number the second term is evaluated. If not the third term is evaluated. | (pos?<br>x<br>x<br>(* -1.0 x)) |

### Common functions
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| abs | 1 | absolute value | (abs x) |
| floor | 1 | nearest integer less than x | (floor x) |
| ceil | 1 | nearest integer greater than x | (ceil x) |
| clamp | 3 | returns (min (max x minVal) maxVal) | (clamp x 0.4 0.6) |
| sign | 1 | signum function | (sign y) |
| trunc | 1 | truncate | (trunc 1.3) |
| round | 1 | round to nearest | (round x) |

### Exponential functions
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| pow | 2 | returns x raised to the y power | (pow x y) |
| log | 1 | returns the natural logarithm of x | (log (abs x)) |
| log2 | 1 | returns the binary logarithm of x | (log2 (abs x)) |

### Trigonometry functions
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| cos | 1 | cosine | (cos 0) |
| ucos | 1 | like cosine but returns values in the range 0.0 to 1.0 | (cos (* 0.1 f)) |
| acos | 1 | inverse cosine | (acos 0) |
| sin | 1 | sine | (sin 1.57) |
| usin | 1 | like sine but returns values in the range 0.0 to 1.0 | (usin -0.5) |
| asin | 1 | inverse sine | (asin 0) |
| tan | 1 | tangent | (tan 0.5) |
| atan | 2 | arc tanget for y over x | (atan y x) |

### Merge values
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| max | 2 | Maximum value | (max 0 x) |
| min | 2 | Minimum value | (min f 100) |
| mix | 3 | linear blend of x and y | (mix x y 0.75) |

### Noise functions
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| rand | 0 | random numbers range 0.0 to 1.0 | (rand) |
| snoise | 3 | simplex noise function | (snoise x y z) |
| unoise | 3 | like noise but returns values approximately in the range 0.0 to 1.0 | ((unoise x y z)) |
| turb | 4 | turbulence noise function<br>last parameter stets the octaves | (turb 0.1 0.2 0.3 3) |

see: http://mrl.nyu.edu/~perlin/noise/

### Procedural textures functions
seen: Darwyn Peachey: Building Procedural Textures, in Texturing and Modeling, 1994

| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| step | 2 |  | (step y -0.99) |
| smooth | 3 | smooth step | (smooth -1 1 x) |
| pulse | 3 |  | (puls -1 1 x) |
| clamp | 3 |  | (clamp -1 1 x) |
| box | 3 |  | (box -1 1 x) |
| gamma | 2 |  | (gamma 0.7 x) |
| gain | 2 |  | (gain 0.8 0.5) |
| bias | 2 |  | (bias 0.7 x) |


### Geometric functions
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| distance | 4 | euclidean distance between two points  | (TODO) |
| mdist | 4 | Manhattan distance between two points  | (mdist 11 12 x y) |
