#pragma once

#include <opencv2/opencv.hpp>

#include "OscListener.h"
#include "Filter.h"
#include "calc/ScalarLisp.h"

class LineTrace {
	public:
    vector<ofFloatColor> colors;
    vector<ofPoint> from;
    vector<ofPoint> to;
    void append(ofFloatColor c, ofPoint p0, ofPoint p1);
    void clear();
    void draw();
};

class LineTracer {
	public:
	deque<LineTrace *> frameQueue;
    void append(LineTrace * line, size_t traceLenght);
    void draw();
};

class CvMotionTrace : public Filter {
	public:
		CvMotionTrace(ofGLWindowSettings settings, OscListener * oscParams);
		virtual ~CvMotionTrace();
		virtual void setup(const std::string id, int numSamples);
		void reload(ofxOscMessage const &msg);
		virtual void update();
		virtual void draw(int li);
		virtual void finish();

	private:
		void drawFeaturePoints();

        ofPixels grabbedPixels;
        bool hasData = false;

        cv::Mat frame_captured;
        cv::Mat frame_gray;
	    cv::Mat frame0;
	    cv::Mat frame1;
	    cv::Mat frame0Gray;
	    cv::Mat frame1Gray;
	    vector<cv::Point2f> pts;
	    vector<cv::Point2f> nextPts;
	    vector<unsigned char> status;
	    vector<float> err;
		int frameIndex = 0;

        // ContourMotionTrace
        ScalarLisp slFeaturePoints;
        size_t featurepoints = 32;
        ScalarLisp slQuality;
        double qualityLevel = 0.1;
        ScalarLisp slMinDistance;
        double minDistance = 50;
        ScalarLisp slTraceLenght;
        size_t traceLenght = 32;
        ScalarLisp slMode;
        int mode = 0;
        ScalarLisp slClear;
        ScalarLisp slUpstream;
	    LineTracer linetraces;
};
