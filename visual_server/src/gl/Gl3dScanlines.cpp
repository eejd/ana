#include "ofMain.h"

#include "stringhelper.h"
#include "Configuration.h"
#include "globalvars.h"
#include "Filter.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispMng.h"
#include "gl/Gl3dScanlines.h"

Gl3dScanlines::Gl3dScanlines(ofGLWindowSettings settings, OscListener * oscParams) : Filter(settings, oscParams){
}

Gl3dScanlines::~Gl3dScanlines(){
    if(vert_color) {
        buildin_shader.unload();
        glDeleteBuffers(1, &vertexbufferID);
        glDeleteVertexArrays(1, &vertexArrayID);
        delete [] vert_color;
        vert_color = nullptr;
    }
}

static std::string builtinVert = STRINGIFY(#version 150\n
uniform mat4 mvpMatrix;\n
in vec3  position;\n
in vec3  color;\n
out vec3 colorVarying;\n
void main() {\n
    gl_Position = mvpMatrix * vec4(position, 1.0);\n
    colorVarying = color;\n
}\n
);

static std::string builtinFrag = STRINGIFY(#version 150\n
in vec3 colorVarying;\n
out vec4 fragColor;\n
void main (void) {\n
    fragColor = vec4(colorVarying, 1.0);\n
}\n
);


void Gl3dScanlines::setup(const std::string id, int numSamples){
	Filter::setup(id, numSamples);
    vert_color = new GLfloat[max_vert_color_index];

    std::string okV = buildin_shader.setupShaderFromSource(GL_VERTEX_SHADER, builtinVert) ? "OK" : "failed";
    std::string okF = buildin_shader.setupShaderFromSource(GL_FRAGMENT_SHADER, builtinFrag) ? "OK" : "failed";
    std::string okP = buildin_shader.linkProgram() ? "OK" : "failed";
    ofLog(OF_LOG_NOTICE) << "shader compiler " << id << ": vertex=" << okV << " fragment=" << okF <<  " link=" << okP;
    progID = buildin_shader.getProgram();

    glGenVertexArrays(1, &vertexArrayID);
    glBindVertexArray(vertexArrayID);

    glGenBuffers(1, &vertexbufferID);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbufferID);
    glBufferData(GL_ARRAY_BUFFER, max_vert_color_index * sizeof(GLfloat), 0, GL_DYNAMIC_DRAW);

    // Specify the layout of the vertex data
    posAttrib = glGetAttribLocation(progID, "position");
    glEnableVertexAttribArray(posAttrib);
    glVertexAttribPointer(
            posAttrib,           // attribute
            3,                   // size
            GL_FLOAT,            // type
            GL_FALSE,            // normalized
            per_vert_color_stride * sizeof(GLfloat), // stride
            0);                  // array buffer offset

    colAttrib = glGetAttribLocation(progID, "color");
    glEnableVertexAttribArray(colAttrib);
    glVertexAttribPointer(
            colAttrib,           // attribute
            3,                   // size
            GL_FLOAT,            // type
            GL_FALSE,            // normalized
            per_vert_color_stride * sizeof(GLfloat), // stride
            (void*)(3 * sizeof(GLfloat))); // array buffer offset

    glBindVertexArray(0);
}

void Gl3dScanlines::reload(ofxOscMessage const &msg) {
    auto narg = msg.getNumArgs();
    if(narg > 1) {
        Filter::setInputTexture(msg.getArgAsString(1));
    }
    ScalarLispMng mng;
    mng.compile(slMode, "mode", "", msg);
    mng.compile(slDx, "dx", "10", msg);
    mng.compile(slDy, "dy", "10", msg);
    mng.compile(slDrift, "drift", "", msg);
    mng.compile(slJitter, "jitter", "", msg);
    mng.compile(slRotX, "rotx", "0", msg);
    mng.compile(slRotZ, "rotz", "0", msg);
    mng.compile(slFoV, "fov", "60", msg);
    mng.compile(slX, "x", "x", msg);
    mng.compile(slY, "y", "y", msg);
    mng.compile(slZ, "z", "z", msg);
    mng.compile(slClear, "clear", "1", msg);
}

void Gl3dScanlines::update() {
#ifdef TRACE_MONOTONIC
    timespec time1, time2;
    clock_gettime(CLOCK_MONOTONIC, &time1);
#endif // TRACE_MONOTONIC
    src_pixel_width = src_pixel_height = 0;
    src_pd = ctrl_pd = nullptr;
    mode = slMode.rpn.execute0();
	if(upstream_ids.size() >= 1) {
		auto in_id = upstream_ids[0];
		if(filterDict.find(in_id) != filterDict.end()) {
			auto f = filterDict[in_id];
			if(f) {
				src_pixel_width = src_pixels.getWidth();
				src_pixel_height = src_pixels.getHeight();
				f->getTexture().readToPixels(src_pixels);
			    src_pd = src_pixels.getData();
			    ctrl_pd = src_pd;
			}
		}
	}
	// try to find the control upstream filter
	// if there is non control filter then ctrl_pd points to src_pd
    if(src_pd && upstream_ids.size() >= 2) {
        auto in_id = upstream_ids[1];
        if(filterDict.find(in_id) != filterDict.end()) {
            auto f = filterDict[in_id];
            if(f) {
                f->getTexture().readToPixels(ctrl_pixels);
                if(src_pixel_width == ctrl_pixels.getWidth() && src_pixel_height == ctrl_pixels.getHeight()) {
                    ctrl_pd = ctrl_pixels.getData();
                }
            }
        }
    }
#ifdef TRACE_MONOTONIC
    clock_gettime(CLOCK_MONOTONIC, &time2);
    ofLog(OF_LOG_NOTICE) << "Gl3dScanlines::update    " << (time2.tv_sec * 1000000000l + time2.tv_nsec) - (time1.tv_sec * 1000000000l + time1.tv_nsec);
#endif // TRACE_MONOTONIC
}

//#define TRACE_MONOTONIC
#undef TRACE_MONOTONIC
void Gl3dScanlines::draw(int li){
	if(src_pixel_width < 1 || src_pixel_height < 1 || src_pd == nullptr || ctrl_pd == nullptr || upstream_ids.size() < 1)
		return;

#ifdef TRACE_MONOTONIC
    timespec time3, time4;
    clock_gettime(CLOCK_MONOTONIC, &time3);
#endif // TRACE_MONOTONIC
    fbo.begin();
    if(slClear.rpn.execute0() >= 0.0) {
        ofClear(0, 0, 0, 255);
    }

    float dx = (float)std::max(0.001, slDx.rpn.execute0());
    float dy = (float)std::max(0.001, slDy.rpn.execute0());

    float rx = slRotX.rpn.execute0();
    float rz = slRotZ.rpn.execute0();

    // see: http://www.opengl-tutorial.org/beginners-tutorials/tutorial-3-matrices/
    glm::mat4 modelTranslateMatrix = glm::translate(glm::mat4(), glm::vec3(-0.5f, -0.5f, 0.0f));
    glm::mat4 modelRotateMatrix = glm::rotate(rx, glm::vec3(1, 0, 0)) * glm::rotate(rz, glm::vec3(0, 0, 1));

    float fov = std::max(5.0f, std::min(175.0f, (float)slFoV.rpn.execute0()));
    // Generates a really hard-to-read matrix, but a normal, standard 4x4 matrix nonetheless
    glm::mat4 projectionMatrix = glm::perspective(
        (float)glm::radians(fov), // The vertical Field of View, in radians: the amount of "zoom". Think "camera lens". Usually between 90° (extra wide) and 30° (quite zoomed in)
        (float)filterWidth / (float)filterHeight,       // Aspect Ratio. Depends on the size of your window. Notice that 4/3 == 800/600 == 1280/960, sounds familiar ?
        0.01f,             // Near clipping plane. Keep as big as possible, or you'll get precision issues.
        10.0f              // Far clipping plane. Keep as little as possible.
    );

    glm::mat4 viewMatrix = glm::lookAt(
        glm::vec3(0, 0, 1), // the position of your camera, in world space
        glm::vec3(0, 0, 0), // where you want to look at, in world space
        glm::vec3(0, 1, 0)  // up vector
    );

    glm::mat4 mvpMatrix = projectionMatrix * viewMatrix * modelRotateMatrix * modelTranslateMatrix;

    glUseProgram(progID);
    GLuint mvpLoc = glGetUniformLocation(progID, "mvpMatrix");
    glUniformMatrix4fv(mvpLoc, 1, GL_FALSE, &mvpMatrix[0][0]);

    glBindVertexArray(vertexArrayID);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbufferID);
    glEnableVertexAttribArray(posAttrib);
    glEnableVertexAttribArray(colAttrib);

    ofPixelFormat pf = src_pixels.getPixelFormat();
    const size_t per_pixel = pf == OF_PIXELS_RGBA ? 4 : 3;
    switch(mode) {
    case 1: {
        dx *= filterWidth;
        dy *= filterHeight;
        for(float ix=0; ix<=src_pixel_width; ix+=dx) {
            size_t vert_color_index = 0;
            float ix_drift = ix + slDrift.rpn.execute3d1i(ix, 0.0, 0.0, 0);
            for(float iy=0; iy<src_pixel_height; iy+=dy) {
                if(vert_color_index < max_vert_color_index && ix_drift >= 0 && ix_drift < filterWidth) {
                    size_t pdi = ((size_t)ix_drift + (size_t)iy * filterWidth) * per_pixel;
                    double jitter = slJitter.rpn.execute4d3i(ctrl_pd[pdi], ctrl_pd[pdi+1], ctrl_pd[pdi+2], ix / src_pixel_width, ix, iy, src_pixel_width);
                    double map_x = slX.rpn.execute4d3i(ix_drift / filterWidth, iy / filterHeight, jitter, ix / src_pixel_width, ix, iy, src_pixel_width);
                    double map_y = slY.rpn.execute4d3i(ix_drift / filterWidth, iy / filterHeight, jitter, ix / src_pixel_width, ix, iy, src_pixel_width);
                    double map_z = slZ.rpn.execute4d3i(ix_drift / filterWidth, iy / filterHeight, jitter, ix / src_pixel_width, ix, iy, src_pixel_width);
                    vert_color[vert_color_index] = map_x;
                    vert_color[vert_color_index + 1] = map_y;
                    vert_color[vert_color_index + 2] = map_z;
                    vert_color[vert_color_index + 3] = src_pd[pdi];
                    vert_color[vert_color_index + 4] = src_pd[pdi + 1];
                    vert_color[vert_color_index + 5] = src_pd[pdi + 2];
                    vert_color_index += per_vert_color_stride;
                }
            }
            glBufferSubData(GL_ARRAY_BUFFER, 0, vert_color_index * sizeof(GLfloat), &vert_color[0]);
            glDrawArrays(GL_LINE_STRIP, 0, vert_color_index/per_vert_color_stride);
        }
        }
        break;
    case 2: {
        dx *= filterWidth;
        size_t w2 = filterWidth  / 2;
        size_t h2 = filterHeight / 2;
        for(float ir=0; ir<=src_pixel_width/2; ir+=dx) {
            size_t vert_color_index = 0;
            for(float ia=0; ia<TWO_PI; ia+=(TWO_PI*dy)) {
                float cx = w2 + ir * std::cos(ia);
                float cy = h2 + ir * std::sin(ia);
                if(vert_color_index < max_vert_color_index && cx >= 0 && cx < filterWidth && cy >= 0 && cy < filterHeight) {
                    size_t pdi = ((size_t)cx + (size_t)cy * filterWidth) * per_pixel;
                    double jitter = slJitter.rpn.execute4d3i(ctrl_pd[pdi], ctrl_pd[pdi+1], ctrl_pd[pdi+2], ir / src_pixel_width, ir, ia, src_pixel_width);
                    double map_x = slX.rpn.execute4d3i(cx / filterWidth, cy / filterHeight, jitter, cx / src_pixel_width, cx, cy, src_pixel_width);
                    double map_y = slY.rpn.execute4d3i(cx / filterWidth, cy / filterHeight, jitter, cx / src_pixel_width, cx, cy, src_pixel_width);
                    double map_z = slZ.rpn.execute4d3i(cx / filterWidth, cy / filterHeight, jitter, cx / src_pixel_width, cx, cy, src_pixel_width);
                    vert_color[vert_color_index] = map_x;
                    vert_color[vert_color_index + 1] = map_y;
                    vert_color[vert_color_index + 2] = map_z;
                    vert_color[vert_color_index + 3] = src_pd[pdi];
                    vert_color[vert_color_index + 4] = src_pd[pdi + 1];
                    vert_color[vert_color_index + 5] = src_pd[pdi + 2];
                    vert_color_index += per_vert_color_stride;
                }
            }
            glBufferSubData(GL_ARRAY_BUFFER, 0, vert_color_index * sizeof(GLfloat), &vert_color[0]);
            glDrawArrays(GL_LINE_LOOP, 0, vert_color_index/per_vert_color_stride);
        }
        }
        break;
    default: {
        dx *= filterWidth;
        dy *= filterHeight;
        for(float iy=0; iy<src_pixel_height; iy+=dy) {
            size_t vert_color_index = 0;
            float iy_drift = iy + slDrift.rpn.execute3d1i(iy, 0.0, 0.0, 0);
            for(float ix=0; ix<=src_pixel_width; ix+=dx) {
                if(vert_color_index < max_vert_color_index && iy_drift >= 0 && iy_drift < filterHeight) {
                    size_t pdi = ((size_t)ix + (size_t)(iy_drift) * filterWidth) * per_pixel;
                    double jitter = slJitter.rpn.execute4d3i(ctrl_pd[pdi], ctrl_pd[pdi+1], ctrl_pd[pdi+2], ix / src_pixel_width, ix, iy, src_pixel_width);
                    double map_x = slX.rpn.execute4d3i(ix / filterWidth, iy_drift / filterHeight, jitter, ix / src_pixel_width, ix, iy, src_pixel_width);
                    double map_y = slY.rpn.execute4d3i(ix / filterWidth, iy_drift / filterHeight, jitter, ix / src_pixel_width, ix, iy, src_pixel_width);
                    double map_z = slZ.rpn.execute4d3i(ix / filterWidth, iy_drift / filterHeight, jitter, ix / src_pixel_width, ix, iy, src_pixel_width);
                    vert_color[vert_color_index] = map_x;
                    vert_color[vert_color_index + 1] = map_y;
                    vert_color[vert_color_index + 2] = map_z;
                    vert_color[vert_color_index + 3] = src_pd[pdi];
                    vert_color[vert_color_index + 4] = src_pd[pdi + 1];
                    vert_color[vert_color_index + 5] = src_pd[pdi + 2];
                    vert_color_index += per_vert_color_stride;
                }
            }
            glBufferSubData(GL_ARRAY_BUFFER, 0, vert_color_index * sizeof(GLfloat), &vert_color[0]);
            glDrawArrays(GL_LINE_STRIP, 0, vert_color_index/per_vert_color_stride);
        }
        }
        break;
    }

    glDisableVertexAttribArray(posAttrib);
    glDisableVertexAttribArray(colAttrib);
    glBindVertexArray(0);
    glUseProgram(0);

    fbo.end();
#ifdef TRACE_MONOTONIC
    clock_gettime(CLOCK_MONOTONIC, &time4);
    ofLog(OF_LOG_NOTICE) << "Gl3dScanlines::ofDrawLine " << (time4.tv_sec * 1000000000l + time4.tv_nsec) - (time3.tv_sec * 1000000000l + time3.tv_nsec);
#endif // TRACE_MONOTONIC
}
