#include <limits>
#include <math.h>
#include <string>

#include "ofMain.h"

#include "ana_features.h"
#include "color/Palette.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "lsys/LsysMachine.h"

//#define TRACE_MONOTONIC
#undef  TRACE_MONOTONIC

LsysMachine::LsysMachine() {
    rule_buf0 = new char[maxString];
    std::memset(rule_buf0, 0, maxString);
    src_string = rule_buf0;

    sl_buf0 = (ScalarLisp **)std::malloc(sizeof(ScalarLisp *) * maxString);

    rule_buf1 = new char[maxString];
    std::memset(rule_buf1, 0, maxString);
    dst_string = rule_buf1;

    sl_buf1 = (ScalarLisp **)std::malloc(sizeof(ScalarLisp *) * maxString);
}

LsysMachine::~LsysMachine() {
    delete rule_buf0;
    delete rule_buf1;
    free(sl_buf0);
    free(sl_buf1);
}

char LsysMachine::mapChar(const std::string & src, size_t i) {
    if(src[i] > 32 || src[i] < 127) {
        return src[i];
    }
    err = 2;
    ofLog(OF_LOG_ERROR) << "not a printable ASCII character " << (int)src[i] << "";
    return 0;
}

void LsysMachine::setIgnore(std::string ignore0) {
    int ix = maxCharacters;
    while(ix-- > 0)
        ignoreCharacter[ix] = false;
    ix = ignore0.size();
    while(ix-- > 0)
        ignoreCharacter[(size_t) (mapChar(ignore0, ix))] = true;
}

bool LsysMachine::isLeNumber(char c) {
    return (c >= '0' &&  c <= '9') || c == '.' || c == '-' || c == '+';
}

// process successor to separate rules from lisp expressions
void LsysMachine::setParamtericSuccessor(const std::vector<std::string> &successor, size_t pred_char, size_t vi) {
    int pcount = 0;
    len_rule_pred[pred_char][numRules[pred_char]] = 0;
    std::string le;
    bool le_number = true;
    size_t co = 0;
    int slo = -1;
    for(size_t ci = 0; ci < successor[vi].size(); ++ci) {
        char c = mapChar(successor[vi], ci);
        switch(c) {
        case '(':
            if(pcount > 0)
                le += '(';
            ++pcount;
            break;
        case ')':
            --pcount;
            if(pcount == 0) {
                if(slo < 0 || slo >= maxSuccessorCharacters) {
                    err = 7;
                    ofLog(OF_LOG_ERROR) << "misplaced lisp expression " << le << "";
                    return;
                }
                ScalarLisp* sl = new ScalarLisp();
                int comperr = sl->compile(le_number ? le : ("(" + le + ")")); //TODO
                if(comperr == 0) {
                    rule_pred_sl[pred_char][numRules[pred_char]][slo] = sl;
                }
                else {
                    err = 4 + 100 * comperr;
                    ofLog(OF_LOG_ERROR) << "invalid lisp expression. error=" << comperr << " Can not compile " << le;
                }
                le.clear();
                le_number = true;
            }
            else {
                le += ')';
            }
            break;
        default:
            if(pcount == 0) {
                rule_pred[pred_char][numRules[pred_char]][co] = c;
                ++len_rule_pred[pred_char][numRules[pred_char]];
                slo = co;
                if(co+1 < maxSuccessorCharacters)
                    ++co;
                else {
                    err = 6;
                    ofLog(OF_LOG_ERROR) << "successor has more than " << maxSuccessorCharacters << " characters";
                    return;
                }
            } else {
                le_number = le_number && isLeNumber(c);
                le += c;
            }
            break;
        }
    }
}

void LsysMachine::setLsys(std::string ignore0, std::string axiom0, std::vector<std::string> predecessor0, std::vector<std::string> successor0, std::vector<std::string> leftContext0, std::vector<std::string> rightContext0) {
    if(predecessor0.size() != successor0.size()) {
        err = 8;
        ofLog(OF_LOG_ERROR) << "number of successors and predecessors differs " << predecessor0.size() << " " << successor0.size();
        return;
    }

    code.str("");
    code << "ignore " << ignore0 << "\n";
    code << "axiom  " << axiom0 << "\n";
    for(size_t vi = 0; vi < predecessor0.size(); ++vi) {
        if(vi < leftContext0.size() && leftContext0[vi].size()) { code << leftContext0[vi] << " < "; }
        code << predecessor0[vi];
        if(vi < rightContext0.size() && rightContext0[vi].size()) { code << " > " << rightContext0[vi]; }
        code  << " -> "<< successor0[vi] << "\n";
    }

    err = 0;
    dst_length = 0;
    std::memset(rule_buf0, 0, maxString);
    std::memset(rule_buf1, 0, maxString);
    std::memset(sl_buf0, 0, sizeof(ScalarLisp *) * maxString);
    std::memset(sl_buf1, 0, sizeof(ScalarLisp *) * maxString);
    std::memset(rule_pred_sl, 0, sizeof(ScalarLisp *) * maxCharacters * maxRules * maxSuccessorCharacters);

    setIgnore(ignore0);
    int ix = maxCharacters;
    while(ix-- > 0)
        ignoreCharacter[ix] = false;
    ix = ignore0.size();
    while(ix-- > 0)
        ignoreCharacter[(size_t) (mapChar(ignore0, ix))] = true;

    axiom = axiom0;
    typeOfLSystem = sys0;
    size_t index = maxCharacters;
    while(index-- > 0)
        numRules[index] = 0;
    for(size_t vi = 0; vi < predecessor0.size(); ++vi) {
        size_t pred_char = mapChar(predecessor0[vi], 0);

        setParamtericSuccessor(successor0, pred_char, vi);

        // D0L and 0L are context free
        lContext[pred_char][numRules[pred_char]] = 0;
        rContext[pred_char][numRules[pred_char]] = 0;
        // get context for IL systems
        if(leftContext0.size() > 0 && rightContext0.size() > 0) {
            typeOfLSystem = sys2L;
            lContext[pred_char][numRules[pred_char]] = mapChar(leftContext0[vi], 0);
            rContext[pred_char][numRules[pred_char]] = mapChar(rightContext0[vi], 0);
        } else if(leftContext0.size() > 0 && rightContext0.size() == 0) {
            typeOfLSystem = sysIL;
            lContext[pred_char][numRules[pred_char]] = mapChar(leftContext0[vi], 0);
        } else if(leftContext0.size() == 0 && rightContext0.size() > 0) {
            typeOfLSystem = sysIL;
            rContext[pred_char][numRules[pred_char]] = mapChar(rightContext0[vi], 0);
        }

        ++numRules[pred_char];
    }

    // check if Lindenmayer system is not deterministic
    if(typeOfLSystem < sysOL) {
        int rx = maxCharacters;
        while(rx-- != 0) {
            if(numRules[rx] > 1) {
                typeOfLSystem = sysOL;
                break;
            }
        }
    }

    if(typeOfLSystem < sysDOL)
        typeOfLSystem = sysDOL;
}

void LsysMachine::expand(size_t depth) {
#ifdef TRACE_MONOTONIC
    timespec time1, time2;
    clock_gettime(CLOCK_MONOTONIC, &time1);
#endif // TRACE_MONOTONIC
    size_t actDepth;
    size_t character;
    size_t selection;
    size_t actPos;
    size_t hits[maxRules];

    src_length = axiom.size();

    // special case: do not expand; copy axiom instead
    if(depth < 1) {
        int index = src_length;
        while(index-- > 0) {
            rule_buf1[index] = axiom[index];
            sl_buf1[index] = nullptr;
        }
        dst_string = rule_buf1;
        dst_sl = sl_buf1;
        dst_length = axiom.size();
        return;
    }

    // expand; start with axiom
    int index = src_length;
    while(index-- > 0) {
        rule_buf0[index] = axiom[index];
        rule_buf1[index] = axiom[index];
        sl_buf0[index] = nullptr;
        sl_buf1[index] = nullptr;
    }

    for (actDepth=1; actDepth<=depth; actDepth++) {
        dst_length = 0;
        if(actDepth % 2 == 1) {
            src_string = rule_buf0;
            dst_string = rule_buf1;
            src_sl = sl_buf0;
            dst_sl = sl_buf1;
        }
        else {
            src_string = rule_buf1;
            dst_string = rule_buf0;
            src_sl = sl_buf1;
            dst_sl = sl_buf0;
        }

        switch(typeOfLSystem) {
        case sysDOL:
            for(actPos = 0; actPos < src_length; actPos++) {
                character = src_string[actPos];
                if(numRules[character] != 0) {
                    size_t len_pred = len_rule_pred[character][0];
                    if(dst_length + len_pred >= maxString) {
                        ofLog(OF_LOG_ERROR) << "production overflow: depth " << actDepth << ", size " << dst_length;
                        return;
                    }
                    std::memcpy(&dst_string[dst_length], &rule_pred[character][0][0], len_pred);
                    for(size_t rx=0; rx<len_pred; ++rx) {
                        dst_sl[dst_length+rx] = rule_pred_sl[character][0][rx];
                    }
                    dst_length += len_pred;
                } else {
                    if(src_length + dst_length + 1 >= maxString) {
                        ofLog(OF_LOG_ERROR) << "production overflow: depth " << actDepth << ", size " << dst_length;
                        return;
                    }
                    dst_string[dst_length] = character;
                    dst_sl[dst_length] = src_sl[actPos];
                    ++dst_length;
                }
            }
            break;

        case sysOL:
            for(actPos = 0; actPos < src_length; actPos++) {
                character = src_string[actPos];
                if(numRules[character] != 0) {
                    selection = std::rand() % numRules[character];
                    size_t len_pred = len_rule_pred[character][selection];
                    if(dst_length + len_pred >= maxString) {
                        ofLog(OF_LOG_ERROR) << "production overflow: depth " << actDepth << ", size " << dst_length;
                        return;
                    }
                    std::memcpy(&dst_string[dst_length], &rule_pred[character][selection][0], len_pred);
                    for(size_t rx=0; rx<len_pred; ++rx) {
                        dst_sl[dst_length+rx] = rule_pred_sl[character][selection][rx];
                    }
                    dst_length += len_pred;
                } else {
                    if(src_length + dst_length + 1 >= maxString) {
                        ofLog(OF_LOG_ERROR) << "production overflow: depth " << actDepth << ", size " << dst_length;
                        return;
                    }
                    dst_string[dst_length] = character;
                    dst_sl[dst_length] = src_sl[actPos];
                    ++dst_length;
                }
            }
            break;

        case sysIL:
        case sys2L:
            for(actPos = 0; actPos < src_length; actPos++) {
                character = src_string[actPos];

                int numHits = 0;
                for(int rx = 0; rx < numRules[character]; ++rx) {
                    bool contextFound = true;
                    if(lContext[character][rx] != 0)
                        contextFound = testLeftContext(actPos, lContext[character][rx]);
                    if(contextFound && rContext[character][rx] != 0)
                        contextFound = testRightContext(actPos, rContext[character][rx]);
                    if(contextFound) {
                        hits[numHits] = rx;
                        ++numHits;
                    }
                }
                if(numHits == 0) {
                    for(int rx = 0; rx < numRules[character]; ++rx) {
                        if(rContext[character][rx] == '\0' && lContext[character][rx] == '\0') {
                            hits[numHits] = rx;
                            ++numHits;
                        }
                    }
                }
                if(numHits != 0) {
                    selection = hits[std::rand() % numHits];
                    size_t len_pred = len_rule_pred[character][selection];
                    if(src_length + dst_length + len_pred >= maxString) {
                        ofLog(OF_LOG_ERROR) << "production overflow: depth " << actDepth << ", size " << dst_length;
                        return;
                    }
                    std::memcpy(&dst_string[dst_length], &rule_pred[character][selection][0], len_pred);
                    for(size_t rx=0; rx<len_pred; ++rx) {
                        dst_sl[dst_length+rx] = rule_pred_sl[character][selection][rx];
                    }
                    dst_length += len_pred;
                } else {
                    if(src_length + dst_length + 1 >= maxString) {
                        ofLog(OF_LOG_ERROR) << "production overflow: depth " << actDepth << ", size " << dst_length;
                        return;
                    }
                    dst_string[dst_length] = character;
                    dst_sl[dst_length] = src_sl[actPos];
                    ++dst_length;
                }
            }
            break;

        }
        dst_string[dst_length] = 0;
        src_length = dst_length;
    }
#ifdef TRACE_MONOTONIC
    clock_gettime(CLOCK_MONOTONIC, &time2);
    ofLog(OF_LOG_NOTICE) << "expand " << (time2.tv_sec * 1000000000l + time2.tv_nsec) - (time1.tv_sec * 1000000000l + time1.tv_nsec);
#endif // TRACE_MONOTONIC
}

/**
* Determines if testCharacter can be found "left" of an given position.
* @param actPos position
* @param testCharacter Occurence of this charater will be tested
* @return true if the left context condition is full filled
*/
bool LsysMachine::testLeftContext(size_t actPos, char testCharacter) {
    int branchCount = 0;
    char character = 0;
    --actPos;
    while(actPos >= 0 && ignoreCharacter[(size_t) src_string[actPos]])
        --actPos;
    if(actPos >= 0) {
        character = src_string[actPos];
        while(branchCount > 0 || character == '[' || character == ']') {
            if(character == '[') {
                if(branchCount > 0)
                    --branchCount;
            } else if(character == ']')
                ++branchCount;
            --actPos;
            while(actPos >= 0 && ignoreCharacter[(size_t) src_string[actPos]])
                --actPos;
            if(actPos >= 0)
                character = src_string[actPos];
            else
                return false;
        }
        return character == testCharacter;
    } else
        return false;
}

/**
* Determines if testCharacter can be found "right" of an given position.
* @param actPos position
* @param testCharacter Occurence of this charater will be tested
* @return true if the left context condition is full filled
*/
bool LsysMachine::testRightContext(size_t actPos, char testCharacter) {
    int branchCount = 0;
    char character;
    ++actPos;
    while(actPos < src_length && ignoreCharacter[(size_t) src_string[actPos]])
        ++actPos;
    if(actPos < src_length) {
        character = src_string[actPos];
        while(branchCount != 0 || character == '[' || character == ']') {
            if(character == '[')
                ++branchCount;
            else if(character == ']')
                --branchCount;
            if(branchCount < 0)
                return false;
            ++actPos;
            while(actPos < src_length && ignoreCharacter[(size_t) src_string[actPos]])
                ++actPos;
            if(actPos < src_length)
                character = src_string[actPos];
            else
                return false;
        }
        return character == testCharacter;
    } else
        return false;
}


const char * LsysMachine::charInterpretation() {
    return dst_string;
}

/**
 * Removes any non turtle characters from the string.
 */
void LsysMachine::postProcess() {
#ifdef WITH_testcases
    size_t old_dst_length = dst_length;
#endif // WITH_testcases
#ifdef TRACE_MONOTONIC
    timespec time1, time2;
    clock_gettime(CLOCK_MONOTONIC, &time1);
#endif // TRACE_MONOTONIC

    size_t ci = 0;
    size_t co = 0;
    while (ci < dst_length) {
        char character = dst_string[ci];
        if (character == 'F'
            || character == 'f'
            || character == '+'
            || character == '-'
            || character == '['
            || character == ']'
            || character == '|'
            || character == '{'
            || character == '}'
            || character == '.'
            || character == '\''
            || character == ','
            || character == ':'
            || character == ';') {
            dst_string[co] = character;
            dst_sl[co] = dst_sl[ci];
            ++co;
        }
        ++ci;
    }
    dst_length = co;
    dst_string[co] = 0;
#ifdef TRACE_MONOTONIC
    clock_gettime(CLOCK_MONOTONIC, &time2);
    ofLog(OF_LOG_NOTICE) << "postProcess " << (time2.tv_sec * 1000000000l + time2.tv_nsec) - (time1.tv_sec * 1000000000l + time1.tv_nsec);
#endif // TRACE_MONOTONIC
#ifdef WITH_testcases
    ofLog(OF_LOG_NOTICE) << "postProcess old dst_length=" << old_dst_length << ", new dst_length=" << dst_length;
#endif // WITH_testcases
}

const std::stringstream& LsysMachine::toCodeString() {
    return code;
}

