(ns lv.demo)
(use 'lv.core)

(layout "grid" [img0 cline1]
               [ras2 pix1])
; (image "Mastodon-#ArtReference/butterfly-354528/butterfly-354528_1920.jpg")
; (image "Mastodon-#ArtReference/for_glitch/mountain-6538890_960_720.jpg")
(image "Mastodon-#ArtReference/for_glitch/trees-5974614_960_720.jpg")

; load image to the texture unit
(rgb :img0 [c0]
	(ty 0.2)
	(rgb c0.rgb)
)

; find contours
(expr :cline1 [img0]
  (count 100)
  (max 2500)
  (thres 0.3)
  (fill 1)
  (paint -1)
  (holes 1)
)

; mix contours with original image
(rgb :ras2 [img0 cline1]
	(rgb (mix (* 2 img0.rgb)
	          cline1.rgb
						0.75))
)

; pixel sorter
(expr :pix1 [ras2]
	(b (+ 0 (usin (* 0.05 f))))
	(w 0.75)
	(comp (- v))
)
(render)
