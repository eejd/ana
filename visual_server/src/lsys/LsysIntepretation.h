#pragma once

#include "ana_features.h"
#include "color/Palette.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "lsys/LsysMachine.h"

class LsysIntepretation  {
public:
    LsysIntepretation();
    virtual ~LsysIntepretation();

    virtual void init(size_t length0, char* string0, ScalarLisp** sl0) = 0;
    virtual void initCanvas(int canvas_width0, int canvas_height0,
                    float borderFactor0);
    virtual void initTransformation();
    virtual void expandBoundigBox() = 0;
    virtual void reset_minmax();
    virtual void draw() = 0;

    size_t turtle_length = 0;
    char* turtle_string = nullptr;
    ScalarLisp** turtle_sl = nullptr;

    ScalarLisp slForward;
    ScalarLisp slAngel;

    Palette lut;

    // word to canvas mapping
    bool expand_only = false;
    float turtle_xmin = -0.1;
    float turtle_xmax =  0.1;
    float turtle_ymin = -0.1;
    float turtle_ymax =  0.1;
    float turtle_diffX = turtle_xmax - turtle_xmin;
    float turtle_diffY = turtle_ymax - turtle_ymin;
    float canvas_width = 1;
    float canvas_height = 1;
    float borderFactor = 0.01;
    float canvas_xmin = -0.1;
    float canvas_xmax =  0.1;
    float canvas_ymin = -0.1;
    float canvas_ymax =  0.1;
    float xscale_to_canvas = canvas_width  / turtle_diffX;
    float yscale_to_canvas = canvas_height / turtle_diffY;
    float scale_to_canvas = 1;
    float center_x = 0.5 * (turtle_xmax + turtle_xmin) * scale_to_canvas;
    float center_y = 0.5 * (turtle_ymax + turtle_ymin) * scale_to_canvas;

};
