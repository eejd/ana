(ns lv.demo)
(use 'lv.core)

(layout "grid" [ras0 frag1]
               [ras3 clag2])
(render)

; kp0    0.40625
; kp1    0.75781
; kp2    0.023622
; kp3    0.11024
; kp4    0
; kp5    0.36719
; kp6    0
; kp7    0.53125
;
; ks0    0.3125
; ks1    0.46875
; ks2    0.5
; ks3    0
; ks4    0
; ks5    0
; ks6    0.54688
; ks7    0.41406


(rgb :ras0 []
  (rot (* 0.1 f))
  (rgb (pos? (- kp7 r)
            (pos? (snoise y x)
               (vec3 0 1 0)
               (vec3 1 0 0))
            (vec3 0 0 0)))
)

(expr :frag1 [clag2]
  "particle-blender/dFdxy-2.frag"
)

(expr :clag2 [ras3]
  "particle-blender/particle-blender-1.cl"
  (paint 1)
  (p0 (* ks0 1))   ; vel
  (p7 (+ ks7 0.1)) ; non linear gray scale
)

(rgb :ras3 [clag2 ras0]
  (rot (* -0.01 f))
  (rgb (mix clag2.rgb ras0.rgb ks6))
)
