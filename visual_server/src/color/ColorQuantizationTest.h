#pragma once

#include "ana_features.h"

class ColorQuantizationTest {
public:
    ColorQuantizationTest();
    virtual ~ColorQuantizationTest();

#ifdef WITH_testcases
    static void runTestCases();

    static int test_exec_failed;
    static int test_syntax_failed;
#endif
};
