#include <math.h>
#include <random>

#include "ofMain.h"

#include "ana_features.h"
#include "calc/ColorNames.h"

//#pragma GCC push_options
//#pragma GCC optimize ("O0")

// handpicked color names
// https://github.com/meodai/color-names
// MIT License
// 
// visual example:
// http://cmuems.com/2019/60212/lsh/11/25/lsh-tli-arsculpture/

static const std::string colornames[] = {
#include "calc/massiveColorNames.inc"
};
static const float rgbcolor[] = {
#include "calc/massiveColorRGBfloat.inc"
};

static const size_t numcolors = sizeof(rgbcolor) / (sizeof(float) * 3);

std::string ColorNames::nearestRGB(float r, float g, float b) {
    size_t base = 0;
    float mindiff = 1E37;
    size_t m = 0;
    for(size_t cx=0; cx<numcolors; ++cx) {
            float cr = r - rgbcolor[base++];
            float cg = g - rgbcolor[base++];
            float cb = b - rgbcolor[base++];
            float rgbdiff = cr*cr + cg*cg + cb*cb;
            if(rgbdiff < mindiff) {
                mindiff = rgbdiff;
                m = cx;
            }
    }
    return colornames[m];
}

#ifdef WITH_testcases
#define TRACE_MONOTONIC
//#undef TRACE_MONOTONIC
void ColorNames::test() {
#ifdef TRACE_MONOTONIC
    timespec time1, time2;
    clock_gettime(CLOCK_MONOTONIC, &time1);
#endif // TRACE_MONOTONIC
    std::string name;
    for(size_t t=0; t<1000; ++t) {
        name = ColorNames::nearestRGB((float)0xe6, (float)0xe1, (float)0xd9);
    }
#ifdef TRACE_MONOTONIC
    clock_gettime(CLOCK_MONOTONIC, &time2);
    ofLog(OF_LOG_NOTICE) << "ColorNames::nearestRGB " << (time2.tv_sec * 1000000000l + time2.tv_nsec) - (time1.tv_sec * 1000000000l + time1.tv_nsec);
#endif // TRACE_MONOTONIC
    ofLog(OF_LOG_NOTICE) << "ColorNames::nearestRGB '" << name << "'";
//
//    size_t base = 0;
//    for(size_t t=0; t<numcolors; ++t) {
//        ofLog(OF_LOG_NOTICE) << rgbcolor[base] << ".0f, " << rgbcolor[base+1] << ".0f, " << rgbcolor[base+2] << ".0f,";
//        base += 3;
//    }
}
#endif // WITH_testcases
//#pragma GCC pop_options
