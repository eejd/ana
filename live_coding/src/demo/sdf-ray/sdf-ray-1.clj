(ns lv.demo)
(use 'lv.core)

(layout "grid" [mov0 frag1]
               [ras2 ras3])
(movie "4movies/*.mp4")

; ks0    0.51969
; ks1    0.3937
; ks2    0.086614
; ks7    0.41732

(texpr
  (t0 (/ 1 (* 2 ks7)))
)

; movie as input stream
(rgb :mov0 [c0 c1 c2 c3]
 (sy 1.5)
 (rgb c1.rgb)
)

(expr :frag1 [mov0]
 "frag/sdf-ray-1.frag"
 (a ks0)
 (b ks1)
 (c ks2)
 (mix ks3)
 (rx (* 10 (snoise (* 0.0011 f) -0.2  0.3)))
 (ry (* 10 (snoise (* 0.0013 f)  0.4 -0.1)))
 (rz (* 10 (snoise (* 0.0017 f)  0.2 -0.5)))
)

(rgb :ras2 [frag1]
  (rgb (pow (* 2 frag1.rgb)
            (vec3 t0 t0 t0)))
)
(render 2)
