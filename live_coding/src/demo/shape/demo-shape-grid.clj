(ns lv.demo)
(use 'lv.core)

(layout "vertical" [shape0 map2d1 mopr2 ras3])
(render)

(expr :shape0 []
  (count (* 12 24))
  (x (+ 50 (* 34 (% i 12))))
  (y (+ 120 (* 34 (floor (/ i 12)))))
  (size 21)
	(rot (+ 45 (* 0.4
		            (- y 900)
		            (sin (* 0.031 f))
								(snoise x y 0.1))))
	(res 4)
	(h 0.46) (s 0.48) (v 0.88)
)

(expr :map2d1 [shape0]
  (x (+ x (* 0.04
		         (- y 900)
		         (snoise (sin (* 0.017 f)) x y))))
  (y (+ y (* 0.04
		         (- y 900)
		         (snoise (sin (* 0.031 f)) y x))))
	(size 21)
  (rot z)
	(res 4)
	(h 0.1) (s 0.48) (v 0.88)
)

(expr :mopr2 [map2d1] "db")

(hsv :ras3 [ras3 mopr2]
  (h (+ mopr2.h 0.36))
  (s (mix ras3.s mopr2.s 0.1))
  (v (mix ras3.v mopr2.v 0.1))
)
