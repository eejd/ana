#pragma once

#include "ofMain.h"
#include "Filter.h"
#include "OscListener.h"
#include "calc/ScalarLisp.h"

class ShapeCounter : public Filter {
	public:
		ShapeCounter(ofGLWindowSettings settings, OscListener * oscParams);
		virtual ~ShapeCounter();
        virtual void setup(const std::string id, int numSamples);
        virtual void reload(ofxOscMessage const &msg);
        virtual void update();
        virtual void draw(int li);

	private:
        std::shared_ptr<Filter> upf = nullptr;
		static constexpr int num_counters = 10;
		ofTrueTypeFont	digit_font;
	    ScalarLisp slClear;
	    ScalarLisp slSize;
};
