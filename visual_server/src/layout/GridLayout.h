#pragma once
#include "Filter.h"
#include "Layout.h"

class GridLayout: public Layout {
public:
    GridLayout(int r, int c);
    virtual ~GridLayout();
    virtual void optTiles();
    virtual int numFilters();
    virtual void draw(int render, std::vector<std::shared_ptr<Filter>> &filterLine);

private:
    int rows = 2;
    int cols = 2;
    int canvas_width = 1920;
    int canvas_height = 1080;
    int ir_filter_width = 1;
    int ir_filter_height = 1;
    int left_border = 0;
    int top_border  = 0;
    float downscale = 1;
};
