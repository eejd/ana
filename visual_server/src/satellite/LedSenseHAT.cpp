#include "ofMain.h"
#include "Configuration.h"
#include "globalvars.h"
#include "OscListener.h"
#include "Filter.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispMng.h"
#include "satellite/LedSenseHAT.h"

LedSenseHAT::LedSenseHAT(ofGLWindowSettings settings, OscListener * oscParams) :
        Filter(settings, oscParams) {
    for(size_t iy = 0; iy < led_x_led; iy++) {
        for(size_t ix = 0; ix < led_x_led; ix++) {
            preview[ix][iy] = ofFloatColor::black;
        }
    }
}

LedSenseHAT::~LedSenseHAT() {
}

void LedSenseHAT::setup(const std::string id, int numSamples) {
    Filter::setup(id, numSamples);

    // open an outgoing connection to the Raspberry Pi SenseHat satellite
    talkToSenseHAT.setup(asString(SATELLITE_SENSEHAT_HOST), asInt(SATELLITE_SENSEHAT_PORT));
}

void LedSenseHAT::reload(ofxOscMessage const &msg) {
    ScalarLispMng mng;
    auto narg = msg.getNumArgs();
    if(narg > 1)
        Filter::setInputTexture(msg.getArgAsString(1));
    mng.compile(slMode, "mode", "0", msg);
    mng.compile(slH, "h", "0", msg);
    mng.compile(slS, "s", "0", msg);
    mng.compile(slV, "v", "0", msg);
}

void LedSenseHAT::addCalculatedToRow(size_t ix, size_t iy, int i, ofxOscMessage &senseHAT) {
    float h = slH.rpn.execute3d1i(ix, iy, 0.0, i);
    float s = slS.rpn.execute3d1i(ix, iy, 0.0, i);
    float v = slV.rpn.execute3d1i(ix, iy, 0.0, i);
    ofFloatColor rgb = ofFloatColor::fromHsb(h, s, v);
//    ofLog(OF_LOG_NOTICE) << "[" << rgb << "]" << ix << " " << iy << " " << i;
    preview[ix][iy] = rgb;
    senseHAT.addFloatArg(rgb.r);
    senseHAT.addFloatArg(rgb.g);
    senseHAT.addFloatArg(rgb.b);
}

void LedSenseHAT::addPixelToRow(const ofFloatPixels &pixels, size_t iy, size_t ypos, ofxOscMessage &senseHAT) {
    size_t step = filterWidth / led_x_led;
    size_t xpos = step / 2;
    for(size_t ix = 0; ix < led_x_led; ix++) {
//        ofLog(OF_LOG_NOTICE) << step << " " << xpos;
        auto rgbC = pixels.getColor(xpos, ypos);
        auto rgbN = pixels.getColor(xpos, ypos-1);
        auto rgbE = pixels.getColor(xpos+1, ypos);
        auto rgbS = pixels.getColor(xpos, ypos+1);
        auto rgbW = pixels.getColor(xpos-1, ypos);
        ofFloatColor rgb;
        rgb.r = 0.2 * (rgbC.r + rgbN.r + rgbE.r + rgbS.r + rgbW.r);
        rgb.g = 0.2 * (rgbC.g + rgbN.g + rgbE.g + rgbS.g + rgbW.g);
        rgb.b = 0.2 * (rgbC.b + rgbN.b + rgbE.b + rgbS.b + rgbW.b);
        preview[ix][iy] = rgb;
        senseHAT.addFloatArg(rgb.r);
        senseHAT.addFloatArg(rgb.g);
        senseHAT.addFloatArg(rgb.b);
        xpos += step;
    }
}

void LedSenseHAT::update() {
    draw_mode = slMode.rpn.execute0();
    switch(draw_mode) {
    case 1: {
        float h = slH.rpn.execute0();
        float s = slS.rpn.execute0();
        float v = slV.rpn.execute0();
        ofFloatColor rgb = ofFloatColor::fromHsb(h, s, v);
        for(size_t iy = 0; iy < led_x_led; iy++) {
            for(size_t ix = 0; ix < led_x_led; ix++) {
                preview[ix][iy] = rgb;
            }
        }
        ofxOscMessage senseHAT;
        senseHAT.setAddress("/satellite/sensehat/clear");
        senseHAT.addFloatArg(rgb.r);
        senseHAT.addFloatArg(rgb.g);
        senseHAT.addFloatArg(rgb.b);
        talkToSenseHAT.sendMessage(senseHAT, false);
    }
        break;
    case 2: {
        int i = 0;
        for(size_t iy = 0; iy < led_x_led; iy++) {
            ofxOscMessage senseHAT;
            senseHAT.setAddress("/satellite/sensehat/setrow");
            senseHAT.addIntArg(iy);
            for(size_t ix = 0; ix < led_x_led; ix++) {
                addCalculatedToRow(ix, iy, i, senseHAT);
                ++i;
            }
            talkToSenseHAT.sendMessage(senseHAT, false);
        }
        break;
    }
    default:
        if(upstream_ids.size() >= 1) {
            auto in_id = upstream_ids[0];
            if(filterDict.find(in_id) != filterDict.end()) {
                auto f = filterDict[in_id];
                if(f) {
                    ofFloatPixels pixels;
                    f->getTexture().readToPixels(pixels);
                    size_t step = filterWidth / led_x_led;
                    size_t ypos = step / 2;
                    for(size_t iy = 0; iy < led_x_led; iy++) {
                        ofxOscMessage senseHAT;
                        senseHAT.setAddress("/satellite/sensehat/setrow");
                        senseHAT.addIntArg(iy);
                        addPixelToRow(pixels, iy, ypos, senseHAT);
                        talkToSenseHAT.sendMessage(senseHAT, false);
                        ypos += step;
                    }
                }
            }
        }
    }
}

void LedSenseHAT::draw(int li) {
    fbo.begin();
    ofClear(0, 0, 0, 255);

    ofPushStyle();
    ofFill();
    int dx = filterWidth/led_x_led;
    int dy = filterHeight/led_x_led;
    for(size_t iy = 0; iy < led_x_led; iy++) {
        for(size_t ix = 0; ix < led_x_led; ix++) {
            ofSetColor(preview[ix][iy]);
            ofDrawRectangle(ix*dx, iy*dy, dx, dy);
        }
    }
    ofPopStyle();

    fbo.end();
}

void LedSenseHAT::toMermaid(std::stringstream & info) {
    Filter::toMermaid(info);
    info << id << " --> " << "senseHAT((LED8x8))" << "\n";
}
