import os 
from pathlib import Path

montage = "montage"
sli = 0

if not Path(montage).exists():
    os.mkdir(montage)
if not Path(montage).is_dir():
    print(montage, "is not an folder")
    exit(1)

def all_add(p):
    global sli
    if not p.endswith('/'):
        print(p, "path is missing trailing /")
        exit(1)
    fns = os.listdir(p)
    fns.sort()
    for x in fns:
        if x.endswith(".png"): 
            fsymlnk = montage + "/ana_" + str(sli).zfill(6) + ".png";
            forig = p + x;
            print(fsymlnk, "->", forig)
            os.symlink(forig, fsymlnk)
            sli += 1

def trg_add(p, start, stop):
    global sli
    if not p.endswith('/'):
        print(p, "path is missing trailing /")
        exit(1)
    fns = os.listdir(p)
    fns.sort()
    capture = False
    for x in fns:
        if x.endswith(".png"): 
            if x.endswith(start):
                capture = True
            if capture:
                fsymlnk = montage + "/ana_" + str(sli).zfill(6) + ".png";
                forig = p + x;
                print(fsymlnk, "->", forig)
                os.symlink(forig, fsymlnk)
                sli += 1
            if x.endswith(stop):
                capture = False

def credits(f, i=75):
    global sli
    forig = f;
    for x in range(i):
        fsymlnk = montage + "/ana_" + str(sli).zfill(6) + ".png";
        print(fsymlnk, "->", forig)
        os.symlink(forig, fsymlnk)
        sli += 1

all_add ("/ssd/ana/Road_Trip/exp_2021-05-02-13-12-27-060/")
trg_add ("/ssd/ana/Road_Trip/exp_2021-05-02-13-24-53-670/", "ana_0000_002798.png", "ana_0000_003166.png")
trg_add ("/ssd/ana/Road_Trip/exp_2021-05-02-13-24-53-670/", "ana_0000_001844.png", "ana_0000_002030.png")
trg_add ("/ssd/ana/Road_Trip/exp_2021-05-02-13-17-13-947/", "ana_0000_004912.png", "ana_0000_005102.png")
trg_add ("/ssd/ana/Road_Trip/exp_2021-05-02-13-17-13-947/", "ana_0000_005183.png", "ana_0000_005411.png")
#trg_add ("/ssd/ana/Road_Trip/exp_2021-05-02-13-17-13-947/", "ana_0000_006004.png", "ana_0000_006205.png")
all_add ("/ssd/ana/Road_Trip/exp_2021-05-02-17-40-09-010/")
all_add ("/ssd/ana/Road_Trip/exp_2021-05-02-16-56-50-023/")
#trg_add ("/ssd/ana/Road_Trip/exp_2021-05-02-13-24-53-670/", "", "")
