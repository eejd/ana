#pragma once

#include "ofMain.h"
#include "IPVideoGrabber.h"
#include "Filter.h"
#include "OscListener.h"

class GlIpVideoFilter : public Filter {
	public:
		GlIpVideoFilter(ofGLWindowSettings settings, OscListener * oscParams);
		virtual ~GlIpVideoFilter();
		virtual void setup(const std::string id, int numSamples);
		virtual void setInputTexture(std::string inputs);
		virtual void reload(ofxOscMessage const &msg);
		virtual void draw(int li);
		virtual void toMermaid(std::stringstream & info);

private:
        std::vector<size_t> channel_ids;
};
