#pragma once

#include "ofMain.h"
#include "ofxCv.h"

#include "Filter.h"
#include "OscListener.h"
#include "calc/ScalarLisp.h"

class ShapeMeshDeform : public Filter {
	public:
		ShapeMeshDeform(ofGLWindowSettings settings, OscListener * oscParams);
		virtual ~ShapeMeshDeform();
		virtual void setup(const std::string id, int numSamples);
		virtual ofTexture & getTexture();
        virtual void reload(ofxOscMessage const &msg);
        virtual void update();
		virtual void draw(int li);

protected:
    void allocateMesh();

	private:
        void distortFromFlow();
        void distortFromHSV();

		std::shared_ptr<Filter> f = nullptr;
        ofPixels grabbedPixels;
        bool hasData = false;
        cv::Mat frame_captured;
        cv::Mat frame_scaled;
        const int reduced_size = 6;
        size_t reduced_filterHeight = 1;
        size_t reduced_filterWidth = 1;
        float mesh_tx = 0;
        float mesh_ty = 0;
        ofxCv::FlowFarneback ffb;

        ScalarLisp slPyrScale;
        ScalarLisp slLevels;
        ScalarLisp slWinSize;
        ScalarLisp slIterations;
        ScalarLisp slPolyN;
        ScalarLisp slPolySigma;
        ScalarLisp slUseGaussian;
        ScalarLisp slInMode;
        ScalarLisp slMode;
        ScalarLisp slClear;
        ScalarLisp slStepSize;
        float stepSize = 0;
        float reduced_stepSize = 1;
        ScalarLisp slDistortionStrength;
        ScalarLisp slUpstream;
	    ofMesh mesh;
        int xSteps = 1;
        int ySteps = 1;
};
