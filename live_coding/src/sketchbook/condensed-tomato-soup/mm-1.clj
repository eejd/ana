(ns lv.demo)
(use 'lv.core)
; video: mm time displacement
(layout "horizontal" [imgs0 imgs1 imgs2 imgs3])
(imagesequ "../../../timelapse/series/20210101-mm/split-P1140101/*.png")
(render)

(hsv :imgs0 [c0]
  (k (% (int (+ i sframe)) slength))
  (discard (< (* r slength) sframe))
  (hsv c0.hsv)
)

(hsv :imgs1 [c0]
  (rot (/ pi 2))
  (k (% (int (* y sframe)) slength))
  (discard (< (* r slength) sframe))
  (hsv c0.hsv)
)

(hsv :imgs2 [c0]
  (k (% (int (+ i sframe)) slength))
  (discard (> (* (abs y)
                 slength)
              (- slength sframe)))
  (hsv c0.hsv)
)

(hsv :imgs3 [c0]
  (k (* slength (smoothstep 100 920 (float sframe))))
  (discard (< (* (abs y) slength) sframe))
  (hsv c0.hsv)
)
