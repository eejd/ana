(ns lv.demo)
(use 'lv.core)

(layout "grid" [ras0 s3line1 s3line2]
	             [mopr3 mopr4 ras5])

(render)

(hsv :ras0 [c0]
	(h (+ r (* 0.001 f)))
	(s 0.5)
	(v (/ (floor (* 9 (- 1 r))) 9))
)

(expr :s3line1 [ras0]
	(mode 0)
	(fov (+ 30 (* 60 (usin (* 0.013 f)))))
	(rotx (* 0.02 f))
	(y (* 1.5 y))
  (dy 0.01)
  (dx 0.01)
	(z (* 13 (unoise x (* 0.011 f) z) z))
	(jitter (* 0.02 (+ r g b)))
)

(expr :s3line2 [ras0]
	(mode 2)
	(fov (+ 10 (* 80 (usin (* 0.013 f)))))
	(rotx (* 0.023 f))
  (dy 0.01)
  (dx 0.01)
	(x (+ 0.5 (* 3 (snoise y (* 0.011 f) z) x)))
	(y (* 3 (snoise x (* 0.017 f) z) y))
	(jitter (* 0.02 (+ r g b)))
)

(expr :mopr3 [s3line1] "dbb")
(expr :mopr4 [s3line2] "ddb")

(rgb :ras5 [mopr3 mopr4]
	(rgb (+ mopr3.rgb mopr4.rgb))
)
