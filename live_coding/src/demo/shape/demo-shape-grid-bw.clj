(ns lv.demo)
(use 'lv.core)

(layout "vertical" [shape0 map2d1 mopr2 ras3])
(render)

(expr :shape0 []
	(clear 1)
  (count (* 12 24))
  (x (+ 50 (* 34 (% i 12))))
  (y (+ 150 (* 34 (floor (/ i 12)))))
  (size 21)
	(rot (+ 45 (* 0.6
		            (- y 940)
		            (sin (* 0.031 f))
								(snoise x y 0.1))))
	(res 4)
	(v 0.5)
)

(expr :map2d1 [shape0]
  (x (+ x (* 0.08
		         (- y 940)
		         (snoise (sin (* 0.017 f)) x y))))
  (y (+ y (* 0.08
		         (- y 940)
		         (snoise (sin (* 0.031 f)) y x))))
	(size 21)
  (rot z)
	(res 4)
	(v 0.5)
)

(expr :mopr2 [map2d1] "eb")

(hsv :ras3 [ras3 map2d1]
  (v (mix ras3.v map2d1.v 0.1))
)
