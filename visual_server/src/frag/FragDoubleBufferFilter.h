#pragma once
#include "ofMain.h"
#include "OscListener.h"
#include "Filter.h"
#include "calc/ScalarLisp.h"
#include "glslhelper.h"

class FragDoubleBufferFilter: public FragFilter {
public:
    FragDoubleBufferFilter(ofGLWindowSettings settings, OscListener * oscParams);
    virtual ~FragDoubleBufferFilter();
    virtual void setup(const std::string id, int numSamples);
    virtual void draw(int li);
    virtual void finish();

private:
    int ringIndex = 0;
    ofFbo fbo0;
    ofFbo fbo1;
};
