#pragma once
#include <string>
#include <unordered_map>

#include "ofMain.h"
#include "ofxOsc.h"

#include "Filter.h"
#include "IPVideoGrabber.h"
#include "PreShot.h"
#include "Inspector.h"
#include "calc/ScalarLisp.h"
#include "ofApp.h"

extern ofApp *apptarget;

extern bool waiting_osc;
extern int num_cl_gpu;

extern PreShot *preShot;

extern std::string pipe0;

extern ofSoundPlayer beat;
extern ofTrueTypeFont font;

extern size_t filterWidth;
extern size_t filterHeight;

extern unsigned int fps;
extern unsigned int frame;
extern float beat_pos;

extern float smoothedVol;
extern float onset;
extern float minimumThreshold;
extern float decayRate;

extern ScalarLisp slT0;
extern ScalarLisp slT1;
extern ScalarLisp slT2;
extern ScalarLisp slT3;
extern ScalarLisp slT4;
extern ScalarLisp slT5;
extern ScalarLisp slT6;
extern ScalarLisp slT7;

extern float u0, u1, u2, u3, u4, u5, u6, u7;
extern float t0, t1, t2, t3, t4, t5, t6, t7;
extern float dt0, dt1, dt2, dt3, dt4, dt5, dt6, dt7;
extern float kp0, kp1, kp2, kp3, kp4, kp5, kp6, kp7;
extern float ks0, ks1, ks2, ks3, ks4, ks5, ks6, ks7;
extern float kb0, kb1, kb2, kb3, kb4, kb5, kb6, kb7;

extern float h0;
extern float s0;
extern float v0;

extern float lx;
extern float ly;
extern float rx;
extern float ry;
extern float l2;
extern float r2;

#ifdef WITH_openCL
extern Inspector inspector;
#endif // WITH_openCL
extern ScalarLisp slInspectChannels;
extern ScalarLisp slInspectX0;
extern ScalarLisp slInspectY0;
extern ScalarLisp slInspectR0;
extern ScalarLisp slInspectX1;
extern ScalarLisp slInspectY1;
extern ScalarLisp slInspectR1;
extern ScalarLisp slInspectX2;
extern ScalarLisp slInspectY2;
extern ScalarLisp slInspectR2;
extern ScalarLisp slInspectX3;
extern ScalarLisp slInspectY3;
extern ScalarLisp slInspectR3;

extern std::string sound0;
constexpr size_t nBandsToGet = 8;
extern std::array<float, nBandsToGet> fftSmoothed;

#define maxImages 8
extern std::string image0;
extern int usedImages;
extern ofFloatImage images[maxImages];
extern std::filesystem::path imagePath[maxImages];

extern std::string imagesequ0;
extern size_t sizeImageSequ;
extern GLuint imageSequ_textureID;

#define maxMovies 8
extern std::string movie0;
extern int usedMovies;
extern ofVideoPlayer movies[maxMovies];

#ifdef WITH_video_grabber
extern ofVideoGrabber *videoGrabber;
#endif // WITH_video_grabber
extern size_t video_width;
extern size_t video_height;

extern std::vector<ofx::Video::IPVideoGrabber*> ipVideoGrabbers;

extern std::unordered_map<std::string, std::shared_ptr<Filter>> filterDict;
extern std::unordered_map<std::string, std::string> fragDict;
extern std::string fallbackVert;

extern std::string frag(std::string &id, std::string *defaultFrag);

extern void sendCurrentOsc(ofxOscSender &talkTo);
extern void sendQuitOsc(ofxOscSender &talkTo);
extern std::string dumpState();
extern std::string dumpNkState();
extern void saveState();
extern void loadState();
