(ns lv.demo)
(use 'lv.core)

(layout "grid" [ras0 ras1]
               [ras2 del1])
(render)
; kp0    0.74016
; kp1    0.031496
;
; ks0    0.67717
; ks1    0.16535

(hsv :ras0 []
  (rot (* 0.011 f))
  (h (mod (usin (* 6 a)) 0.5))
  (s 1)
  (v (* (- 1 r) (usin (* 24 a x))))
)

(rgb :ras1 [ras0 del1]
  (rot (* -0.1 kp0 del1.h))
  (rgb (mix ras0.rgb del1.rgb ks0))
)

(rgb :ras2 [ras1]
  (rot (* 0.1 kp1))
  (ty (* ks1 ras1.v))
	(rgb ras1.rgb)
)

(rgb :del1 [ras2]
  (k (mod (+ 51 (* x r sframe)) slength))
	(rgb ras2.rgb)
)
