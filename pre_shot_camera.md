# “Pre shot” camera
aNa comes with a screen recorder that is different from typical screen casting strategies.
The build in recorder managed a very huge ring buffer of previous displayed frames. This ring buffer is located in the RAM of your computer. After every animation frame display buffer data are copied from the graphic card to save them in the CPU RAM without compression. After you notice a interesting animation sequence you can press the save button. Immediately the aNa server will pause to write all previous known frames to your hard-disk as a lossless PNG image  files.

The motivation behind this strategy is that i need high preciseness input not to lose to much details in a 25fps 1080px movie. Glitch art often contains 1 pixel wide lines and shows hard edges. But typical screen recorders compress during recording and loose to much of what is relevant for glitch effects.

The main principles are:
- Press the trigger after an interesting sequence is shown.
- Always append new animation frames without losing details.
- Forget old frames.
- Using your file manager to to remove parts of a stored image sequence by deleting boring PNG files.
- Use your preference video compressor.

The disadvantages are
- Needs lot of memory.
- Pauses the animation to write the image sequence.

No need to use this “pre shot” camera. You can use your desktops screen recorder.

## Privacy warning
This build in screen recorder captures full screens. Before starting the visualization server with pre shot enabled close other applications containing private information.

## Memory using warning
**The size of the ring buffer must be smaller than your computers main memory.** There must be enough space for the operating system and the executable. If the ring buffer is to huge your system will very frequently swap to disk. This will lead straight to hell.

## How to activate the built in screen recorder
First calculate the memory requirements.
```
1080 pixel lines * 1920 pixel columns * 4 bytes per pixel = 8294400 bytes per frame
```

If you want to spend 30 GB of your CPU RAM for the “pre shot” camera:
```
30 * 1024**3 / 8294400 = 3880 frames can that be hold in the ring buffer
```

Edit `visual_server/bin/visual_server.config`
```
#preshot = 0
preshot =  3880
preshot_path = ~/tmp/analog
```
Running with 25 frames per second you can save the last 155 seconds of the animation using this configuration on a 32GByte RAM machine.

## How to press the trigger of the camera
- Press the R key on your keyboard. This works only if the visualization server has the focus and receives keystrokes. Often your editor owns the focus.
- Press the X button on your joystick.
- Send a `/preshot` OSC message to port 57220. You can send this message over the network or from your local computer.

## How to convert a image sequence.
Start this bash script in the folder containing the PNG image files.
```bash
#!/bin/bash
timestamp=$(date +%s)
ffmpeg -pattern_type glob -framerate 25 -i 'ana_*.png' -vcodec libx265 video-$timestamp.mp4
```

Or import the image strip to Blender, kdenlive ...
