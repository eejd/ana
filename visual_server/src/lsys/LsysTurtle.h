#pragma once

#include "ana_features.h"
#include "color/Palette.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "lsys/LsysMachine.h"
#include "lsys/LsysIntepretation.h"

#define maxDepthLeafStack 100
#define maxPolyPoints 100

#define maxDepthBranchStack 1000
#define maxDepthLeafStack 100

class LsysTurtle : public LsysIntepretation  {
public:
    LsysTurtle();
    virtual ~LsysTurtle();

    virtual void init(size_t length0, char* string0, ScalarLisp** sl0);

    virtual void draw();
    virtual void expandBoundigBox();
    void nextState(size_t ci);

    double start_direction = 1.5*M_PI;
    double turtle_direction = 0;
    float turtle_x = 0.0;
    float turtle_y = 0.0;

    float xPolyPoints[maxDepthLeafStack][maxPolyPoints];
    float yPolyPoints[maxDepthLeafStack][maxPolyPoints];

    size_t branchTOS = 0;
    size_t leafTOS = 0;
    int colorIndex = 0;
    float turtleStackX[maxDepthBranchStack];
    float turtleStackY[maxDepthBranchStack];
    float turtleStackDir[maxDepthBranchStack];
    int turtleStackColor[maxDepthBranchStack];

    bool stackMessageSend = false;
};
