s.waitForBoot({

	// https://sccode.org/1-4UP by Bruno Ruviaro
	SynthDef(\risset, {|out = 0, pan = 0, freq = 400, amp = 0.1, dur = 2, gate = 1|
		var amps = #[1, 0.67, 1, 1.8, 2.67, 1.67, 1.46, 1.33, 1.33, 1, 1.33];
		var durs = #[1, 0.9, 0.65, 0.55, 0.325, 0.35, 0.25, 0.2, 0.15, 0.1, 0.075];
		var frqs = #[0.56, 0.56, 0.92, 0.92, 1.19, 1.7, 2, 2.74, 3, 3.76, 4.07];
		var dets = #[0, 1, 0, 1.7, 0, 0, 0, 0, 0, 0, 0];
		var doneActionEnv = EnvGen.ar(Env.linen(0, dur, 0), gate, doneAction: 2);
		var src = Mix.fill(11, {|i|
			var env = EnvGen.ar(Env.perc(0.005, dur*durs[i], amps[i], -4.5), gate);
			SinOsc.ar(freq*frqs[i] + dets[i], 0, amp*env);
		});
		src = src * doneActionEnv * 0.5; // make sure it releases node after the end.
		Out.ar(out, Pan2.ar(src, pan));
	}).add;

	g.free;
	g = OSCFunc({ |msg, time, addr, recvPort|
		[time, msg].postln;
		Synth(\risset, [
			\freq, (msg[1] * 5) + 25,
		//	\amp,  (msg[3].abs * 0.0005) + 0.05
		               ]);
	}, '/ana/hit/leave');

	h.free;
	h = OSCFunc({ |msg, time, addr, recvPort|
		[time, msg].postln;
		Synth(\risset, [
			\freq, (msg[1] * 5) + 25,
		//	\amp,  (msg[3].abs * 0.0005) + 0.05
		               ]);
	}, '/ana/hit/enter');
	q.free;
	q = OSCFunc({ |msg, time, addr, recvPort|
		[time, msg].postln;
		s.quit;
		0.exit;
	}, '/quit');

});

// ~pl = Synth(\risset, [ \freq, 150 ]);
