#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

#include "ofMain.h"

#include "ana_build.h"
#include "ana_features.h"
#include "ofApp.h"
#include "Configuration.h"
#include "globalvars.h"

// see: https://www.khronos.org/opengl/wiki/OpenGL_Error#Catching_errors_.28the_easy_way.29
#undef LOG_ALL_GL_CALLBACKS
const int len_gl_errors = 8;
static GLuint last_gl_errors[len_gl_errors];
static int index_gl_errors = 0;
static int soft_limit = 32;
static bool fresh_gl_error(const GLuint new_gl_error) {
    for(int ix = 0; ix < len_gl_errors; ++ix) {
        if(last_gl_errors[ix] == new_gl_error) {
            return false;
        }
    }
    last_gl_errors[index_gl_errors] = new_gl_error;
    index_gl_errors = (index_gl_errors + 1) % len_gl_errors;
    return true;
}
void GLAPIENTRY MessageCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *userParam) {
#ifdef LOG_ALL_GL_CALLBACKS
		ofLog(OF_LOG_WARNING) << "GL callback " << id << ": " << message;
	#else
    if(soft_limit > 0 || fresh_gl_error(id)) {
        if(type == GL_DEBUG_TYPE_ERROR) {
            ofLog(OF_LOG_ERROR) << "GL error " << id << ": " << message;
        } else if(id != 131185) {
            ofLog(OF_LOG_WARNING) << "GL warn " << id << ": " << message;
        }
        soft_limit = soft_limit > 0 ? soft_limit - 1 : 0;
    }
#endif
}

static int xlib_error_handler(Display * d, XErrorEvent * e)
{
    static unsigned int last_error_code = ~(0);
    if(e->error_code != last_error_code) {
        std::cerr << "Xlib error code: " << (int)(e->error_code) << std::endl;
        last_error_code = e->error_code;
    }
    return 0;
}


int main(int argc, char *argv[]) {
    for(int ix = 0; ix < len_gl_errors; ++ix) {
        last_gl_errors[ix] = 0;
    }

    ofSetLogLevel(OF_LOG_NOTICE);
    ofSetLogLevel("ofThread", OF_LOG_NOTICE);
    ofLog(OF_LOG_NOTICE) << "aNa revision " << ANA_REVISION;

    load_configuration(argc >= 2 ? argv[1] : nullptr);
    ofSetDataPathRoot(asString(MEDIA_PATH));
    ofLog(OF_LOG_NOTICE) << "Using media path '" << ofFilePath::getAbsolutePath("") << "'";

//    ofGLWindowSettings settings;
    ofGLFWWindowSettings settings;
    int gl_major = asInt(GL_MALOR);
    int gl_minor = asInt(GL_MINOR);
    if(gl_major > 0) {
        settings.setGLVersion(gl_major, gl_minor);
    }

    filterWidth = asInt(FILTER_WIDTH);
    filterHeight = asInt(FILTER_HEIGHT);

#define MULTI_MONITOR
//#undef MULTI_MONITOR
    if(asBool(FULLSCREEN)) {
#ifdef MULTI_MONITOR
        settings.setSize(asInt(CANVAS_WIDTH), asInt(CANVAS_HEIGHT));
        settings.windowMode = OF_FULLSCREEN;
        settings.multiMonitorFullScreen = true;
        settings.decorated = false;
#else
        settings.setSize(asInt(CANVAS_WIDTH), asInt(CANVAS_HEIGHT));
        settings.windowMode = OF_FULLSCREEN;
#endif // MULTI_MONITOR
    } else {
//         settings.setSize(asInt(FILTER_WIDTH), asInt(FILTER_HEIGHT)); // TODO FILTER_ or CANVAS_
        settings.setSize(asInt(CANVAS_WIDTH), asInt(CANVAS_HEIGHT)); // TODO FILTER_ or CANVAS_
        glm::vec2 position = { 0, 0 };
        settings.setPosition(position);
        settings.windowMode = OF_WINDOW;
    }
    ofCreateWindow(settings);
    ofSetWindowTitle("analog Not analog");

    glDebugMessageCallback(MessageCallback, 0);
    glEnable(GL_DEBUG_OUTPUT);
    int real_gl_major = 0;
    int real_gl_minor = 0;
    glGetIntegerv(GL_MAJOR_VERSION, &real_gl_major);
    glGetIntegerv(GL_MINOR_VERSION, &real_gl_minor);
    ofLog(OF_LOG_NOTICE) << "GL version " << real_gl_major << "." << real_gl_minor << (GLEW_ARB_compatibility ? " ARB_compatibility mode" : "");

    int gi = 0;
    glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &gi);
    ofLog(OF_LOG_NOTICE) << "GL_MAX_TEXTURE_IMAGE_UNITS " << gi;
    glGetIntegerv(GL_MAX_SAMPLES, &gi);
    ofLog(OF_LOG_NOTICE) << "GL_MAX_SAMPLES " << gi;
    glGetIntegerv(GL_MAX_3D_TEXTURE_SIZE, &gi);
    ofLog(OF_LOG_NOTICE) << "GL_MAX_3D_TEXTURE_SIZE " << gi;
    glGetIntegerv(GL_MAX_TEXTURE_SIZE, &gi);
    ofLog(OF_LOG_NOTICE) << "GL_MAX_TEXTURE_SIZE " << gi;

    XSetErrorHandler(xlib_error_handler);

    std::shared_ptr<ofApp> app = make_shared<ofApp>(settings);
    ofRunApp(app);
}
