#pragma once

#include "ana_build_date.h"

#ifndef VERSION_BUILD_DATE
#define VERSION_BUILD_DATE "UNKNOWN"
#endif

#define __REVISION VERSION_BUILD_DATE
#ifdef DEBUG
#define ANA_REVISION "DEBUG"
#else
#define ANA_REVISION __REVISION
#endif
