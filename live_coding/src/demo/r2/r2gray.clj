(ns lv.demo)
(use 'lv.core)

(layout "grid" [igen0 ras3]
               [ras2 frag1])

 ; kp0    0.62205
 ; kp1    0.30709
 ; kp2    0.023622
 ; kp3    0.11024
 ; kp4    0
 ; kp5    0.20472
 ; kp6    0.1875
 ; kp7    0.42188
 ;
 ; ks0    0.46875
 ; ks1    0.28125
 ; ks2    0.55469
 ; ks3    0.63281
 ; ks4    0.094488
 ; ks5    0.88189
 ; ks6    0.40157
 ; ks7    0.11719


(expr :igen0 [ras3]
  "r2/r2gray.cl"
  (p0 (* ks0 3))
  (p1 (* ks1 3))
  (p2 (* ks2 3))
  (p3 (* ks3 3))
  (p4 (* ks4 3))
  (p7 (* ks7 3))
)

(expr :frag1 [igen0]
 "frag/bilateral_blur.frag"
 (csigma (* 20 kp6))
 (bsigma (* 2 kp7))
)

(hsv :ras2 [igen0] (rot (* -0.01 f)) (v (- 1 igen0.v)))

(hsv :ras3 [frag1]
  (kaleid 2.3)
  (kaleid 2.7)
  (rot (* 0.01 f))
  (mirror)
  (h frag1.v)
  (s 0)
  (v frag1.v)
)
(render)
