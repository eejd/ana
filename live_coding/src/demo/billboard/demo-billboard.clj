(ns lv.demo)
(use 'lv.core)

(layout "grid" [bill0 ras1] [fdiff2 ras3])
(render)

(expr :bill0 []
  ; TODO pattern and palette is NOT implemented
  ; (load pattern "billboard/ring-alpha.png")
  ; (load palette "billboard/Warm_Colors.gpl")
  (roty f)
  (count (+ 1 (* 1500
                 (usin (/ f 60))
                 (usin (/ f 60)))))
  (x (* 1000 (snoise (* 5 (sin (* 0.00073 f)))
                     (* 5 (sin (* 0.00091 f)))
                     (* i 0.01))))
  (y (* 1000 (snoise (* 5 (sin (* 0.00051 f)))
                     (* 5 (sin (* 0.00019 f)))
                     (* i 0.04))))
  (z (* 1000 (snoise (* 5 (sin (* 0.00023 f)))
                     (* 5 (sin (* 0.00031 f)))
                     (* i 0.09))))
  (size (* 150 (unoise (/ i 1000) 0.1 0.1)))
)
(render 0)

(hsv :ras1 [bill0]
  (h bill0.h)
  (s bill0.s)
  (v (* 2 bill0.v))
)
(render 1)

(expr :fdiff2 [ras1])
(render 2)

(rgb :ras3 [fdiff2 ras3]
  (rgb (mix (* 3 fdiff2.rgb)
            ras3.rgb
            0.98))
)
(render 3)
