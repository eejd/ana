// this OpenCL code is published with Ready 0.7
// see: https://github.com/GollyGang/ready
// This sample ist based on Cornus Ammonis source code

#define SQRT_2_PI 2.50662827463
#define STD_DEVS 3.0

float gaussian1d_vertical(__global float* in, float sigma, float devs, int x, int y, int X, int Y) {
    const float a = 1.0f / (sigma * SQRT_2_PI);
    const float d = 2.0f * sigma * sigma;
    const int r = devs * sigma;
    float acc = 0.0f;
    for (int i = -r; i <= r; i++) {
        float isq = -1.0f * i * i;
        float g = a * exp( isq / d );
        int ty = (y+i+Y) & (Y-1);
        acc += g * in[ X*ty + x ];
    }
    return acc;
}

float gaussian1d_horizontal(__global float* in, float sigma, float devs, int x, int y, int X, int Y) {
    const float a = 1.0f / (sigma * SQRT_2_PI);
    const float d = 2.0f * sigma * sigma;
    const int r = devs * sigma;
    float acc = 0.0f;
    for (int i = -r; i <= r; i++) {
        float isq = -1.0f * i * i;
        float g = a * exp( isq / d );
        int tx = (x+i+X) & (X-1);
        acc += g * in[ X*y + tx ];
    }
    return acc;
}

__kernel void image_change(
__global float* a_in,
__global float* a_out,
__global float* b_in,
__global float* b_out,
__global float* c_in,
__global float* c_out,
__global float* d_in,
__global float* d_out,
__global float* e_in,
__global float* e_out,
__global float* f_in,
__global float* f_out,
__global float* g_in,
__global float* g_out,
__global float* h_in,
__global float* h_out,
__write_only image2d_t image2,
const int flags
) 
{
    #define NUM_SCALES 6
    #define NUM_RADII 7

    const float radii[ NUM_RADII ] = { 92.0f, 48.0f, 24.0f, 12.0f, 6.0f, 3.0f, 1.0f };
    const int pairs[ NUM_SCALES ][ 2 ] = { {1, 0}, {2, 1}, {3, 2}, {4, 3}, {5, 4}, {6, 5} }; // inner, outer indices into radii[]
    const float amount[ NUM_SCALES ] = { 0.006f, 0.005f, 0.004f, 0.003f, 0.002f, 0.001f };

    const int bx = get_global_id(0);
    const int by = get_global_id(1);
    const int BX = get_global_size(0);
    const int BY = get_global_size(1);
    const int i_here = BX*by + bx;

    float densities[ NUM_RADII ] = { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };

    // compute the density of every disk
    b_out[i_here] = gaussian1d_vertical(a_in, radii[ 0 ], STD_DEVS, bx, by, BX, BY);
    c_out[i_here] = gaussian1d_vertical(a_in, radii[ 1 ], STD_DEVS, bx, by, BX, BY);
    d_out[i_here] = gaussian1d_vertical(a_in, radii[ 2 ], STD_DEVS, bx, by, BX, BY);
    e_out[i_here] = gaussian1d_vertical(a_in, radii[ 3 ], STD_DEVS, bx, by, BX, BY);
    f_out[i_here] = gaussian1d_vertical(a_in, radii[ 4 ], STD_DEVS, bx, by, BX, BY);
    g_out[i_here] = gaussian1d_vertical(a_in, radii[ 5 ], STD_DEVS, bx, by, BX, BY);
    h_out[i_here] = gaussian1d_vertical(a_in, radii[ 6 ], STD_DEVS, bx, by, BX, BY);

    densities[ 0 ] = gaussian1d_horizontal(b_in, radii[ 0 ], STD_DEVS, bx, by, BX, BY);
    densities[ 1 ] = gaussian1d_horizontal(c_in, radii[ 1 ], STD_DEVS, bx, by, BX, BY);
    densities[ 2 ] = gaussian1d_horizontal(d_in, radii[ 2 ], STD_DEVS, bx, by, BX, BY);
    densities[ 3 ] = gaussian1d_horizontal(e_in, radii[ 3 ], STD_DEVS, bx, by, BX, BY);
    densities[ 4 ] = gaussian1d_horizontal(f_in, radii[ 4 ], STD_DEVS, bx, by, BX, BY);
    densities[ 5 ] = gaussian1d_horizontal(g_in, radii[ 5 ], STD_DEVS, bx, by, BX, BY);
    densities[ 6 ] = gaussian1d_horizontal(h_in, radii[ 6 ], STD_DEVS, bx, by, BX, BY);

    // add variation on whichever scale has least
    float lowest_variation = 1e6f;
    float diff;
    for( int i = 0; i < NUM_SCALES; i++ )
    {
        float density_difference = densities[ pairs[ i ][ 0 ] ] - densities[ pairs[ i ][ 1 ] ];
        float variation = fabs(density_difference);
        if( variation < lowest_variation )
        {
            lowest_variation = variation;
            diff = sign( density_difference ) * amount[ i ];
        }
    }
	float result = clamp(a_in[i_here] + diff, -1.0f, 1.0f);
	a_out[i_here] = result;
	const int2 pixelcoord = (int2)(get_global_id(0), get_global_id(1));
	write_imagef(image2, pixelcoord, (float4)(result * 0.5f + 0.5f, 0, 0, 1));
}

