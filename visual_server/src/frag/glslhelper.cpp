#include <string>
#include "../ana_features.h"
#include "ofMain.h"
#include "globalvars.h"
#include "util/stringhelper.h"
#include "frag/glslhelper.h"

bool reloadFragProg(const bool force, GlslProgMng & glslProg) {
    if(glslProg.fatalFragProg)
        return false;

    bool need = force;
    long sourceModTime = 0;
    if(!need) {
        sourceModTime = std::filesystem::last_write_time(glslProg.pathFragProg);
        need = sourceModTime != glslProg.fragProgModTime;
        glslProg.fragProgModTime = sourceModTime;
    }

    if(need) {
        glslProg.fragProgModTime = std::filesystem::last_write_time(glslProg.pathFragProg);
        ofLog(OF_LOG_NOTICE) << "compiling " << glslProg.fragProgName;
        compilePrivateShader(glslProg);
    }
    return need;
}

void compilePrivateShader(GlslProgMng & glslProg) {
    glslProg.shader.unload();
    std::stringstream src;

    bool has_inject = glslProg.inject.size() > 0;
    bool has_maindef = glslProg.maindef.size() > 0;
    std::ifstream file(glslProg.fragProgName);
    std::string line;
    while (std::getline(file, line)) {
        if(has_inject && trim(line) == "/*-pi-*/") {
            size_t ll = glslProg.inject.size();
            for(size_t lx=0; lx<ll; ++lx) {
                src << glslProg.inject.at(lx) << '\n';
            }
        }
        else if(has_maindef && trim(line) == "/*-maindef-*/") {
            src << glslProg.maindef << '\n';
        }
        else {
            src << line << '\n';
        }
    }
    std::string okV = glslProg.shader.setupShaderFromSource(GL_VERTEX_SHADER, fallbackVert) ? "OK" : "failed";
    std::string okF = glslProg.shader.setupShaderFromSource(GL_FRAGMENT_SHADER, src.str()) ? "OK" : "failed";
    std::string okP = glslProg.shader.linkProgram() ? "OK" : "failed";
// for printing generated shader source
//#define PRINT_SHADER_SRC
#undef PRINT_SHADER_SRC
#ifdef PRINT_SHADER_SRC
    ofLog(OF_LOG_NOTICE) << glslProg.shader.getShaderSource(GL_VERTEX_SHADER) << "\n";
    ofLog(OF_LOG_NOTICE) << glslProg.shader.getShaderSource(GL_FRAGMENT_SHADER) << "\n";
#endif // PRINT_SHADER_SRC
    ofLog(OF_LOG_NOTICE) << "fragmet '" << glslProg.fragProgName << "' shader compiler : vertex=" << okV << " fragment=" << okF <<  " link=" << okP;

    GLint count;
    GLint size; // size of the variable
    GLenum type; // type of the variable (float, vec3 or mat4, etc)

    const GLsizei bufSize = 31; // maximum name length
    GLchar name[bufSize]; // variable name in GLSL
    GLsizei length; // name length
    glGetProgramiv(glslProg.shader.getProgram(), GL_ACTIVE_UNIFORMS, &count);
    printf("Number of active uniforms: %d\n", count);

    std::vector<UnifomExpression> new_uniforms;
    for (GLint i = 0; i < count; i++)
    {
        glGetActiveUniform(glslProg.shader.getProgram(), (GLuint)i, bufSize, &length, &size, &type, name);
        printf("Uniform #%d Type: %u Name: %s\n", i, type, name);
        switch(type) {
        case 5126: {
            UnifomExpression fu;
            fu.name = name;
            new_uniforms.push_back(fu);
        }
        }
    }
    for(auto &nu : new_uniforms) {
        for(auto &u : glslProg.uniforms) {
            if(nu.name == u.name) {
                nu.slFloat = u.slFloat;
            }
        }
    }
    glslProg.uniforms.clear();
    for(auto &nu : new_uniforms) {
        glslProg.uniforms.push_back(nu);
    }
    new_uniforms.clear();
}
