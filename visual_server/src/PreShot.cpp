#include <thread>
#include "Configuration.h"
#include "PreShot.h"

std::vector<void*> PreShot::buffers;
unsigned int PreShot::width = 0;
unsigned int PreShot::height = 0;

PreShot::PreShot() {
}

PreShot::~PreShot() {
    clear();
}

void PreShot::setup() {
    epoch = 0;
    capture_lenght = 0;
    width = asInt(CANVAS_WIDTH);
    height = asInt(CANVAS_HEIGHT);

    clear();

    const size_t bufsize = 3 * width * height;
    preshot_max = asInt(PRESHOT);
    for(size_t bx = 0; bx < preshot_max; ++bx) {
        PreShot::buffers.push_back(std::malloc(bufsize + sizeof(unsigned int)));
    }
    capture_index = 0;
    ofLog(OF_LOG_NOTICE) << "PreShot::setup " << width << 'x' << height << " cache size=" << preshot_max;
}

#undef TRACE_MONOTONIC
void PreShot::update() {
    if(preshot_max > 0) {
#ifdef TRACE_MONOTONIC
		timespec time1, time2;
		clock_gettime(CLOCK_MONOTONIC, &time1);
#endif // TRACE_MONOTONIC
        glFinish();
        glReadBuffer(GL_FRONT);
        unsigned int *pf = (unsigned int*) PreShot::buffers[capture_index];
        *pf = preshot_frame;
        glReadPixels(0, 0, width, height, GL_BGR, GL_UNSIGNED_BYTE, pf + 1);
        capture_index = (capture_index + 1) % preshot_max;
        capture_lenght = capture_lenght < preshot_max ? capture_lenght + 1 : capture_lenght;
        ++preshot_frame;
#ifdef TRACE_MONOTONIC
		clock_gettime(CLOCK_MONOTONIC, &time2);
		ofLog(OF_LOG_NOTICE) << "PreShot::update " << time2.tv_sec-time1.tv_sec << ':' << time2.tv_nsec-time1.tv_nsec;
#endif // TRACE_MONOTONIC
    }
}

static void writePng(std::string prefix, size_t from, size_t length) {
    try {
        ofPixels pixels;
        for(size_t bx = 0; bx < length; ++bx) {
            unsigned int *pf = (unsigned int*) PreShot::buffers[bx + from];
            if(pf) {
                unsigned char *p = (unsigned char*) (pf + 1);
                std::ostringstream capture_counter;
                pixels.setFromPixels(p, PreShot::width, PreShot::height, OF_PIXELS_BGR);
                pixels.mirror(true, false);
                capture_counter << std::setfill('0') << std::setw(6) << (*pf);
                ofSaveImage(pixels, prefix + "_" + capture_counter.str() + ".png");
            }
        }
    } catch(const std::exception &ex) {
        ofLogError() << "PngWriter failed with exception '" << ex.what() << "'";
    }
}

void PreShot::saveRecording(std::string prefix) {
    if(preshot_max > 0 && capture_lenght > 0) {
        try {
            std::ostringstream epoch_counter;
            epoch_counter << std::setfill('0') << std::setw(4) << PreShot::epoch;
            const size_t cores = asInt(HARDWARE_CONCURRENCY);
            std::thread pngWriter[cores];
            size_t si = 0;
            size_t rest = capture_lenght;
            size_t div = capture_lenght / cores;
            for(size_t ci = 0; ci < cores - 1; ++ci) {
                pngWriter[ci] = std::thread(writePng, prefix + epoch_counter.str(), si, div);
                si += div;
                rest -= div;
            }
            pngWriter[cores - 1] = std::thread(writePng, prefix + epoch_counter.str(), si, rest);

            for(size_t ci = 0; ci < cores; ++ci) {
                pngWriter[ci].join();
            }

            PreShot::reset();
        } catch(const std::exception &ex) {
            ofLogError() << "saveRecording failed with exception '" << ex.what() << "'";
        }
        ++epoch;
    }
}

void PreShot::reset() {
    capture_lenght = 0;
    capture_index = 0;
}

void PreShot::clear() {
    for(size_t bx = 0; bx < preshot_max && bx < buffers.size(); ++bx) {
        if(buffers[bx]) {
            std::free(buffers[bx]);
            buffers[bx] = nullptr;
        }
    }
    buffers.clear();
    reset();
}
