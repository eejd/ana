(ns lv.demo)
(use 'lv.core)

(layout "grid" [pl1 uspl2 uspl3]
               [ras4 conv5 mopr6]
               [ras7])
(render)

(expr :pl1 []
  (count 17)
  (x (* (+ 1 i) 20 (sin (* (+ 1 i) 0.0011 f))))
  (y (* (+ 1 i) 20 (cos (* (+ 1 i) 0.0013 f))))
  (w 10)
  (h (+ u0 (* u1 i)))
  (s 1)
  (v 1)
)
; (expr :pl1 []
;   (count 8)
;   (x (* (+ 1 i) 80 (sin (* (+ 1 i) 0.0011 f))))
;   (y (* (+ 1 i) 80 (cos (* (+ 1 i) 0.0013 f))))
;   (w 10)
;   (h (+ u0 (* u1 i)))
;   (s 1)
;   (v 1)
; )

(expr :uspl2 [pl1]
  "kings-color/VoronoiUpstreamPoints.cl"
)

(expr :uspl3 [pl1]
  "kings-color/VoronoiBlendUpstreamPoints.cl"
)

(rgb :ras4 [uspl2 uspl3]
  (rgb (* uspl3.rgb (- (vec3 1 1 1) uspl2.rgb)))
)

; Edge detection
(matrix :conv5 [uspl3]
  0 -3  0
 -3 12 -3
  0 -3  0
)

; ; Embossing Filter
; (matrix :conv5 [uspl3]
;   -2 -1  0
;   -1  1  1
;    0  1  2
; )

(expr :mopr6 [conv5]
  "ddd"
)

(rgb :ras7 [ras4 mopr6]
  (rgb (+ ras4.rgb mopr6.rgb))
)
(render 6)
