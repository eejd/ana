#pragma once
#include "../ana_features.h"
#ifdef WITH_openCL

#include "ofxOsc.h"
#include "MSAOpenCL.h"
#include "ofApp.h"
#include "calc/FloatArrays.h"
#include "calc/ScalarLisp.h"
#include "cl/ClFilter.h"

// see: addons/ofxMSAOpenCL/example-Particles/src/
typedef struct {
    float mass;
    float rad;
    float d2;
    float d3;
} AgentAttributes;

typedef struct {
    float4 pos;
    float4 vel;
} AgentState;

class ClVerticesPingPong : public ClFilter {
	public:
		ClVerticesPingPong(ofGLWindowSettings settings, OscListener * oscParams);
		virtual ~ClVerticesPingPong();
		virtual void setup(const std::string id, int numSamples);
	    virtual ofTexture& getTexture();
		virtual void reload(ofxOscMessage const &msg);
	    virtual bool reloadClProg(const bool force);
		virtual void update();
	    virtual void draw(int li);

    protected:
        msa::OpenCLKernelPtr ag_setup_kernel;
        msa::OpenCLKernelPtr ag_move_kernel;

        size_t agent_length = 32;
        size_t trace_length = 4*1024;
        size_t trace_index = 0;

        ofShader buildin_shader;
        int progID = -1;
        const size_t per_vert_stride = 16; // __global float16* traceBuffer
        GLuint vertexArrayID = -1;
        GLuint vertexbufferID = -1;
        GLint posAttrib = -1;
        GLint normalAttrib = -1;
        GLint colorAttrib = -1;

        msa::OpenCLBuffer clAgentAttributeBuffer;
        msa::OpenCLBuffer clAgentStateBuffers[2]; // ping pong buffers with the same size
        msa::OpenCLBufferManagedT<float> traceBuffer;
//        msa::OpenCLBuffer traceBuffer;

        int  pingPongIndex = 0;

        ScalarLisp slAl;
        ScalarLisp slTl;
        ScalarLisp slLightPositionX;
        ScalarLisp slLightPositionY;
        ScalarLisp slLightPositionZ;
        ScalarLisp slRx;
        ScalarLisp slRy;
        ScalarLisp slRz;
        ScalarLisp slP0;
        ScalarLisp slP1;
        ScalarLisp slP2;
        ScalarLisp slP3;
        ScalarLisp slP4;
        ScalarLisp slP5;
        ScalarLisp slP6;
        ScalarLisp slP7;
        double p0 = 0;
        double p1 = 0;
        double p2 = 0;
        double p3 = 0;
        double p4 = 0;
        double p5 = 0;
        double p6 = 0;
        double p7 = 0;

        void allocateBuffers(size_t al, size_t tl);
        void releaseBuffers();
        void run_ag_kernel(msa::OpenCLKernelPtr kernel);
};
#endif // WITH_openCL
