(ns lv.demo)
(use 'lv.core)

(layout "grid" [imgs0 imgs1] [ras2])
(imagesequ "20210401-hirsch-1/*.png")
(render)

(hsv :imgs0 [c0]
  ; (kaleid (+ 4 (* 2 (sin f))))
  (kaleid (+ 4 (* (/ f 400))))
  (rot (* -0.01 f))
  ; (mirror)
  (k (even? (* 181 y)
           sframe
           (- slength sframe)))
  (h c0.h)
  (s c0.s)
  ; (v (sqrt (* (- 1 r) c0.v)))
  (v (* 1.5 (- 1 (sqr r)) c0.v))
)
(render 0)

(hsv :imgs1 [c0]
  (rot (* -0.01 f))
  ; (mirror)
  (k (even? (* 71 y)
           sframe
           (- slength sframe)))
  (h c0.h)
  (s c0.s)
  ; (v (sqrt (* (- 1 r) c0.v)))
  (v (* 2 (- 1 (sqr r)) c0.v))
)
(render 1)

(rgb :ras2 [imgs0 imgs1 ras2]
  ; (mirror)
  ; (kaleid 3)
  (rgb (mix ras2.rgb
            (* imgs0.rgb imgs1.rgb)
            0.3))
)
(render 2)
