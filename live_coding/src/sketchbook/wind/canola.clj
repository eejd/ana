(ns lv.demo)
(use 'lv.core)

(layout "grid" [mov0 cline1]
               [mopr2 ras3])

(movie "4mt/P*.MOV")
(render)
(hsv :mov0 [c0 c1 c2 c3 c4 c5 c6 c7]
  ; (mirror)
  ; (pixelate-x (* (unoise x y) 75))
  (pixelate-x 25)
  (pixelate-y 25)
  ; (kaleid 5)
  (sx 1.5)
  (sy 1.52)
  (hsv c3.hsv)
)

(expr :dist1 [mov0]
  (step 20)
  (in 0)
  (mode 0)
  (dist 7)
)
(expr :cline1 [mov0]
  (count 150)
  (max 5000)
  (thres (usin (* 0.1 f)))
  (fill -1)
  (paint -1)
  (holes 1)
)

(expr :mopr2 [cline1]
  "db"
)
(rgb :ras3 [mov0 mopr2]
  ; (rgb (* 1.5 mov0.rgb dist1.rgb))
  (rgb (+ (* 0.8 mov0.rgb)
          (* 1.5 mopr2.rgb)))
)
(render 3)
