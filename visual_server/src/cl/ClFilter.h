#pragma once
#include "../ana_features.h"
#ifdef WITH_openCL
#include "ofMain.h"
#include "MSAOpenCL.h"
#include "OscListener.h"
#include "Filter.h"

#define CLFREE(c) if(c) { clReleaseMemObject(c->getCLMem()); c = nullptr; }

typedef struct {
    unsigned int numBuffers;
    unsigned int numChannels;
    unsigned int width;
    unsigned int height;
    float * buf;
} BufferTransfer;


class ClFilter: public Filter {
public:
    ClFilter(ofGLWindowSettings settings, OscListener * oscParams);
    virtual ~ClFilter();
    virtual bool reloadClProg(const bool force);
    virtual void reload(ofxOscMessage const &msg);
    virtual void draw(int li);

    string clProgName;
    long clProgModTime = 0;
    bool fatalClProg = false;
    std::filesystem::path pathClProg;
    msa::OpenCLProgramPtr pcl = nullptr;

    msa::OpenCLKernelPtr setup_kernel;
    msa::OpenCLKernelPtr update_kernel;

    msa::OpenCLImage clOutImage;
};
#endif // WITH_openCL
