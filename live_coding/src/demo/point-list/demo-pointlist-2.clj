(ns lv.demo)
(use 'lv.core)

(layout "grid" [pl0 pl1] [mopr2 ras3])
(render)

(expr :pl0 []
  (count 300)
  (x (* 2 i (cos (* (+ f i) 2.3998))))
  (y (* 2 i (sin (* (+ f i) 2.3998))))
  (w (* 0.05 (distance 0 0 x y)))
)

(expr :pl1 []
  (mode 2)
  (count 300)
  (x (* 2 i (cos (* (+ f i) 2.3998))))
  (y (* 2 i (sin (* (+ f i) 2.3998))))
  (w (* 0.05 (distance 0 0 x y)))
  (s 1)
)

(expr :mopr2 [pl1]
  "edddd"
)

(rgb :ras3 [pl0 mopr2]
  (rot (* a -0.01 f))
  (rgb (mix pl0.rgb mopr2.rgb (- 1.1 r)))
  ; (rgb (max pl0.rgb mopr2.rgb))
)
