#include "../ana_features.h"
#ifdef WITH_openCL
#ifdef BROKEN_stuff

#include "ofMain.h"
#include "MSAOpenCL.h"
#include "ofApp.h"
#include "globalvars.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispMng.h"
#include "cl/ClImagePingPong.h"

ClImagePingPong::ClImagePingPong(ofGLWindowSettings settings, OscListener * oscParams) :
ClFilter(settings, oscParams) {
}

ClImagePingPong::~ClImagePingPong() {
}

size_t ClImagePingPong::getKernelWidth() {
    return filterWidth;
}

size_t ClImagePingPong::getKernelHeight() {
    return filterHeight;
}

void ClImagePingPong::setup(const std::string id, int numSamples) {
    Filter::setup(id, numSamples);

    pingPongIndex = 0;

//    clImageInput.initWithTexture(getKernelWidth(), getKernelHeight(), GL_RGBA32F); // TODO
    clImageBuffers[0].initWithTexture(getKernelWidth(), getKernelHeight(), GL_RGBA32F);
	clImageBuffers[1].initWithTexture(getKernelWidth(), getKernelHeight(), GL_RGBA32F);
}

void ClImagePingPong::reload(ofxOscMessage const &msg) {
    ClFilter::reload(msg);

    ScalarLispMng mng;
    mng.compile(slUpstream, "paint", "-1", msg);
    mng.compile(slLoops, "loops", "1", msg);
    mng.compile(slP0, "p0", "1.0", msg);
    mng.compile(slP1, "p1", "1.0", msg);
    mng.compile(slP2, "p2", "1.0", msg);
    mng.compile(slP3, "p3", "1.0", msg);
    mng.compile(slP4, "p4", "1.0", msg);
    mng.compile(slP5, "p5", "1.0", msg);
    mng.compile(slP6, "p6", "1.0", msg);
    mng.compile(slP7, "p7", "1.0", msg);

    pingPongIndex = 0;
    glFinish();
    runKernel(setup_kernel);
    openCL.finish();
}

ofTexture& ClImagePingPong::getTexture() {
    openCL.finish();
    return fbo.getTexture();
}

void ClImagePingPong::runKernel(msa::OpenCLKernelPtr kernel) {
    if(isSaveForKernel()) {
        kernel->setArg(0, clImageInput);
        kernel->setArg(1, clImageBuffers[pingPongIndex]);
        kernel->setArg(2, clImageBuffers[1 - pingPongIndex]);

        kernel->setArg(3,     (float)slP0.rpn.execute0());
        kernel->setArg(3 + 1, (float)slP1.rpn.execute0());
        kernel->setArg(3 + 2, (float)slP2.rpn.execute0());
        kernel->setArg(3 + 3, (float)slP3.rpn.execute0());
        kernel->setArg(3 + 4, (float)slP4.rpn.execute0());
        kernel->setArg(3 + 5, (float)slP5.rpn.execute0());
        kernel->setArg(3 + 6, (float)slP6.rpn.execute0());
        kernel->setArg(3 + 7, (float)slP7.rpn.execute0());
        kernel->run2D(getKernelWidth(), getKernelHeight());
        pingPongIndex = 1 - pingPongIndex;
    }
}

void ClImagePingPong::update(){
    if(clProgName.empty())
        return;

    glFinish();
    if(upstream_ids.size() >= 1 && slUpstream.rpn.execute0() > 0) {
        auto in_id = upstream_ids[0];
        if(filterDict.find(in_id) != filterDict.end()) {
            auto f = filterDict[in_id];
            if(f && !hasImageInput) {
                clImageInput.initFromTexture(f->getTexture());
                hasImageInput = true;
            }
        }
    }

    if(reloadClProg(false)) {
        runKernel(setup_kernel);
    }
    size_t loops = std::max(1.0, slLoops.rpn.execute0());
    for(size_t li=0; li<loops; ++li) {
        runKernel(update_kernel);
    }
}

bool ClImagePingPong::isSaveForKernel() {
    return !fatalClProg && !clProgName.empty() && hasImageInput;
}

void ClImagePingPong::draw(int li) {
    fbo.begin();
    if(isSaveForKernel()) {
        // make sure all OpenCL kernels have finished executing before drawing
        openCL.finish();
        clImageBuffers[1 - pingPongIndex].getTexture().draw(0, 0);
    }
    else {
        ofClear(0, 0, 0, 255);
    }
    fbo.end();
}
#endif // BROKEN_stuff
#endif // WITH_openCL
