(ns lv.demo)
(use 'lv.core)

(layout "grid" [imgs0 img1] [ras2 ras3])
(imagesequ "20210401-hirsch-1/*.png")
(image "slitscan/orbit-FixedVerticalSlit-121125.png")
; (render)

(hsv :imgs0 [c0]
  (k (* slength
        (unoise (* 0.01 r sframe)
        (* 2.5 x))))
  (h c0.h)
  (s c0.s)
  (v (* 1.2 (- 1 (sqr r)) c0.v))
)
(render 0)

(hsv :img1 [c0]
  (tx (+ -1 (* 0.002 f)))
  (ty -0.05)
  (h c0.h)
  (s c0.s)
  (v c0.v)
)
(render 1)

(rgb :ras2 [imgs0 img1 ras2]
  (rgb (mix ras2.rgb
            (* imgs0.rgb img1.rgb)
            0.3))
)
(render 2)

(hsv :ras3 [ras2 imgs0]
  ; (inv)
  ; (mirror)
  ; (rot (* 0.01 f))
  ; (kaleid 2)
  (hsv ras2.hsv)
  (v (sqrt (atan ras2.v imgs0.v))))
)
(render 3)
