#include "ana_features.h"
#ifdef WITH_nanoKontrol2_midi_input
#include "ofMain.h"
#include "ofxMidi.h"
#include "ofxMidiIn.h"
#include "ofxMidiMessage.h"

#include "globalvars.h"
#include "input/NanoKontrol2_midi_input.h"

NanoKontrol2::NanoKontrol2() {
}

NanoKontrol2::~NanoKontrol2() {
}

void NanoKontrol2::setup() {
    name = "";
    bool ok = false;

    size_t numPorts = midiIn.getNumInPorts();
    unsigned int nanoKontrolPort = 1;
    const std::string nanoKONTROL2("nanoKONTROL2");
    for(size_t i = 0; i < numPorts; ++i) {
        if(midiIn.getInPortName(i).substr(0, nanoKONTROL2.size()) == nanoKONTROL2) {
            name = midiIn.getInPortName(i);
            nanoKontrolPort = i;
            ok = midiIn.openPort(nanoKontrolPort);
            // add ofApp as a listener
            midiIn.addListener(this);

            // Don't ignore sysex, timing, or active sensing messages.
            midiIn.ignoreTypes(false, false, false);
            // print received messages to the console
            //midiIn.setVerbose(true);
            break;
        }
    }
    if(!ok)
        ofLog(OF_LOG_WARNING) << "can not open nanoKONTROL2";
    else
        ofLog(OF_LOG_NOTICE) << "found " << name;

}

void NanoKontrol2::newMidiMessage(ofxMidiMessage& msg) {
//    ofLog(OF_LOG_NOTICE) << "NanoKontrol2: " << msg.toString();
    const float max_val = 128.0f;
    switch(msg.status) {
    case MIDI_CONTROL_CHANGE:
        switch(msg.control) {
        // slider
        case 0x0:
            ks0 = (float)msg.value / max_val;
            return;
        case 0x1:
            ks1 = (float)msg.value / max_val;
            return;
        case 0x2:
            ks2 = (float)msg.value / max_val;
            return;
        case 0x3:
            ks3 = (float)msg.value / max_val;
            return;
        case 0x4:
            ks4 = (float)msg.value / max_val;
            return;
        case 0x5:
            ks5 = (float)msg.value / max_val;
            return;
        case 0x6:
            ks6 = (float)msg.value / max_val;
            return;
        case 0x7:
            ks7 = (float)msg.value / max_val;
            return;

        // potentiometer
        case 0x10:
            kp0 = (float)msg.value / max_val;
            return;
        case 0x11:
            kp1 = (float)msg.value / max_val;
            return;
        case 0x12:
            kp2 = (float)msg.value / max_val;
            return;
        case 0x13:
            kp3 = (float)msg.value / max_val;
            return;
        case 0x14:
            kp4 = (float)msg.value / max_val;
            return;
        case 0x15:
            kp5 = (float)msg.value / max_val;
            return;
        case 0x16:
            kp6 = (float)msg.value / max_val;
            return;
        case 0x17:
            kp7 = (float)msg.value / max_val;
            return;

        // push button
        case 0x20:
            if(msg.value > 0) kb0 = 1;
            return;
        case 0x30:
            if(msg.value > 0) kb0 = 0;
            return;
        case 0x40:
            if(msg.value > 0) kb0 = -1;
            return;

        case 0x21:
            if(msg.value > 0) kb1 = 1;
            return;
        case 0x31:
            if(msg.value > 0) kb1 = 0;
            return;
        case 0x41:
            if(msg.value > 0) kb1 = -1;
            return;

        case 0x22:
            if(msg.value > 0) kb2 = 1;
            return;
        case 0x32:
            if(msg.value > 0) kb2 = 0;
            return;
        case 0x42:
            if(msg.value > 0) kb2 = -1;
            return;

        case 0x23:
            if(msg.value > 0) kb3 = 1;
            return;
        case 0x33:
            if(msg.value > 0) kb3 = 0;
            return;
        case 0x43:
            if(msg.value > 0) kb3 = -1;
            return;

        case 0x24:
            if(msg.value > 0) kb4 = 1;
            return;
        case 0x34:
            if(msg.value > 0) kb4 = 0;
            return;
        case 0x44:
            if(msg.value > 0) kb4 = -1;
            return;

        case 0x25:
            if(msg.value > 0) kb5 = 1;
            return;
        case 0x35:
            if(msg.value > 0) kb5 = 0;
            return;
        case 0x45:
            if(msg.value > 0) kb5 = -1;
            return;

        case 0x26:
            if(msg.value > 0) kb6 = 1;
            return;
        case 0x36:
            if(msg.value > 0) kb6 = 0;
            return;
        case 0x46:
            if(msg.value > 0) kb6 = -1;
            return;

        case 0x27:
            if(msg.value > 0) kb7 = 1;
            return;
        case 0x37:
            if(msg.value > 0) kb7 = 0;
            return;
        case 0x47:
            if(msg.value > 0) kb7 = -1;
            return;

        default:
            return;
        }
        return;
    default:
        return;
    }
}

std::string NanoKontrol2::getName() {
    return name;
}

#endif // WITH_nanoKontrol2_midi_input
