(ns lv.demo)
(use 'lv.core)

(layout "grid" [imgs0]
               [ras3])

(imagesequ "CalArts-Dance-2019/side-p1/*.png")
(render 0)
(rgb :imgs0 [c0]
  (k (% (int (+ (* 0.05 i) sframe)) slength))
  (rgb (* 1.5 c0.rgb))
)

(rgb :ras3 [imgs0 imgs1 imgs2]
)
