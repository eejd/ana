#include <memory>
#include <opencv2/opencv.hpp>

#include "ofMain.h"

#include "util/stringhelper.h"
#include "globalvars.h"
#include "Configuration.h"
#include "Filter.h"
#include "ofApp.h"
#include "calc/MsgLisp.h"
#include "Filter.h"
#include "cl/ClAgentPingPong.h"
#include "cl/ClScaledPingPong.h"
#include "cl/ClPingPong.h"
#include "cl/ClImagePingPong.h"
#include "cl/ClUpstreamPingPong.h"
#include "cl/ClUpstreamPoints.h"
#include "cl/ClVerticesPingPong.h"
#include "cl/ClImageGenerator.h"
#include "cv/CvBackground.h"
#include "cv/CvFlowFarneback.h"
#include "cv/CvMotionTrace.h"
#include "fv/FvFromCl.h"
#include "fv/FvHit.h"
#include "fv/FvPl.h"
#include "fv/FvRemapPoints.h"
#include "gl/GlScanlines.h"
#include "gl/Gl3dScanlines.h"
#include "gl/GlBillboard.h"
#include "gl/GlConvolutionFilter.h"
#include "gl/GlFrameDelayFilter.h"
#include "gl/GlFrameDiff.h"
#include "gl/GlImageFilter.h"
#include "gl/GlImageSequenceFilter.h"
#include "gl/GlIpVideoFilter.h"
#include "gl/GlLoadFilter.h"
#include "gl/GlMoOprFilter.h"
#include "gl/GlMovieFilter.h"
#include "gl/GlRasterFilter.h"
#include "gl/GlScreenFilter.h"
#include "gl/GlVideoFilter.h"
#include "gl/GlWindowFilter.h"
#include "frag/FragFilter.h"
#include "frag/FragDoubleBufferFilter.h"
#include "frag/FragMultipassFilter.h"
#include "img/ImgPixelSort.h"
#include "img/ImgTileSort.h"
#include "satellite/LedSenseHAT.h"
#include "shape/ShapeContour.h"
#include "shape/ShapeCounter.h"
#include "shape/ShapeLsys.h"
#include "shape/ShapeMeshDeform.h"
#include "shape/ShapeNumber.h"
#include "shape/ShapePainter.h"
#include "shape/ShapeVboDeform.h"
#include "layout/HorizontalLayout.h"
#include "layout/VerticalLayout.h"
#include "layout/GridLayout.h"
#include "layout/LayoutMng.h"

LayoutMng::LayoutMng() {
}

LayoutMng::~LayoutMng() {
    if(layout)
        delete layout;
}

void LayoutMng::setRender(std::string descr) {
    MsgLisp l1;
    ast_m* res = l1.compile(descr);
    bool ok = res
            && res->count() == 2
            && res->at(0)->tok == "render";
    if(ok) {
        render = -1;
        ast_m* r1 = res->at(1);
        if(r1->tok == "[" && r1->count() >= 1) {
            render = std::stoi(r1->at(0)->tok);
        }
    }
}

void LayoutMng::setRender(int r) {
    if(!layout)
        return;

    if(r < 0) {
        render = -1;
    }
    else {
        render = r % layout->numFilters();
    }
}

void LayoutMng::nextRender() {
    if(!layout)
        return;

    render = (render + 1) % layout->numFilters();
}

void LayoutMng::previousRender() {
    if(!layout)
        return;

    --render;
    if(render < 0)
        render = layout->numFilters() - 1;
}

Layout* LayoutMng::update(std::string descr) {
    Layout* new_layout = nullptr;
    bool layout_not_understood = true;
    ofLog(OF_LOG_NOTICE) << descr;

    MsgLisp l1;
    ast_m* res = l1.compile(descr);
    bool ok = res
            && res->count() >= 3
            && res->at(0)->tok == "layout";
    if(ok) {
        if(res->at(1)->tok == "\"grid\""
                && res->count() >= 3
                && res->childCount() >= 1) {
            rows = 0;
            cols = std::numeric_limits<int>::min();
            fid.clear();
            for(int cx=0; cx<res->count(); ++cx) {
                if(res->at(cx)->tok == "[") {
                    ++rows;
                    int temp_cols = 0;
                    for(int fx=0; fx<res->at(cx)->count(); ++fx) {
                        ++temp_cols;
                        fid.push_back(res->at(cx)->at(fx)->tok);
                    }
                    if(temp_cols > cols)
                        cols = temp_cols;
                }
            }
            layout_not_understood = cols < 1;
            if(!layout_not_understood)
                new_layout = new GridLayout(rows, cols);
        }
        else if(res->at(1)->tok == "\"vertical\""
                && res->count() >= 3) {
            rows = 1;
            cols = res->at(2)->count();
            fid.clear();
            for(int fx=0; fx<res->at(2)->count(); ++fx) {
                fid.push_back(res->at(2)->at(fx)->tok);
            }
            new_layout = new VerticalLayout(cols);
            layout_not_understood = false;
        }
        else if(res->at(1)->tok == "\"horizontal\""
                && res->count() >= 3) {
            rows = res->at(2)->count();
            cols = 1;
            fid.clear();
            for(int fx=0; fx<res->at(2)->count(); ++fx) {
                fid.push_back(res->at(2)->at(fx)->tok);
            }
            new_layout = new HorizontalLayout(rows);
            layout_not_understood = false;
        }
    }

    if(layout_not_understood || !new_layout) {
        ofLog(OF_LOG_FATAL_ERROR) << "layout not understood ’" << descr << "’";
        return nullptr;
    }
    return new_layout;
}

void LayoutMng::draw(std::vector<std::shared_ptr<Filter>> &filterLine) {
    if(layout)
        layout->draw(render, filterLine);
}

std::shared_ptr<Filter> LayoutMng::loadInstance(const std::string id, ofGLWindowSettings window_settings, OscListener * oscParams) {
    std::shared_ptr<Filter> filter = nullptr;
    if(id.find("ras") == 0) {
        filter = std::make_shared<GlRasterFilter>(window_settings, oscParams);
#ifdef WITH_openCL
    } else if(num_cl_gpu > 0 && id.find("uspl") == 0) {
        filter = std::make_shared<ClUpstreamPoints>(window_settings, oscParams);
    } else if(num_cl_gpu > 0 && id.find("clpp") == 0) {
        filter = std::make_shared<ClPingPong>(window_settings, oscParams);
    } else if(num_cl_gpu > 0 && id.find("clspp") == 0) {
        filter = std::make_shared<ClScaledPingPong>(window_settings, oscParams);
    } else if(num_cl_gpu > 0 && id.find("clipp") == 0) {
#ifdef BROKEN_stuff
        filter = std::make_shared<ClImagePingPong>(window_settings, oscParams);
#endif // BROKEN_stuff
    } else if(num_cl_gpu > 0 && id.find("clup") == 0) {
        filter = std::make_shared<ClUpstreamPingPong>(window_settings, oscParams);
    } else if(num_cl_gpu > 0 && id.find("clag") == 0) {
        filter = std::make_shared<ClAgentPingPong>(window_settings, oscParams);
    } else if(num_cl_gpu > 0 && id.find("clvb") == 0) {
        filter = std::make_shared<ClVerticesPingPong>(window_settings, oscParams);
    } else if(num_cl_gpu > 0 && id.find("igen") == 0) {
        filter = std::make_shared<ClImageGenerator>(window_settings, oscParams);
    } else if(num_cl_gpu > 0 && id.find("clfv") == 0) {
        filter = std::make_shared<FvFromCl>(window_settings, oscParams);
#endif // WITH_openCL
    } else if(id.find("imgs") == 0) {
        filter = std::make_shared<GlImageSequenceFilter>(window_settings, oscParams);
    } else if(id.find("img") == 0) {
        filter = std::make_shared<GlImageFilter>(window_settings, oscParams);
    } else if(id.find("conv") == 0) {
        filter = std::make_shared<GlConvolutionFilter>(window_settings, oscParams);
    } else if(id.find("mov") == 0) {
        filter = std::make_shared<GlMovieFilter>(window_settings, oscParams);
    } else if(id.find("pattern") == 0) {
        filter = std::make_shared<ShapePainter>(window_settings, oscParams, 1);
    } else if(id.find("pihat") == 0) {
        filter = std::make_shared<LedSenseHAT>(window_settings, oscParams);
    } else if(id.find("shape") == 0) {
        filter = std::make_shared<ShapePainter>(window_settings, oscParams, 0);
    } else if(id.find("count") == 0) {
        filter = std::make_shared<ShapeCounter>(window_settings, oscParams);
    } else if(id.find("numb") == 0) {
        filter = std::make_shared<ShapeNumber>(window_settings, oscParams);
#ifdef WITH_video_grabber
    } else if(id.find("vid") == 0) {
        filter = std::make_shared<GlVideoFilter>(window_settings, oscParams);
#endif // WITH_video_grabber
    } else if(id.find("wvid") == 0) {
        filter = std::make_shared<GlIpVideoFilter>(window_settings, oscParams);
    } else if(id.find("mopr") == 0) {
        filter = std::make_shared<GlMoOprFilter>(window_settings, oscParams);
    } else if(id.find("bill") == 0) {
        filter = std::make_shared<GlBillboard>(window_settings, oscParams);
    } else if(id.find("fdiff") == 0) {
        filter = std::make_shared<GlFrameDiff>(window_settings, oscParams);
    } else if(id.find("cline") == 0) {
        filter = std::make_shared<ShapeContour>(window_settings, oscParams);
    } else if(id.find("dmesh") == 0) {
        filter = std::make_shared<ShapeVboDeform>(window_settings, oscParams);
    } else if(id.find("dist") == 0) {
        filter = std::make_shared<ShapeMeshDeform>(window_settings, oscParams);
    } else if(id.find("sline") == 0) {
        filter = std::make_shared<GlScanlines>(window_settings, oscParams);
    } else if(id.find("s3line") == 0) {
        filter = std::make_shared<Gl3dScanlines>(window_settings, oscParams);
    } else if(id.find("hit") == 0) {
        filter = std::make_shared<FvHit>(window_settings, oscParams);
    } else if(id.find("pl") == 0) {
        filter = std::make_shared<FvPl>(window_settings, oscParams);
    } else if(id.find("mt") == 0) {
        filter = std::make_shared<CvMotionTrace>(window_settings, oscParams);
    } else if(id.find("ffb") == 0) {
        filter = std::make_shared<CvFlowFarneback>(window_settings, oscParams);
    } else if(id.find("bkg") == 0) {
        filter = std::make_shared<CvBackground>(window_settings, oscParams);
    } else if(id.find("map2d") == 0) {
        filter = std::make_shared<FvRemapPoints>(window_settings, oscParams);
    } else if(id.find("lsys") == 0) {
        filter = std::make_shared<ShapeLsys>(window_settings, oscParams);
    } else if(id.find("pix") == 0) {
        filter = std::make_shared<ImgPixelSort>(window_settings, oscParams);
    } else if(id.find("tile") == 0) {
        filter = std::make_shared<ImgTileSort>(window_settings, oscParams);
    } else if(id.find("screen") == 0) {
        filter = std::make_shared<GlScreenFilter>(window_settings, oscParams);
    } else if(id.find("win") == 0) {
        filter = std::make_shared<GlWindowFilter>(window_settings, oscParams);
    } else if(id.find("frag") == 0) {
        filter = std::make_shared<FragFilter>(window_settings, oscParams);
    } else if(id.find("dfrag") == 0) {
        filter = std::make_shared<FragDoubleBufferFilter>(window_settings, oscParams);
    } else if(id.find("del") == 0) {
        filter = std::make_shared<GlFrameDelayFilter>(window_settings, oscParams);
    } else if(id.find("load") == 0) {
        filter = std::make_shared<GlLoadFilter>(window_settings, oscParams);
    } else if(id.find("fx") == 0) {
        filter = std::make_shared<FragMultipassFilter>(window_settings, oscParams);
    }

    if(filter == nullptr) {
        if(num_cl_gpu > 0)
            ofLog(OF_LOG_ERROR) << "unknown filter type '" << id << "'";
        else
            ofLog(OF_LOG_ERROR) << "unknown filter type, or filter needs OpenCL: '" << id << "'";
    } else {
        ofLog(OF_LOG_NOTICE) << "using filter '" << id << "'";
        filterDict[id] = filter;
    }
    return filter;
}

void LayoutMng::loadPipeline(std::vector<std::shared_ptr<Filter>> &filterLine, ofGLWindowSettings window_settings, OscListener * oscParams) {
    Layout* new_layout = update(pipe0);
    if(new_layout) {
        if(layout)
            delete layout;
        layout = new_layout;
    }

    if(!layout)
        return;

    std::vector<std::shared_ptr<Filter>> tempFilterLine;
    for(auto s : filterLine) {
        tempFilterLine.push_back(s);
    }
    filterLine.clear();

    for(auto it = fid.begin(); it != fid.end(); ++it) {
        std::string id = trim(*it);
        bool found = false;
        for(auto s : tempFilterLine) {
            if(s->getId() == id) {
                found = true;
                filterLine.push_back(s);
//                std::cout << "--- copy to filterLine: " << s->getId() << '\n';
                break;
            }
        }
        if(!found) {
            auto filter = loadInstance(id, window_settings, oscParams);
            if(filter) {
                filter->setup(id, 0);
//                std::cout << "--- new in filterLine: " << filter->getId() << '\n';
                filterLine.push_back(filter);
            }
        }
    }
    tempFilterLine.clear();
}


