(ns lv.demo)
(use 'lv.core)

(layout "grid" [win0 ras1]
               [ras2 ras3])
(render)

; read from a visible X11 window
(expr :win0 []
  "score 3.0.0 - ana/live_coding/src/demo/ossia/aNa2.score"
)

(hsv :ras1 [win0]
  (h (+ u6 win0.h))
  (s win0.s)
  (v (* u7 win0.v))
)

(hsv :ras2 [win0]
  (sx u0)(sy (+ 1 u1))
  (kaleid (+ 3 u1 u0))
  (h win0.h)
  (s win0.s)
  (v (* win0.v (- 1 r)))
)

; mix ras1 with ras2
(rgb :ras3 [ras1 ras2]
  (rgb (max ras1.rgb ras2.rgb))
)
(render 3)
