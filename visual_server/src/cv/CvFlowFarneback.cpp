#include "ofMain.h"
#include "ofxCv.h"

#include "globalvars.h"
#include "util/p3circle.h"
#include "OscListener.h"
#include "Filter.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispMng.h"
#include "cv/CvFlowFarneback.h"

CvFlowFarneback::CvFlowFarneback(ofGLWindowSettings settings, OscListener *oscParams) :
        Filter(settings, oscParams) {
}

CvFlowFarneback::~CvFlowFarneback() {
}

void CvFlowFarneback::setup(const std::string id, int numSamples) {
    Filter::setup(id, numSamples);
}

void CvFlowFarneback::reload(ofxOscMessage const &msg) {
    auto narg = msg.getNumArgs();
    if(narg > 1) {
        Filter::setInputTexture(msg.getArgAsString(1));
    }
    ScalarLispMng mng;
    mng.compile(slReduce, "reduce", "12", msg);
    reduced_size = std::max(1, (int)slReduce.rpn.execute0());
    reduced_filterWidth  = filterWidth  / reduced_size;
    reduced_filterHeight = filterHeight / reduced_size;
    need_realloc = true;
    mng.compile(slPyrScale, "pscale", "0.5", msg);
    mng.compile(slLevels, "levels", "4", msg);
    mng.compile(slWinSize, "win", "8", msg);
    mng.compile(slIterations, "iter", "2", msg);
    mng.compile(slPolyN, "polyn", "7", msg);
    mng.compile(slPolySigma, "polys", "1.5", msg);
    mng.compile(slUseGaussian, "gauss", "-1", msg);
    mng.compile(slMode, "mode", "", msg);
    mng.compile(slClear, "clear", "1", msg);
    mng.compile(slSaturation, "s", "1", msg);
    mng.compile(slStepSize, "step", "4", msg);
    mng.compile(slCircleAlpha, "a", "0.5", msg);
    mng.compile(slCircleRadius, "r", "5", msg);
    mng.compile(slUpstream, "paint", "-1", msg);
}

void CvFlowFarneback::update() {
    hasData = false;
    mode = slMode.rpn.execute0();

    if(upstream_ids.size() >= 1) {
        auto in_id = upstream_ids[0];
        if(filterDict.find(in_id) != filterDict.end()) {
            f = filterDict[in_id];
            if(f) {
                f->getTexture().readToPixels(grabbedPixels);
                frame_captured = ofxCv::toCv(grabbedPixels);
                cv::resize(frame_captured, frame_scaled, cv::Size(reduced_filterWidth, reduced_filterHeight), cv::INTER_AREA);
                cv::cvtColor(frame_scaled, frame_scaled, cv::COLOR_RGBA2GRAY);
                if(need_realloc) {
                    prev_scaled.create(frame_scaled.size(), CV_32FC2);
                    distortion.create(frame_captured.size(), CV_32FC2);
                    need_realloc = false;
                }
                else {
                    cv::calcOpticalFlowFarneback(prev_scaled, frame_scaled, flow,
                            slPyrScale.rpn.execute0(),
                            slLevels.rpn.execute0(),
                            slWinSize.rpn.execute0(),
                            slIterations.rpn.execute0(),
                            slPolyN.rpn.execute0(),
                            slPolySigma.rpn.execute0(),
                            slUseGaussian.rpn.execute0() >= 0.0 ? cv::OPTFLOW_FARNEBACK_GAUSSIAN : 0);

                    if(mode == 3) {
                        int i = 0;
                        for (int y = 0; y < distortion.rows; ++y) {
                            for (int x = 0; x < distortion.cols; ++x) {
                                cv::Point2f fl = flow.at<cv::Point2f>(y/reduced_size, x/reduced_size);
                                float cr = slCircleRadius.rpn.execute3d1i(x, y, r2, i);
                                distortion.at<cv::Point2f>(y, x) = cv::Point2f(x + fl.x*cr, y + fl.y*cr);
//                                cv::Point2f fl0 = flow.at<cv::Point2f>(std::max(0,y-1)/reduced_size, std::max(0,x-1)/reduced_size);
//                                cv::Point2f fl1 = flow.at<cv::Point2f>(y/reduced_size, x/reduced_size);
//                                float ipx = (float)x / (float)reduced_size - x/reduced_size;
//                                float ipy = (float)y / (float)reduced_size - y/reduced_size;
//                                float dx = ipx*fl1.x + (1.0f - ipx)*fl0.x;
//                                float dy = ipy*fl1.y + (1.0f - ipy)*fl0.y;
//                                float cr = slCircleRadius.rpn.execute3d1i(x, y, r2, i);
//                                distortion.at<cv::Point2f>(y, x) = cv::Point2f(x + dx*cr, y + dy*cr);
                                ++i;
                            }
                        }
                        cv::remap(frame_captured, frame_distored, distortion, cv::Mat(), CV_INTER_CUBIC);
                    }

                    hasData = true;
                }
                frame_scaled.copyTo(prev_scaled);
            }
        }
    }
}

void CvFlowFarneback::draw(int li) {
    if(!hasData)
        return;

    fbo.begin();
    if(slClear.rpn.execute0() >= 0.0)
        ofClear(0, 0, 0, 255);

    if(f && slUpstream.rpn.execute0() >= 0.0)
        f->getTexture().draw(0, 0);

    int stepSize = slStepSize.rpn.execute0();
    int cols = flow.cols;
    int rows = flow.rows;
    switch(mode) {
    case 1: {
        ofPushMatrix();
        ofScale(reduced_size, reduced_size);
        int i = 0;
        for(int y = stepSize/2; y < rows; y += stepSize) {
            for(int x = stepSize/2; x < cols; x += stepSize) {
                const cv::Vec2f& vec = flow.at<cv::Vec2f>(y, x);
                float r2 = std::sqrt(vec[0]*vec[0] + vec[1]*vec[1]);
                float h = std::atan2(vec[1], vec[0]) / (2 * M_PI);
                while(h < 0.0)
                    h += 1;
                ofFloatColor rgb = ofFloatColor::fromHsb(
                        h,
                        slSaturation.rpn.execute3d1i(vec[0], vec[1], r2, i),
                        std::sqrt(vec[0]*vec[0] + vec[1]*vec[1]));
                rgb.a = slCircleAlpha.rpn.execute3d1i(vec[0], vec[1], r2, i);
                ofSetColor(rgb);
                float cr = slCircleRadius.rpn.execute3d1i(vec[0], vec[1], r2, i);
                if(cr > 0.01) {
                    ofDrawLine(x, y, x + cr * vec[0], y + cr * vec[1]);
                }
                ++i;
            }
        }
        ofPopMatrix();
        }
        break;
    case 2: {
        ofPushMatrix();
        ofScale(reduced_size, reduced_size);
        int i = 0;
        for(int y = stepSize/2; y < rows; y += stepSize) {
            for(int x = stepSize/2; x < cols; x += stepSize) {
                const cv::Vec2f& vec = flow.at<cv::Vec2f>(y, x);
                float r2 = std::sqrt(vec[0]*vec[0] + vec[1]*vec[1]);
                float h = std::atan2(vec[1], vec[0]) / (2 * M_PI);
                while(h < 0.0)
                    h += 1;
                ofFloatColor rgb = ofFloatColor::fromHsb(
                        h,
                        slSaturation.rpn.execute3d1i(vec[0], vec[1], r2, i),
                        std::sqrt(vec[0]*vec[0] + vec[1]*vec[1]));
                rgb.a = slCircleAlpha.rpn.execute3d1i(vec[0], vec[1], r2, i);
                ofSetColor(rgb);
                float cr = slCircleRadius.rpn.execute3d1i(vec[0], vec[1], r2, i);
                if(cr > 0.01) {
                    ofDrawCircle(x, y, cr);
                }
                ++i;
            }
        }
        ofPopMatrix();
        }
        break;
    case 3: {
        ofxCv::drawMat(frame_distored, 0, 0);
        }
        break;
    default: {
        ofFloatColor rgb(1.0f, 1.0f, 1.0f);
        ofPushMatrix();
        ofScale(reduced_size, reduced_size);
        int i = 0;
        for(int y = stepSize/2; y < rows; y += stepSize) {
            for(int x = stepSize/2; x < cols; x += stepSize) {
                const cv::Vec2f& vec = flow.at<cv::Vec2f>(y, x);
                float r2 = std::sqrt(vec[0]*vec[0] + vec[1]*vec[1]);
                float cr = slCircleRadius.rpn.execute3d1i(vec[0], vec[1], r2, i);
                if(cr > 0.01) {
                    rgb.a = slCircleAlpha.rpn.execute3d1i(vec[0], vec[1], r2, i);
                    ofSetColor(rgb);
                    ofDrawLine(x, y, x + cr * vec[0], y + cr * vec[1]);
                }
                ++i;
            }
        }
        ofPopMatrix();
        }
        break;
    }

    fbo.end();
}
