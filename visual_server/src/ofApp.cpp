#include <time.h>
#include <stdlib.h>

#include "ofMain.h"
#ifdef WITH_openCL
#include "MSAOpenCL.h"
#endif

#include "ana_build.h"
#include "ana_features.h"
#include "globalvars.h"
#include "util/pathhelper.h"
#include "util/stringhelper.h"
#include "util/os_depend.h"
#include "Configuration.h"
#include <CursorManager.h>
#include "PreShot.h"
#include "OscListener.h"
#include "Filter.h"
#include "input/Joystick_dev_input_js.h"
#include "input/NanoKontrol2_midi_input.h"
#include "layout/HorizontalLayout.h"
#include "layout/VerticalLayout.h"
#include "layout/GridLayout.h"
#include "calc/MsgLisp.h"
#include "calc/ScalarRPN.h"
#include "calc/MsgLispTest.h"
#include "calc/ScalarLispTest.h"
#include "calc/ScalarLisp.h"
#include "gl/GlIpVideoFilter.h"
#include "Inspector.h"
#include "ofApp.h"

#ifdef WITH_openCL
msa::OpenCL openCL;
#endif

ofApp::ofApp(ofGLWindowSettings settings) :
        window_settings(settings) {
    apptarget = this;
}

void ofApp::loadPipeline() {
    layoutMng.loadPipeline(filterLine, window_settings, oscParams);
}

void ofApp::loadImage() {
    std::vector<std::filesystem::path> files = buildFileList(image0);
    usedImages = 0;
    for(auto it = std::begin(files); it != std::end(files); ++it) {
        if(usedImages >= maxImages)
            break;
        auto fn = *it;
        if(file_exists(fn.string()) && images[usedImages].load(fn)) {
            imagePath[usedImages] = fn;
            ++usedImages;
            ofLogNotice() << "Loaded '" << fn.string() << "'";
        } else {
            ofLogError() << "Failed to load '" << fn.string() << "'";
        }
    }
}

void ofApp::loadMovie() {
    std::vector<std::filesystem::path> files = buildFileList(movie0);
    usedMovies = 0;
    for(auto it = std::begin(files); it != std::end(files); ++it) {
        if(usedMovies >= maxMovies)
            break;
        auto fn = *it;
        if(file_exists(fn.string()) && movies[usedMovies].load(fn.string())) {
            movies[usedMovies].setLoopState(OF_LOOP_NORMAL);
            movies[usedMovies].setVolume(0);
            movies[usedMovies].play();
            ++usedMovies;
            ofLogNotice() << "Loaded '" << fn << "'";
        } else {
            ofLogError() << "Failed to load '" << fn << "'";
        }
    }
}

void ofApp::loadSound() {
    beat.setVolume(0);
    beat.stop();
    beat.unload();
    MsgLisp l1;
    ast_m* res = l1.compile(sound0);
    bool ok = res
            && res->count() >= 2
            && res->at(0)->tok == "sound";
    if(ok) {
        ast_m* r1 = res->at(1);
        if(r1->tok == "[") {
            //TODO support playing multiple sound files. Remove pi < 1
            for(int pi=0; pi < 1 && pi < r1->count(); ++pi) {
                std::string pattern = trim(r1->at(pi)->tok, '"', '"');
                std::string full_path = asString(MEDIA_PATH) + "/" + pattern;
                if(file_exists(full_path) && beat.load(full_path)) {
                    beat.setVolume(1.0);
                    beat.setLoop(asBool(SOUND_LOOP));
                    beat.play();
                    ofLogNotice() << "Loaded '" << pattern << "'";
                } else {
                    ofLogError() << "Failed to load '" << pattern << "'";
                }
            }
        }
    }
}

void ofApp::loadGrabber() {
#ifdef WITH_video_grabber
    closeVideoGrabber();
    std::vector<std::string> videolist = file_names("/dev/video*", false);
    if(videolist.size() > 0) {
        video_width = asInt(VIDEO_WIDTH);
        video_height = asInt(VIDEO_HEIGHT);
        videoGrabber = new ofVideoGrabber();
        videoGrabber->setVerbose(asBool(GLVIDEOFILTER_VERBOSE_GRABBER));
        videoGrabber->setDeviceID(2);
        videoGrabber->setDesiredFrameRate(25);
        videoGrabber->setup(video_width, video_height);
        if(videoGrabber->isInitialized()) {
            ofLog(OF_LOG_NOTICE) << "video grabber " << videoGrabber->getWidth() << "x" << videoGrabber->getHeight() << " " << videoGrabber->isUsingTexture() << " ";
        } else {
            closeVideoGrabber();
        }
    }
#endif // WITH_video_grabber
}

void ofApp::loadIpVideoGrabber(ofxOscMessage const &msg) {
    closeIpVideoGrabber();

//    ipVideoGrabber0.clear();

    auto numArgs = msg.getNumArgs();
    if(numArgs >= 1 && msg.getArgType(0) == OFXOSC_TYPE_STRING) {
        MsgLisp l1;
        ast_m* res = l1.compile(msg.getArgAsString(0));
        bool ok = res
                && res->count() >= 2
                && res->at(0)->tok == "uri";
        if(ok) {
            ast_m* r1 = res->at(1);
            if(r1->tok == "[") {
                for(int pi=0; pi < r1->count(); ++pi) {
                    std::string pattern = trim(r1->at(pi)->tok, '"', '"');
                    ofx::Video::IPVideoGrabber* vg = new ofx::Video::IPVideoGrabber();
                    vg->setURI(pattern);
                    vg->setMaxReconnects(-1);
                    vg->connect();
                    if(vg->isConnected()) {
                        ofLog(OF_LOG_NOTICE) << "ip video grabber connected '" << vg->getURI() << "' " << vg->getWidth() << "x" << vg->getHeight() << " " << vg->isUsingTexture() << " ";
                    } else {
                        ofLog(OF_LOG_ERROR) << " No connection to ip video grabber '" << pattern << "'";
                    }
                    ipVideoGrabbers.push_back(vg);
                }
            }
        }
    }
}

#define TRACE_MONOTONIC
//#undef TRACE_MONOTONIC
void ofApp::loadImageSequence() {
#ifdef TRACE_MONOTONIC
    timespec time1, time2;
    clock_gettime(CLOCK_MONOTONIC, &time1);
#endif // TRACE_MONOTONIC
    sizeImageSequ = 0;
    if(imagesequ0.empty())
        return;
    std::vector<std::filesystem::path> files = buildFileList(imagesequ0);
    size_t found_images = files.size();
    if(found_images < 1)
        return;

    int gi = 0;
    glGetIntegerv(GL_MAX_SHADER_STORAGE_BLOCK_SIZE, &gi);
    size_t max_shader_storage_block_size = gi;
    ofLog(OF_LOG_NOTICE) << "GL_MAX_SHADER_STORAGE_BLOCK_SIZE " << max_shader_storage_block_size;

    glGetIntegerv(GL_MAX_ARRAY_TEXTURE_LAYERS, &gi);
    size_t max_array_texture_layers = gi;
    ofLog(OF_LOG_NOTICE) << "GL_MAX_ARRAY_TEXTURE_LAYERS " << max_array_texture_layers;

    ofImage texture;
    texture.load(files[0]);
    size_t width = texture.getWidth();
    size_t height = texture.getHeight();
    texture.clear();

    bool scale_down = false;
    size_t filterWidth = asInt(FILTER_WIDTH);
    size_t filterHeight = asInt(FILTER_HEIGHT);
    if(width > filterWidth) {
        width = filterWidth;
        scale_down = true;
    }
    if(height > filterHeight) {
        height = filterHeight;
        scale_down = true;
    }

    size_t max_images = std::min(max_array_texture_layers, std::min(found_images, max_shader_storage_block_size / (width * height * 3u)));
    size_t desired_mem_size = max_images * width * height * 3u;

    if(max_images > 0) {
        if(imageSequ_textureID > 0)
            glDeleteTextures(1, &imageSequ_textureID);
        glGenTextures(1, &imageSequ_textureID);
        glBindTexture(GL_TEXTURE_2D_ARRAY, imageSequ_textureID);
        // Allocate the storage.
        ofLog(OF_LOG_NOTICE) << "glTexStorage3D " << width << "x" << height << "x" << max_images  << " Desired GPU memory size " << desired_mem_size;
        glTexStorage3D(GL_TEXTURE_2D_ARRAY, 1, GL_RGB8, width, height, max_images);

        for(auto it = std::begin(files); it != std::end(files); ++it) {
            if(sizeImageSequ >= max_images)
                break;
            auto fn = *it;
            if(texture.load(fn)) {
                if(scale_down)
                    texture.resize(width, height);
                // Upload pixel data.
                // The first 0 refers to the mipmap level (level 0, since there's only 1)
                // The following 2 zeroes refers to the x and y offsets in case you only want to specify a subrectangle.
                // The sizeImageSequ refers to the layer index offset.
                // Altogether you can specify a 3D box subset of the overall texture_array, but only one mip level at a time.
                if(texture.getWidth() == width && texture.getHeight() == height) {
                    glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, sizeImageSequ, width, height, 1, GL_RGB, GL_UNSIGNED_BYTE, texture.getPixels().getData());
                }
                else {
                    ofLog(OF_LOG_ERROR) << "need all images with same size. expected: " << width << "x" << height << " " << " got '" << fn << "' " << texture.getWidth() << "x" << texture.getHeight();
                }
                if(sizeImageSequ % 100 == 0) {
                    ofLog(OF_LOG_NOTICE) << "load '" << fn << "' " << (sizeImageSequ * 100) / max_images << "%";
                }
                ++sizeImageSequ;
            } else {
                ofLogError() << "Failed to load '" << fn << "'";
            }
            texture.clear();
        }
        ofLog(OF_LOG_NOTICE) << sizeImageSequ << " images of " << found_images << " loaded.";

        glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_R, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_REPEAT);

        glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
    } else
        ofLogError() << "No images loaded";
#ifdef TRACE_MONOTONIC
    clock_gettime(CLOCK_MONOTONIC, &time2);
    ofLog(OF_LOG_NOTICE) << "GlImageSequenceFilter::setup()    " << (time2.tv_sec * 1000000000l + time2.tv_nsec) - (time1.tv_sec * 1000000000l + time1.tv_nsec);
#endif // TRACE_MONOTONIC
}
#undef TRACE_MONOTONIC

void ofApp::setup() {
#ifdef WITH_deteced_ipv4
    ipv4_list = os_list_ipv4_addr();
    ofLog(OF_LOG_NOTICE) << "ipv4 " << ipv4_list;
#endif // WITH_deteced_ipv4
    ofLog(OF_LOG_NOTICE) << "port " << asInt(ANALOG_PORT);
#ifdef WITH_testcases
    stringhelper_runTestCases();
    ScalarRPN::runTestCases();
    MsgLispTest::runAstTestCases();
    MsgLispTest::runLispTestCases();
    ScalarLispTest::runTestCases();
    ScalarLispTest::runProfile();
#endif // WITH_testcases

    beat.stop();
    smoothedVol = 0;
    fft_smooth = asFloat(FFT_SMOOTH);

    ofTrueTypeFont::setGlobalDpi(asInt(FONT_DPI));
    infofont.load(asString(INFOFONT), asInt(INFOFONT_SIZE));
    font.load(asString(FONT), asInt(FONT_SIZE));

    preShot.setup();

    oscParams = new OscListener();
    oscParams->setup();

    exportPreshotPath = asString(PRESHOT_PATH);
    exportPreshotPath += "/exp_" + ofGetTimestampString() + "/";
    exportCsvPath = asString(CSV_EXPORT_PATH);
    exportCsvPath += "/exp_" + ofGetTimestampString() + "/";

    if(isContinuousActive && makeDirectory(exportContinuousPath)) {
    }
    else
        isContinuousActive = false;

    int fps = asInt(FPS);
    ofSetFrameRate(fps);
    ofSetVerticalSync(asBool(VERTICAL_SYNC));

    cursor.setup();

    // What Is SIG32 During a GDB Debug Session?
    // https://gnobal.net/591/what-is-sig32-during-a-gdb-debug-session
    // handle SIG32 nostop
    ofSoundStreamSettings settings;
    auto devices = soundStream.getDeviceList();
    for(auto d : devices) {
        ofLogNotice() << "found sound device " << d.deviceID << " '" << d.name << "'";
    }

    // if you want to set the device id to be different than the default
    // auto devices = soundStream.getDeviceList();
    // settings.device = devices[4];

    // you can also get devices for an specific api
    // auto devices = soundStream.getDevicesByApi(ofSoundDevice::Api::PULSE);
    // settings.device = devices[0];

    // or get the default device for an specific api:
    // settings.api = ofSoundDevice::Api::PULSE;

    // or by name
    auto sound_in_device = soundStream.getMatchingDevices("default");
    if(!sound_in_device.empty()) {
        ofLogNotice() << "input from sound device " << sound_in_device[0].deviceID << " '" << sound_in_device[0].name << "'";
        settings.setInDevice(sound_in_device[0]);
    }

    settings.setInListener(this);
    settings.sampleRate = 44100;
    settings.numOutputChannels = 0;
    settings.numInputChannels = 2;
    settings.bufferSize = 256;
    soundStream.setup(settings);

#ifdef WITH_js_dev_input
    js0.setup(asString(JOYSTICK_DEV_INPUT));
#endif // WITH_js_dev_input
#ifdef WITH_nanoKontrol2_midi_input
    nk0.setup();
#endif // WITH_nanoKontrol2_midi_input

    // open an outgoing connection to the SuperCollider interpreter
    talkToSClang.setup(asString(SC_LANG_HOST), asInt(SC_LANG_PORT));

    send_vital_signs = asBool(SEND_VITAL_SIGNS);
    if(send_vital_signs)
        vital_signs.setup(asString(VITAL_SIGNS_HOST), asInt(VITAL_SIGNS_PORT));

#ifdef WITH_openCL
    num_cl_gpu = openCL.getDeviceInfos(CL_DEVICE_TYPE_GPU);
    if(num_cl_gpu > 0) {
        openCL.setupFromOpenGL();
    }
    else {
        ofLogFatalError() << "No GPU OpenCL device found";
    }
    inspector.setup();
#endif // WITH_openCL

    pipe0 = asString(LAYOUT);
    layoutMng.loadPipeline(filterLine, window_settings, oscParams);

    ofSetBackgroundColor(0, 0, 0);
    loadState();
}

void ofApp::updateJoystick() {
#ifdef WITH_js_dev_input
    js0.update();
    if(js0.isRisingEdge(0)) {
        startPreshotExport();
    }
    if(js0.isRisingEdge(2)) {
        toggleCsvExport();
    }
    if(js0.isButtonPressed(3)) {
        exit("Joystick button [] pressed.");
        std::exit(0);
    }
    if(js0.isRisingEdge(14)) {
        layoutMng.setRender(-1);
    }
    if(js0.isRisingEdge(15)) {
        layoutMng.nextRender();
    }
    if(js0.isRisingEdge(16)) {
        layoutMng.previousRender();
    }
//    js0.logPressedButtons();

    lx = js0.readAxis(0);
    ly = js0.readAxis(1);
    rx = js0.readAxis(3);
    ry = js0.readAxis(4);
    l2 = js0.readAxis(2);
    r2 = js0.readAxis(5);
#endif // WITH_js_dev_input
}

void ofApp::updateExpressions() {
    oscMsg[0] = t0 = slT0.rpn.execute0();
    oscMsg[1] = t1 = slT1.rpn.execute0();
    oscMsg[2] = t2 = slT2.rpn.execute0();
    oscMsg[3] = t3 = slT3.rpn.execute0();
    oscMsg[4] = t4 = slT4.rpn.execute0();
    oscMsg[5] = t5 = slT5.rpn.execute0();
    oscMsg[6] = t6 = slT6.rpn.execute0();
    oscMsg[7] = t7 = slT7.rpn.execute0();

    ofxOscMessage s;
    s.setAddress("/f_tn");

    s.addFloatArg(t0);
    s.addFloatArg(t1);
    s.addFloatArg(t2);
    s.addFloatArg(t3);
    s.addFloatArg(t4);
    s.addFloatArg(t5);
    s.addFloatArg(t6);
    s.addFloatArg(t7);

    apptarget->talkToSClang.sendMessage(s, false);
}

void ofApp::updateMovies() {
    for(int mx = 0; mx < usedMovies; ++mx) {
        movies[mx].update();
//TODO single step       movies[mx].nextFrame();
    }
}

void ofApp::updateVideoGrabber() {
#ifdef WITH_video_grabber
    if(videoGrabber && videoGrabber->isInitialized()) {
        videoGrabber->update();
    } else {
        if((frame % 25) == 0)
            loadGrabber();
    }

    for(ofx::Video::IPVideoGrabber* vg : ipVideoGrabbers) {
        if(vg && vg->isInitialized())
            vg->update();
    }
#endif // WITH_video_grabber
}

void ofApp::updateSound() {
    ofSoundUpdate();

    beat_pos = beat.getPosition();
    // grab the fft, and put in into a "smoothed" array,
    // by taking maximums, as peaks and then smoothing downward
    float *val = ofSoundGetSpectrum(nBandsToGet); // request nBandsToGet values for fft
    for(size_t i = 0; i < nBandsToGet; ++i) {
        // let the smoothed value sink to zero:
        fftSmoothed[i] *= fft_smooth;
        // take the max, either the smoothed or the incoming:
        if(fftSmoothed[i] < val[i])
            fftSmoothed[i] = val[i];
    }
}

void ofApp::update() {
    if(stopsrv)
        return;
    try {
        oscParams->update();
        updateMovies();
        updateVideoGrabber();
        updateSound();
        updateJoystick();

#ifdef WITH_openCL
        inspector.reloadClAnalysisProg(false);
#endif // WITH_openCL

        updateExpressions();

#ifdef TRACE_MONOTONIC
			timespec time1, time2;
			clock_gettime(CLOCK_MONOTONIC, &time1);
#endif // TRACE_MONOTONIC
		if(send_vital_signs) {
		    sendCurrentOsc(talkToSClang);
		}
        for(auto s : filterLine) {
            s->update();
        }
#ifdef TRACE_MONOTONIC
			clock_gettime(CLOCK_MONOTONIC, &time2);
			ofLog(OF_LOG_NOTICE) << "s->update()    " << time2.tv_sec-time1.tv_sec << ':' << time2.tv_nsec-time1.tv_nsec;
#endif // TRACE_MONOTONIC

    } catch(const std::exception &ex) {
        ofLogError() << "ofApp::update failed with exception '" << ex.what() << "'";
    }
}

#undef TRACE_MONOTONIC
void ofApp::draw() {
    if(stopsrv)
        return;
    try {
#ifdef TRACE_MONOTONIC
	timespec ltime1, ltime2;
	clock_gettime(CLOCK_MONOTONIC, &ltime1);
#endif // TRACE_MONOTONIC
        cursor.nextFrame();
        ofSetColor(ofColor::white);

        int li = 0;
        for(auto s : filterLine) {
#ifdef TRACE_MONOTONIC
		timespec time1, time2;
		clock_gettime(CLOCK_MONOTONIC, &time1);
#endif // TRACE_MONOTONIC
            s->draw(li++);
#ifdef TRACE_MONOTONIC
		clock_gettime(CLOCK_MONOTONIC, &time2);
		ofLog(OF_LOG_NOTICE) << "s->draw()    " << time2.tv_sec-time1.tv_sec << ':' << time2.tv_nsec-time1.tv_nsec;
#endif // TRACE_MONOTONIC
        }

        if(isCsvExpActive) {
            int li = 0;
            for(auto s : filterLine) {
#ifdef TRACE_MONOTONIC
        timespec time1, time2;
        clock_gettime(CLOCK_MONOTONIC, &time1);
#endif // TRACE_MONOTONIC
                s->exportCsv(li++, export_stream);
#ifdef TRACE_MONOTONIC
            clock_gettime(CLOCK_MONOTONIC, &time2);
            ofLog(OF_LOG_NOTICE) << "s->exportCsv()    " << time2.tv_sec-time1.tv_sec << ':' << time2.tv_nsec-time1.tv_nsec;
#endif // TRACE_MONOTONIC
            }
        }

        if(isContinuousActive) {
#ifdef TRACE_MONOTONIC
        timespec time1, time2;
        clock_gettime(CLOCK_MONOTONIC, &time1);
#endif // TRACE_MONOTONIC
            int li = 0;
            ofPixels pixels;
            for(auto s : filterLine) {
                ofFbo &fbo = s->getFbo();
                fbo.readToPixels(pixels);
//                pixels.mirror(true, false);
                std::ostringstream capture_counter;
                capture_counter << "ana_";
                capture_counter << std::setfill('0') << std::setw(6) << frame;
                capture_counter << "_";
                capture_counter << std::setfill('0') << std::setw(4) << li;
                ofSaveImage(pixels, exportContinuousPath + capture_counter.str() + ".png");
                ++li;
            }
#ifdef TRACE_MONOTONIC
            clock_gettime(CLOCK_MONOTONIC, &time2);
            ofLog(OF_LOG_NOTICE) << "s->exportCsv()    " << time2.tv_sec-time1.tv_sec << ':' << time2.tv_nsec-time1.tv_nsec;
#endif // TRACE_MONOTONIC
        }

#ifdef WITH_openCL
        if(inspector.hasClAnalysisProg && inspector.inspect_this_filter != nullptr) {
            inspector.osc2channels();
            if(draw_inspector)
                inspector.draw2Markers();
        }
#endif // WITH_openCL

        layoutMng.draw(filterLine);

        if(preShot.isEnabled())
            preShot.update();

        for(auto s : filterLine) {
            s->finish();
            dt0 += 0.0025 * (kp0 - 0.5);
            dt1 += 0.0025 * (kp1 - 0.5);
            dt2 += 0.0025 * (kp2 - 0.5);
            dt3 += 0.0025 * (kp3 - 0.5);
            dt4 += 0.0025 * (kp4 - 0.5);
            dt5 += 0.0025 * (kp5 - 0.5);
            dt6 += 0.0025 * (kp6 - 0.5);
            dt7 += 0.0025 * (kp7 - 0.5);
        }

#ifdef TRACE_MONOTONIC
	clock_gettime(CLOCK_MONOTONIC, &ltime2);
	ofLog(OF_LOG_NOTICE) << "frame     " << frame << "    " << ltime2.tv_sec-ltime1.tv_sec << ':' << ltime2.tv_nsec-ltime1.tv_nsec;
#endif // TRACE_MONOTONIC

        if(waiting_osc) {
            ofSetColor(viewcolor);
            std::string welcome = "aNa " ANA_REVISION "\n\nWaiting for incoming OSC messages.\n";
            welcome += "ipv4 " + ipv4_list + "\n";
            welcome += "port " + to_string(asInt(ANALOG_PORT)) + "\n\n";
            welcome += "Press ESC to exit.";
            infofont.drawString(welcome, 20, 30);
        }
        else {
            switch(viewstate) {
            case 1:
                ofSetColor(viewcolor);
                infofont.drawString(dumpState().c_str(), 20, 30);
                break;
            case 2:
                ofSetColor(viewcolor);
                infofont.drawString(dumpNkState().c_str(), 20, 30);
                break;
// var pool is removed yet
//            case 2:
//                ofSetColor(viewcolor);
//                infofont.drawString(Pool::dumpState().c_str(), 20, 30);
//                break;
            case 3: {
                ofSetColor(viewcolor);
                infofont.drawString(("fps " + to_string((int) std::round(ofGetFrameRate()))).c_str(), 20, 30);
                std::string vmX;
    #ifdef WITH_memoryusage
                os_memusage(vmX);
    #endif // WITH_memoryusage
                infofont.drawString(vmX, 20, 100);
                }
                break;
            case 4: {
                ofSetColor(ofColor::red);
                int line_pos = 20;
                for(auto s : filterLine) {
                    std::string state = s->dumpState();
                    if(!state.empty()) {
                        infofont.drawString(s->dumpState().c_str(), line_pos, 30);
                        line_pos += 20;
                    }
                }
                }
                break;
            }
        }
    } catch(const std::exception &ex) {
        ofLogError() << "ofApp::draw failed with exception '" << ex.what() << "'";
    }

    ++frame;
}

void ofApp::closeVideoGrabber() {
#ifdef WITH_video_grabber
    if(videoGrabber) {
        videoGrabber->close();
        delete videoGrabber;
        videoGrabber = nullptr;
    }
#endif // WITH_video_grabber
}

void ofApp::closeIpVideoGrabber() {
    for(ofx::Video::IPVideoGrabber* vg : ipVideoGrabbers) {
        if(vg->isConnected()) {
            vg->disconnect();
            vg->waitForDisconnect();
            vg->close();
        }
        delete vg;
    }
    ipVideoGrabbers.clear();
}

void ofApp::exit(std::string msg) {
    try {
        ofLog(OF_LOG_NOTICE) << msg << " Exit called.";
        sendQuitOsc(talkToSClang);

        beat.unload();
        soundStream.close();
        ofSoundShutdown();

        closeIpVideoGrabber();
        closeVideoGrabber();

        filterLine.clear();
        filterDict.clear();

        saveState();
        if(isCsvExpActive) {
            export_stream.close();
            isCsvExpActive = false;
        }

        if(imageSequ_textureID > 0) {
            glDeleteTextures(1, &imageSequ_textureID);
            imageSequ_textureID = -1;
        }

        preShot.clear();

        if(oscParams) {
            delete oscParams;
            oscParams = nullptr;
        }

    } catch(const std::exception &ex) {
        ofLogError() << "ofApp::exit with exception '" << ex.what() << "'";
    }
}

void ofApp::mouseMoved(int x, int y) {
    cursor.moved();
}

void ofApp::startPreshotExport() {
    cursor.show();
    if(makeDirectory(exportPreshotPath)) {
        preShot.saveRecording(exportPreshotPath + "ana" + "_");
    }
}

void ofApp::toggleCsvExport() {
    if(isCsvExpActive) {
        export_stream.close();
        ++ofApp::epoch;
        isCsvExpActive = false;
        ofLog(OF_LOG_NOTICE) << "closing export";
    } else {
        std::ostringstream epoch_counter;
        epoch_counter << std::setfill('0') << std::setw(4) << ofApp::epoch;
        if(makeDirectory(exportCsvPath)) {
            std::string fn = exportCsvPath + "ana" + "_" + epoch_counter.str() + ".csv";
            export_stream.open(fn);
            isCsvExpActive = export_stream.is_open() && !export_stream.bad();
            if(isCsvExpActive)
                ofLog(OF_LOG_NOTICE) << "exporting to " << fn;
            else
                ofLog(OF_LOG_ERROR) << "failed to open " << fn << " for writing";
        }
    }
}

void ofApp::keyPressed(int key) {
    switch(key) {
    case 27:
        exit("ESC key pressed.");
        break;

    case 'i':
    case 'I':
        draw_inspector = !draw_inspector;
        break;

    case 'f':
    case 'F':
        viewstate = (viewstate + 1) % 5;
        ofLog(OF_LOG_NOTICE) << "fps " << to_string((int) std::round(ofGetFrameRate()));
        ofLog(OF_LOG_NOTICE) << dumpState();
        ofLog(OF_LOG_NOTICE) << dumpNkState();
        break;

    case 'w':
    case 'W':
        viewred = !viewred;
        viewcolor = viewred ? ofColor::red : ofColor::white;
        break;

    case '*':
            layoutMng.setRender(-1);
            break;
    case '+':
            layoutMng.nextRender();
            break;
    case '-':
            layoutMng.previousRender();
            break;

    case 'm':
    case 'M':
        ofLog(OF_LOG_NOTICE) << mermaidGraph.toGraphString(filterLine);
        break;

    case 'r':
    case 'R':
        startPreshotExport();
        break;

    case 'c':
    case 'C':
        toggleCsvExport();
        break;

    case 'z':
        frame = 0;
        beat.setPositionMS(0);
        u0 = u1 = u2 = u3 = u4 = u5 = u6 = u7 = 0;
        t0 = t1 = t2 = t3 = t4 = t5 = t6 = t7 = 0;
        dt0 = dt1 = dt2 = dt3 = dt4 = dt5 = dt6 = dt7 = 0;
        break;

    case 'Z':
        frame = 0;
        beat.setPositionMS(0);
        preShot.reset();
        u0 = u1 = u2 = u3 = u4 = u5 = u6 = u7 = 0;
        t0 = t1 = t2 = t3 = t4 = t5 = t6 = t7 = 0;
        dt0 = dt1 = dt2 = dt3 = dt4 = dt5 = dt6 = dt7 = 0;
        break;
    }
}

void ofApp::audioIn(ofSoundBuffer &input) {
    float rms = 0.0;

    // samples are "interleaved"
    int numCounted = 0;
    //lets go through each sample and calculate the root mean square which is a rough way to calculate volume
    for(size_t i = 0; i < input.getNumFrames(); i++) {
        float left = input[i * 2] * 0.5;
        float right = input[i * 2 + 1] * 0.5;
        rms += left * left + right * right;
        numCounted += 2;
    }

    //mean of rms
    rms /= (float) numCounted;
    // root of rms
    rms = sqrt(rms);

    smoothedVol *= 0.93;
    smoothedVol += 0.07 * rms;

    raw_onset = ofLerp(raw_onset, minimumThreshold, decayRate);
    if(rms > raw_onset) {
        // onset detected!
        raw_onset = rms;
    }
    onset = std::max(0.0f, raw_onset - minimumThreshold);
}
