(ns lv.demo)
(use 'lv.core)

(layout "grid" [ras0 ras1] [ras2 ras3])
(render)

(hsv :ras0 []
  (set vv 1)
  (dotimes [n 11]
    (set dir (even? n 1 -1))
    (set speed (* dir (+ n 1) 0.011 f))
    (set! vv (* vv (distance  (* (vec2 x y) 2)
                         (vec2 (cos speed)
                               (sin speed))))))
  (set edge 2.0)
  (set border 0.25)
  (v (smoothstep (- edge border) (+ edge border) (* vv 5)))
)
(render 0)

(hsv :ras1 [ras0]
  (rot (* f 0.05))
  (v (- 1 ras0.v))
)
(render 1)

(hsv :ras2 [ras0 ras1]
  (kaleid (* 2 (usin (* 0.05 f))))
  (repeat-x 2) (repeat-y 2)
  (v (mod (+ ras0.v ras1.v) 1.0))
)
(render 2)

(hsv :ras3 [ras0 ras2 ras3]

  (repeat-y -1.01)
  (repeat-x -1.01)
  (v (* 1.05 ras0.v (mix ras2.v ras3.v 0.975)))
)
(render 3)
(render)
