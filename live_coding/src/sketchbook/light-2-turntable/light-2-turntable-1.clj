(ns lv.demo)
(use 'lv.core)
; video: two lights on a turntable
(layout "grid" [imgs0 imgs1 ras2]
               [cline3 ras4 ras5])
(imagesequ "../../../timelapse/series/20150428-1-drehteller-licht/split-2525/*.png")

(hsv :imgs0 [c0]
  (k (* slength (unoise (* 0.003 sframe)
                        (* 0.5 r))))
  (hsv c0.hsv)
)
(render 0)

(hsv :imgs1 [c0]
  (k (* slength (unoise (* 0.01 sframe)
                        (* (unoise a y) r))))
  (hsv c0.hsv)
)
(render 1)

(rgb :ras2 [imgs0 imgs1]
  (rgb (mix imgs0.rgb imgs1.rgb 0.5))
)
(render 2)

(expr :cline3 [ras2]
  (count 250)
  (max 500)
  (thres 0.5)
  (paint -1)
  (fill 1)
  (holes 1)
)
(render 3)

(rgb :ras4 [ras2 cline3]
  (mirror)
  (rot (* f 0.1))
  (rgb (mix (* 3 ras2.rgb) cline3.rgb 0.7))
)
(render 4)

(rgb :ras5 [imgs0 ras4]
  (rgb (sqrt (sqrt (* imgs0.rgb ras4.rgb))))
)
(render 5)
(render)
