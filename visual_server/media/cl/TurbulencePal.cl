// this OpenCL code is published with Ready 0.7
// see: https://github.com/GollyGang/ready
// A faster but more memory-intensive implementation of Jonathan McCabes Cyclic Symmetric Multi-Scale Turing Patterns.
// Uses 2-pass 1D gaussian convolution to compute difference-of-gaussian laplacian neighborhoods instead of circular neighborhoods.
// This sample ist based on Cornus Ammonis source code

// --- noise ----------------------------------------------------------
// see: https://thebookofshaders.com/11/
// see: https://github.com/ashima/webgl-noise/blob/master/src/noise2D.glsl
//
// Description : Array and textureless GLSL 2D simplex noise function.
//      Author : Ian McEwan, Ashima Arts.
//  Maintainer : stegu
//     Lastmod : 20110822 (ijm)
//     License : Copyright (C) 2011 Ashima Arts. All rights reserved.
//               Distributed under the MIT License. See LICENSE file.
//               https://github.com/ashima/webgl-noise
//               https://github.com/stegu/webgl-noise
//

float3 mod289_3(float3 x) {
  return (float3)(x.x - floor(x.x * (1.0 / 289.0)) * 289.0,
                  x.y - floor(x.y * (1.0 / 289.0)) * 289.0,
                  x.z - floor(x.z * (1.0 / 289.0)) * 289.0);
}

float2 mod289_2(float2 x) {
  return (float2)(x.x - floor(x.x * (1.0 / 289.0)) * 289.0,
                  x.y - floor(x.y * (1.0 / 289.0)) * 289.0);
}

float3 permute(float3 x) {
  return mod289_3((float3)(((x.x*34.0)+1.0)*x.x,
                  ((x.y*34.0)+1.0)*x.y,
                  ((x.z*34.0)+1.0)*x.z));
}


float snoise(float2 v)
{
  const float4 C = (float4)(0.211324865405187,  // (3.0-sqrt(3.0))/6.0
                      0.366025403784439,  // 0.5*(sqrt(3.0)-1.0)
                     -0.577350269189626,  // -1.0 + 2.0 * C.x
                      0.024390243902439); // 1.0 / 41.0
// First corner
  float2 i  = floor(v + dot(v, C.yy) );
  float2 x0 = v -   i + dot(i, C.xx);

// Other corners
  float2 i1;
  i1 = (x0.x > x0.y) ? (float2)(1.0, 0.0) : (float2)(0.0, 1.0);
  float4 x12 = x0.xyxy + C.xxzz;
  x12.xy -= i1;

// Permutations
  i = mod289_2(i); // Avoid truncation effects in permutation
  float3 p = permute( permute( i.y + (float3)(0.0, i1.y, 1.0 ))
		+ i.x + (float3)(0.0, i1.x, 1.0 ));

  float3 m = max((float3)(0.5) - (float3)(dot(x0,x0), dot(x12.xy,x12.xy), dot(x12.zw,x12.zw)), (float3)(0.0));
  m = m*m ;
  m = m*m ;

// Gradients: 41 points uniformly over a line, mapped onto a diamond.
// The ring size 17*17 = 289 is close to a multiple of 41 (41*7 = 287)

  float3 dummy;
  float3 x = (float3)(2.0) * fract(p * C.www, &dummy) - (float3)(1.0);
  float3 h = fabs(x) - (float3)(0.5);
  float3 ox = floor(x + (float3)(0.5));
  float3 a0 = x - ox;

// Normalise gradients implicitly by scaling m
// Approximation of: m *= inversesqrt( a0*a0 + h*h );
  m *= (float3)(1.79284291400159) - (float3)(0.85373472095314) * ( a0*a0 + h*h );

// Compute final noise value at P
  float3 g;
  g.x  = a0.x  * x0.x  + h.x  * x0.y;
  g.yz = a0.yz * x12.xz + h.yz * x12.yw;
  return 130.0 * dot(m, g);
}


float turb(float2 v, float octaves) {
  float t = 0;
  float f = 1;
  int on = (int)(fabs(octaves));
  for (int io = 0; io < on; io++) {
      float value = snoise(v);
      t += fabs(value) / f;
//       t += value / f;
      f *= 2;
  }
  return t;
}


__kernel void image_setup(
__global float* a_in,
__global float* a_out,
__global float* b_in,
__global float* b_out,
__global float* c_in,
__global float* c_out,
__global float* d_in,
__global float* d_out,
__global float* e_in,
__global float* e_out,
__global float* f_in,
__global float* f_out,
__global float* g_in,
__global float* g_out,
__global float* h_in,
__global float* h_out,
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7,
__write_only image2d_t image2
) 
{
    const int bx = get_global_id(0);
    const int by = get_global_id(1);
    const int BX = get_global_size(0);
    const int ixy = BX*by + bx;
    a_out[ixy] = a_in[ixy];
    b_out[ixy] = b_in[ixy];
    c_out[ixy] = c_in[ixy];
    d_out[ixy] = d_in[ixy];
    e_out[ixy] = e_in[ixy];
    f_out[ixy] = f_in[ixy];
    g_out[ixy] = g_in[ixy];
    h_out[ixy] = h_in[ixy];
}

__kernel void image_change(
__global float* a_in,
__global float* a_out,
__global float* b_in,
__global float* b_out,
__global float* c_in,
__global float* c_out,
__global float* d_in,
__global float* d_out,
__global float* e_in,
__global float* e_out,
__global float* f_in,
__global float* f_out,
__global float* g_in,
__global float* g_out,
__global float* h_in,
__global float* h_out,
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7,
__write_only image2d_t image2
)
{
    #define NUM_COLORS 6
    const float4 colpal[ NUM_COLORS ] = {
        // /usr/share/gimp/2.0/palettes/Warm_Colors.gpl/Warm_Colors.gpl
        {196.0f / 256.0f,   9.0f / 256.0f,   9.0f / 256.0f, 1.0f},
        {216.0f / 256.0f, 213.0f / 256.0f,   8.0f / 256.0f, 1.0f},
        {237.0f / 256.0f, 110.0f / 256.0f,   0.0f / 256.0f, 1.0f},
        {232.0f / 256.0f,   0.0f / 256.0f,  50.0f / 256.0f, 1.0f},
        {140.0f / 256.0f,  11.0f / 256.0f,  11.0f / 256.0f, 1.0f},
        {228.0f / 256.0f, 170.0f / 256.0f,   4.0f / 256.0f, 1.0f}
    };
    const int2 pixelcoord = (int2)(get_global_id(0), get_global_id(1));
    
// #define SIMPLE
#ifdef SIMPLE
    float gray = turb((float2)((float)(pixelcoord.x-512)/100.0f*p0,
                               (float)(pixelcoord.y-512)/100.0f*p1), p7);
#else
    float x0 = turb((float2)((float)(pixelcoord.x-512)/100.0f*p0,
                             (float)(pixelcoord.y-512)/100.0f*p1), p6);
    float y0 = turb((float2)((float)(pixelcoord.y-512)/100.0f*p0,
                             (float)(pixelcoord.x-512)/100.0f*p1), p6);
    float gray = turb((float2)(x0, y0), p7);
#endif

    gray = clamp(0.0f, 0.99f, gray);
// #define HARD
#ifdef HARD
    float4 color = colpal[(int)(NUM_COLORS * gray)];
#else
    float ci = NUM_COLORS * gray;
    int i0 = floor(ci);
    float d = ci - i0;
    float4 color = mix(colpal[i0], colpal[i0+1], 1-d);
#endif
    write_imagef(image2, pixelcoord, (float4)(gray*color.x,
                                              gray*color.y,
                                              gray*color.z,
                                              1.0f));
}
