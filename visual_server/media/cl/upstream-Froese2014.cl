// Tom Froese, Nathaniel Virgo, Takashi Ikegami (2014)
// "Motility at the Origin of Life: Its Characterization and a Model"
// Artificial Life 20(1): 55-76. (PDF)
// see: https://arxiv.org/abs/1311.2531

// This example can bee found in
// Ready, version 0.7
// The Ready Bunch: Tim Hutton, Robert Munafo, Andrew Trevorrow, Tom Rokicki, Dan Wills.
// https://github.com/GollyGang/ready


// :clpp + cl/Froese2014.cl
// --> width and hight of the canvas must be a power of 2 
#define XY(x,y) ((x) & ((X)-1)) * (Y) + ((y) & ((Y)-1))

const sampler_t SAMPLER = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_LINEAR;

bool inside(float x, float y, float mx, float my) {
	float dx = mx-x;
	float dy = my-y;
// 	return (dx*dx + dy * dy) < (0.04 * 0.04);
 	return (dx * dx + dy * dy) < 0.0005;
}

__kernel void image_setup(
__global float* a_in,
__global float* a_out,
__global float* b_in,
__global float* b_out,
__global float* c_in,
__global float* c_out,
__global float* d_in,
__global float* d_out,
__global float* e_in,
__global float* e_out,
__global float* f_in,
__global float* f_out,
__global float* g_in,
__global float* g_out,
__global float* h_in,
__global float* h_out,
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7,
__read_only image2d_t image_in,
__write_only image2d_t image2
) 
{

    const int x = get_global_id(0);
    const int y = get_global_id(1);
    const int X = get_global_size(0);
    const int Y = get_global_size(1);
    const int ixy = XY(x, y);
    
    if(        inside((float)x/(float)X, (float)y/(float)Y, 0.50, 0.50)
            || inside((float)x/(float)X, (float)y/(float)Y, 0.30, 0.70)
            || inside((float)x/(float)X, (float)y/(float)Y, 0.50, 0.82)
            || inside((float)x/(float)X, (float)y/(float)Y, 0.50, 0.20)
            || inside((float)x/(float)X, (float)y/(float)Y, 0.20, 0.20)
            || inside((float)x/(float)X, (float)y/(float)Y, 0.80, 0.80)
            || inside((float)x/(float)X, (float)y/(float)Y, 0.82, 0.30)) {
// 	    a_out[ixy] = 0.3 + 0.7 * a_in[ixy];
	    a_out[ixy] = 0.2 + 0.8 * a_in[ixy];
	    b_out[ixy] = 0.36 * b_in[ixy];
	} else {
	    a_out[ixy] = 1;
	    b_out[ixy] = 0;
	}
    c_out[ixy] = 0.0;
    
	
	const int2 pixelcoord = (int2)(x, y);
//     const float amp = 1.0/15.0;
    const float amp = 0.5;
	write_imagef(image2, pixelcoord, (float4)(amp*a_out[ixy], amp*b_out[ixy], amp*c_out[ixy], 1));
}

__kernel void image_change(
__global float *a_in,
__global float *a_out,
__global float *b_in,
__global float *b_out,
__global float *c_in,
__global float *c_out,
__global float* d_in,
__global float* d_out,
__global float* e_in,
__global float* e_out,
__global float* f_in,
__global float* f_out,
__global float* g_in,
__global float* g_out,
__global float* h_in,
__global float* h_out,
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7,
__read_only image2d_t image_in,
__write_only image2d_t image2)
{

    const int x = get_global_id(0);
    const int y = get_global_id(1);
    const int X = get_global_size(0);
    const int Y = get_global_size(1);
    const int ixy = XY(x, y);
    const int i_n  = XY(x  , y+1);
    const int i_ne = XY(x+1, y+1);
    const int i_e  = XY(x+1, y  );
    const int i_se = XY(x+1, y-1);
    const int i_s  = XY(x  , y-1);
    const int i_sw = XY(x-1, y-1);
    const int i_w  = XY(x-1, y  );
    const int i_nw = XY(x-1, y+1);

    const float a = a_in[ixy];
    const float b = b_in[ixy];
    const float c = c_in[ixy];

    const float a_n  = a_in[i_n];
    const float a_ne = a_in[i_ne];
    const float a_e  = a_in[i_e];
    const float a_se = a_in[i_se];
    const float a_s  = a_in[i_s];
    const float a_sw = a_in[i_sw];
    const float a_w  = a_in[i_w];
    const float a_nw = a_in[i_nw];
    const float b_n  = b_in[i_n];
    const float b_ne = b_in[i_ne];
    const float b_e  = b_in[i_e];
    const float b_se = b_in[i_se];
    const float b_s  = b_in[i_s];
    const float b_sw = b_in[i_sw];
    const float b_w  = b_in[i_w];
    const float b_nw = b_in[i_nw];
    const float c_n  = c_in[i_n];
    const float c_ne = c_in[i_ne];
    const float c_e  = c_in[i_e];
    const float c_se = c_in[i_se];
    const float c_s  = c_in[i_s];
    const float c_sw = c_in[i_sw];
    const float c_w  = c_in[i_w];
    const float c_nw = c_in[i_nw];

    // 2D standard 9-point stencil: [ 1,4,1; 4,-20,4; 1,4,1 ] / 6
    const float _K0 = -20.0 / 6.0; // center weight
    const float _K1 =   4.0 / 6.0; // edge-neighbors
    const float _K2 =   1.0 / 6.0; // vertex-neighbors

    // compute the Laplacians of each chemical
    float laplacian_a = a_n*_K1 + a_s*_K1 + a_w*_K1 + a_e*_K1
                      + a_ne*_K2 + a_nw*_K2 + a_se*_K2 + a_sw*_K2
                      + a*_K0;
    float laplacian_b = b_n*_K1 + b_s*_K1 + b_w*_K1 + b_e*_K1
                      + b_ne*_K2 + b_nw*_K2 + b_se*_K2 + b_sw*_K2
                      + b*_K0;
    float laplacian_c = c_n*_K1 + c_s*_K1 + c_w*_K1 + c_e*_K1
                      + c_ne*_K2 + c_nw*_K2 + c_se*_K2 + c_sw*_K2
                      + c*_K0;

    const int2 pixelcoord = (int2)(x, y);
    float4 rgba =  read_imagef(image_in, SAMPLER, pixelcoord);
    const float timestep = 0.500000f;
//     const float D_a = 0.300000f;
//     const float D_b = 0.150000f;
    #define D_a      rgba.x
    #define D_b      rgba.y
    const float k   = 0.094200f;
    const float k_c = 0.000200f;
    const float w   = 0.015000f;
    const float r   = 0.032000f;

    const float delta_a = D_a * laplacian_a - exp(-w*c)*a*b*b + r*(1.0f-a);
    const float delta_b = D_b * laplacian_b + exp(-w*c)*a*b*b - k*b;
    const float delta_c = k*b - k_c*c;
      

    a_out[ixy] = a + timestep * delta_a;
    b_out[ixy] = b + timestep * delta_b;
    c_out[ixy] = c + timestep * delta_c;

    const float amp = 0.5;
    write_imagef(image2, pixelcoord, (float4)(amp*a_out[ixy], amp*b_out[ixy], amp*c_out[ixy], 1));
}

