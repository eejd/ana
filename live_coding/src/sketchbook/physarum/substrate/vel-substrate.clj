(ns lv.demo)
(use 'lv.core)

(layout "grid" [ras0]
               [clag1])
(movie "4movies/*.mp4")

; ks0    1
; ks1    0.92126
; ks2    1
; ks3    1
; ks4    1
; ks5    0.56693
; ks6    0.74016
; ks7    0.67717

(hsv :ras0 []
  (v (- 1 (/ (floor (* 5 r)) 5)))
)

(expr :clag1 [ras0]
  "physarum/substrate/vel-substrate.cl"
  (paint 1)

  (p0 (+ (* ks0 0.005) 0.005))
  (p1 (* ks1 0.01))
  (p2 (* ks2 0.2))
  (p3 (* ks3 0.05))
  (p5 (sign (- (rand) 0.9)))
  (p6 (sqrt (/ ks6 16)))
  (p7 (+ ks7 0.1))
)
(render 1)
