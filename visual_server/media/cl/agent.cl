__kernel void image_setup(
__global float2* pos,
__global float2* pos_out,
__global float2* vel,
__global float2* vel_out,
__global float* head,
__global float* head_out,
const int img_x_max,
const int img_y_max,
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7,
__write_only image2d_t image2
) 
{
    const int ax = get_global_id(0);
    pos_out[ax] = (float2)(ax*1.0/50000.0, 0.5);
//     vel_out[ax] = vel[ax];
    vel_out[ax] = (float2)(0.02f, 0.001f);
    head_out[ax] = head[ax];
}

__kernel void image_change(
__global float2* pos,
__global float2* pos_out,
__global float2* vel,
__global float2* vel_out,
__global float* head,
__global float* head_out,
const int img_x_max,
const int img_y_max,
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7,
__write_only image2d_t image2
)
{
    const int ax = get_global_id(0);
    pos_out[ax] = pos[ax] + (float2)(0.01) * (float2)(cos(head[ax]), sin(head[ax]));
//     pos_out[ax] = pos[ax] + vel[ax];
//     pos_out[ax] = (float2)(0.5,0.5);
//     vel_out[ax] = (float2)(0,0);
    vel_out[ax] = 0.995f*vel[ax];
    head_out[ax] = head[ax]+vel[ax].x*ax;

    const int2 pixelcoord = clamp((int2)(pos_out[ax].x*1024, pos_out[ax].y*1024),
                                  (int2)(0,0),
                                  (int2)(img_x_max,img_y_max));
    float gray = 1.0f;
    write_imagef(image2, pixelcoord, (float4)(gray, gray, gray, 1));
}
