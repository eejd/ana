(ns lv.demo)
(use 'lv.core)

(layout "grid" [img0 igen1]
               [ras2 ras3])

(image "flower-sampler/*.png")

(rgb :img0 [c0 c1 c2 c3]
  (rgb (mix c3.rgb c2.rgb (usin (* 0.031 f))))
)

(expr :igen1 [ras3]
  "r2/r2image.cl"
  (p0 (* ks0 3))
  (p1 (* (usin (* 0.007 f)) 3))
  (p2 (* (usin (* 0.011 f)) 3))
  (p3 (* (usin (* 0.013 f)) 3))
  (p4 (* (usin (* 0.023 f)) 3))
  (p7 (* ks7 3))
)

(rgb :ras2 [img0 igen1 ras2]
  (rgb (mix (* 3 img0.rgb igen1.rgb) ras2.rgb 0.95))
)

(rgb :ras3 [img0 igen1]
  (rgb (+ img0.rgb igen1.rgb))
)
(render)
