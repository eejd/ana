#include "globalvars.h"
#include "Configuration.h"
#include "Filter.h"
#include "ofApp.h"
#include "layout/GridLayout.h"

GridLayout::GridLayout(int r, int c) {
    rows = r > 0 ? r : 1;
    cols = c > 0 ? c : 1;
    optTiles();
}

GridLayout::~GridLayout() {
}

int GridLayout::numFilters() {
    return rows * cols;
}

void GridLayout::optTiles() {
    double reduced_filter_width = asInt(FILTER_WIDTH);
    double reduced_filter_height = asInt(FILTER_HEIGHT);
    canvas_width = asInt(CANVAS_WIDTH);
    canvas_height = asInt(CANVAS_HEIGHT);
    float ar = reduced_filter_width / reduced_filter_height;
    while(reduced_filter_width*cols > canvas_width || reduced_filter_height*rows > canvas_height) {
        reduced_filter_width  -= 1.0;
        reduced_filter_height -= 1.0/ar;
    }
    ir_filter_width = std::round(reduced_filter_width);
    ir_filter_height = std::round(reduced_filter_height);
    left_border = std::max(0, canvas_width - ir_filter_width * cols);
    top_border  = std::max(0, canvas_height - ir_filter_height * rows);
    downscale = reduced_filter_width / asInt(FILTER_WIDTH);
//    ofLog(OF_LOG_NOTICE) << "optTiles " << ir_filter_width << "x" << ir_filter_height
//                                        << " " <<  left_border << " " << top_border
//                                        << " " << downscale;
}

void GridLayout::draw(int render, std::vector<std::shared_ptr<Filter>> &filterLine) {
    bool badrange = false;
    if(render >= 0) {
        if((int) filterLine.size() > render) {
            auto s = filterLine[render];
            ofFbo &fbo = s->getFbo();
            left_border = std::max(0, canvas_width - asInt(FILTER_WIDTH));
            top_border = std::max(0, (canvas_height - asInt(FILTER_HEIGHT)) / 2);
            fbo.draw(left_border, top_border);
        } else {
            badrange = true;
        }
    }
    if(badrange || render < 0) {
        left_border = std::max(0, canvas_width - ir_filter_width * cols);
        top_border = std::max(0, (canvas_height - ir_filter_height * rows) / 2);
        int u = 0;
        int v = 0;
        for(auto s : filterLine) {
            ofFbo &fbo = s->getFbo();
            ofPushMatrix();
            ofTranslate(left_border + ir_filter_width * u, top_border + ir_filter_height * v);
            ofScale(downscale);
            fbo.draw(0, 0);
            ofPopMatrix();
            ++u;
            if(u >= cols) {
                u = 0;
                ++v;
            }
        }
    }
}
