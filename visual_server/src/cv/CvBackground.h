#pragma once

#include <opencv2/opencv.hpp>
#include "ofxCv.h"

#include "OscListener.h"
#include "Filter.h"
#include "calc/ScalarLisp.h"

class CvBackground : public Filter {
	public:
		CvBackground(ofGLWindowSettings settings, OscListener * oscParams);
		virtual ~CvBackground();
		virtual void setup(const std::string id, int numSamples);
		void reload(ofxOscMessage const &msg);
		virtual void update();
		virtual void draw(int li);

	private:
		void drawFeaturePoints();

        ofPixels grabbedPixels;
        bool hasData = false;
        cv::Mat frame_captured;
        ofxCv::RunningBackground background;
        ofImage thresholded;

        ScalarLisp slThreshold;
        double threshold = 0.5;
        ScalarLisp slLearningTime;
        double learningTime = 2.0;
        ScalarLisp slReset;
};
