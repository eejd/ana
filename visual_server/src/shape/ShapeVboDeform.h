#pragma once

#include "ofMain.h"
#include "Filter.h"
#include "OscListener.h"
#include "calc/ScalarLisp.h"

class ShapeVboDeform : public Filter {
	public:
		ShapeVboDeform(ofGLWindowSettings settings, OscListener * oscParams);
		virtual ~ShapeVboDeform();
		virtual void setup(const std::string id, int numSamples);
		virtual ofTexture & getTexture();
        virtual void reload(ofxOscMessage const &msg);
        virtual void update();
		virtual void draw(int li);

	private:
		string modelName = "";
		std::vector<glm::vec3> vertices;
		std::vector<glm::vec3> normals;
		std::vector<glm::vec3> transformed_vertices;
		GLuint vertexArrayID = -1;
		GLuint vertexbufferID = -1;
		GLuint normalbufferID = -1;

		ofShader phong_shader;

        ScalarLisp slWireframe;
        bool wireframe = true;
        ScalarLisp slScale;
        float sx = 1;
        float sy = 1;
        float sz = 1;

        ScalarLisp slAmplitude;
		ScalarLisp slLiquidness;
		ScalarLisp slSpeed;
        ScalarLisp slRx;
		ScalarLisp slRy;
		ScalarLisp slRz;

        ScalarLisp   slAmbientColor;
//		ofFloatColor ambientColor;
        ScalarLisp   slDiffuseColor;
//		ofFloatColor diffuseColor;
        ScalarLisp   slSpecularColor;
//		ofFloatColor specularColor;

		float elapsedTime = 0;

		void drawMesh(ofEasyCam * cam);
		void deformMesh();
		void loadMesh(const std::string &modelName);
		void setColorUniform(int progID, ScalarLisp &sl, const char *uname);
};
