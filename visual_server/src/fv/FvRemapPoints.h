#pragma once
#include "ofMain.h"
#include "OscListener.h"
#include "ClFilter.h"
#include "calc/ScalarLisp.h"

class FvRemapPoints : public Filter {
	public:
		FvRemapPoints(ofGLWindowSettings settings, OscListener * oscParams);
	    virtual ~FvRemapPoints();
		virtual void setup(const std::string id, int numSamples);
	    virtual void reload(ofxOscMessage const &msg);
		virtual void update();
	    virtual void draw(int li);

	    ScalarLisp slX;
	    ScalarLisp slY;
	    ScalarLisp slSize;
	    ScalarLisp slRotate;
	    ScalarLisp slResolution;
	    ScalarLisp slH;
	    ScalarLisp slS;
	    ScalarLisp slV;
	    ScalarLisp slFill;
	    ScalarLisp slClear;

	private:
        int draw_mode = 0;
	    std::shared_ptr<Filter> upf = nullptr;
};
