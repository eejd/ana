#include <string>
#include <unordered_map>

#include "ofMain.h"
#include "util/stringhelper.h"
#include "Configuration.h"

static std::unordered_map<std::string, bool> bool_conf;
static std::unordered_map<std::string, int> int_conf;
static std::unordered_map<std::string, float> float_conf;
static std::unordered_map<std::string, std::string> str_conf;

static void pathToHome(const char *key, const char *homedir) {
    if(str_conf[key].size() > 2 && str_conf[key].at(0) == '~' && str_conf[key].at(1) == '/') {
        std::string home(homedir);
        str_conf[key] = home + str_conf[key].substr(1);
    }
}

void load_configuration(char *conf) {
    bool_conf[FULLSCREEN] = true;

    int_conf[FPS] = 25;
    bool_conf[VERTICAL_SYNC] = true;
    int_conf[GL_MALOR] = 4;
    int_conf[GL_MINOR] = 6;

    str_conf[LAYOUT] = "(layout \"grid\" [ras0 ras1] [ras2 ras3])";
    int_conf[CANVAS_WIDTH] = 1920;
    int_conf[CANVAS_HEIGHT] = 1080;

//    int_conf[FILTER_ROWS] = 2;
//    int_conf[FILTER_COLS] = 2;
    int_conf[FILTER_WIDTH] = 1080;
    int_conf[FILTER_HEIGHT] = 1080;

    int_conf[VIDEO_WIDTH] = 1920;
    int_conf[VIDEO_HEIGHT] = 1080;
    bool_conf[GLVIDEOFILTER_VERBOSE_GRABBER] = false;

    str_conf[MEDIA_PATH] = "media";

    int_conf[PRESHOT] = 0;
    str_conf[PRESHOT_PATH] = "~/ana_exp";
    str_conf[CSV_EXPORT_PATH] = "~/ana_exp";

    str_conf[INFOFONT] = "sans-serif";
    int_conf[INFOFONT_SIZE] = 24;
    str_conf[FONT] = "sans-serif";
    int_conf[FONT_SIZE] = 24;
    int_conf[FONT_DPI] = 96;

    int_conf[CAMERA] = 1;
    bool_conf[HIDE_CURSOR] = false;

    str_conf[SC_LANG_HOST] = "localhost";
    int_conf[SC_LANG_PORT] = 57120;
    int_conf[ANALOG_PORT] = 57220;
    str_conf[SATELLITE_SENSEHAT_HOST] = "192.168.2.64";
    int_conf[SATELLITE_SENSEHAT_PORT] = 57510;

    int_conf[HARDWARE_CONCURRENCY] = 0;
    bool_conf[SOUND_LOOP] = true;
    float_conf[FFT_SMOOTH] = 0.75;

    int_conf[SHAPEVBODEFORM_MAX_SAMPLES] = 0;
    int_conf[GLVIDEOFILTER_ROTATE] = 0;

    bool_conf[DUMP_OSC_INPUT] = false;
    bool_conf[DUMP_FRAG_SRC] = false;
    bool_conf[SEND_VITAL_SIGNS] = false;
    str_conf[VITAL_SIGNS_HOST] = "localhost";
    int_conf[VITAL_SIGNS_PORT] = 57120;

    str_conf[JOYSTICK_DEV_INPUT] = "/dev/input/js0";

    if(conf) {
        std::ifstream in;
        in.open(conf);
        std::string line;
        if(in.good()) {
            ofLogNotice() << "Using configuration file '" << realpath(conf, NULL) << "'";
            while(in.good() && getline(in, line)) {
                std::string::size_type begin = line.find_first_not_of(" \f\t\v");
                if(begin == std::string::npos)
                    continue;
                if(std::string("#").find(line[begin]) != std::string::npos)
                    continue;
                std::string firstWord;
                try {
                    firstWord = trim(line.substr(0, line.find("=")));
                    std::string value = trim(line.substr(line.find("=") + 1, line.length()), ' ', ' ');
                    if(firstWord == FULLSCREEN)
                        bool_conf[FULLSCREEN] = value == "true";
                    else if(firstWord == FPS)
                        int_conf[FPS] = std::stoi(value);
                    else if(firstWord == VERTICAL_SYNC)
                        bool_conf[VERTICAL_SYNC] = value == "true";
                    else if(firstWord == GL_MALOR)
                        int_conf[GL_MALOR] = std::stoi(value);
                    else if(firstWord == GL_MINOR)
                        int_conf[GL_MINOR] = std::stoi(value);
                    else if(firstWord == LAYOUT)
                        str_conf[LAYOUT] = value;
                    else if(firstWord == CANVAS_WIDTH)
                        int_conf[CANVAS_WIDTH] = std::stoi(value);
                    else if(firstWord == CANVAS_HEIGHT)
                        int_conf[CANVAS_HEIGHT] = std::stoi(value);
//                    else if(firstWord == FILTER_ROWS)
//                        int_conf[FILTER_ROWS] = std::stoi(value);
//                    else if(firstWord == FILTER_COLS)
//                        int_conf[FILTER_COLS] = std::stoi(value);
                    else if(firstWord == FILTER_WIDTH)
                        int_conf[FILTER_WIDTH] = std::stoi(value);
                    else if(firstWord == FILTER_HEIGHT)
                        int_conf[FILTER_HEIGHT] = std::stoi(value);
                    else if(firstWord == VIDEO_WIDTH)
                        int_conf[VIDEO_WIDTH] = std::stoi(value);
                    else if(firstWord == VIDEO_HEIGHT)
                        int_conf[VIDEO_HEIGHT] = std::stoi(value);
                    else if(firstWord == MEDIA_PATH)
                        str_conf[MEDIA_PATH] = value;
                    else if(firstWord == GLVIDEOFILTER_VERBOSE_GRABBER)
                        bool_conf[GLVIDEOFILTER_VERBOSE_GRABBER] = value == "true";
                    else if(firstWord == PRESHOT)
                        int_conf[PRESHOT] = std::stoi(value);
                    else if(firstWord == PRESHOT_PATH)
                        str_conf[PRESHOT_PATH] = value;
                    else if(firstWord == CSV_EXPORT_PATH)
                        str_conf[CSV_EXPORT_PATH] = value;
                    else if(firstWord == INFOFONT)
                        str_conf[INFOFONT] = value;
                    else if(firstWord == INFOFONT_SIZE)
                        int_conf[INFOFONT_SIZE] = std::stoi(value);
                    else if(firstWord == FONT)
                        str_conf[FONT] = value;
                    else if(firstWord == FONT_SIZE)
                        int_conf[FONT_SIZE] = std::stoi(value);
                    else if(firstWord == FONT_DPI)
                        int_conf[FONT_DPI] = std::stoi(value);
                    else if(firstWord == CAMERA)
                        int_conf[CAMERA] = std::stoi(value);
                    else if(firstWord == HIDE_CURSOR)
                        bool_conf[HIDE_CURSOR] = value == "true";
                    else if(firstWord == SC_LANG_PORT)
                        int_conf[SC_LANG_PORT] = std::stoi(value);
                    else if(firstWord == SC_LANG_HOST)
                        str_conf[SC_LANG_HOST] = value;
                    else if(firstWord == ANALOG_PORT)
                        int_conf[ANALOG_PORT] = std::stoi(value);
                    else if(firstWord == SATELLITE_SENSEHAT_HOST)
                        str_conf[SATELLITE_SENSEHAT_HOST] = value;
                    else if(firstWord == SATELLITE_SENSEHAT_PORT)
                        int_conf[SATELLITE_SENSEHAT_PORT] = std::stoi(value);
                    else if(firstWord == HARDWARE_CONCURRENCY)
                        int_conf[HARDWARE_CONCURRENCY] = std::stoi(value);
                    else if(firstWord == SOUND_LOOP)
                        bool_conf[SOUND_LOOP] = value == "true";
                    else if(firstWord == FFT_SMOOTH)
                        float_conf[FFT_SMOOTH] = std::stof(value);
                    else if(firstWord == SHAPEVBODEFORM_MAX_SAMPLES)
                        int_conf[SHAPEVBODEFORM_MAX_SAMPLES] = std::max(0, std::stoi(value));
                    else if(firstWord == GLVIDEOFILTER_ROTATE)
                        int_conf[GLVIDEOFILTER_ROTATE] = std::stoi(value);
                    else if(firstWord == DUMP_OSC_INPUT)
                        bool_conf[DUMP_OSC_INPUT] = value == "true";
                    else if(firstWord == DUMP_FRAG_SRC)
                        bool_conf[DUMP_FRAG_SRC] = value == "true";
                    else if(firstWord == SEND_VITAL_SIGNS)
                        bool_conf[SEND_VITAL_SIGNS] = value == "true";
                    else if(firstWord == VITAL_SIGNS_PORT)
                        int_conf[VITAL_SIGNS_PORT] = std::stoi(value);
                    else if(firstWord == VITAL_SIGNS_HOST)
                        str_conf[VITAL_SIGNS_HOST] = value;
                    else if(firstWord == JOYSTICK_DEV_INPUT)
                        str_conf[JOYSTICK_DEV_INPUT] = value;
                } catch(std::exception &ex) {
                    ofLogError() << "Failed to read parameter '" << firstWord << "' from " << conf << "' " << ex.what();
                }
            }
        } else {
            ofLogError() << "Failed to open configuration file '" << conf << "'. Using build in configuration.";
        }
    } else {
        ofLogWarning() << "No configuration file given. Using build in configuration.";
    }

    const char *homedir;
    if((homedir = getenv("HOME")) != NULL) {
        pathToHome(MEDIA_PATH, homedir);
        pathToHome(PRESHOT_PATH, homedir);
        pathToHome(CSV_EXPORT_PATH, homedir);
    }

    if(int_conf[HARDWARE_CONCURRENCY] < 1)
        int_conf[HARDWARE_CONCURRENCY] = std::thread::hardware_concurrency();
}

bool asBool(const std::string key) {
    if(bool_conf.find(key) != bool_conf.end()) {
        return bool_conf[key];
    }
    ofLog(OF_LOG_FATAL_ERROR) << "asBool: unknown configuration key " << key;
    return false;
}

int asInt(const std::string key) {
    if(int_conf.find(key) != int_conf.end()) {
        return int_conf[key];
    }
    ofLog(OF_LOG_FATAL_ERROR) << "asInt: unknown configuration key " << key;
    return 0;
}

float asFloat(const std::string key) {
    if(float_conf.find(key) != float_conf.end()) {
        return float_conf[key];
    }
    ofLog(OF_LOG_FATAL_ERROR) << "asFloat: unknown configuration key " << key;
    return 0.0f;
}

std::string asString(const std::string key) {
    if(str_conf.find(key) != str_conf.end()) {
        return str_conf[key];
    }
    ofLog(OF_LOG_FATAL_ERROR) << "asString: unknown configuration key " << key;
    static const std::string empty = "";
    return empty;
}
