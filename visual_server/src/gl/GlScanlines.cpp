#include "ofMain.h"

#include "stringhelper.h"
#include "Configuration.h"
#include "globalvars.h"
#include "Filter.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispMng.h"
#include "gl/GlScanlines.h"

GlScanlines::GlScanlines(ofGLWindowSettings settings, OscListener * oscParams) : Filter(settings, oscParams){
}

GlScanlines::~GlScanlines(){
    if(vert_color) {
        buildin_shader.unload();
        glDeleteBuffers(1, &vertexbufferID);
        glDeleteVertexArrays(1, &vertexArrayID);
        delete [] vert_color;
        vert_color = nullptr;
    }
}

static std::string simple2dVert = STRINGIFY(#version 330\n
in vec2 position;\n
in vec3 color;\n
out vec3 colorVarying;\n
void main()\n
{\n
    colorVarying = color;\n
    gl_Position = vec4(position*2.0-1.0, 0.0, 1.0);\n
}\n
);

static std::string simple2dFrag = STRINGIFY(#version 330\n
in vec3 colorVarying;\n
out vec4 outColor;\n
void main()\n
{\n
    outColor = vec4(colorVarying, 1.0);\n
}\n
);


void GlScanlines::setup(const std::string id, int numSamples){
	Filter::setup(id, numSamples);
    vert_color = new GLfloat[max_vert_color_index];

    std::string okV = buildin_shader.setupShaderFromSource(GL_VERTEX_SHADER, simple2dVert) ? "OK" : "failed";
    std::string okF = buildin_shader.setupShaderFromSource(GL_FRAGMENT_SHADER, simple2dFrag) ? "OK" : "failed";
    std::string okP = buildin_shader.linkProgram() ? "OK" : "failed";
    ofLog(OF_LOG_NOTICE) << "shader compiler " << id << ": vertex=" << okV << " fragment=" << okF <<  " link=" << okP;
    progID = buildin_shader.getProgram();

    glGenVertexArrays(1, &vertexArrayID);
    glBindVertexArray(vertexArrayID);

    glGenBuffers(1, &vertexbufferID);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbufferID);
    glBufferData(GL_ARRAY_BUFFER, max_vert_color_index * sizeof(GLfloat), 0, GL_DYNAMIC_DRAW);

    // Specify the layout of the vertex data
    posAttrib = glGetAttribLocation(progID, "position");
    glEnableVertexAttribArray(posAttrib);
    glVertexAttribPointer(
            posAttrib,           // attribute
            2,                   // size
            GL_FLOAT,            // type
            GL_FALSE,            // normalized
            per_vert_color_stride * sizeof(GLfloat), // stride
            0);                  // array buffer offset

    colAttrib = glGetAttribLocation(progID, "color");
    glEnableVertexAttribArray(colAttrib);
    glVertexAttribPointer(
            colAttrib,           // attribute
            3,                   // size
            GL_FLOAT,            // type
            GL_FALSE,            // normalized
            per_vert_color_stride * sizeof(GLfloat), // stride
            (void*)(2 * sizeof(GLfloat))); // array buffer offset

    glBindVertexArray(0);
}

void GlScanlines::reload(ofxOscMessage const &msg) {
    auto narg = msg.getNumArgs();
    if(narg > 1) {
        Filter::setInputTexture(msg.getArgAsString(1));
    }
    ScalarLispMng mng;
    mng.compile(slMode, "mode", "", msg);
    mng.compile(slDx, "dx", "10", msg);
    mng.compile(slDy, "dy", "10", msg);
    mng.compile(slDrift, "drift", "", msg);
    mng.compile(slJitter, "jitter", "", msg);
    mng.compile(slX, "x", "x", msg);
    mng.compile(slY, "y", "y", msg);
    mng.compile(slClear, "clear", "1", msg);
}

void GlScanlines::update() {
#ifdef TRACE_MONOTONIC
    timespec time1, time2;
    clock_gettime(CLOCK_MONOTONIC, &time1);
#endif // TRACE_MONOTONIC
    src_pixel_width = src_pixel_height = 0;
    src_pd = ctrl_pd = nullptr;
    mode = slMode.rpn.execute0();
	if(upstream_ids.size() >= 1) {
		auto in_id = upstream_ids[0];
		if(filterDict.find(in_id) != filterDict.end()) {
			auto f = filterDict[in_id];
			if(f) {
				src_pixel_width = src_pixels.getWidth();
				src_pixel_height = src_pixels.getHeight();
				f->getTexture().readToPixels(src_pixels);
			    src_pd = src_pixels.getData();
			    ctrl_pd = src_pd;
			}
		}
	}
	// try to find the control upstream filter
	// if there is non control filter then ctrl_pd points to src_pd
    if(src_pd && upstream_ids.size() >= 2) {
        auto in_id = upstream_ids[1];
        if(filterDict.find(in_id) != filterDict.end()) {
            auto f = filterDict[in_id];
            if(f) {
                f->getTexture().readToPixels(ctrl_pixels);
                if(src_pixel_width == ctrl_pixels.getWidth() && src_pixel_height == ctrl_pixels.getHeight()) {
                    ctrl_pd = ctrl_pixels.getData();
                }
            }
        }
    }
#ifdef TRACE_MONOTONIC
    clock_gettime(CLOCK_MONOTONIC, &time2);
    ofLog(OF_LOG_NOTICE) << "GlScanlines::update    " << (time2.tv_sec * 1000000000l + time2.tv_nsec) - (time1.tv_sec * 1000000000l + time1.tv_nsec);
#endif // TRACE_MONOTONIC
}

//#define TRACE_MONOTONIC
#undef TRACE_MONOTONIC
void GlScanlines::draw(int li){
	if(src_pixel_width < 1 || src_pixel_height < 1 || src_pd == nullptr || ctrl_pd == nullptr || upstream_ids.size() < 1)
		return;

#ifdef TRACE_MONOTONIC
    timespec time3, time4;
    clock_gettime(CLOCK_MONOTONIC, &time3);
#endif // TRACE_MONOTONIC
    fbo.begin();
    if(slClear.rpn.execute0() >= 0.0) {
        ofClear(0, 0, 0, 255);
    }

    float dy = (float)std::max(0.001, slDy.rpn.execute0());
    float dx = (float)std::max(0.001, slDx.rpn.execute0());

    glUseProgram(progID);
    GLuint scaleLoc = glGetUniformLocation(progID, "scale");
    glUniform1f(scaleLoc, 0.5f * std::max(filterWidth, filterHeight));

    glBindVertexArray(vertexArrayID);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbufferID);
    glEnableVertexAttribArray(posAttrib);
    glEnableVertexAttribArray(colAttrib);

    ofPixelFormat pf = src_pixels.getPixelFormat();
    const size_t per_pixel = pf == OF_PIXELS_RGBA ? 4 : 3;
    switch(mode) {
    case 1: {
        dx *= filterWidth;
        dy *= filterHeight;
        for(float ix=0; ix<=src_pixel_width; ix+=dx) {
            size_t vert_color_index = 0;
            float ix_drift = ix + filterWidth * slDrift.rpn.execute3d1i(ix, 0.0, 0.0, 0);
            for(float iy=0; iy<src_pixel_height; iy+=dy) {
                size_t pdi = ((size_t)ix_drift + (size_t)iy * filterWidth) * per_pixel;
                if(vert_color_index < max_vert_color_index && ix_drift >= 0 && ix_drift < filterWidth) {
                    double jitter = filterWidth * slJitter.rpn.execute4d3i(ctrl_pd[pdi], ctrl_pd[pdi+1], ctrl_pd[pdi+2], ix / src_pixel_width, ix, iy, src_pixel_width);
                    double map_x = slX.rpn.execute4d3i((ix_drift+jitter) / filterWidth, iy / filterHeight, jitter, ix / src_pixel_width, ix, iy, src_pixel_width);
                    double map_y = slY.rpn.execute4d3i((ix_drift+jitter) / filterWidth, iy / filterHeight, jitter, ix / src_pixel_width, ix, iy, src_pixel_width);
                    vert_color[vert_color_index] = map_x;
                    vert_color[vert_color_index + 1] = map_y;
                    vert_color[vert_color_index + 2] = src_pd[pdi];
                    vert_color[vert_color_index + 3] = src_pd[pdi + 1];
                    vert_color[vert_color_index + 4] = src_pd[pdi + 2];
                    vert_color_index += per_vert_color_stride;
                }
            }
            glBufferSubData(GL_ARRAY_BUFFER, 0, vert_color_index * sizeof(GLfloat), &vert_color[0]);
            glDrawArrays(GL_LINE_STRIP, 0, vert_color_index/per_vert_color_stride);
        }
        }
        break;
    case 2: {
        dx *= filterWidth;
        size_t w2 = filterWidth  / 2;
        size_t h2 = filterHeight / 2;
        for(float ir=0; ir<=src_pixel_width/2; ir+=dx) {
            size_t vert_color_index = 0;
            for(float ia=0; ia<TWO_PI; ia+=(TWO_PI*dy)) {
                float cx = w2 + ir * std::cos(ia);
                float cy = h2 + ir * std::sin(ia);
                if(vert_color_index < max_vert_color_index && cx >= 0 && cx < filterWidth && cy >= 0 && cy < filterHeight) {
                    size_t pdi = ((size_t)cx + (size_t)cy * filterWidth) * per_pixel;
                    double jitter = (filterWidth  / 2) * slJitter.rpn.execute4d3i(ctrl_pd[pdi], ctrl_pd[pdi+1], ctrl_pd[pdi+2], ir / src_pixel_width, ir, ia, src_pixel_width);
                    float cjx = w2 + (ir + jitter) * std::cos(ia);
                    float cjy = h2 + (ir + jitter) * std::sin(ia);
                    double map_x = slX.rpn.execute4d3i(cjx / filterWidth, cjy / filterHeight, jitter, ir / src_pixel_width, ir, ia, src_pixel_width);
                    double map_y = slY.rpn.execute4d3i(cjx / filterWidth, cjy / filterHeight, jitter, ir / src_pixel_width, ir, ia, src_pixel_width);
                    vert_color[vert_color_index] = map_x;
                    vert_color[vert_color_index + 1] = map_y;
                    vert_color[vert_color_index + 2] = src_pd[pdi];
                    vert_color[vert_color_index + 3] = src_pd[pdi + 1];
                    vert_color[vert_color_index + 4] = src_pd[pdi + 2];
                    vert_color_index += per_vert_color_stride;
                }
            }
            glBufferSubData(GL_ARRAY_BUFFER, 0, vert_color_index * sizeof(GLfloat), &vert_color[0]);
            glDrawArrays(GL_LINE_LOOP, 0, vert_color_index/per_vert_color_stride);
        }
        }
        break;
    default: {
        dx *= filterWidth;
        dy *= filterHeight;
        for(float iy=0; iy<src_pixel_height; iy+=dy) {
            size_t vert_color_index = 0;
            float iy_drift = iy + filterHeight * slDrift.rpn.execute3d1i(iy, 0.0, 0.0, 0);
            for(float ix=0; ix<=src_pixel_width; ix+=dx) {
                if(vert_color_index < max_vert_color_index && iy_drift >= 0 && iy_drift < filterHeight) {
                    size_t pdi = ((size_t)ix + (size_t)(iy_drift) * filterWidth) * per_pixel;
                    double jitter = filterHeight * slJitter.rpn.execute4d3i(ctrl_pd[pdi], ctrl_pd[pdi+1], ctrl_pd[pdi+2], ix / src_pixel_width, ix, iy, src_pixel_width);
                    double map_x = slX.rpn.execute4d3i(ix / filterWidth, (iy_drift+jitter) / filterHeight, jitter, ix / src_pixel_width, ix, iy, src_pixel_width);
                    double map_y = slY.rpn.execute4d3i(ix / filterWidth, (iy_drift+jitter) / filterHeight, jitter, ix / src_pixel_width, ix, iy, src_pixel_width);
                    vert_color[vert_color_index] = map_x;
                    vert_color[vert_color_index + 1] = map_y;
                    vert_color[vert_color_index + 2] = src_pd[pdi];
                    vert_color[vert_color_index + 3] = src_pd[pdi + 1];
                    vert_color[vert_color_index + 4] = src_pd[pdi + 2];
                    vert_color_index += per_vert_color_stride;
                }
            }
            glBufferSubData(GL_ARRAY_BUFFER, 0, vert_color_index * sizeof(GLfloat), &vert_color[0]);
            glDrawArrays(GL_LINE_STRIP, 0, vert_color_index/per_vert_color_stride);
        }
        }
        break;
    }

    glDisableVertexAttribArray(posAttrib);
    glDisableVertexAttribArray(colAttrib);
    glBindVertexArray(0);
    glUseProgram(0);

    fbo.end();
#ifdef TRACE_MONOTONIC
    clock_gettime(CLOCK_MONOTONIC, &time4);
    ofLog(OF_LOG_NOTICE) << "GlScanlines::ofDrawLine " << (time4.tv_sec * 1000000000l + time4.tv_nsec) - (time3.tv_sec * 1000000000l + time3.tv_nsec);
#endif // TRACE_MONOTONIC
}
