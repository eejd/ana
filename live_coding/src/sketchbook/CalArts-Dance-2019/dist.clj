(ns lv.demo)
(use 'lv.core)

(layout "grid" [imgs0 dist1])

(imagesequ "CalArts-Dance-2019/arm/*.png")
(render)
(hsv :imgs0 [c0]
  (rot (* -0.01 f))
  (k sframe)
  (hsv (* (vec3 1 1 1) c0.hsv))
)

(expr :dist1 [imgs0]
  (step 7)
)
; (render 1)
