import os 
from pathlib import Path

montage = "montage"
sli = 0

if not Path(montage).exists():
    os.mkdir(montage)
if not Path(montage).is_dir():
    print(montage, "is not an folder")
    exit(1)

def all_add(p):
    global sli
    if not p.endswith('/'):
        print(p, "path is missing trailing /")
        exit(1)
    fns = os.listdir(p)
    fns.sort()
    for x in fns:
        if x.endswith(".png"): 
            fsymlnk = montage + "/ana_" + str(sli).zfill(6) + ".png";
            forig = p + x;
            print(fsymlnk, "->", forig)
            os.symlink(forig, fsymlnk)
            sli += 1

def rev_add(p):
    global sli
    if not p.endswith('/'):
        print(p, "path is missing trailing /")
        exit(1)
    fns = os.listdir(p)
    fns.sort()
    fns.reverse()
    for x in fns:
        if x.endswith(".png"): 
            fsymlnk = montage + "/ana_" + str(sli).zfill(6) + ".png";
            forig = p + x;
            print(fsymlnk, "->", forig)
            os.symlink(forig, fsymlnk)
            sli += 1

def trg_add(p, start, stop):
    global sli
    if not p.endswith('/'):
        print(p, "path is missing trailing /")
        exit(1)
    fns = os.listdir(p)
    fns.sort()
    capture = False
    for x in fns:
        if x.endswith(".png"): 
            if x.endswith(start):
                capture = True
            if capture:
                fsymlnk = montage + "/ana_" + str(sli).zfill(6) + ".png";
                forig = p + x;
                print(fsymlnk, "->", forig)
                os.symlink(forig, fsymlnk)
                sli += 1
            if x.endswith(stop):
                capture = False

def credits(f, i=75):
    global sli
    forig = f;
    for x in range(i):
        fsymlnk = montage + "/ana_" + str(sli).zfill(6) + ".png";
        print(fsymlnk, "->", forig)
        os.symlink(forig, fsymlnk)
        sli += 1

#trg_add ('/home/strom/ana_exp/physarum/exp_2021-07-13-19-01-21-138/', "ana_0000_002981.png", "ana_0000_003599.png")
#all_add ('/home/strom/ana_exp/physarum/exp_2021-07-14-09-32-38-564/')
#trg_add ('/home/strom/ana_exp/physarum/exp_2021-07-14-07-04-24-528/', "ana_0000_022171.png", "ana_0000_022768.png")
#trg_add ('/home/strom/ana_exp/physarum/exp_2021-07-14-11-56-30-905/', "ana_0000_010777.png", "ana_0000_011315.png")
#rev_add ('/home/strom/ana_exp/physarum/exp_2021-07-14-16-40-32-118/')
#all_add ('/home/strom/ana_exp/physarum/exp_2021-07-15-06-56-26-165/')

#all_add ("/ssd/ana/Road_Trip/exp_2021-05-02-13-12-27-060/")
#trg_add ("/ssd/ana/Road_Trip/exp_2021-05-02-13-24-53-670/", "ana_0000_002798.png", "ana_0000_003166.png")
#trg_add ("/ssd/ana/Road_Trip/exp_2021-05-02-13-24-53-670/", "ana_0000_001844.png", "ana_0000_002030.png")
#trg_add ("/ssd/ana/Road_Trip/exp_2021-05-02-13-17-13-947/", "ana_0000_004912.png", "ana_0000_005102.png")
#trg_add ("/ssd/ana/Road_Trip/exp_2021-05-02-13-17-13-947/", "ana_0000_005183.png", "ana_0000_005411.png")
#trg_add ("/ssd/ana/Road_Trip/exp_2021-05-02-13-17-13-947/", "ana_0000_006004.png", "ana_0000_006205.png")
#all_add ("/ssd/ana/Road_Trip/exp_2021-05-02-17-40-09-010/")
#all_add ("/ssd/ana/Road_Trip/exp_2021-05-02-16-56-50-023/")
#trg_add ("/ssd/ana/Road_Trip/exp_2021-05-02-13-24-53-670/", "", "")


#trg_add ('/home/strom/ana_exp/sdf/exp_2021-06-03-19-23-48-419/', 'ana_0000_002856.png', 'ana_0000_003223.png')
#trg_add ('/home/strom/ana_exp/sdf/exp_2021-06-02-16-51-26-712/', 'ana_0000_003288.png', 'ana_0000_004055.png')
#trg_add ('/home/strom/ana_exp/sdf/exp_2021-06-03-19-23-48-419/', 'ana_0000_003223.png', 'ana_0000_003513.png')
#all_add ('/home/strom/ana_exp/sdf/exp_2021-06-05-16-54-18-273/')
#trg_add ('/home/strom/ana_exp/sdf/exp_2021-06-03-19-23-48-419/', 'ana_0000_003513.png', 'ana_0000_004355.png')
#all_add ('/home/strom/ana_exp/sdf/exp_2021-06-06-09-26-25-957/2/')

