#include "ofMain.h"

#include "globalvars.h"
#include "Configuration.h"
#include "OscListener.h"
#include "gl/GlVideoFilter.h"

GlVideoFilter::GlVideoFilter(ofGLWindowSettings settings, OscListener *oscParams) :
        Filter(settings, oscParams) {
}

GlVideoFilter::~GlVideoFilter() {
}

void GlVideoFilter::setup(const std::string id, int numSamples) {
    Filter::setup(id, numSamples);

    rotate = asInt(GLVIDEOFILTER_ROTATE);
}

void GlVideoFilter::reload(ofxOscMessage const &msg) {
    auto narg = msg.getNumArgs();
    if(narg > 2) {
        Filter::compileShader(msg.getArgAsString(0), msg.getArgAsString(3));
    }
}

void GlVideoFilter::draw(int li) {
    fbo.begin();

    shader.begin();
    sendUniforms(shader, li);

#ifdef WITH_video_grabber
    if(videoGrabber && videoGrabber->isInitialized()) {
        size_t vx = 0;
        size_t vy = 0;
        switch(rotate) {
        case 0:
            vx = filterWidth > video_width ? (filterWidth - video_width) / 2u : 0u;
            vy = filterHeight > video_height ? (filterHeight - video_height) / 2u : 0u;
            videoGrabber->draw(vx, vy);
            break;
        default:
            videoGrabber->getPixels().rotate90To(dst90, rotate);
            img90.setFromPixels(dst90);
            img90.draw(0, 0);
            break;
        }
    } else {
        ofClear(0, 0, 0, 255);
    }
#else
    ofClear(0, 0, 0, 255);
#endif // WITH_video_grabber
    shader.end();
    fbo.end();
}

void GlVideoFilter::toMermaid(std::stringstream & info) {
    info << "camera((camera))" << " -- c0 --> " << id << "\n";
    Filter::toMermaid(info);
}
