(ns lv.demo)
(use 'lv.core)

(layout "grid" [img0 img1 clspp0]
               [clpp1 ras2 ras3])
(image "flower-sampler/*.png")

(rgb img0 [c0 c1 c2 c3]
  (rgb c2.rgb)
)

(rgb img1 [c0 c1 c2 c3]
  (rgb c3.rgb)
)

(expr :clspp0 []
  ; paramters used by Dan Wills
  ; https://www.youtube.com/watch?v=0D2YIwn6TtQ
  "cl/Ginzburg-Landau-Dan_Wills.cl"
  (p0 0.3)   ; D_a   = 0.3f;
  (p1 0.151) ; D_b   = 0.151f;
  (p2 0.076) ; alpha = 0.076;
  (p3 0.8)   ; beta  = 0.8;
  (p4 1.31)  ; delta = 1.31;
  (p5 0.01)  ; gamma = 0.01;
  (loops 11)
)

(expr :clpp1 []
  ; paramters used by Dan Wills
  ; https://www.youtube.com/watch?v=0D2YIwn6TtQ
  "cl/Ginzburg-Landau-Dan_Wills.cl"
  (p0 0.3)   ; D_a   = 0.3f;
  (p1 0.151) ; D_b   = 0.151f;
  (p2 0.076) ; alpha = 0.076;
  (p3 0.8)   ; beta  = 0.8;
  (p4 1.31)  ; delta = 1.31;
  (p5 0.01)  ; gamma = 0.01;
  (loops 19)
)

(hsv :ras2 [clspp0 clpp1]
  (v (* 4 (+ clspp0.g clpp1.g)))
)

(rgb :ras3 [img0 img1 ras2]
  (rgb (mix img1.rgb img0.rgb (* (* f 0.001) ras2.g)))
)
(render 5)
