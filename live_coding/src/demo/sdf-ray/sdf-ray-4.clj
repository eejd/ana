(ns lv.demo)
(use 'lv.core)

(layout "grid" [mov0 frag1]
               [cline2 mopr3 ras4])
(movie "4movies/*.mp4")
(render)

; movie as input stream
(rgb :mov0 [c0 c1 c2 c3]
 (sy 1.45)
 ; (rgb (- 1 c1.rgb))
 (rgb (* 1.2 c0.rgb))
)

; kp6    0.37795
;
; ks0    0.51969
; ks1    1

(expr :frag1 [mov0]
 "frag/sdf-ray-4.frag"
 (a ks0)
 (b ks1)
 (rx (* 31 (snoise (* 0.0011 f)  0.4 -0.3)))
 (ry (* 30 (snoise (* 0.0013 f)  0.2 -0.1)))
 (rz (* 39 (snoise (* 0.0017 f)  0.3 -0.2)))
)

(expr :cline2 [frag1]
  (count 50)
  (max 2500)
  (thres kp6)
  (fill -1)
  (names -1)
  (clear (zero? (% f 30) 1 -1))
  (paint -1)
  (holes 1)
)

(expr :mopr3 [cline2] "ddb")

(rgb :ras4 [frag1 mopr3]
  (rgb (max frag1.rgb mopr3.rgb))
)
