#pragma once

#include "ofMain.h"
#include "Filter.h"
#include "OscListener.h"
#include "calc/ScalarLisp.h"

class GlScreenFilter : public Filter {
	public:
		GlScreenFilter(ofGLWindowSettings settings, OscListener * oscParams);
		virtual ~GlScreenFilter();
		virtual void setup(const std::string id, int numSamples);
        virtual void reload(ofxOscMessage const &msg);
        virtual void update();
        virtual void draw(int li);
		virtual void toMermaid(std::stringstream & info);

        ScalarLisp slX;
        int ix = 0;
        ScalarLisp slY;
        int iy = 0;
        ScalarLisp slW;
        int iw = -1;
        ScalarLisp slH;
        int ih = -1;
        ScalarLisp slClear;

private:
//        char * scrbuf = nullptr;
        bool hasPixelBuffer = false;
        int w_old = -1;
        int h_old = -1;
		ofPixels pixelBuffer;
		ofImage pixelImage;

    void clearPixelBuffer();
    void allocatePixelBuffer();
};
