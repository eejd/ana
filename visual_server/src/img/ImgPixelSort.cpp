#include "ofMain.h"
#include "Configuration.h"
#include "globalvars.h"
#include "OscListener.h"
#include "Filter.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispMng.h"
#include "img/ImgPixelSort.h"

ImgPixelSort::ImgPixelSort(ofGLWindowSettings settings, OscListener * oscParams) :
        Filter(settings, oscParams) {
}

ImgPixelSort::~ImgPixelSort() {
}

void ImgPixelSort::setup(const std::string id, int numSamples) {
    Filter::setup(id, numSamples);
}

void ImgPixelSort::reload(ofxOscMessage const &msg) {
    ScalarLispMng mng;
    auto narg = msg.getNumArgs();
    if(narg > 1)
        Filter::setInputTexture(msg.getArgAsString(1));
    mng.compile(slCompare, "comp", "(+ h)", msg);
    mng.compile(slThresholdBand,  "b", "0.5", msg);
    mng.compile(slThresholdWidth, "w", "0.1", msg);
}

static bool comparePixel(Hpix i1, Hpix i2)
{
    return i1.comp < i2.comp;
}

#define SCAN_SEARCH 0
#define SCAN_ACCEPT 1

void ImgPixelSort::horizontal(const float threshold_min, const float threshold_max, float *pd) {
    for(size_t pyi = 0; pyi < filterHeight; ++pyi) {
        int scan_state = SCAN_SEARCH;
        size_t edge = 0;
        for(size_t pxi = 0; pxi < filterWidth; ++pxi) {
            size_t pi = (pxi + pyi * filterWidth) * 4;
            float v = (pd[pi] + pd[pi + 1] + pd[pi + 2]) / 3.0f;
            switch(scan_state) {
            case SCAN_SEARCH:
                if(v > threshold_min && v < threshold_max) {
                    //                                hpix.clear();
                    //                                ofFloatColor f(pd[pi], pd[pi+1], pd[pi+2]);
                    //                                f.getHsb(h0, s0, v0);
                    //                                const Hpix hx { pd[pi], pd[pi+1], pd[pi+2], (float)slCompare.rpn.execute3d1i(pxi, pyi, 0.0, hpix.size()) };
                    //                                hpix.push_back(hx);
                    edge = pxi;
                    scan_state = SCAN_ACCEPT;
                }
                break;
            case SCAN_ACCEPT:
                ofFloatColor f(pd[pi], pd[pi + 1], pd[pi + 2]);
                f.getHsb(h0, s0, v0);
                const Hpix hx { pd[pi], pd[pi + 1], pd[pi + 2], (float) (slCompare.rpn.execute3d1i(pxi, pyi, 0.0, hpix.size())) };
                hpix.push_back(hx);
                if(v < threshold_min || v > threshold_max) {
                    sort(hpix.begin(), hpix.end(), comparePixel);
                    size_t hlen = hpix.size();
                    for(size_t ih = 0; ih < hlen && edge + ih < filterWidth; ++ih) {
                        size_t pi = (edge + ih + pyi * filterWidth) * 4;
                        pd[pi] = hpix[ih].r;
                        pd[pi + 1] = hpix[ih].g;
                        pd[pi + 2] = hpix[ih].b;
                    }
                    hpix.clear();
                    scan_state = SCAN_SEARCH;
                }
                break;
            }
        }
        hpix.clear();
    }
}

void ImgPixelSort::veritcal(const float threshold_min, const float threshold_max, float *pd) {
    for(size_t pxi = 0; pxi < filterWidth; ++pxi) {
        int scan_state = SCAN_SEARCH;
        size_t edge = 0;
        for(size_t pyi = 0; pyi < filterHeight; ++pyi) {
            size_t pi = (pxi + pyi * filterWidth) * 4;
            float v = (pd[pi] + pd[pi + 1] + pd[pi + 2]) / 3.0f;
            switch(scan_state) {
            case SCAN_SEARCH:
                if(v > threshold_min && v < threshold_max) {
                    edge = pyi;
                    scan_state = SCAN_ACCEPT;
                }
                break;
            case SCAN_ACCEPT:
                ofFloatColor f(pd[pi], pd[pi + 1], pd[pi + 2]);
                f.getHsb(h0, s0, v0);
                const Hpix hx { pd[pi], pd[pi + 1], pd[pi + 2], (float) (slCompare.rpn.execute3d1i(pxi, pyi, 0.0, hpix.size())) };
                hpix.push_back(hx);
                if(v < threshold_min || v > threshold_max) {
                    sort(hpix.begin(), hpix.end(), comparePixel);
                    size_t hlen = hpix.size();
                    for(size_t ih = 0; ih < hlen && edge + ih < filterHeight; ++ih) {
                        size_t pi = (pxi + ((edge + ih) * filterWidth)) * 4;
                        pd[pi] = hpix[ih].r;
                        pd[pi + 1] = hpix[ih].g;
                        pd[pi + 2] = hpix[ih].b;
                    }
                    hpix.clear();
                    scan_state = SCAN_SEARCH;
                }
                break;
            }
        }
        hpix.clear();
    }
}

void ImgPixelSort::update() {
    const float w2 = std::fabs(slThresholdWidth.rpn.execute0()) / 2.0f;
    const float threshold_min   = slThresholdBand.rpn.execute0() - w2;
    const float threshold_max = threshold_min + w2;
    if(upstream_ids.size() >= 1) {
        auto in_id = upstream_ids[0];
        if(filterDict.find(in_id) != filterDict.end()) {
            auto f = filterDict[in_id];
            if(f) {
                f->getTexture().readToPixels(pixels);
                float* pd = pixels.getData();
                hpix.clear();

                veritcal(threshold_min, threshold_max, pd);
//                horizontal(threshold_min, threshold_max, pd);
            }
        }
    }
}

void ImgPixelSort::draw(int li) {
    fbo.begin();
    ofClear(0, 0, 0, 255);

    ofFloatImage img;
    img.setFromPixels(pixels);
    img.draw(0, 0);

    fbo.end();
}
