#include "ofMain.h"
#include "ofxCv.h"

#include "stringhelper.h"
#include "pathhelper.h"
#include "globalvars.h"
#include "Configuration.h"
#include "Filter.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispMng.h"
#include "shape/ShapeMeshDeform.h"

//#define TRACE_MONOTONIC
#undef TRACE_MONOTONIC
#ifdef TRACE_MONOTONIC
static timespec time1, time2, time3a, time3b, time3c, time4, time5, time6;
#endif // TRACE_MONOTONIC

    ShapeMeshDeform::ShapeMeshDeform(ofGLWindowSettings settings, OscListener * oscParams) : Filter(settings, oscParams){
}


ShapeMeshDeform::~ShapeMeshDeform() {
    mesh.clear();
}


void ShapeMeshDeform::setup(const std::string id, int numSamples) {
    Filter::setup(id, numSamples);

    reduced_filterWidth  = filterWidth  / reduced_size;
    reduced_filterHeight = filterHeight / reduced_size;
    mesh.setMode(OF_PRIMITIVE_TRIANGLES);
}

void ShapeMeshDeform::allocateMesh() {
    float nextStepSize = slStepSize.rpn.execute0();

    if(nextStepSize > 0.001 && nextStepSize != stepSize) {
        mesh.clear();
        mesh.setMode(OF_PRIMITIVE_TRIANGLES);

        stepSize = nextStepSize;
        reduced_stepSize = stepSize / reduced_size;
        ySteps = 1 + filterHeight / stepSize;
        xSteps = 1 + filterWidth / stepSize;
        for(int y = 0; y < ySteps; y++) {
            for(int x = 0; x < xSteps; x++) {
                mesh.addVertex(ofVec3f(x * stepSize, y * stepSize, 0.0));
                mesh.addTexCoord(ofVec2f(x * stepSize, y * stepSize));
            }
        }
        for(int y = 0; y + 1 < ySteps; y++) {
            for(int x = 0; x + 1 < xSteps; x++) {
                int nw = y * xSteps + x;
                int ne = nw + 1;
                int sw = nw + xSteps;
                int se = sw + 1;
                mesh.addIndex(nw);
                mesh.addIndex(ne);
                mesh.addIndex(se);
                mesh.addIndex(nw);
                mesh.addIndex(se);
                mesh.addIndex(sw);
            }
        }

        mesh_tx = (float)reduced_filterWidth  - (xSteps - 1) * reduced_stepSize;
        mesh_tx *= 0.5;
        mesh_ty = (float)reduced_filterHeight - (ySteps - 1) * reduced_stepSize;
        mesh_ty *= 0.5;

    }
}

void ShapeMeshDeform::reload(ofxOscMessage const &msg) {
    auto narg = msg.getNumArgs();
    if(narg > 1) {
        Filter::setInputTexture(msg.getArgAsString(1));
    }
    ScalarLispMng mng;
    mng.compile(slPyrScale, "pscale", "0.5", msg);
    mng.compile(slLevels, "levels", "4", msg);
    mng.compile(slWinSize, "win", "8", msg);
    mng.compile(slIterations, "iter", "2", msg);
    mng.compile(slPolyN, "polyn", "7", msg);
    mng.compile(slPolySigma, "polys", "1.5", msg);
    mng.compile(slUseGaussian, "gauss", "-1", msg);
    mng.compile(slInMode, "in", "", msg);
    mng.compile(slMode, "mode", "", msg);
    mng.compile(slClear, "clear", "1", msg);
    mng.compile(slStepSize, "step", "4", msg);
    mng.compile(slDistortionStrength, "dist", "4", msg);
    mng.compile(slUpstream, "paint", "-1", msg);

    allocateMesh();
}

ofTexture & ShapeMeshDeform::getTexture() {
    return fbo.getTexture();
}

void ShapeMeshDeform::distortFromFlow() {
#ifdef TRACE_MONOTONIC
    clock_gettime(CLOCK_MONOTONIC, &time3a);
#endif // TRACE_MONOTONIC
    f->getTexture().readToPixels(grabbedPixels);
#ifdef TRACE_MONOTONIC
    clock_gettime(CLOCK_MONOTONIC, &time3b);
#endif // TRACE_MONOTONIC
    frame_captured = ofxCv::toCv(grabbedPixels);
#ifdef TRACE_MONOTONIC
    clock_gettime(CLOCK_MONOTONIC, &time3c);
#endif // TRACE_MONOTONIC
    cv::resize(frame_captured, frame_scaled, cv::Size(reduced_filterWidth, reduced_filterHeight));
#ifdef TRACE_MONOTONIC
    clock_gettime(CLOCK_MONOTONIC, &time4);
#endif // TRACE_MONOTONIC
    ffb.setPyramidScale(slPyrScale.rpn.execute0());
    ffb.setNumLevels(slLevels.rpn.execute0());
    ffb.setWindowSize(slWinSize.rpn.execute0());
    ffb.setNumIterations(slIterations.rpn.execute0());
    ffb.setPolyN(slPolyN.rpn.execute0());
    ffb.setPolySigma(slPolySigma.rpn.execute0());
    ffb.setUseGaussian(slUseGaussian.rpn.execute0() >= 0.0);
#ifdef TRACE_MONOTONIC
    clock_gettime(CLOCK_MONOTONIC, &time5);
#endif // TRACE_MONOTONIC
    ffb.calcOpticalFlow(frame_scaled);
#ifdef TRACE_MONOTONIC
    clock_gettime(CLOCK_MONOTONIC, &time6);
#endif // TRACE_MONOTONIC
    auto & mv = mesh.getVertices();
    for(int y = 0; y < ySteps; y++) {
        float py = y * reduced_stepSize;
        float sample_py = py;
        if(sample_py + reduced_stepSize >= reduced_filterHeight)
            sample_py = reduced_filterHeight - reduced_stepSize; //TODO
        for(int x = 0; x < xSteps; x++) {
            float px = x * reduced_stepSize;
            float sample_px = px;
            if(sample_px + reduced_stepSize >= reduced_filterWidth)
                sample_px = reduced_filterWidth - reduced_stepSize; //TODO
            int i = y * xSteps + x;
            ofVec2f offset = ffb.getAverageFlowInRegion(ofRectangle(sample_px, sample_py, reduced_stepSize, reduced_stepSize));
            float distortionStrength = slDistortionStrength.rpn.execute3d1i(x, y, 0.0, i);
            mv[i] = ofVec3f((mesh_tx + px + distortionStrength * offset.x) * reduced_size,
                            (mesh_ty + py + distortionStrength * offset.y) * reduced_size,
                            0.0);
        }
    }
#ifdef TRACE_MONOTONIC
    clock_gettime(CLOCK_MONOTONIC, &time2);
    ofLog(OF_LOG_NOTICE) << "read pixel              " << (time3b.tv_sec * 1000000000l + time3b.tv_nsec) - (time3a.tv_sec * 1000000000l + time3a.tv_nsec);
    ofLog(OF_LOG_NOTICE) << "toCV                    " << (time3c.tv_sec * 1000000000l + time3c.tv_nsec) - (time3b.tv_sec * 1000000000l + time3b.tv_nsec);
    ofLog(OF_LOG_NOTICE) << "cv:resize               " << (time4.tv_sec * 1000000000l + time4.tv_nsec) - (time3c.tv_sec * 1000000000l + time3c.tv_nsec);
    ofLog(OF_LOG_NOTICE) << "calcOpticalFlow         " << (time6.tv_sec * 1000000000l + time6.tv_nsec) - (time5.tv_sec * 1000000000l + time5.tv_nsec);
#endif // TRACE_MONOTONIC
}

void ShapeMeshDeform::distortFromHSV() {
    f->getTexture().readToPixels(grabbedPixels);

    auto & mv = mesh.getVertices();
    for(int y = 0; y < ySteps; y++) {
        float py = y * reduced_stepSize;
        for(int x = 0; x < xSteps; x++) {
            float px = x * reduced_stepSize;
            int i = y * xSteps + x;
            auto rgbC = grabbedPixels.getColor(px, py);
            float h = rgbC.getHue();
            float v = rgbC.getBrightness() / 256.0;
            float offset_x = v*std::cos(h*2.0f*M_PI);
            float offset_y = v*std::sin(h*2.0f*M_PI);
            float distortionStrength = slDistortionStrength.rpn.execute3d1i(x, y, 0.0, i);
            mv[i] = ofVec3f((mesh_tx + px + distortionStrength * offset_x) * reduced_size,
                            (mesh_ty + py + distortionStrength * offset_y) * reduced_size,
                            0.0);
        }
    }
}

void ShapeMeshDeform::update() {
#ifdef TRACE_MONOTONIC
    clock_gettime(CLOCK_MONOTONIC, &time1);
#endif // TRACE_MONOTONIC
    hasData = false;
    allocateMesh();

    if(upstream_ids.size() >= 1) {
        auto in_id = upstream_ids[0];
        if(filterDict.find(in_id) != filterDict.end()) {
            f = filterDict[in_id];
            if(f) {
                if(slInMode.rpn.execute0() > 0.00001)
                    distortFromHSV();
                else
                    distortFromFlow();
                hasData = true;
            }
        }
    }
#ifdef TRACE_MONOTONIC
    clock_gettime(CLOCK_MONOTONIC, &time2);
    ofLog(OF_LOG_NOTICE) << "ShapeMeshDeform::update " << (time2.tv_sec * 1000000000l + time2.tv_nsec) - (time1.tv_sec * 1000000000l + time1.tv_nsec);
#endif // TRACE_MONOTONIC
}

void ShapeMeshDeform::draw(int li) {
    if(!hasData)
        return;

    fbo.begin();
    if(slClear.rpn.execute0() >= 0.0)
        ofClear(0, 0, 0, 255);

    f->getTexture().bind();
    if(slMode.rpn.execute0() > 0.00001)
        mesh.draw();
    else
        mesh.drawWireframe();
    f->getTexture().unbind();

    fbo.end();
}
