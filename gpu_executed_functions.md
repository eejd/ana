(raster :r0
  )
  # GPU executed functions
This will be translated to OpenGL fragment shader code.

[[_TOC_]]

## Read values
Input values can be read in expressions. It does not make sense to overwrite these values within an expression.

| read from | description | example |
| --- | --- |  --- |
| x | x coordinate of the current pixel<br>in range -1.0 .. 1.0 | x |
| y | y coordinate of the current pixel<br>in range -1.0 .. 1.0 | y |
| r | distance of the current pixel from the origin of the coordinate system<br>radius | r |
| a | polar angle of the current pixel | a |
| f | current frame number of the animation | f |
| li | index number of the current filter | li |
| vol | sound input volume | vol |
| onset | sound onset detection<br>TODO very low values, <br>TODO did not work properly yet | onset |
| f0<br>..<br>f7 | sound input<br>Fast Fourier Transform<br>from bin 0 to bin 7 | f0<br>f1<br>f2 |
| slength | number of images in a texture array.<br>img**s** filters only | slength |
| sframe | current frame counter modulo number of images in a texture array.<br>img**s** filters only | sframe |

## Read values from a game pad
Input values can be read in expressions. It does not make sense to overwrite these values within an expression.

| read from | description | range |
| --- | --- |  --- |
| lx | left joystick button<br> x coordinate | -1.0 .. 1.0 |
| ly | left joystick button<br> y coordinate | -1.0 .. 1.0 |
| l2 | left joystick L2 button<br> y coordinate | -1.0 .. 1.0 |
| rx | right joystick button<br> x coordinate | -1.0 .. 1.0 |
| ry | right joystick button<br> y coordinate | -1.0 .. 1.0 |
| r2 | right joystick R2 button<br> y coordinate | -1.0 .. 1.0 |

![](./stuff/doc/js-lx_rx.jpg)

## Read values from nanoKONTROL2
Input values can be read in expressions. It does not make sense to overwrite these values within an expression.

| read from | description | range |
| --- | --- |  --- |
| ks0 | nanoKONTROL2 slider 0 | 0.0 .. 1.0 |
| ks1 | nanoKONTROL2 slider 1 | 0.0 .. 1.0 |
| ks2 | nanoKONTROL2 slider 2 | 0.0 .. 1.0 |
| ks3 | nanoKONTROL2 slider 3 | 0.0 .. 1.0 |
| ks4 | nanoKONTROL2 slider 4 | 0.0 .. 1.0 |
| ks5 | nanoKONTROL2 slider 5 | 0.0 .. 1.0 |
| ks6 | nanoKONTROL2 slider 6 | 0.0 .. 1.0 |
| ks7 | nanoKONTROL2 slider 7 | 0.0 .. 1.0 |
| ks0 | nanoKONTROL2 potentiometer 0 | 0.0 .. 1.0 |
| ks1 | nanoKONTROL2 potentiometer 1 | 0.0 .. 1.0 |
| ks2 | nanoKONTROL2 potentiometer 2 | 0.0 .. 1.0 |
| ks3 | nanoKONTROL2 potentiometer 3 | 0.0 .. 1.0 |
| ks4 | nanoKONTROL2 potentiometer 4 | 0.0 .. 1.0 |
| ks5 | nanoKONTROL2 potentiometer 5 | 0.0 .. 1.0 |
| ks6 | nanoKONTROL2 potentiometer 6 | 0.0 .. 1.0 |
| ks7 | nanoKONTROL2 potentiometer 7 | 0.0 .. 1.0 |
| kb0 | nanoKONTROL2 S M R button 0 | -1.0, 0.0, 1.0 |
| kb1 | nanoKONTROL2 S M R button 1 | -1.0, 0.0, 1.0 |
| kb2 | nanoKONTROL2 S M R button 2 | -1.0, 0.0, 1.0 |
| kb3 | nanoKONTROL2 S M R button 3 | -1.0, 0.0, 1.0 |
| kb4 | nanoKONTROL2 S M R button 4 | -1.0, 0.0, 1.0 |
| kb5 | nanoKONTROL2 S M R button 5 | -1.0, 0.0, 1.0 |
| kb6 | nanoKONTROL2 S M R button 6 | -1.0, 0.0, 1.0 |
| kb7 | nanoKONTROL2 S M R button 7 | -1.0, 0.0, 1.0 |

![](./stuff/doc/nanoKONTROL2.jpg)

## Write to fragment output
Output values should be in the range 0.0 to 1.0.

| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| h | 1 | set hue of the current pixel | (h 0.5) |
| s | 1 | set saturation of the current pixel | (s (abs x)) |
| v | 1 | set brightness of the current pixel | (v (usin y)) |
| hsv | use one 3<br>component<br>vector | set all three HSV components of the current pixel | (hsv (vec3 0.5 0.6 0.7)) |
| r | 1 | set red component of the current pixel | (r 1) |
| g | 1 | set red component of the current pixel | (g 0.5) |
| b | 1 | set red component of the current pixel | (b (ucos y)) |
| rgb | use one 3<br>component<br>vector | set all three RGB components of the current pixel | (rgb (vec3 0.5 0.6 0.7)) |


## Write to set the an index for texture array access
This index should be in the range 0 to number of images - 1.

| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| k | 1 | select image to read form | (k 0)<br>(k (* ks0 slength)) |


## Deforming the coordinates
### Transform 2D
Mapping in the filters 2D coordinate system. Do this before reading color values.

![](./stuff/doc/2d-coord.jpg)

| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| kaleid | 1 | kaleidoscope effect | (kaleid 7) |
| mirror | 0 | image tiling effect<br>mirrors x and y axes | (mirror) |
| inv | 0 | invert x and y axes<br> 1/x 1/y | (inv) |
| rot | 1 | rotate around center | (rot (* f 0.005)) |
| tx | 1 | translate x coordinate | (tx 0.05) |
| ty | 1 | translate y coordinate | (ty 0.1) |
| sx | 1 | scale x coordinate | (sx 0.5) |
| sy | 1 | scale y coordinate | (sy 1.2) |
| repeat-x | 1 | image tiling effect<br>use negative values to mirror | (repeat-x 2) |
| repeat-y | 1 | image tiling effect<br>use negative values to mirror | (repeat-y -3) |
| pixelate-x | 1 | paint coarse pixel<br>x dimension<br>should be 2 or greater | (pixelate-x 5) |
| pixelate-x | 1 | paint coarse pixel<br>y dimension<br>should be 2 or greater | (pixelate-y 50) |

## Expressions
### Simple arithmetic
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| + | 2 .. n | sum of all parameters | (+ 0.1 x)<br><br>(+ 1 2 3) |
| - | 2 .. n | subtract from first parameter all following parameters | (- 0.1 x)<br><br>(- 1 2 3) |
| * | 2 .. n | multiply all parameters | (* 0.1 x)<br><br>(* 0.05 f x) |
| / | 2 .. n | dived the first parameter by all following parameters | (/ x 2)<br><br>(+ 16 4 2) |
| % | 2 | modulo TODO |  |

### Compare values
For use in conditionals.

| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| > | 2 | greater then | (> x -1000.0) |
| >= | 2 | greater equal | (> 2 1) |
| == TODO| 2 | equal | (== 5 5) |
| <= | 2 | less or equal | (<= 5 5)  |
| < | 2 | less then | (< 4 5) |

### Boolean operators
For use in conditionals.

| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| && | 2 | logical and | (&&<br>(> x 0.2)<br>(> y 0.4))|
| \|\| | 2 | logical or | (\|\|<br>(> x 0.2)<br>(> y 0.4))|

### Conditionals
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| if? | 3 | If the first term is true evaluates the second term. If not the third term is evaluated. | (if?<br>(> x 0.5)<br>0.5<br>x) |
| zero? | 3 | If the first term evaluates to 0 the second term is evaluated. If not the third term is evaluated. | (zero?<br>(/ x 1000)<br>1<br>(+ 1 x)) |
| pos? | 3 | If the first term evaluates to an positive number the second term is evaluated. If not the third term is evaluated. | (pos?<br>x<br>x<br>(* -1.0 x)) |
| even? | 3 | If the first term evaluates to an even number the second term is evaluated. If not the third term is evaluated. | (even?<br>(+ 1 1)<br>1<br>-1.0 )|

### Type casts
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| int | 1 | convert float to int | (int 0.9)<br><br>(int x) |
| vec2 | 2 | construct a two component vector from two scalar values | (vec2 1.1 2.2) |
| vec3 | 3 | construct a three component vector from three scalar values | (vec3 0.1 0.2 x)<br><br>(* (vec3 1 0.7 0.5) c0.hsv) |

### Common functions
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| abs | 1 | absolute value | (abs x) |
| sign | 2 | sign of input value<br>returns -1.0 or 0.0 or 1.0 | (TODO) |
| mod TODO |  |  | (TODO) |
| floor | 1 | nearest integer less than x | (floor x) |
| ceil | 1 | nearest integer greater than x | (ceil x) |
| trunc TODO | 1 |  | (TODO) |
| round | 1 | round to nearest integer | (round 3.1415) |
| clamp | 3 | returns (min (max x minVal) maxVal) | (clamp x 0.4 0.6) |

### Exponential functions
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| pow | 2 | returns x raised to the y power | (pow x y) |
| exp | 1 | returns the natural exponentiation of x | (exp 2) |
| log | 1 | returns the natural logarithm of x | (log (abs x)) |
| log2 TODO | 1 |  | (TODO) |
| sqrt | 1 | square root | (sqrt x) |
| inversesqrt TODO | 1 | inverse square root<br>(/ 1 (sqrt x))| (inversesqrt x) |
| sqr | 1 | input vale multiplied by it's self<br>(* x x) | (sqr y) |

### Trigonometry functions
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| cos | 1 | cosine | (cos 0) |
| ucos | 1 | like cosine but returns values in the range 0.0 to 1.0 | (ucos (* 0.1 f)) |
| acos | 1 | arc cosine | (acos 1.23) |
| sin | 1 | sine | (sin 1.57) |
| usin | 1 | like sine but returns values in the range 0.0 to 1.0 | (usin -0.5) |
| asin | 1 | arc sine | (asin x) |
| tan | 1 | tangent | (tan 0.5) |
| atan | 2 | arc tanget for y over x | (atan y x) |

### Merge values
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| max | 2 | Maximum value | (max 0 x) |
| min | 2 | Minimum value | (min f 100) |
| mix | 3 | linear blend of x and y<br>Can mix two scalar values<br>or two vectors.<br>Last parameter must be a scalar. | (mix x y 0.75)<br>(mix ras1.rgb ras2.rgb 0.9) |

### Noise functions
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| snoise | 2 | signed simplex noise function at x y | (snoise x y) |
| unoise | 2 | like snoise but returns values approximately in the range 0.0 to 1.0 | ((unoise x y)) |
| turb | 3 | turbulence function at x y, 3rd parameter is for octaves and should be in the range 0 .. 8 | ((turb x y 3)) |

see: https://github.com/ashima/webgl-noise/blob/master/src/noise2D.glsl

### Geometric functions
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| distance | 2 | distance between two points  | (TODO) |
| lenght | 1 | length of a vector | (TODO) |

### Color functions
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| hdiff | 2 | angel between two hue values on the color circle | (hdiff c0.h c1.h) |
