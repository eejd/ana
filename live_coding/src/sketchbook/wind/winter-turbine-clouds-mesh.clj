(ns lv.demo)
(use 'lv.core)

(layout "grid" [mov0 cline1]
               [dist1 ras3])

(movie "6wind/*.MOV")
(render)
(hsv :mov0 [c0 c1 c2 c3 c4 c5 c6 c7]
  ; (sx 1.5)
  ; (sy 1.52)
  (hsv c0.hsv)
  (v (sqr c0.r))
)

(expr :dist1 [cline1]
  (step 30)
  (in 0)
  (mode 1)
  (dist 0.5)
)
; (expr :cline1 [mov0]
;   (count 150)
;   (max 5000)
;   (thres 0.7)
;   (fill 1)
;   (paint 1)
;   (holes 1)
; )
; (expr :mt1 [cline1]
;   (mode 1)
;   (min 10)
;   (trace 100)
;   (qual 0.01)
;   (paint -1)
; )

; (expr :mopr2 [dist1]
;   "db"
; )

(rgb :ras3 [mov0 dist1]
  ; (rgb (* 1.5 mov0.rgb dist1.rgb))
  (rgb (max (* 0.8 mov0.rgb)
          (* 1.5 dist1.rgb)))
)
(render 3)
