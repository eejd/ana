(ns lv.demo)
(use 'lv.core)

(layout "grid"
  [imgs0 ffb1] [mopr2])

; (movie "../../../timelapse/series/20210126-wind-winter/P1140127.MOV")
; (hsv :mov0 [c0 c1 c2 c3 c4 c5 c6 c7]
;   (hsv c0.hsv)
; )

(imagesequ "../../../timelapse/series/20210126-wind-winter/split-P1140127/*.png")
(hsv :imgs0 [c0]
  (k sframe)
  (hsv c0.hsv)
)

(expr :ffb1 [imgs0]
  (reduce 1)
  (mode 3)
  (gauss -1)
  (levels 2)
  (win 3)
  (iter 4)
  (polyn 7)
  (polys 1.2)
  (r 20)
  (step 3)
)
(render 1)

(expr :mopr2 [ffb1]
  "dd"
)
(render 2)
