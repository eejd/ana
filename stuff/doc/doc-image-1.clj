(ns lv.demo)
(use 'lv.core)

(layout "vertical" [img0])
; assume there are up to 4 PNG images in the media folder.
(image "flower-sampler/*.png")

; deform the first channel c0
(hsv :img0 [c0]
  (rot (* f 0.005))
  (sx (+ 2 (snoise (* f 0.01) y)))
  (sy (+ 2 (snoise x (* f 0.01))))
  (hsv c0.hsv)
)
