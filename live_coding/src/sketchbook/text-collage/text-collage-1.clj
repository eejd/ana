(ns lv.demo)
(use 'lv.core)
; inspired by nervousdata
; Making text-collages with Hydra and p5js
;https://nervousdata.com/wiese/txt_phydra.html
(layout "grid" [win0 ras1]
               [ras2 ras3])
(render 3)

; read from a visible X11 window
; 3 == atom editor
(expr :win0 [] (i 3))

; translate x
(hsv :ras1 [win0]
  (tx (snoise (* 0.011 f) 0.1))
  (v (- 1 win0.v))
)

; translate x with different params
(hsv :ras2 [win0]
  (tx (snoise (* 0.013 f) 0.2))
  (mirror)
  (v (- 1 win0.v))
)

; switch between both sources
(hsv :ras3 [ras1 ras2]
  (v (pos? (snoise x y) ras1.v ras2.v))
)
