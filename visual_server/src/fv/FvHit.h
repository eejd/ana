#pragma once

#include "ofMain.h"
#include "ofxOsc.h"

#include "Filter.h"
#include "OscListener.h"
#include "calc/ScalarLisp.h"

class FvHit: public Filter {
public:
    FvHit(ofGLWindowSettings settings, OscListener * oscParams);
    virtual ~FvHit();
    virtual void setup(const std::string id, int numSamples);
    virtual void reload(ofxOscMessage const &msg);
    virtual void update();
    virtual void draw(int li);

    ScalarLisp slMode;
    int draw_mode = 0;

    ScalarLisp slThreshold;
    ScalarLisp slClear;
    ScalarLisp slUpstream;

private:
    ofFloatPixels grabbedPixels;
    std::shared_ptr<Filter> upf = nullptr;
    std::shared_ptr<Filter> maskf = nullptr;
    size_t hit_state_size = 0;
    int * hit_state = nullptr;

    void talk(std::string path, size_t i, float fx, float fy, float fz);
};
