#pragma once

#include "ofMain.h"
#include "Filter.h"
#include "OscListener.h"
#include "calc/ScalarLisp.h"

class ShapeNumber : public Filter {
	public:
		ShapeNumber(ofGLWindowSettings settings, OscListener * oscParams);
		virtual ~ShapeNumber();
        virtual void setup(const std::string id, int numSamples);
        virtual void reload(ofxOscMessage const &msg);
        virtual void draw(int li);

	private:
		ofTrueTypeFont	digit_font;
	    ScalarLisp slClear;
        ScalarLisp slSize;
        ScalarLisp slValue;
        ofRectangle bb;
        std::string format = "%f";
};
