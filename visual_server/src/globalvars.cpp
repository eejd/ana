#include <string>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <unordered_map>

#include "ofMain.h"
#include "ofxOsc.h"

#include "util/stringhelper.h"
#include "Filter.h"
#include "globalvars.h"

class ofApp;

ofApp *apptarget = nullptr;

bool waiting_osc = true;
int num_cl_gpu = 0;

std::string pipe0;

ofSoundPlayer beat;
ofTrueTypeFont font;

size_t filterWidth = 1;
size_t filterHeight = 1;

unsigned int fps = 25;
unsigned int frame = 0;
float beat_pos = 0.0;

float smoothedVol = 0;
float onset = 0;
float minimumThreshold = 0.05;
float decayRate = 0.05;

float u0 = 0;
float u1 = 0;
float u2 = 0;
float u3 = 0;
float u4 = 0;
float u5 = 0;
float u6 = 0;
float u7 = 0;

ScalarLisp slT0;
ScalarLisp slT1;
ScalarLisp slT2;
ScalarLisp slT3;
ScalarLisp slT4;
ScalarLisp slT5;
ScalarLisp slT6;
ScalarLisp slT7;
float t0 = 0;
float t1 = 0;
float t2 = 0;
float t3 = 0;
float t4 = 0;
float t5 = 0;
float t6 = 0;
float t7 = 0;
float dt0 = 0;
float dt1 = 0;
float dt2 = 0;
float dt3 = 0;
float dt4 = 0;
float dt5 = 0;
float dt6 = 0;
float dt7 = 0;

float kp0 = 0, kp1 = 0, kp2 = 0, kp3 = 0, kp4 = 0, kp5 = 0, kp6 = 0, kp7 = 0;
float ks0 = 0, ks1 = 0, ks2 = 0, ks3 = 0, ks4 = 0, ks5 = 0, ks6 = 0, ks7 = 0;
float kb0 = 0, kb1 = 0, kb2 = 0, kb3 = 0, kb4 = 0, kb5 = 0, kb6 = 0, kb7 = 0;

float h0 = 0;
float s0 = 0;
float v0 = 0;

float lx = 0;
float ly = 0;
float rx = 0;
float ry = 0;
float l2 = 0;
float r2 = 0;

#ifdef WITH_openCL
Inspector inspector;
#endif // WITH_openCL
ScalarLisp slInspectChannels;
ScalarLisp slInspectX0;
ScalarLisp slInspectY0;
ScalarLisp slInspectR0;
ScalarLisp slInspectX1;
ScalarLisp slInspectY1;
ScalarLisp slInspectR1;
ScalarLisp slInspectX2;
ScalarLisp slInspectY2;
ScalarLisp slInspectR2;
ScalarLisp slInspectX3;
ScalarLisp slInspectY3;
ScalarLisp slInspectR3;

std::string sound0 = "";
std::array<float, nBandsToGet> fftSmoothed { { 0 } };

std::string image0 = "()"; //TODO
int usedImages = 0;
ofFloatImage images[maxImages];
std::filesystem::path imagePath[maxImages];

std::string imagesequ0;
size_t sizeImageSequ = 0;
GLuint imageSequ_textureID = -1;

std::string movie0 = "()"; //TODO
int usedMovies = 0;
ofVideoPlayer movies[maxMovies];

#ifdef WITH_video_grabber
ofVideoGrabber *videoGrabber = nullptr;
#endif // WITH_video_grabber
size_t video_width = 0;
size_t video_height = 0;

std::vector<ofx::Video::IPVideoGrabber*> ipVideoGrabbers;

std::unordered_map<std::string, std::string> fragDict;
std::unordered_map<std::string, std::shared_ptr<Filter>> filterDict;

std::string dumpState() {
    std::stringstream info;
    info.precision(5);
    info.width(9);
    info.flags(std::ios::fixed);
    info.flags(std::ios::right); //TODO
    info << "\nf     " << frame << "\nb0    " << beat_pos << "\n\nvol   " << smoothedVol << "\nonset " << onset;
    for(size_t i = 0; i < nBandsToGet; ++i) {
        info << "\nfft" << i << "  " << fftSmoothed[i];
    }

    info << "\n\nlx    " << lx << "\nly    " << ly << "\nrx    " << rx << "\nry    " << ry << "\nl2    " << l2 << "\nr2    " << r2;
    info << "\n\nu0    " << u0 << "\nu1    " << u1 << "\nu2    " << u2 << "\nu3    " << u3 << "\nu4    " << u4 << "\nu5    " << u5 << "\nu6    " << u6 << "\nu7    " << u7;
    info << "\n\nt0    " << t0 << "\nt1    " << t1 << "\nt2    " << t2 << "\nt3    " << t3 << "\nt4    " << t4 << "\nt5    " << t5 << "\nt6    " << t6 << "\nt7    " << t7;
    return info.str();
}

std::string dumpNkState() {
    std::stringstream info;
    info.precision(5);
    info.width(9);
    info.flags(std::ios::fixed);
    info.flags(std::ios::right); //TODO
    info <<   "\nkp0    " << kp0 << "\nkp1    " << kp1 << "\nkp2    " << kp2 << "\nkp3    " << kp3 << "\nkp4    " << kp4 << "\nkp5    " << kp5 << "\nkp6    " << kp6 << "\nkp7    " << kp7;
    info << "\n\nks0    " << ks0 << "\nks1    " << ks1 << "\nks2    " << ks2 << "\nks3    " << ks3 << "\nks4    " << ks4 << "\nks5    " << ks5 << "\nks6    " << ks6 << "\nks7    " << ks7;
    info << "\n\nkb0    " << kb0 << "\nkb1    " << kb1 << "\nkb2    " << kb2 << "\nkb3    " << kb3 << "\nkb4    " << kb4 << "\nkb5    " << kb5 << "\nkb6    " << kb6 << "\nkb7    " << kb7;
    info << "\n\ndt0    " << dt0 << "\ndt1    " << dt1 << "\ndt2    " << dt2 << "\ndt3    " << dt3 << "\ndt4    " << dt4 << "\ndt5    " << dt5 << "\ndt6    " << dt6 << "\ndt7    " << dt7;
    return info.str();
}

#define STATE_FILE "visual_server.state"
void saveState() {
    std::ofstream state_file;
    state_file.open(STATE_FILE);
    if(state_file.is_open()) {
        state_file.precision(5);
        state_file << "\n\nlx," << lx << "\nly," << ly << "\nrx," << rx << "\nry," << ry << "\nl2," << l2 << "\nr2," << r2;
        state_file << "\n\nu0," << u0 << "\nu1," << u1 << "\nu2," << u2 << "\nu3," << u3 << "\nu4," << u4 << "\nu5," << u5 << "\nu6," << u6 << "\nu7," << u7;
        state_file << "\n\nt0," << t0 << "\nt1," << t1 << "\nt2," << t2 << "\nt3," << t3 << "\nt4," << t4 << "\nt5," << t5 << "\nt6," << t6 << "\nt7," << t7;
        state_file << "\n\nkp0," << kp0 << "\nkp1," << kp1 << "\nkp2," << kp2 << "\nkp3," << kp3 << "\nkp4," << kp4 << "\nkp5," << kp5 << "\nkp6," << kp6 << "\nkp7," << kp7;
        state_file << "\n\nks0," << ks0 << "\nks1," << ks1 << "\nks2," << ks2 << "\nks3," << ks3 << "\nks4," << ks4 << "\nks5," << ks5 << "\nks6," << ks6 << "\nks7," << ks7;
        state_file << "\n\nkb0," << kb0 << "\nkb1," << kb1 << "\nkb2," << kb2 << "\nkb3," << kb3 << "\nkb4," << kb4 << "\nkb5," << kb5 << "\nkb6," << kb6 << "\nkb7," << kb7;
        state_file.close();
    }
}

void loadState() {
    std::ifstream state_file;
    state_file.open(STATE_FILE);
    if(state_file.is_open()) {
        std::string line;
        while(getline(state_file, line)) {
            std::vector<std::string> parts;
            split(line, ',', parts);
            if(parts.size() == 2) {
                if     (parts[0] == "lx") lx = std::atof(parts[1].c_str());
                else if(parts[0] == "ly") ly = std::atof(parts[1].c_str());
                else if(parts[0] == "rx") rx = std::atof(parts[1].c_str());
                else if(parts[0] == "ry") ry = std::atof(parts[1].c_str());
                else if(parts[0] == "l2") l2 = std::atof(parts[1].c_str());
                else if(parts[0] == "r2") r2 = std::atof(parts[1].c_str());

                else if(parts[0] == "u0") u0 = std::atof(parts[1].c_str());
                else if(parts[0] == "u1") u1 = std::atof(parts[1].c_str());
                else if(parts[0] == "u2") u2 = std::atof(parts[1].c_str());
                else if(parts[0] == "u3") u3 = std::atof(parts[1].c_str());
                else if(parts[0] == "u4") u4 = std::atof(parts[1].c_str());
                else if(parts[0] == "u5") u5 = std::atof(parts[1].c_str());
                else if(parts[0] == "u6") u6 = std::atof(parts[1].c_str());
                else if(parts[0] == "u7") u7 = std::atof(parts[1].c_str());

                else if(parts[0] == "t0") t0 = std::atof(parts[1].c_str());
                else if(parts[0] == "t1") t1 = std::atof(parts[1].c_str());
                else if(parts[0] == "t2") t2 = std::atof(parts[1].c_str());
                else if(parts[0] == "t3") t3 = std::atof(parts[1].c_str());
                else if(parts[0] == "t4") t4 = std::atof(parts[1].c_str());
                else if(parts[0] == "t5") t5 = std::atof(parts[1].c_str());
                else if(parts[0] == "t6") t6 = std::atof(parts[1].c_str());
                else if(parts[0] == "t7") t7 = std::atof(parts[1].c_str());

                else if(parts[0] == "kp0") kp0 = std::atof(parts[1].c_str());
                else if(parts[0] == "kp1") kp1 = std::atof(parts[1].c_str());
                else if(parts[0] == "kp2") kp2 = std::atof(parts[1].c_str());
                else if(parts[0] == "kp3") kp3 = std::atof(parts[1].c_str());
                else if(parts[0] == "kp4") kp4 = std::atof(parts[1].c_str());
                else if(parts[0] == "kp5") kp5 = std::atof(parts[1].c_str());
                else if(parts[0] == "kp6") kp6 = std::atof(parts[1].c_str());
                else if(parts[0] == "kp7") kp7 = std::atof(parts[1].c_str());

                else if(parts[0] == "ks0") ks0 = std::atof(parts[1].c_str());
                else if(parts[0] == "ks1") ks1 = std::atof(parts[1].c_str());
                else if(parts[0] == "ks2") ks2 = std::atof(parts[1].c_str());
                else if(parts[0] == "ks3") ks3 = std::atof(parts[1].c_str());
                else if(parts[0] == "ks4") ks4 = std::atof(parts[1].c_str());
                else if(parts[0] == "ks5") ks5 = std::atof(parts[1].c_str());
                else if(parts[0] == "ks6") ks6 = std::atof(parts[1].c_str());
                else if(parts[0] == "ks7") ks7 = std::atof(parts[1].c_str());

                else if(parts[0] == "kb0") kb0 = std::atof(parts[1].c_str());
                else if(parts[0] == "kb1") kb1 = std::atof(parts[1].c_str());
                else if(parts[0] == "kb2") kb2 = std::atof(parts[1].c_str());
                else if(parts[0] == "kb3") kb3 = std::atof(parts[1].c_str());
                else if(parts[0] == "kb4") kb4 = std::atof(parts[1].c_str());
                else if(parts[0] == "kb5") kb5 = std::atof(parts[1].c_str());
                else if(parts[0] == "kb6") kb6 = std::atof(parts[1].c_str());
                else if(parts[0] == "kb7") kb7 = std::atof(parts[1].c_str());
            }
        }
        state_file.close();
    }
}

static std::string fallbackFrag = STRINGIFY(uniform ivec2 panel;\n in vec2 texCoordVarying;\n out vec4 outputColor;\n void main()\n {\n vec2 _xy = 2.0 * gl_FragCoord.xy / panel - 1.0;\n float r = clamp(ceil(5.0*(1.0-length(_xy)))/5.0, 0.0, 1.0); outputColor = vec4(r, r, r, 1);\n }\n);

std::string frag(std::string &id, std::string *defaultFrag) {
    auto search = fragDict.find(id);
    if(search != fragDict.end()) {
        ofLog(OF_LOG_VERBOSE) << id << " using fragment " << search->second;
        return search->second;
    }
    if(defaultFrag) {
        ofLog(OF_LOG_VERBOSE) << id << " using default fragment " << *defaultFrag;
        return *defaultFrag;
    }
    ofLog(OF_LOG_VERBOSE) << id << " using fall back fragment" << fallbackFrag;
    return fallbackFrag;
}

std::string fallbackVert = STRINGIFY(#version 330\n
        uniform mat4 modelViewProjectionMatrix;\n
        in vec4 position;\n
        in vec2 texcoord;\n
        out vec2 texCoordVarying;\n
        void main()\n
        {\n
            texCoordVarying = vec2(texcoord.x, texcoord.y);\n
            gl_Position = modelViewProjectionMatrix * position;\n
        }\n
);

void sendCurrentOsc(ofxOscSender &talkTo) {
    static ofxOscMessage msg_frame;
    msg_frame.clear();
    msg_frame.setAddress("/current/frame");
    msg_frame.addIntArg(frame);
    talkTo.sendMessage(msg_frame, false);

//    static ofxOscMessage msg_axes;  // TODO do we need this?
//    msg_axes.clear();
//    msg_axes.setAddress("/current/js/axes");
//    msg_axes.addFloatArg(lx);
//    msg_axes.addFloatArg(ly);
//    msg_axes.addFloatArg(rx);
//    msg_axes.addFloatArg(ry);
//    msg_axes.addFloatArg(l2);
//    msg_axes.addFloatArg(r2);
//    talkTo.sendMessage(msg_axes, false);
}

void sendQuitOsc(ofxOscSender &talkTo) {
    static ofxOscMessage msg_frame;
    msg_frame.clear();
    msg_frame.setAddress("/quit");
    talkTo.sendMessage(msg_frame, false);
}

