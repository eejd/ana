# A DSL for analog Not analog
The aNa language is a domain specific language build on top of the Clojure programming language. Parts of your code will be compiled to OpenGL shaders running in the hardware accelerated graphics pipeline of the GPU. Other parts are compiled to byte code and are executed an the CPU.

aNa has a data driven concept. Filters are the basic computation components. These filters are connected by channels. Images are running through these channels and are manipulated by filters. Without loosing image data an algorithm inside a filter can be reprogrammed. Connections between filters can be changed on the fly.

![](./live_coding/src/demo/video-fbo-loop/video-fbo-loop.jpg)

## A shader code example
![](./live_coding/src/demo/video-fbo-loop/demo-video-fbo-loop.png)

* line 1 - 2: As often seen in Clojure: Define your own name space and import some core functionality.

* line 4: Define the screen layout. Four filter nodes are used in this animation.

* line 5: All filters will be drawn to screen. If you want to display only one of this filters add a number. Numbering start with 0. In this example you can use the range from `(render 0)` to `(render 3)`

* line 8: A hue-saturation-value filter definition with the name `vid0` reading its input from channel `c0`.

* line 9: This code sequence will be converted to OpenGL fragment shader code. Looks like Clojure. But only a very limited amount of functions can be used.

  The starting term of the expression `(hsv` means write the result of expression `(* (vec3 (usin (* 0.01 f)) 1 1) c0.hsv)` as hue-saturation-value.

  `c0.hsv` is the term for reading the hue-saturation-value from from channel `c0`.

* line 13 - 17: Another filter running in HSV color space. Hue and saturation of the video stream is copied without modification. But the value / brightness of the image will be inverted.

* line 21 - 25: A filter node for processing RGB values. This filter reads from the upstream `vid0` filter and from its own `ras2` output. The expression `(rgb (mix vid0.rgb ras2.rgb 0.5))` produces an feedback effect because 50% oft the current output is taken from the same filter, but one frame in the past.

  The functions `(kaleid 3)` and `(mirror)` are adding geometrical effects before sampling the input channels.

* Line 28 – 32: Again a feedback effect combined with geometry. New in this filter is that it reads the builtin variables `f` and `x` to influence the effect. `f` is the current frame number. `x` is the x-coordinate of the pixel.

## Structure

### Filter pipeline and feedback loops
There are different types of filters:
- Filters reading from external image sources: Movie files, camera input or image sequences.
- Filters reading from other filters to modify pixel data.
- Some filters analyses image data and send OSC messages to other programs.  

#### Filter types
In the above example we have the source filters `vid0` and `vid1` reading the camera. `ras2` is for image processing. `ras3` is an analyzing filter, producing OSC messages to control the Raspberry Pi SenseHat.

```mermaid
graph LR
id1((Camera)) -- c0 --> :vid0
id1((Camera)) -- c0 --> :vid1
:vid0 -- vid0 --> :ras2
:ras2 -- ras2 --> :ras2

:vid1 -- vid1 --> :ras3
:ras3 -- ras3 --> :ras3
```

### Naming conventions
#### Declaring filter names
`:filter012`
To declare a filter name it must begin with a colon. This is required for Clojure keywords.
Next some letters. These are used by the visualization server to select the relating C++ implementation for this filter. Numbers at the end of a name are used to have unique filter labels.

#### Using filter names
For reading values from a filter use the name of the filter without the starting colon.
* line 21: The parameter list `[vid0 ras2]` enables the read only access to the output from filter `vid0` and `ras2`.
* line 24: The terms `vid0.rgb` and `ras2.rgb` are reading RGB values from input channels.

### Input channels
#### The parameter list
Input channels are listed in the filters parameter list between brackets.
- A filter without input channels needs an empty parameter list `[]`.
- A movie *source* filter for movie files can play up to 8 movie files. Use `[c0]` if its only one movie. Write `[c0 c1 c2 c3]` for 4 input movies. etc...
- A video *source* filter can read only a single WEB cam. A limitation in aNa is that only one camera is supported yet. For camera input use always a parameter list containing one item `[c0]`.
- A *processing* filter reads from other filters in the graph. The names of its input channels are the names of the filters to read from. Except that the leading colon must be removed in the parameter list to reference the input filter. `[vid0 ras3]`

#### Using input channels in expressions
For a HSV filter
- read as 3 component vector: `c0.hsv`
- read components separately: `c0.h`, `c0.s` or `c0.v`.

For a RGB filter
- read as 3 component vector: `c0.rgb`
- read components separately: `c0.r`, `c0.g` or `c0.b`.

Mixing `hsv` and `rgb` terms inside one  filter is not supported. You need to define tow separate filters. But a `rgb` filter can read from a `hsv` filter and vice versa.

## A CPU code example
WARNING: DSL syntax and C++ implementation of these filters are not stable.
TODO

## An OpenCL example
TODO

## Filter types
[List of filter types](./filter_types.md)

[List of functions for GPU shader filters.](./gpu_executed_functions.md)

[List of functions for CPU based filters](./cpu_executed_functions.md)
