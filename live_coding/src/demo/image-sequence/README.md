# Animating an image sequence

This example loads lots of images into a texture array on the graphics card.

Write to `k` to select witch image should be read at run time.

The number of loaded images can be read from `slength`.

> :warning: Loading lots images needs time. Sending `(imagesequ ...)` to the visualization server will freeze the visual server while loading.

![](./image-sequence.jpg)

```mermaid
graph LR
c0>imagesequ] -- c0 --> imgs0
c0>imagesequ] -- c0 --> imgs1
```

```clojure
(layout "grid" [imgs0] [imgs1])
; assume there are lots of JPEG images in the media/1080 folder.
; all imgaes have the same size 1080 x 1080.

; Loading lots images needs time
; This will freeze the visual server while loading.
(imagesequ "1080/*.jpg")

(render)

(hsv :imgs0 [c0]
  (rot (* 0.01 f))
  (k (* slength (unoise (* 0.01 f)
                        (snoise a x))))
  (h c0.h)
  (s c0.s)
  (v (* (- 1 (sqr r)) c0.v))
)
; (render 0)

(hsv :imgs1 [c0]
  (tx 0.25) (ty 0.5)
  (sx 2)    (sy 2)
  (k (* slength
        (unoise (* 0.01 f)
                (snoise (* 0.001 f) r))))
  (h c0.h)
  (s c0.s)
  (v (* (- 1 (sqr r)) c0.v))
)
; (render 1)
```
