(ns lv.demo)
(use 'lv.core)
; video: two lights on a turntable
(layout "grid" [bill0 count1]
               [mopr2 conv3])

(expr :bill0 []
  (clear 0)
  (count (+ 1 (* 31
                 (usin (/ f 62))
                 (usin (/ f 62)))))
  (x (* 2000 (snoise (* 3 (sin (* 0.00073 f)))
                     (* 3 (sin (* 0.00091 f)))
                     (* i 0.01))))
  (y (*  700 (snoise (* 5 (sin (* 0.00051 f)))
                     (* 5 (sin (* 0.00019 f)))
                     (* i 0.04))))
  (size (* 150 (unoise (/ i 1000) 0.1 0.1)))
)

(expr :count1 [bill0]
  (clear (zero? (% f 300) 0 -1))
)

; (hsv :ras2 [count1]
;   ; (tx (* -0.5 (unoise (* 0.01 y) 0.5)))
;   (ty (* 0.5 (unoise (* 0.01 x) 0.5)))
;   (v count1.v)
; )
(rgb :ras2 [conv3 count1]
  ; (rgb (mix conv3.rgb count1.rgb 0.25))
  (rgb (max conv3.rgb count1.rgb))
)
; Embossing Filter
(matrix :conv3 [count1]
  2  0  0
  0 -1  0
  0  0 -1
)
; Edge detection
(matrix :conv3 [count1]
  0 -3  0
 -3 12 -3
  0 -3  0
)
; Morphological Operations
; 4 * dilate
; 2 * erode
; 1 * blur
(patch :mopr2 [conv3]
  "ddb"
)
; (expr :fdiff3 [count1])
(render)
