(ns lv.demo)
(use 'lv.core)

(layout "vertical" [ras0])
(render 0)

; color wheel
(hsv :ras0 []
  ; convert from rotation angle to rainbow colors
  (h (/ a 2 pi))
  ; full saturated
  (s 1)
  ; set to black outside the unite circle.
  (v (pos? (- 1 r) 1 0))
)

; moving colored noise
(rgb :ras0 []
  ; translate in x
  (tx (* 0.001 f))
  ; set red component
  (r (usin (snoise x y)))
  ; translate in x again
  (tx (* 0.005 f))
  ; set green component
  (g (usin (snoise x y)))
  ; translate in x again and again
  (tx (* 0.1 f))
  ; set blue component
  (b (usin (snoise x y)))
)
