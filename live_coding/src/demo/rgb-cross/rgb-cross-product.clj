; internaly calculate on a 64x64 grid
; but width and height of the canvas can have arbitary siz
(ns lv.demo)
(use 'lv.core)

(layout "grid" [mov0 mov1]
               [frag0 ras3])

(movie "4movies/*.mp4")

; movie as input stream
(rgb :mov0 [c0 c1 c2 c3]
 (sy 1.5)
 (rgb c3.rgb)
)

(rgb :mov1 [c0 c1 c2 c3]
 (sy 1.5)
 (rgb c2.rgb)
)

(expr :frag0 [mov0 mov1]
 "frag/test.frag"
 ; (r ks0)
 ; (g ks1)
 ; (b ks2)
)

; kp0    0.023622
; kp1    0.062992
; kp2
(hsv :ras3 [frag0]
  (mirror)
  ; (kaleid 13)
  (hsv (* 10 kp2
          (vec3 (+ kp0 (* 0.01 (usin (* 0.23 f))))
                (+ kp1 (* 0.05 (usin (* 0.31 f))))
                1)
          frag0.hsv))
)
(render 3)
