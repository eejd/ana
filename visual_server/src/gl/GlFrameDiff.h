#pragma once

#include "ofMain.h"
#include "Filter.h"
#include "OscListener.h"

class GlFrameDiff : public Filter{
	public:
		GlFrameDiff(ofGLWindowSettings settings, OscListener * oscParams);
		virtual ~GlFrameDiff();
		virtual void setup(const std::string id, int numSamples);
        virtual void reload(ofxOscMessage const & msg);
		virtual void draw(int li);
		virtual void finish();

	private:
		bool toggle = false;
		ofFbo* p0 = nullptr;
		ofFbo* p1 = nullptr;
};
