(ns lv.demo)
(use 'lv.core)

(layout "grid" [img0 img1]
               [ras2 ras3])
(image "iss/scale_100.png")

(texpr
  ; use nanoKONTROL2 als input
  ; ks0    0.82677
  ; ks1    0.49606
  ; ks2    0.30709
  ; ks3    0.12598
  (t0 (- 1 (* 2 ks0)))
  (t1 (- 1 (* 2 ks1)))
  (t2 (- 1 (* 2 ks2)))
  (t3 (- 1 (* 2 ks3)))
)

(rgb :img0 [c0]
  (ty (* 19.4 (unoise 0.1 (* 0.001 f))))
  (tx -0.08)
  (sx 0.35)
  (rgb c0.rgb)
)

(rgb :img1 [c0]
  (rot (* -0.005 f))
  (ty (* 0.094 19.4))
  (sx 2)
  (rgb c0.rgb)
)

(rgb :ras2 [img0 img1]
  (rgb (mix (sqr img0.rgb) img1.rgb 0.24))
)
(render 2)

(inspect [ras2]
  "color2sound/analyse_quadhead_color.cl"
  (channels 4)
  (x0 (* 1.5 (snoise (* 0.0051 f) 0.1 0.2)))
  (y0 t0)
  (r0 (* 0.25 (unoise f 0.3 0.4)))

  (x1 (* 1.5 (snoise (* 0.0053 f) 0.5 0.6)))
  (y1 t1)
  (r1 (* 0.15 (unoise (* 0.1 f) 0.7 0.8)))

  (x2 (* 1.5 (snoise (* 0.0047 f) 0.5 0.6)))
  (y2 t2)
  (r2 (* 0.15 (unoise (* 0.157 f) 0.9 0.1)))

  (x3 (* 1.5 (snoise (* 0.0043 f) 0.5 0.6)))
  (y3 t3)
  (r3 (* 0.15 (unoise (* 0.071 f) 0.2 0.5)))
)
