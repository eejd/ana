(ns lv.demo)
(use 'lv.core)

(layout "grid" [ras0 ras1]
               [ras2 ras3])
(render)
; The origin of the coordinate system is
; in the center of the square drawing area.
; The x axis and the y axis runs from -1.0 to +1.0

; All rgb and hsv components have a
; value range between 0.0 and 1.0.
; Therefore the absolute value is used.
(rgb :ras0 []
  (r (abs x))
  (g (abs y))
)

; r is the distance from the origin of the coordinate system.
(hsv :ras1 []
  (v r)
)

; a is the rotation angle around the origin of the coordinate system.
(hsv :ras2 []
  (h (/ a 2 3.14159))
  (s 1)
  (v 1)
)

; playing around with axes.
(hsv :ras3 []
  (h (* r a))
  (s (+ 0.82 x y))
  (v (- 1 r))
)
