#pragma once

#include "ofMain.h"
#include "ofxOpenCv.h"
#include "Filter.h"
#include "OscListener.h"
#include "calc/ScalarLisp.h"

class ShapeContour : public Filter {
	public:
		ShapeContour(ofGLWindowSettings settings, OscListener * oscParams);
		virtual ~ShapeContour();
		virtual void setup(const std::string id, int numSamples);
	    virtual void reload(ofxOscMessage const &msg);
		virtual void update();
		virtual void draw(int li);

	private:
        ofxCvContourFinder 	contourFinder;
        ofxCvColorImage		colorImg;
        ofxCvGrayscaleImage	grayImage;
        ofPixels pixels;
        ScalarLisp slThreshold;
        ScalarLisp slConsidered;
        ScalarLisp slMinArea;
        ScalarLisp slMaxArea;
        ScalarLisp slFindHoles;
        ScalarLisp slFill;
        ScalarLisp slNames;
        int mode = 0;
        ScalarLisp slClear;
        ScalarLisp slUpstream;
        int lineColor = -1;
};
