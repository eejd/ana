#include <random>
#include <chrono>

#include "ofMain.h"

#include "calc/FloatArrays.h"

FloatArrays::FloatArrays() {
}

float * FloatArrays::zeros(size_t capacity) {
	float * buffer = new float[capacity];
	std::memset(buffer, 0, capacity * sizeof(float));
	return buffer;
}

float * FloatArrays::constant(float value, size_t capacity) {
	float * buffer = new float[capacity];
	for(size_t bl=0; bl<capacity; ++bl) {
		buffer[bl] = value;
	}
	return buffer;
}

float * FloatArrays::uniform(float min, float max, size_t capacity) {
	float * buffer = new float[capacity];
	std::mt19937 mt(time(nullptr) ^ (unsigned long)buffer);
	std::uniform_real_distribution <float> dist(min, max);
	for(size_t bl=0; bl<capacity; ++bl) {
		buffer[bl] = dist(mt);
	}
	return buffer;
}

float * FloatArrays::gaussian(size_t capacity) {
    float * buffer = new float[capacity];
    std::mt19937 mt(time(nullptr) ^ (unsigned long)buffer);
    std::normal_distribution <> dist{0, 1};
    for(size_t bl=0; bl<capacity; ++bl) {
        buffer[bl] = (float)dist(mt);
    }
    return buffer;
}
