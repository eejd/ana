#pragma once

#include "ofMain.h"
#include "Filter.h"
#include "OscListener.h"
#ifdef WITH_openCL
#include "MSAOpenCL.h"
#endif // WITH_openCL

class GlRasterFilter: public Filter {
public:
    GlRasterFilter(ofGLWindowSettings settings, OscListener * oscParams);
    virtual ~GlRasterFilter();
    virtual void setup(const std::string id, int numSamples);
    virtual void reload(ofxOscMessage const &msg);
    virtual ofTexture& getTexture();
    virtual ofFbo& getFbo();
#ifdef WITH_openCL
    virtual msa::OpenCLImage& getAnalysisImage();
#endif // WITH_openCL
    virtual void draw(int li);
    virtual void finish();

protected:
    void compileShader(std::size_t narg, const ofxOscMessage &msg);

private:
    int ringIndex = 0;
    ofFbo fbo0;
    ofFbo fbo1;
#ifdef WITH_openCL
    msa::OpenCLImage clAnalysisImage0;
    msa::OpenCLImage clAnalysisImage1;
#endif // WITH_openCL
};
