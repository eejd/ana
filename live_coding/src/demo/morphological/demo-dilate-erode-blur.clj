(ns lv.demo)
(use 'lv.core)

(layout "horizontal" [vid0 conv1
                      conv2 conv3
                      mopr4])
(render)

; camera as input stream
(rgb :vid0 [c0]
  (rgb c0.rgb)
)

; Embossing Filter
(matrix :conv1 [vid0]
 -2 -1  0
 -1  1  1
  0  1  2
)

; Edge detection
(matrix :conv2 [conv1]
  0  0  0
 -4  8 -4
  0  0  0
)

; Edge detection
(matrix :conv3 [conv2]
  0 -3  0
 -3 12 -3
  0 -3  0
)

; Morphological Operations
; 4 * dilate
; 2 * erode
; 1 * blur
(patch :mopr4 [conv3]
  "deeb"
)
