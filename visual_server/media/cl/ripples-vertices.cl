// --- random ----------------------------------------------------------
// this OpenCL code is published with Ready 0.7
// see: https://github.com/GollyGang/ready
// This is a simple pseudo random number generator as used for example in shadertoys.
// It is based on scrambling the bits of the coordinates of the currently sampled point.
float rand_float(int n) {
  n = (n << 13) ^ n; 
  return (float)((n * (n*n*15731+789221) + 1376312589) & 0x7fffffff) / (float)(0x7fffffff);
}

// --- noise ----------------------------------------------------------
// see: https://thebookofshaders.com/11/
// see: https://github.com/ashima/webgl-noise/blob/master/src/noise2D.glsl
//
// Description : Array and textureless GLSL 2D simplex noise function.
//      Author : Ian McEwan, Ashima Arts.
//  Maintainer : stegu
//     Lastmod : 20110822 (ijm)
//     License : Copyright (C) 2011 Ashima Arts. All rights reserved.
//               Distributed under the MIT License. See LICENSE file.
//               https://github.com/ashima/webgl-noise
//               https://github.com/stegu/webgl-noise
//

float3 mod289_3(float3 x) {
  return (float3)(x.x - floor(x.x * (1.0 / 289.0)) * 289.0,
                  x.y - floor(x.y * (1.0 / 289.0)) * 289.0,
                  x.z - floor(x.z * (1.0 / 289.0)) * 289.0);
}

float2 mod289_2(float2 x) {
  return (float2)(x.x - floor(x.x * (1.0 / 289.0)) * 289.0,
                  x.y - floor(x.y * (1.0 / 289.0)) * 289.0);
}

float3 permute(float3 x) {
  return mod289_3((float3)(((x.x*34.0)+1.0)*x.x,
                  ((x.y*34.0)+1.0)*x.y,
                  ((x.z*34.0)+1.0)*x.z));
}


float snoise(float2 v)
{
  const float4 C = (float4)(0.211324865405187,  // (3.0-sqrt(3.0))/6.0
                      0.366025403784439,  // 0.5*(sqrt(3.0)-1.0)
                     -0.577350269189626,  // -1.0 + 2.0 * C.x
                      0.024390243902439); // 1.0 / 41.0
// First corner
  float2 i  = floor(v + dot(v, C.yy) );
  float2 x0 = v -   i + dot(i, C.xx);

// Other corners
  float2 i1;
  i1 = (x0.x > x0.y) ? (float2)(1.0, 0.0) : (float2)(0.0, 1.0);
  float4 x12 = x0.xyxy + C.xxzz;
  x12.xy -= i1;

// Permutations
  i = mod289_2(i); // Avoid truncation effects in permutation
  float3 p = permute( permute( i.y + (float3)(0.0, i1.y, 1.0 ))
		+ i.x + (float3)(0.0, i1.x, 1.0 ));

  float3 m = max((float3)(0.5) - (float3)(dot(x0,x0), dot(x12.xy,x12.xy), dot(x12.zw,x12.zw)), (float3)(0.0));
  m = m*m ;
  m = m*m ;

// Gradients: 41 points uniformly over a line, mapped onto a diamond.
// The ring size 17*17 = 289 is close to a multiple of 41 (41*7 = 287)

  float3 dummy;
  float3 x = (float3)(2.0) * fract(p * C.www, &dummy) - (float3)(1.0);
  float3 h = fabs(x) - (float3)(0.5);
  float3 ox = floor(x + (float3)(0.5));
  float3 a0 = x - ox;

// Normalise gradients implicitly by scaling m
// Approximation of: m *= inversesqrt( a0*a0 + h*h );
  m *= (float3)(1.79284291400159) - (float3)(0.85373472095314) * ( a0*a0 + h*h );

// Compute final noise value at P
  float3 g;
  g.x  = a0.x  * x0.x  + h.x  * x0.y;
  g.yz = a0.yz * x12.xz + h.yz * x12.yw;
  return 130.0 * dot(m, g);
}


__kernel void map_r2(
__global float * result,
const int lines,
const int rows,
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7,
const float dt0,
const float dt1,
const float dt2,
const float dt3,
const float dt4,
const float dt5,
const float dt6,
const float dt7
) 
{
    const int vi = get_global_id(0);
    
    #define xspace    p0
    #define yspace    p1
    #define xnoise    p2
    #define ynoise    p3
    #define xscatter  p4
    #define yscatter  p5
    #define density   p6
    #define amplitude p7
    
    if (rand_float(vi-1) < density) {
        float2 pos = (float2)(((vi % rows)),
                              ((vi / rows)));
        pos -= (float2)(rows / 2, lines / 2);
        pos.y += amplitude * snoise((pos) * (float2)(xnoise, ynoise) + (float2)(dt0, dt1));
        pos *= (float2)(xspace, yspace);
        pos += (float2)((rand_float(vi) - 0.5) * xscatter,
                        (rand_float(vi+1) - 0.5) * yscatter);
/* x */    result[vi*8+0] = 5.0 * pos.x;
/* y */    result[vi*8+1] = 5.0 * pos.y;
/* z */    result[vi*8+2] = 0.0;
/* w */    result[vi*8+3] = 2.0;
#undef HSV_MODE
#ifdef HSV_MODE
/* h */    result[vi*8+4] = vi / (float)(lines * rows);
/* s */    result[vi*8+5] = 0.5;
/* v */    result[vi*8+6] = 1.0;
#else
/* r */    result[vi*8+4] = 1;
/* g */    result[vi*8+5] = 1;
/* b */    result[vi*8+6] = 1;
#endif
/* a */    result[vi*8+7] = 0.8;
    }
    else {
/* x */    result[vi*8+0] = 0.0;
/* y */    result[vi*8+1] = 0.0;
/* z */    result[vi*8+2] = 0.0;
/* w */    result[vi*8+3] = -1.0; // -1 = do not draw
/* h */    result[vi*8+4] = 0.0;
/* s */    result[vi*8+5] = 0.0;
/* v */    result[vi*8+6] = 0.0;
/* a */    result[vi*8+7] = 0.0;
    }
}
