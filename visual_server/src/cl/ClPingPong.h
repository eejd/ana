#pragma once
#include "../ana_features.h"
#ifdef WITH_openCL

#include "ofxOsc.h"
#include "MSAOpenCL.h"
#include "calc/FloatArrays.h"
#include "calc/ScalarLisp.h"
#include "cl/ClFilter.h"
#include "MSAOpenCL.h"

class ClPingPong : public ClFilter {
	public:
		ClPingPong(ofGLWindowSettings settings, OscListener * oscParams);
		virtual ~ClPingPong();
		virtual void setup(const std::string id, int numSamples);
		virtual void reload(ofxOscMessage const &msg);
		virtual void update();

    protected:
		virtual size_t getKernelWidth();
		virtual size_t getKernelHeight();

        static const size_t half_as_much_buffers = 8;
        static const size_t num_buffers = 2*half_as_much_buffers;
		FloatArrays floats;
        msa::OpenCLBuffer clBuffers[num_buffers]; // ping pong buffers with the same size

        int  pingPongIndex = 0;
        ofFbo clOutFbo;

        ScalarLisp slLoops;
        ScalarLisp slP0;
        ScalarLisp slP1;
        ScalarLisp slP2;
        ScalarLisp slP3;
        ScalarLisp slP4;
        ScalarLisp slP5;
        ScalarLisp slP6;
        ScalarLisp slP7;

        virtual void runKernel(msa::OpenCLKernelPtr kernel);
};
#endif // WITH_openCL
