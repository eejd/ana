import os 
from pathlib import Path

montage = "montage"
sli = 0

if not Path(montage).exists():
    os.mkdir(montage)
if not Path(montage).is_dir():
    print(montage, "is not an folder")
    exit(1)

def symadd(p):
    global sli
    if not p.endswith('/'):
        print(p, "path is missing trailing /")
        exit(1)
    fns = os.listdir(os.environ['HOME'] + '/' + p)
    fns.sort()
    for x in fns:
        if x.endswith(".png"): 
            fsymlnk = montage + "/ana_" + str(sli).zfill(6) + ".png";
            forig = os.environ['HOME'] + '/' + p + x;
            print(fsymlnk, "->", forig)
            os.symlink(forig, fsymlnk)
            sli += 1

def credits(f, i=75):
    global sli
    forig = os.environ['HOME'] + '/' + f;
    for x in range(i):
        fsymlnk = montage + "/ana_" + str(sli).zfill(6) + ".png";
        print(fsymlnk, "->", forig)
        os.symlink(forig, fsymlnk)
        sli += 1

symadd ("ana_exp/winter-wind/exp_2021-01-26-21-10-54-703A/")
symadd ("ana_exp/winter-wind/exp_2021-01-26-16-42-45-038A/")
symadd ("ana_exp/winter-wind/exp_2021-01-26-17-22-53-022A/")
symadd ("ana_exp/winter-wind/exp_2021-01-26-16-55-48-908A/")
symadd ("ana_exp/winter-wind/exp_2021-01-26-20-49-09-365A/")
