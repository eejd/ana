#include "../ana_features.h"
#ifdef WITH_openCL
#include "ofMain.h"
#include "globalvars.h"
#include "MSAOpenCL.h"
#include "Configuration.h"
#include "calc/MsgLisp.h"
#include "util/stringhelper.h"
#include "Inspector.h"

Inspector::Inspector() {
    memset(zero, 0, resultlen * sizeof(int32_t));
    memset(inParams, 0, resultlen * sizeof(int32_t));
    memset(results, 0, resultlen * sizeof(int32_t));
}

Inspector::~Inspector() {
}

void Inspector::setup() {
    filterWidth = asInt(FILTER_WIDTH);
    filterHeight = asInt(FILTER_HEIGHT);
    inspect_this_filter = nullptr;
    clInParamsBuffer.initBuffer(resultlen * sizeof(int32_t), CL_MEM_READ_WRITE);
    clResultBuffer.initBuffer(resultlen * sizeof(int32_t), CL_MEM_READ_WRITE);
}

void Inspector::setInputFilter(std::string descr) {
    inspect_this_filter = nullptr;
    std::string tid = trim(descr, '[', ']');
    if(filterDict.find(tid) != filterDict.end()) {
        inspect_this_filter = filterDict[tid];
    }
}

bool Inspector::reloadClAnalysisProg(const bool force) {
    if(clAnalysisProgName.empty()) {
        hasClAnalysisProg = false;
        return false;
    }

    bool need = force;
    if(!need) {
        long sourceModTime = std::filesystem::last_write_time(pathClAnalysisProg);
        need = sourceModTime != clAnalysisProgModTime;
    }

    if(need) {
        hasClAnalysisProg = false;
        clAnalysisProgModTime = std::filesystem::last_write_time(pathClAnalysisProg);
        ofLog(OF_LOG_NOTICE) << "compiling " << clAnalysisProgName;
        msa::OpenCLProgramPtr pcl = openCL.loadProgramFromFile(clAnalysisProgName);
        if(pcl) {
            analysis_kernel = pcl->loadKernel("image_analysis");
            std::string kn = analysis_kernel->getName();
            if(kn.empty())
                ofLog(OF_LOG_NOTICE) << "analysis  kernel: N/A";
            else {
                hasClAnalysisProg = true;
                ofLog(OF_LOG_NOTICE) << "analysis  kernel: " << kn;
            }
        }
    }
    return need;
}

void Inspector::setParams(size_t offset, double ix, double iy, double ir) {
    inParams[offset+0] = ofMap(ix, -1.0, 1.0, 0, filterWidth - 1, true);
    inParams[offset+1] = ofMap(iy, -1.0, 1.0, 0, filterHeight - 1, true);
    inParams[offset+2] = ofMap(ir, 0.0, 1.0, 0, 0.25 * (filterWidth + filterHeight), true);
    if(inParams[offset+2] < 1)
        inParams[offset+2] = 1;

    if(inParams[offset+0] <= inParams[offset+2])
        inParams[offset+0] = inParams[offset+2] + 1;
    else if(inParams[offset+0] + inParams[offset+2] >= (int) (filterWidth))
        inParams[offset+0] = filterWidth - inParams[offset+2] - 1;

    if(inParams[offset+1] <= inParams[offset+2])
        inParams[offset+1] = inParams[offset+2] + 1;
    else if(inParams[offset+1] + inParams[offset+2] >= (int) (filterHeight))
        inParams[offset+1] = filterHeight - inParams[offset+2] - 1;
}

ofFloatColor Inspector::toRGB(size_t offset) {
    ofFloatColor rgb(
            ofClamp(((float) (results[offset+0]) / (float) (results[offset+3])) / 32768.0f, 0.0f, 1.0f),
            ofClamp(((float) (results[offset+1]) / (float) (results[offset+3])) / 32768.0f, 0.0f, 1.0f),
            ofClamp(((float) (results[offset+2]) / (float) (results[offset+3])) / 32768.0f, 0.0f, 1.0f));
    return rgb;
}

void Inspector::osc2channels() {
    if(hasClAnalysisProg && inspect_this_filter != nullptr) {
        channels = std::round(slInspectChannels.rpn.execute0());

        double i0x = slInspectX0.rpn.execute0();
        double i0y = slInspectY0.rpn.execute0();
        double i0r = slInspectR0.rpn.execute0();
        setParams(0, i0x, i0y, i0r);

        double i1x = slInspectX1.rpn.execute0();
        double i1y = slInspectY1.rpn.execute0();
        double i1r = slInspectR1.rpn.execute0();
        setParams(3, i1x, i1y, i1r);

        double i2x = slInspectX2.rpn.execute0();
        double i2y = slInspectY2.rpn.execute0();
        double i2r = slInspectR2.rpn.execute0();
        setParams(6, i2x, i2y, i2r);

        double i3x = slInspectX3.rpn.execute0();
        double i3y = slInspectY3.rpn.execute0();
        double i3r = slInspectR3.rpn.execute0();
        setParams(9, i3x, i3y, i3r);

        msa::OpenCLImage & clAnalysisImage = inspect_this_filter->getAnalysisImage();
        glFinish();
        clInParamsBuffer.write(inParams, 0, resultlen * sizeof(int32_t));
        clResultBuffer.write(zero, 0, resultlen * sizeof(int32_t));
        analysis_kernel->setArg(0, clAnalysisImage);
        analysis_kernel->setArg(1, clInParamsBuffer);
        analysis_kernel->setArg(2, clResultBuffer);
        analysis_kernel->run2D(filterWidth, filterHeight);

        clResultBuffer.read(results, 0, resultlen * sizeof(int32_t));
        openCL.finish();

        ofxOscMessage s;
        s.setAddress("/f_hsb4c");

        float hue = 0;
        float saturation = 0;
        float brightness = 0;
        toRGB(0).getHsb(hue, saturation, brightness);;
        s.addIntArg(channels);     // channels == 2 -> data from a dual head sampler
        s.addFloatArg((float)i0x); // x0
        s.addFloatArg((float)i0y); // y0
        s.addFloatArg((float)i0r); // r0
        s.addFloatArg(hue);
        s.addFloatArg(saturation);
        s.addFloatArg(brightness);

        if(channels > 1) {
            toRGB(4).getHsb(hue, saturation, brightness);;
            s.addFloatArg((float)i1x); // x1
            s.addFloatArg((float)i1y); // y1
            s.addFloatArg((float)i1r); // r1
            s.addFloatArg(hue);
            s.addFloatArg(saturation);
            s.addFloatArg(brightness);

            if(channels > 2) {
                toRGB(8).getHsb(hue, saturation, brightness);;
                s.addFloatArg((float)i2x); // x2
                s.addFloatArg((float)i2y); // y2
                s.addFloatArg((float)i2r); // r2
                s.addFloatArg(hue);
                s.addFloatArg(saturation);
                s.addFloatArg(brightness);

                if(channels > 3) {
                    toRGB(12).getHsb(hue, saturation, brightness);;
                    s.addFloatArg((float)i3x); // x3
                    s.addFloatArg((float)i3y); // y3
                    s.addFloatArg((float)i3r); // r3
                    s.addFloatArg(hue);
                    s.addFloatArg(saturation);
                    s.addFloatArg(brightness);
                }
            }
        }

        apptarget->talkToSClang.sendMessage(s, false);
    }
}

void Inspector::drawRectMarker(const int xx, const int yy, const int rl) {
    const int rl2 = 2 * rl;
    ofSetColor(ofColor::red);
    ofDrawRectangle(xx - rl - 1, yy - rl - 1, rl2 + 2, rl2 + 2);
    ofSetColor(ofColor::green);
    ofDrawRectangle(xx - rl + 1, yy - rl + 1, rl2 - 2, rl2 - 2);
    ofSetColor(ofColor::black);
    ofDrawRectangle(xx - rl, yy - rl, rl2, rl2);
}

void Inspector::draw2Markers() {
    if(hasClAnalysisProg && inspect_this_filter != nullptr) {
        ofFbo &fbo = inspect_this_filter->getFbo();
        fbo.begin();
        ofNoFill();
        drawRectMarker(inParams[0], inParams[1], inParams[2]);
        if(channels > 1) {
            drawRectMarker(inParams[3], inParams[4], inParams[5]);
            if(channels > 2) {
                drawRectMarker(inParams[6], inParams[7], inParams[8]);
                if(channels > 3) {
                    drawRectMarker(inParams[9], inParams[10], inParams[11]);
                }
            }
        }
        fbo.end();
    }
}

#endif // WITH_openCL
