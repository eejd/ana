#pragma once

#include "ofMain.h"
#include "Filter.h"
#include "OscListener.h"

class GlVideoFilter : public Filter {
	public:
		GlVideoFilter(ofGLWindowSettings settings, OscListener * oscParams);
		virtual ~GlVideoFilter();
		virtual void setup(const std::string id, int numSamples);
        virtual void reload(ofxOscMessage const &msg);
		virtual void draw(int li);
		virtual void toMermaid(std::stringstream & info);

private:
		int rotate = 0;
		ofPixels dst90;
		ofImage img90;
};
