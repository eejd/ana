#include <filesystem>
#include <algorithm>
#include <chrono>

#include "ofMain.h"

#include "stringhelper.h"
#include "globalvars.h"
#include "OscListener.h"
#include "Filter.h"
#include "gl/GlImageFilter.h"

GlImageFilter::GlImageFilter(ofGLWindowSettings settings, OscListener * oscParams) : Filter(settings, oscParams){
}

GlImageFilter::~GlImageFilter(){
}

void GlImageFilter::reload(ofxOscMessage const &msg) {
    auto narg = msg.getNumArgs();
    if(narg > 2) {
        Filter::compileShader(msg.getArgAsString(0), msg.getArgAsString(3));
    }
}

void GlImageFilter::draw(int li){
	fbo.begin();
	ofClear(0, 0, 0, 255);

	shader.begin();
	sendUniforms(shader, li);

	for(auto ix = 0; ix < usedImages; ++ix){
		shader.setUniformTexture("tex" + to_string(ix), images[ix], ix);
	}

	ofDrawRectangle(0, 0, filterWidth, filterHeight);

	shader.end();
	fbo.end();
}

void GlImageFilter::toMermaid(std::stringstream & info) {
    for(auto ix = 0; ix < usedImages; ++ix){
        info << "c" << ix << ">" << imagePath[ix].filename().string() << "]" << " -- c" << ix << " --> " << id << "\n"; //TODO quote [ ]
    }
    Filter::toMermaid(info);
}
