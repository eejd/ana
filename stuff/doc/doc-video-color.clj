(ns lv.demo)
(use 'lv.core)

; assume a camera is connected
; and can be read by openFrameworks
; on linux gstreamer reads from /dev/video0
(layout "vertical" [vid0])

; shift hue. f = current frame number
(hsv :vid0 [c0]
  (h (+ (* x (sin f)) c0.h))
  (s c0.s)
  (v c0.v)
)

; Kaleidoscope from camera input
(rgb :vid0 [c0]
  (kaleid 7)
  (rot (* 0.005 f))
  (rgb c0.rgb)
)
