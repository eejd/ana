(ns lv.demo)
(use 'lv.core)

(layout "grid" [mov0 mopr2 clpar1]
               [cline1 cline2 ras3])
(render)

(movie "../../../Desktop/fc37d73ff764cc7b.mp4")
(hsv :mov0 [c0]
  (sx 2.8) (sy 3) (ty kp7)
  (hsv (* (vec3 1 1.2 1) c0.hsv))
)
; kp0    0.30469
; kp1    0.15625
; kp2    0.023622
; kp3    0.11024
; kp4    0
; kp5    0.36719
; kp6    0.29688
; kp7    0.3125
;
; ks0    0.67969
; ks1    0.26562
; ks2    0.30469
; ks3    0.71875
; ks4    0.57812
; ks5    0.71094
; ks6    0.3125
; ks7    0.49219

; find contours
(expr :cline1 [mov0]
  (count 50)
  (max 5000)
  (thres (* 0.5 kp0))
  (fill -1)
  (paint -1)
  (holes 1)
)

; find contours
(expr :cline2 [mov0]
  (count 50)
  (max 5000)
  (thres kp1)
  (fill -1)
  (paint -1)
  (holes 1)
)

(rgb :ras3 [cline1 cline2]
  (rgb (+ cline1.rgb cline2.rgb))
)


(expr :mopr2 [ras3] "ddddbbbb")

(expr :clpar1 [mopr2]
  "particle-model/particle-model-6.cl"
  (paint 1)
  (p0 (* 1 ks0)) ; acc
  (p1 (* 0.1 ks1)) ; vel
  (p2 (* 0.01 ks2)) ; pos
  (p3 (* 0.001 ks3))
  (p4 (* 4 ks4))   ; disipate
  (p7 (+ 0.001 (* 0.1 ks7)))
)
