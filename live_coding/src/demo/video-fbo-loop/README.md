# A feedback loop running in the frame buffer.

TODO

![](./video-fbo-loop.jpg)

```mermaid
graph LR
camera((camera)) -- c0 --> :vid0
camera((camera)) -- c0 --> :vid1
:vid0 -- vid0 --> :ras2
:ras2 -- ras2 --> :ras2
:vid1 -- vid1 --> :ras3
:ras3 -- ras3 --> :ras3
```

```clojure
(ns lv.demo)
(use 'lv.core)

(layout "grid" [vid0 vid1] [ras2 ras3])
(render)

; read from a WEB camera and shift color hue.
(hsv :vid0 [c0]
  (hsv (* (vec3 (usin (* 0.01 f)) 1 1) c0.hsv))
)

; read from a WEB camera and invert brightness.
(hsv :vid1 [c0]
  (h c0.h)
  (s c0.s)
  (v (- 1 c0.v))
)

; mix the upstream video with the current frame buffer content.
; reads from buffer ras2 and writes the result back to buffer ras2.
(rgb :ras2 [vid0 ras2]
  (kaleid 3)
  (mirror)
  (rgb (mix vid0.rgb ras2.rgb 0.5))
)

; mix upsteam video with the own frame buffer content
(rgb :ras3 [vid1 ras3]
  (pixelate-y 100)
  (rgb (mix vid1.rgb ras3.rgb 0.95))
)
```
