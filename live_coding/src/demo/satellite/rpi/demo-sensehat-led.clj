(ns lv.demo)
(use 'lv.core)

(layout "grid" [pihat0] [])
(render 0)

; mark the top left LED
(expr :pihat0 []
  (mode 2) ; draw mode 2:
           ; calculate the color for every single LEDs
  (h 0.5)
  (s 1)
  (v (zero? x (zero? y 1 0) 0))
)

; set all LED to the same color
(expr :pihat0
  (mode 1) ; draw mode 1:
           ; use the same color for all LEDs
  (h (+ 0.5 (* 0.5 (sin (* 0.051 f)))))
  (s (+ 0.75 (* 0.25 (sin (* 0.053 f)))))
  (v (unoise (* 0.025 f) 0.5 0.5))
)

; set every LED to the different color
; rainbow color animation
(expr :pihat0 []
  (mode 2) ; draw mode 2:
           ; calculate the color for every single LEDs
  (h (/ (% (+ f i) 64) 64))
  (s 1)
  (v 1)
)
