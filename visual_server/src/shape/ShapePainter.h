#pragma once

#include "ofMain.h"
#include "ofxOsc.h"
#include "Filter.h"
#include "OscListener.h"
#include "calc/ScalarLisp.h"

class ShapePainter: public Filter {
public:
    ShapePainter(ofGLWindowSettings settings, OscListener * oscParams, int draw);
    virtual ~ShapePainter();
    virtual void setup(const std::string id, int numSamples);
    virtual void reload(ofxOscMessage const &msg);
    virtual void draw(int li);

    int draw_mode = 0;

    ScalarLisp slCount;
    ScalarLisp slX;
    ScalarLisp slY;
    ScalarLisp slSize;
    ScalarLisp slRotate;
    ScalarLisp slH;
    ScalarLisp slS;
    ScalarLisp slV;
    ScalarLisp slResolution;
    ScalarLisp slFill;
    ScalarLisp slClear;
};
