#include <limits>
#include <math.h>
#include <string>

#include "ofMain.h"

#include "ana_features.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"

#define RPN_DUMP() std::cout << rpn.dump()

ScalarLisp::ScalarLisp() {
    map_opr = {
        { "pos?", opr_desc(tOprIfPositive, oprIfPositive, 3) },
        { "zero?", opr_desc(tOprIfZero, oprIfZero, 3) },
        //TODO add even?
        //TODO add if?
        //TOD0 add >= > = <= <
        //TOD0 add && ||
        { "+", opr_desc(tOprAdd, oprAdd, -1) },
        { "-", opr_desc(tOprSub, oprSub, -1) },
        { "*", opr_desc(tOprMult, oprMult, -1) },
        { "/", opr_desc(tOprDiv, oprDiv, -1) },
        { "unoise", opr_desc(tOprUNoise, oprUNoise, 3) },
        { "snoise", opr_desc(tOprSNoise, oprSNoise, 3) },
        { "rand", opr_desc(tOprURandom, oprURandom, 0) },
        { "turb", opr_desc(tOprTurbulence, oprTurbulence, 4) }, //TODO diff
        { "pow", opr_desc(tOprPow, oprPow, 2) },
        //TODO add exp
        { "sqrt", opr_desc(tOprSqrt, oprSqrt, 1) },
        { "sqr", opr_desc(tOprSqr, oprSqr, 1) },
        //TODO add inversesqrt
        { "tan", opr_desc(tOprTan, oprTan, 1) },
        { "sin", opr_desc(tOprSin, oprSin, 1) },
        { "usin", opr_desc(tOprUSin, oprUSin, 1) },
        { "asin", opr_desc(tOprASin, oprASin, 1) },
        { "cos", opr_desc(tOprCos, oprCos, 1) },
        { "ucos", opr_desc(tOprUCos, oprUCos, 1) },
        { "acos", opr_desc(tOprACos, oprACos, 1) },
        { "atan", opr_desc(tOprAtan2, oprAtan2, 2) },
        { "log", opr_desc(tOprLog, oprLog, 1) },
        { "log2", opr_desc(tOprLog2, oprLog2, 1) },
        { "mix", opr_desc(tOprMix, oprMix, 3) },
        { "step", opr_desc(tOprStep, oprStep, 2) },
        { "puls", opr_desc(tOprPuls,    oprPuls, 3) },
        { "clamp", opr_desc(tOprClamp, oprClamp, 3) },
        { "max", opr_desc(tOprMax, oprMax, -1) },
        { "min", opr_desc(tOprMin, oprMin, -1) },
        { "abs", opr_desc(tOprAbs, oprAbs, 1) },
        { "sign", opr_desc(tOprSign, oprSign, 1) },
        { "trunc", opr_desc(tOprTrunc, oprTrunc, 1) },
        { "round", opr_desc(tOprRound, oprRound, 1) },
        { "smooth", opr_desc(tOprSmoothstep, oprSmoothstep, 3) }, //TODO diff
        { "box", opr_desc(tOprBoxstep, oprBoxstep, 3) },
        { "%", opr_desc(tOprMod, oprMod, 2) },
        { "floor", opr_desc(tOprFloor, oprFloor, 1) },
        { "ceil", opr_desc(tOprCeil, oprCeil, 1) },
        { "gamma", opr_desc(tOprGamma, oprGamma, 2) },
        { "gain", opr_desc(tOprGain, oprGain, 2) },
        { "bias", opr_desc(tOprBias,    oprBias, 2) },
        { "distance", opr_desc(tOprEuclideDistance,  oprEuclideanDistance, 4) },
        { "mdist", opr_desc(tOprAbsoluteDistance, oprAbsoluteDistance, 4) },
    };
}

ScalarLisp::~ScalarLisp() {
}

bool ScalarLisp::isNumber(char test) {
    switch(test) {
    case '.':
    case '0':
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
        return true;
    }
    return false;
}

void ScalarLisp::skip() {
    while(ix < len && isspace(inp[ix])) {
        ++ix;
    }
}

void ScalarLisp::collect() {
    skip();
    tok = tEOF;
    tokBuffer.clear();
    if(ix < len) {
        switch(inp[ix]) {
        case '(':
            tok = tOpen;
            tokBuffer += inp[ix];
            ++ix;
            break;
        case ')':
            tok = tClose;
            tokBuffer += inp[ix];
            ++ix;
            break;
        default:
            while(ix < len && !(isspace(inp[ix]) || inp[ix] == '(' || inp[ix] == ')')) {
                tokBuffer += inp[ix];
                ++ix;
            }
            if(tokBuffer == "(") {
                tok = tOpen;
            } else if(tokBuffer == ")") {
                tok = tClose;
            } else if(tokBuffer.size() >= 2 && tokBuffer[0] == '-' && isNumber(tokBuffer[1])) {
                tok = tNumber;
            } else if(tokBuffer.size() >= 1 && isNumber(tokBuffer[0])) {
                tok = tNumber;
            } else if(map_opr.find(tokBuffer) != map_opr.end()) {
                tok = map_opr[tokBuffer].token;
            } else {
                tok = tVar;
            }
        }
    }
}

struct odef_t {
    int o;
    double d;
    int i;
    bool fix;
};

void ScalarLisp::appendVar(std::string &t) {
    rpn.isConst = false;
    if(t == "i") {
        rpn.appendOpr(oprPushI);
    } else if(t == "j") {
        rpn.appendOpr(oprPushJ);
    } else if(t == "k") {
        rpn.appendOpr(oprPushK);
    } else if(t == "f") {
        rpn.appendOpr(oprPushF);
    } else if(t == "x") {
        rpn.appendOpr(oprPushX);
    } else if(t == "y") {
        rpn.appendOpr(oprPushY);
    } else if(t == "z") {
        rpn.appendOpr(oprPushZ);
    } else if(t == "w") {
        rpn.appendOpr(oprPushW);
    } else if(t == "lx") {
        rpn.appendOpr(oprPushLx);
    } else if(t == "ly") {
        rpn.appendOpr(oprPushLy);
    } else if(t == "rx") {
        rpn.appendOpr(oprPushRx);
    } else if(t == "ry") {
        rpn.appendOpr(oprPushRy);
    } else if(t == "l2") {
        rpn.appendOpr(oprPushL2);
    } else if(t == "r2") {
        rpn.appendOpr(oprPushR2);
    } else if(t == "vol") {
        rpn.appendOpr(oprPushVol);
    } else if(t == "onset") {
        rpn.appendOpr(oprPushOnset);

    } else if(t == "u0") {
        rpn.appendOpr(oprPushU0);
    } else if(t == "u1") {
        rpn.appendOpr(oprPushU1);
    } else if(t == "u2") {
        rpn.appendOpr(oprPushU2);
    } else if(t == "u3") {
        rpn.appendOpr(oprPushU3);
    } else if(t == "u4") {
        rpn.appendOpr(oprPushU4);
    } else if(t == "u5") {
        rpn.appendOpr(oprPushU5);
    } else if(t == "u6") {
        rpn.appendOpr(oprPushU6);
    } else if(t == "u7") {
        rpn.appendOpr(oprPushU7);

    } else if(t == "t0") {
        rpn.appendOpr(oprPushT0);
    } else if(t == "t1") {
        rpn.appendOpr(oprPushT1);
    } else if(t == "t2") {
        rpn.appendOpr(oprPushT2);
    } else if(t == "t3") {
        rpn.appendOpr(oprPushT3);
    } else if(t == "t4") {
        rpn.appendOpr(oprPushT4);
    } else if(t == "t5") {
        rpn.appendOpr(oprPushT5);
    } else if(t == "t6") {
        rpn.appendOpr(oprPushT6);
    } else if(t == "t7") {
        rpn.appendOpr(oprPushT7);

    } else if(t == "kp0") {
        rpn.appendOpr(oprPushKP0);
    } else if(t == "kp1") {
        rpn.appendOpr(oprPushKP1);
    } else if(t == "kp2") {
        rpn.appendOpr(oprPushKP2);
    } else if(t == "kp3") {
        rpn.appendOpr(oprPushKP3);
    } else if(t == "kp4") {
        rpn.appendOpr(oprPushKP4);
    } else if(t == "kp5") {
        rpn.appendOpr(oprPushKP5);
    } else if(t == "kp6") {
        rpn.appendOpr(oprPushKP6);
    } else if(t == "kp7") {
        rpn.appendOpr(oprPushKP7);

    } else if(t == "ks0") {
        rpn.appendOpr(oprPushKS0);
    } else if(t == "ks1") {
        rpn.appendOpr(oprPushKS1);
    } else if(t == "ks2") {
        rpn.appendOpr(oprPushKS2);
    } else if(t == "ks3") {
        rpn.appendOpr(oprPushKS3);
    } else if(t == "ks4") {
        rpn.appendOpr(oprPushKS4);
    } else if(t == "ks5") {
        rpn.appendOpr(oprPushKS5);
    } else if(t == "ks6") {
        rpn.appendOpr(oprPushKS6);
    } else if(t == "ks7") {
        rpn.appendOpr(oprPushKS7);

    } else if(t == "kb0") {
        rpn.appendOpr(oprPushKB0);
    } else if(t == "kb1") {
        rpn.appendOpr(oprPushKB1);
    } else if(t == "kb2") {
        rpn.appendOpr(oprPushKB2);
    } else if(t == "kb3") {
        rpn.appendOpr(oprPushKB3);
    } else if(t == "kb4") {
        rpn.appendOpr(oprPushKB4);
    } else if(t == "kb5") {
        rpn.appendOpr(oprPushKB5);
    } else if(t == "kb6") {
        rpn.appendOpr(oprPushKB6);
    } else if(t == "kb7") {
        rpn.appendOpr(oprPushKB7);

    } else if(t == "h") {
        rpn.appendOpr(oprPushH0);
    } else if(t == "s") {
        rpn.appendOpr(oprPushS0);
    } else if(t == "v") {
        rpn.appendOpr(oprPushV0);
    } else if(t == "r") {
        rpn.appendOpr(oprPushX);
    } else if(t == "g") {
        rpn.appendOpr(oprPushY);
    } else if(t == "b") {
        rpn.appendOpr(oprPushZ);
    } else if(t == "a") {
        rpn.appendOpr(oprPushW);
    } else {
        if(inp) ofLog(OF_LOG_ERROR) << inp;
        ofLog(OF_LOG_ERROR) << "'" << t << "' is unknown";
        err = 4;
        rpn.appendOpr(oprPushConst);
    }
//    RPN_DUMP();
}

void ScalarLisp::compileTerm() {
    switch(tok) {
    case tOprIfZero:
    case tOprIfPositive: {
        int argslen = 0;
        int label_end = genLabel();
        int label_else = genLabel();
        int opcode = tok == tOprIfZero ? oprIfZero : oprIfPositive;
        collect();
        if(tok == tOpen) {
            collect();
            compileTerm();
            ++argslen;
        } else if(tok == tNumber) {
            rpn.appendOpr(oprPushConst, std::stod(tokBuffer), 0);
            collect();
            ++argslen;
        } else if(tok == tVar) {
            appendVar(tokBuffer);
            collect();
            ++argslen;
        }
        rpn.appendOpr(opcode, 0.0, label_else);

        if(tok == tOpen) {
            collect();
            compileTerm();
            ++argslen;
        } else if(tok == tNumber) {
            rpn.appendOpr(oprPushConst, std::stod(tokBuffer), 0);
            collect();
            ++argslen;
        } else if(tok == tVar) {
            appendVar(tokBuffer);
            collect();
            ++argslen;
        }
        rpn.appendOpr(oprJmp, 0.0, label_end);

        rpn.appendOpr(oprLabel, 0.0, label_else);
        if(tok == tOpen) {
            collect();
            compileTerm();
            ++argslen;
        } else if(tok == tNumber) {
            rpn.appendOpr(oprPushConst, std::stod(tokBuffer), 0);
            collect();
            ++argslen;
        } else if(tok == tVar) {
            appendVar(tokBuffer);
            collect();
            ++argslen;
        }

        if(argslen != 3) {
            if(inp) ofLog(OF_LOG_ERROR) << inp;
            ofLog(OF_LOG_ERROR) << "conditional operator needs 3 arguments. " << argslen << " given";
            err = 3;
        }
        if(tok != tClose) {
            if(inp) ofLog(OF_LOG_ERROR) << inp;
            ofLog(OF_LOG_ERROR) << "wrong number of arguments in conditional operator. needs 3 arguments.";
            err = 3;
        }
        rpn.appendOpr(oprLabel, 0.0, label_end);
        collect();
        break;
    }
    default: {
        if(tok == tOprURandom)
            rpn.isConst = false;
        int argslen = 0;
        int opcode = oprStop;
        int found = 0;
        int al = 0;
        for(auto t : map_opr) {
            if(t.second.token == tok) {
                ++found;
                opcode = t.second.rpnoper;
                al = t.second.rewind;
            }
        }
        if(found != 1) {
            if(inp) ofLog(OF_LOG_ERROR) << inp;
            ofLog(OF_LOG_ERROR) << "token not handled " << tok;
            err = 5;
            return;
        }

        collect();
        while(tok == tOpen || tok == tNumber || tok == tVar || tok == tClose) {
            if(tok == tOpen) {
                collect();
                compileTerm();
                ++argslen;
            } else if(tok == tNumber) {
                rpn.appendOpr(oprPushConst, std::stod(tokBuffer), 0);
                collect();
                ++argslen;
            } else if(tok == tVar) {
                appendVar(tokBuffer);
                collect();
                ++argslen;
            } else if(tok == tClose) {
                if(al != -1 && argslen != al) {
                    if(inp) ofLog(OF_LOG_ERROR) << inp;
                    ofLog(OF_LOG_ERROR) << "wrong number arguments in operator. needs " << al << " arguments.";
                    err = 2;
                }
                if(opcode == oprSub && argslen == 1)
                    rpn.appendOpr(oprSub1, 0.0, 1);
                else if(opcode == oprDiv && argslen == 1)
                    rpn.appendOpr(oprDiv1, 0.0, 1);
                else
                    rpn.appendOpr(opcode, 0.0, argslen);
                collect();
                break;
            }
        }
    }
    }
//  RPN_DUMP();
}

void ScalarLisp::genCode() {
    rpn.appendOpr(oprStop);
    map_label.clear();
    int ip = 0;
    while(ip < rpn.tapeLength) {
        if(rpn.codeTape[ip] == oprLabel)
            map_label[rpn.iOper[ip]] = ip;

        ++ip;
    }
    ip = 0;
    while(ip < rpn.tapeLength) {
        switch(rpn.codeTape[ip]) {
        case oprJmp: {
            int jmp_target = 1 + map_label[rpn.iOper[ip]];
            rpn.iOper[ip] = jmp_target;
            if(rpn.codeTape[jmp_target] == oprStop) {
                rpn.codeTape[ip] = oprStop;
            }
        }
            break;
        case oprIfZero:
        case oprIfPositive:
            rpn.iOper[ip] = 1 + map_label[rpn.iOper[ip]];
            break;
        }
        ++ip;
    }
//  RPN_DUMP();
}

void ScalarLisp::calculateIfConst() {
    if(rpn.tapeLength < 1) {
        rpn.const_result = 0;
        rpn.isConst = true;
    }
    else if(rpn.isConst) {
        rpn.isConst = false;
        rpn.const_result = rpn.execute4d3i(0.0, 0.0, 0.0, 0.0, 0, 0, 0);
        rpn.isConst = true;
    }
}

void ScalarLisp::countParenthesis() {
    int open_count = 0;
    int close_count = 0;
    for(int cx = 0; cx < len; ++cx) {
        switch(inp[cx]) {
        case '(':
            ++open_count;
            break;
        case ')':
            ++close_count;
            break;
        }
    }
    if(open_count > close_count) {
        if(inp)
            ofLog(OF_LOG_ERROR) << inp;

        ofLog(OF_LOG_ERROR) << "missing closing parenthesis ). " << open_count - close_count;
        err = 1;
    } else if(open_count < close_count) {
        if(inp)
            ofLog(OF_LOG_ERROR) << inp;

        ofLog(OF_LOG_ERROR) << "too many closing parenthesis ). " << close_count - open_count;
        err = 1;
    }
}

int ScalarLisp::compile(std::string src) {
    label_counter = 0;
    err = 0;
    ix = 0;
    rpn.clear();
    len = src.size();

    inp = src.c_str();
    countParenthesis();
    if(err != 0)
        return err;

    if(err == 0 || len > 0) {
        collect();
        if(tok == tOpen) {
            collect();
            compileTerm();
        } else if(tok == tNumber) {
            rpn.appendOpr(oprPushConst, std::stod(tokBuffer), 0);
        } else if(tok == tVar) {
            appendVar(tokBuffer);
        }
    }

    if(len == 0 || err) {
        // an empty program is OK and returns 0.0
        // error found -> program should return 0.0
        rpn.clear();
        rpn.isConst = true;
        rpn.const_result = 0.0;
        return err;
    }

    genCode();
    calculateIfConst();

    return err;
}
