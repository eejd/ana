#include "ofMain.h"

#include "globalvars.h"
#include "Configuration.h"
#include "OscListener.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispMng.h"
#include "gl/GlScreenFilter.h"

GlScreenFilter::GlScreenFilter(ofGLWindowSettings settings, OscListener *oscParams) :
        Filter(settings, oscParams) {
//    scrbuf = std::malloc(1920 * 1080 * 3); // TODO use real screen size
}

GlScreenFilter::~GlScreenFilter() {
    clearPixelBuffer();
}

void GlScreenFilter::setup(const std::string id, int numSamples) {
    Filter::setup(id, numSamples);
}

void GlScreenFilter::clearPixelBuffer() {
    if(hasPixelBuffer) {
//        ofLogNotice() << "clearPixelBuffer " << pixelBuffer.getWidth() << "x" << pixelBuffer.getHeight();
        pixelBuffer.clear();
        w_old = h_old = -1;
        pixelImage.clear();
        hasPixelBuffer = false;
    }
}

void GlScreenFilter::allocatePixelBuffer() {
    if(iw > 0 && ih > 0) {// TODO limit to real screen size
        pixelBuffer.allocate(iw, ih, OF_PIXELS_RGB);
        w_old = iw;
        h_old = ih;
        hasPixelBuffer = true;
//        ofLogNotice() << "allocatePixelBuffer " << iw << "x" << ih;
    }
}

void GlScreenFilter::reload(ofxOscMessage const &msg) {
//    auto narg = msg.getNumArgs();
//    if(narg > 2) {
//        Filter::compileShader(msg.getArgAsString(0), msg.getArgAsString(3));
//    }
    ScalarLispMng mng;
    mng.compile(slX, "x", "960", msg); // TODO use real screen size
    mng.compile(slY, "y", "270", msg);
    mng.compile(slW, "w", "540", msg);
    mng.compile(slH, "h", "540", msg);
    mng.compile(slClear, "clear", "1", msg);
}

void GlScreenFilter::update() {
    ix = std::max(0, std::min((int)filterWidth  - 1, (int)slX.rpn.execute0()));
    iy = std::max(0, std::min((int)filterHeight - 1, (int)slY.rpn.execute0()));
    iw = (((int)slW.rpn.execute0() / 8) + 1) * 8;
    ih = (((int)slH.rpn.execute0() / 8) + 1) * 8;
    iw = std::max(0, std::min((int)filterWidth,      iw));
    ih = std::max(0, std::min((int)filterHeight,     ih));
    if(ix + iw > (int)filterWidth) {
        iw = (int)filterWidth - ix;
    }
    if(iy + ih > (int)filterHeight) {
        ih = (int)filterHeight - iy;
    }

    if(iw <= 0 || ih <= 0) {
        clearPixelBuffer();
    }
    else if(iw != w_old || ih != h_old) {
        clearPixelBuffer();
        allocatePixelBuffer();
    }

//    glReadPixels(ix, iy, iw, ih, GL_RGB, GL_UNSIGNED_BYTE, scrbuf);
    if(hasPixelBuffer) {
        glReadBuffer(GL_FRONT);
//        ofLogNotice() << "update " << iw << "x" << ih << "  " << pixelBuffer.getTotalBytes() << "  " << pixelBuffer.getWidth() << "x" << pixelBuffer.getHeight();
        glReadnPixels(ix, iy, iw, ih, GL_RGB, GL_UNSIGNED_BYTE, pixelBuffer.getTotalBytes(), pixelBuffer.getData());
        pixelImage.setFromPixels(pixelBuffer);
        pixelImage.mirror(true, false);
    }
}

void GlScreenFilter::draw(int li) {
    fbo.begin();
    if(slClear.rpn.execute0() >= 0.0)
        ofClear(0, 0, 0, 255);

    if(hasPixelBuffer) {
        ofPushMatrix();
        ofScale(filterWidth / iw, filterHeight / ih);
        pixelImage.draw(0, 0);
        ofPopMatrix();
    }

    fbo.end();
}

void GlScreenFilter::toMermaid(std::stringstream & info) {
    info << "screen((screen))" << " -- c0 --> " << id << "\n";
    Filter::toMermaid(info);
}
