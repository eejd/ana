(ns lv.demo)
(use 'lv.core)

(layout "grid" [mov0 clipp1])
(movie "bkgrd/*.MOV")

(hsv :mov0 [c0 c1]
  (sy 2.5)
  (ty 0.1)
  (hsv c0.hsv)
)

(expr :clipp1 [mov0]
  "cl/image_diffusion.cl"
  (paint 1)
  (p0 (* kp0 0.1))
  (p1 (* ks7 (- ks0 0.5) 8))
  (p2 (* ks7 (- ks1 0.5) 8))
  (p3 (* ks7 (- ks2 0.5) 8))
  (p4 (* ks7 (- ks3 0.5) 8))
)
(render 1)
