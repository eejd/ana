from osc4py3.as_eventloop import *
from osc4py3 import oscbuildparse
from sense_hat import SenseHat

osc_startup()
# 57220 is the port where the Analog Not Analog server
# is listening for incomming OSC commands
osc_udp_client("192.168.2.4", 57220, "analog_port")

sense = SenseHat()
sense.set_imu_config(False,True,True) # gyro and accel only mag turned off

while True:
    orientation = sense.get_orientation_radians ()
    pitch = orientation['pitch']
    roll = orientation['roll']
    yaw = orientation['yaw']

    acceleration = sense.get_accelerometer_raw()
    x = acceleration ['x']
    y = acceleration ['y']
    z = acceleration ['z']

    msg = oscbuildparse.OSCMessage("/ana_uniform", ",ffffff", [pitch,yaw,roll,x,y,z])
    osc_send(msg, "analog_port")
    osc_process()

    print("pitch={0}, roll={1}, yaw={2}".format(pitch,yaw,roll))
    print("x={0}, y={2}, z{2}".format(x,y,z))