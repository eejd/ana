import os 
from pathlib import Path

montage = "montage"
sli = 0

if not Path(montage).exists():
    os.mkdir(montage)
if not Path(montage).is_dir():
    print(montage, "is not an folder")
    exit(1)

def symadd(p):
    global sli
    if not p.endswith('/'):
        print(p, "path is missing trailing /")
        exit(1)
    fns = os.listdir(os.environ['HOME'] + '/' + p)
    fns.sort()
    for x in fns:
        if x.endswith(".png"): 
            fsymlnk = montage + "/ana_" + str(sli).zfill(6) + ".png";
            forig = os.environ['HOME'] + '/' + p + x;
            print(fsymlnk, "->", forig)
            os.symlink(forig, fsymlnk)
            sli += 1

def credits(f, i=75):
    global sli
    forig = os.environ['HOME'] + '/' + f;
    for x in range(i):
        fsymlnk = montage + "/ana_" + str(sli).zfill(6) + ".png";
        print(fsymlnk, "->", forig)
        os.symlink(forig, fsymlnk)
        sli += 1

symadd ("ana_exp/CalArts-Dance-2019/arm/exp_2021-01-16-06-21-41-794A/")
symadd ("ana_exp/CalArts-Dance-2019/side/exp_2021-01-15-09-35-09-458/")
symadd ("ana_exp/CalArts-Dance-2019/arm/exp_2021-01-17-18-27-52-917A/")
symadd ("ana_exp/CalArts-Dance-2019/blue-distflow/exp_2021-01-23-13-05-39-617/")
symadd ("ana_exp/CalArts-Dance-2019/red/exp_2021-01-17-13-33-48-197/")
symadd ("ana_exp/CalArts-Dance-2019/blue/exp_2021-01-14-07-36-17-443A/")
symadd ("ana_exp/CalArts-Dance-2019/red/exp_2021-01-17-12-15-22-316/")
symadd ("ana_exp/CalArts-Dance-2019/red-distflow/exp_2021-01-23-13-21-30-978/")
credits("ana/live_coding/src/sketchbook/CalArts-Dance-2019/credits.png")

