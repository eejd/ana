(ns lv.demo)
(use 'lv.core)

(layout "grid" [imgs0 mt1]
               [ras2 uspl3])

(imagesequ "CalArts-Dance-2019/side/*.png")
(render)
(hsv :imgs0 [c0]
  (k (* slength (usin (* 0.02 sframe))))
  ; (k sframe)
  (hsv (* (vec3 1 1 1) c0.hsv))
)

(expr :mt1 [imgs0]
  (mode 0)
  (count 100)
  (trace 32)
  (qual 0.02)
  (paint -1)
)

(expr :uspl3 [mt1]
  "CalArts-Dance-2019/processVoronoiPoints.cl"
)

(rgb ras2[imgs0 mt1 uspl3]
  (rgb (* (* 4 (+ imgs0.rgb mt1.rgb))
          (* 0.5 uspl3.rgb)))
)
(render 2)
