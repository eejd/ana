#include "../ana_features.h"
#ifdef WITH_openCL
#include <random>

#include "ofMain.h"
#include "MSAOpenCL.h"
#include "ofApp.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispMng.h"
#include "cl/ClPingPong.h"
#include "globalvars.h"

ClPingPong::ClPingPong(ofGLWindowSettings settings, OscListener * oscParams) :
ClFilter(settings, oscParams) {
}

ClPingPong::~ClPingPong() {
}

size_t ClPingPong::getKernelWidth() {
    return filterWidth;
}

size_t ClPingPong::getKernelHeight() {
    return filterHeight;
}

void ClPingPong::setup(const std::string id, int numSamples) {
    Filter::setup(id, numSamples);

    pingPongIndex = 0;
    clOutFbo.allocate(getKernelWidth(), getKernelHeight(), GL_RGBA32F_ARB);
    clOutFbo.begin();
    ofClear(0, 0, 0, 255);
    clOutFbo.end();
    clOutImage.initFromTexture(clOutFbo.getTexture());

	const size_t buflen = getKernelWidth() * getKernelHeight();
	for(auto bx = 0u; bx < half_as_much_buffers; ++bx) {
        clBuffers[2 * bx].initBuffer(buflen * sizeof(float), CL_MEM_READ_WRITE);
		clBuffers[2 * bx + 1].initBuffer(buflen * sizeof(float), CL_MEM_READ_WRITE);
	}
}

void ClPingPong::reload(ofxOscMessage const &msg) {
    ClFilter::reload(msg);

    ScalarLispMng mng;
    mng.compile(slLoops, "loops", "1", msg);
    mng.compile(slP0, "p0", "1.0", msg);
    mng.compile(slP1, "p1", "1.0", msg);
    mng.compile(slP2, "p2", "1.0", msg);
    mng.compile(slP3, "p3", "1.0", msg);
    mng.compile(slP4, "p4", "1.0", msg);
    mng.compile(slP5, "p5", "1.0", msg);
    mng.compile(slP6, "p6", "1.0", msg);
    mng.compile(slP7, "p7", "1.0", msg);

    pingPongIndex = 0;
    const size_t buflen = getKernelWidth() * getKernelHeight();
    for(auto bx = 0u; bx < half_as_much_buffers; ++bx) {
        float * chemical = floats.uniform(0.0f, 1.0f, buflen);
        clBuffers[2 * bx].write(chemical, 0, buflen * sizeof(float));
        // second CL buffer clBuffers[2 * bx + 1], content needs not to be initialized
        delete[] chemical;
    }
    runKernel(setup_kernel);
}

void ClPingPong::runKernel(msa::OpenCLKernelPtr kernel) {
    if(!fatalClProg) {
        for (auto bx = 0u; bx < half_as_much_buffers; ++bx) {
            kernel->setArg(2 * bx, clBuffers[2 * bx + pingPongIndex]);
            kernel->setArg(2 * bx + 1, clBuffers[2 * bx + (1 - pingPongIndex)]);
        }
        kernel->setArg(2 * half_as_much_buffers,     (float)slP0.rpn.execute0());
        kernel->setArg(2 * half_as_much_buffers + 1, (float)slP1.rpn.execute0());
        kernel->setArg(2 * half_as_much_buffers + 2, (float)slP2.rpn.execute0());
        kernel->setArg(2 * half_as_much_buffers + 3, (float)slP3.rpn.execute0());
        kernel->setArg(2 * half_as_much_buffers + 4, (float)slP4.rpn.execute0());
        kernel->setArg(2 * half_as_much_buffers + 5, (float)slP5.rpn.execute0());
        kernel->setArg(2 * half_as_much_buffers + 6, (float)slP6.rpn.execute0());
        kernel->setArg(2 * half_as_much_buffers + 7, (float)slP7.rpn.execute0());
        kernel->setArg(2 * half_as_much_buffers + 8, clOutImage);
        kernel->run2D(getKernelWidth(), getKernelHeight());
        pingPongIndex = 1 - pingPongIndex;
    }
}

void ClPingPong::update(){
    if(clProgName.empty())
        return;

    if(reloadClProg(false)) {
        runKernel(setup_kernel);
    }
    size_t loops = std::max(1.0, slLoops.rpn.execute0());
    for(size_t li=0; li<loops; ++li) {
        runKernel(update_kernel);
    }
}
#endif // WITH_openCL
