#include "ofMain.h"
#include "ofxOsc.h"

#include "Configuration.h"
#include "globalvars.h"
#include "OscListener.h"
#include "Filter.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispMng.h"
#include "fv/FvPl.h"

FvPl::FvPl(ofGLWindowSettings settings, OscListener * oscParams) :
        Filter(settings, oscParams) {
}

FvPl::~FvPl() {
}

void FvPl::setup(const std::string id, int numSamples) {
    Filter::setup(id, numSamples);
}

void FvPl::reload(ofxOscMessage const &msg) {
    ScalarLispMng mng;
    mng.compile(slMode, "mode", "0", msg);
    mng.compile(slClear, "clear", "1", msg);
    mng.compile(slCount, "count", "0", msg);
    mng.compile(slX, "x", "0", msg);
    mng.compile(slY, "y", "0", msg);
    mng.compile(slZ, "z", "0", msg);
    mng.compile(slW, "w", "0", msg);
    mng.compile(slH, "h", "0", msg);
    mng.compile(slS, "s", "0", msg);
    mng.compile(slV, "v", "1", msg);
    mng.compile(slA, "a", "1", msg);
}

void FvPl::update() {
    draw_mode = slMode.rpn.execute0();
    size_t count = std::round(std::max(0.0, slCount.rpn.execute0()));
    if(count > vertices_capacity) {
        clearVertices();
        reallocateVertices(count);
    }
    for(size_t i=0; i<count; ++i) {
        *FVx(i) = slX.rpn.execute3d1i(0.0, 0.0, 0.0, i);
        *FVy(i) = slY.rpn.execute3d1i(*FVx(i), 0.0, 0.0, i);
        *FVz(i) = slZ.rpn.execute3d1i(*FVx(i), *FVy(i), 0.0, i);
        *FVw(i) = slW.rpn.execute3d1i(*FVx(i), *FVy(i), *FVz(i), i);
        *FVh(i) = slH.rpn.execute3d1i(*FVx(i), *FVy(i), *FVz(i), i);
        *FVs(i) = slS.rpn.execute3d1i(*FVx(i), *FVy(i), *FVz(i), i);
        *FVv(i) = slV.rpn.execute3d1i(*FVx(i), *FVy(i), *FVz(i), i);
        *FVa(i) = slA.rpn.execute3d1i(*FVx(i), *FVy(i), *FVz(i), i);
    }
    vertices_size = count;
}

void FvPl::draw(int li) {
    fbo.begin();
    if(slClear.rpn.execute0() >= 0.0)
        ofClear(0, 0, 0, 255);

    if(vertices_size > 0) {
        ofPushStyle();
        ofPushMatrix();
        ofTranslate(filterWidth / 2, filterHeight / 2, 0);
        switch(draw_mode) {
        case 1: {
            size_t s2 = vertices_size / 2;
            size_t i2 = 0;
            for(size_t i = 0; i < s2; ++i) {
                ofSetColor(ofFloatColor::fromHsb(*FVh(i2), *FVs(i2), *FVv(i2), *FVa(i2)));
                ofDrawLine(*FVx(i2), *FVy(i2), *FVx(i2+1), *FVy(i2+1));
                i2 += 2;
            }
            break;
        }
        case 2: {
            ofNoFill();
            ofBeginShape();
            ofSetColor(ofFloatColor::fromHsb(*FVh(0), *FVs(0), *FVv(0), *FVa(0)));
            for(size_t i = 0; i < vertices_size; ++i) {
                ofVertex(*FVx(i), *FVy(i), *FVz(i));
            }
            ofEndShape(true);
            break;
        }
        default: {
            ofFill();
            for(size_t i = 0; i < vertices_size; ++i) {
                ofSetColor(ofFloatColor::fromHsb(*FVh(i), *FVs(i), *FVv(i), *FVa(i)));
                ofDrawCircle(*FVx(i), *FVy(i), *FVz(i), *FVw(i));
            }
        }
        }
        ofPopMatrix();
        ofPopStyle();
    }

    fbo.end();
}
