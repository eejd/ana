(ns lv.demo)
(use 'lv.core)

(layout "grid"
  [imgs0 mt1] [mopr2 ras3])

(imagesequ "../../../timelapse/series/20201231-1-plattenspieler/split/*.png")
(render)

(hsv :imgs0 [c0]
  (k sframe)
  (hsv c0.hsv)
)
(expr :mt1 [imgs0]
  (mode (* (usin (* 0.011 f)) 2.1))
  (clear (- (sin (* 0.023 f)) 0.5))
  (trace 75)
  (paint -1)
  (count 90)
  (qual 0.01)
  (min 50)
)

(expr :mopr2 [mt1]
  "dddbb"
)
(rgb :ras3 [imgs0 mopr2]
  (rgb (* imgs0.b
          (* 2.8 imgs0.rgb)
          (* 1.0 mopr2.rgb)))
)
(render 3)
