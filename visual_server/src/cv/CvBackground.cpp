#include "ofMain.h"
#include "ofxCv.h"

#include "globalvars.h"
#include "util/p3circle.h"
#include "OscListener.h"
#include "Filter.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispMng.h"
#include "cv/CvBackground.h"

CvBackground::CvBackground(ofGLWindowSettings settings, OscListener *oscParams) :
        Filter(settings, oscParams) {
}

CvBackground::~CvBackground() {
}

void CvBackground::setup(const std::string id, int numSamples) {
    Filter::setup(id, numSamples);
}

void CvBackground::reload(ofxOscMessage const &msg) {
    auto narg = msg.getNumArgs();
    if(narg > 1) {
        Filter::setInputTexture(msg.getArgAsString(1));
    }
    ScalarLispMng mng;
    mng.compile(slThreshold, "thres", "0.5", msg);
    mng.compile(slLearningTime, "learn", "2", msg);
    mng.compile(slReset, "reset", "-1", msg);
    background.reset();
}

void CvBackground::update() {
    hasData = false;

    if(slReset.rpn.execute0() > 0)
        background.reset();

    if(upstream_ids.size() >= 1) {
        auto in_id = upstream_ids[0];
        if(filterDict.find(in_id) != filterDict.end()) {
            auto f = filterDict[in_id];
            if(f) {
                f->getTexture().readToPixels(grabbedPixels);
                frame_captured = ofxCv::toCv(grabbedPixels);
                video_width = grabbedPixels.getWidth();
                video_height = grabbedPixels.getHeight();
                threshold = slThreshold.rpn.execute0();
                learningTime = slLearningTime.rpn.execute0();
                background.setDifferenceMode(ofxCv::RunningBackground::DifferenceMode::BRIGHTER);
                background.setThresholdValue(255*threshold);
                background.setLearningTime(learningTime);
                background.update(frame_captured, thresholded);
                thresholded.update();
                hasData = true;
            }
        }
    }
}

void CvBackground::draw(int li) {
    if(!hasData)
        return;

    fbo.begin();
    ofClear(0, 0, 0, 255);

    if(thresholded.isAllocated()) {
        thresholded.draw(0, 0);
    }

    fbo.end();
}
