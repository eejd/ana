# Some 3x3 convolution filters

Yet only 3x3 convolution filters are supported.

![](./convolution.jpg)

```mermaid
graph LR
c2>P1080896-1080x1080.png] -- c2 --> img0
img0 -- img0 --> conv1
img0 -- img0 --> conv2
conv2 -- conv2 --> conv3
```

```clojure
(ns lv.demo)
(use 'lv.core)

(layout "grid" [img0 conv1] [conv2 conv3])
(image "flower-sampler/*.png")
(render)

; movie as input stream
(rgb :img0 [c0 c1 c2 c3]
  (rgb c2.rgb)
)

; Identity
(matrix :conv1 [img0]
  0  0  0
  0  1  0
  0  0  0
)

; sharpen
(matrix :conv1 [img0]
  0 -1  0
 -1  5 -1
  0 -1  0
)

; Embossing Filter
(matrix :conv1 [img0]
  2  0  0
  0 -1  0
  0  0 -1
)

; Box blur
(matrix :conv1 [img0]
  0.111  0.111  0.111
  0.111  0.111  0.111
  0.111  0.111  0.111
)

; Gaussian blur
(matrix :conv1 [v0]
  0.0625  0.125  0.0625
  0.125   0.25   0.125
  0.0625  0.125  0.0625
)

; Gaussian blur
; 5x5 kernels are NOT implemented
; (matrix :conv1 [img0]
;   0.003765 0.015019 0.023792 0.015019 0.003765
;   0.015019 0.059912 0.094907 0.059912 0.015019
;   0.023792 0.094907 0.150342 0.094907 0.023792
;   0.015019 0.059912 0.094907 0.059912 0.015019
;   0.003765 0.015019 0.023792 0.015019 0.003765
; )

; X half Sobel Filter
(matrix :conv1 [img0]
 -1  0  1
 -2  0  2
 -1  0  1
)

; Y half Sobel Filter
; Embossing Filter
(matrix :conv1 [img0]
 -2 -1  0
 -1  1  1
  0  1  2
)

(matrix :conv2 [img0]
 -1 -2 -1
  0  0  0
  1  2  1
)

; Edge detection
(matrix :conv2 [img0]
  0 -1  0
 -1  4 -1
  0 -1  0
)

; Edge detection
(matrix :conv2 [img0]
  -1 -1 -1
  -1  8 -1
  -1 -1 -1
)

; Edge detection
(matrix :conv2 [img0]
  0  0  0
 -4  8 -4
  0  0  0
)

; Edge detection
(matrix :conv3 [conv2]
  0 -3  0
 -3 12 -3
  0 -3  0
)
```
