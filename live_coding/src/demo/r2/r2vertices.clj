(ns lv.demo)
(use 'lv.core)

(layout "grid" [clfv0 ras1])

(expr :clfv0 []
  "r2/r2vertices.cl"
  (lines 50)
  (rows  50)
  (p0 (+ ks0 0.1))
  (p1 (+ ks1 0.1))
  (p2 (+ ks2 0.1))
  (p3 (+ ks3 0.1))
  (p4 (+ ks4 0.1))
  (p7 (+ ks7 3))
)

(expr :clfv0 [img0]
  "r2/r2vertices.cl"
  (count (* 50 50))
  (p0 (* 10 (sin (* 0.0011 f))))
  (p1 (+ 0.1 (sin (* 0.0013 f))))
  (p2 (+ 0.1 (sin (* 0.0017 f))))
  (p3 (+ 0.1 (sin (* 0.0019 f))))
  (p4 (* ks4 3))
  (p7 (* ks7 3))
)

(rgb :ras1 [clfv0 ras1]
  (rgb (mix clfv0.rgb ras1.rgb 0.95))
)

(render 1)
