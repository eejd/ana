(ns lv.demo)
(use 'lv.core)

(layout "horizontal" [pl0 ras1 cline2 mopr3 ras4])
(render)

(expr :pl0 []
  ; (count (* 250 u5))
  (count (* (min f 250) u5))
  (x (* 900 (sin (* 0.031 (+ 1 i) f))))
  (y (pos? (cos (* 0.011  (+ 1 i) f)) 40 -40))
  (w (* u4 50))
  (h (ucos (* 0.01 (+ 1 i) f)))
  (s u0)
  (v u1)
  (a u3)
)
(render 0)

(rgb :ras1 [pl0 ras1]
  (rgb (* (+ 1 u6) (mix pl0.rgb ras1.rgb u7)))
)
(render 1)

(expr :cline2 [ras1]
  (count 25)
  (max 500)
  (thres 0.25)
  (paint 1)
  (fill -1)
  (holes 1)
)
(render 2)

(expr :mopr3 [cline2]
  "dd"
)
(render 3)

(hsv :ras4 [mopr3]
  (hsv (pos? (- f x)
            (* (vec3 1 1 (* 0.01 (- 500 f))) mopr3.hsv)
            mopr3.hsv))
)
(render 4)
