#pragma once

#include "ofMain.h"
#include "Filter.h"
#include "OscListener.h"
#include "lsys/LsysMachine.h"

class ShapeLsys : public Filter {
	public:
		ShapeLsys(ofGLWindowSettings settings, OscListener * oscParams);
		virtual ~ShapeLsys();
		virtual void setup(const std::string id, int numSamples);
	    virtual void reload(ofxOscMessage const &msg);
		virtual void update();
		virtual void draw(int li);

	private:
	    LsysMachine l1;
};
