(ns lv.demo)
(use 'lv.core)

(layout "grid" [bill0 ffb1 mopr1] [ras2 ras3])
(render)

(expr :bill0 []
  ; (roty f)
  (count 38)
  (x (+ -960 (* 50 i)))
  (y (* ;(- (+ i -19))
        400
        (snoise (* 0.05 f) i 0.1)
        ))
  (size 120)
)
(render 0)

(expr :ffb1 [ras2]
  (reduce 2)
  (paint 1)
  (clear -1)
  (step 12)
  (mode 1)
)
(render 1)
(expr :mopr1 [ffb1]
  "ddb"
)

(hsv :ras2 [bill0]
  (mirror)
  (tx (sin (* 0.01 f)))
  (h bill0.h)
  (s bill0.s)
  (v (* 1.2 bill0.v))
)
(render 2)

(rgb :ras3 [mopr1 ras2 ras3]
  (rgb (+ (* 0.5 mopr1.rgb)
          (* 0.5 ras2.rgb)
          (* 0.7 ras3.rgb)
            ))
)
(render 3)
