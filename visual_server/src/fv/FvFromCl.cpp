#include "../ana_features.h"
#ifdef WITH_openCL
#include "ofMain.h"
#include "ofxOsc.h"

#include "Configuration.h"
#include "globalvars.h"
#include "OscListener.h"
#include "Filter.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispMng.h"
#include "fv/FvFromCl.h"

FvFromCl::FvFromCl(ofGLWindowSettings settings, OscListener * oscParams) :
ClFilter(settings, oscParams) {
    memset(results, 0, max_resultlen * 8 * sizeof(float));
}

FvFromCl::~FvFromCl() {
}

void FvFromCl::setup(const std::string id, int numSamples) {
    Filter::setup(id, numSamples);
    clResultBuffer.initBuffer(max_resultlen * 8 * sizeof(float), CL_MEM_READ_WRITE);
}

void FvFromCl::reload(ofxOscMessage const &msg) {
    ClFilter::reload(msg);

    ScalarLispMng mng;
    mng.compile(slMode, "mode", "0", msg);
    mng.compile(slLines, "lines", "5", msg);
    mng.compile(slRows, "rows", "25", msg);
    mng.compile(slP0, "p0", "0.0", msg);
    mng.compile(slP1, "p1", "0.0", msg);
    mng.compile(slP2, "p2", "0.0", msg);
    mng.compile(slP3, "p3", "0.0", msg);
    mng.compile(slP4, "p4", "0.0", msg);
    mng.compile(slP5, "p5", "0.0", msg);
    mng.compile(slP6, "p6", "0.0", msg);
    mng.compile(slP7, "p7", "0.0", msg);
}

bool FvFromCl::reloadClProg(const bool force) {
    if(fatalClProg)
        return false;

    bool reloaded = ClFilter::reloadClProg(force);
    if(reloaded) {
        map_r2_kernel = pcl->loadKernel("map_r2");
    }
    return reloaded;
}

bool FvFromCl::isSaveForKernel() {
    return !fatalClProg && !clProgName.empty();
}


void FvFromCl::update() {
    glFinish();

    int lines = std::max(0, (int)slLines.rpn.execute0());
    int rows  = std::max(0, (int)slRows.rpn.execute0());
    int count = lines * rows;
    if(count > max_resultlen)
        return;
//    vertices_size = 0;

    reloadClProg(false);
    if(!fatalClProg) {
        int arg = 0;
        map_r2_kernel->setArg(arg++, clResultBuffer);
        map_r2_kernel->setArg(arg++, lines);
        map_r2_kernel->setArg(arg++, rows);
        map_r2_kernel->setArg(arg++, (float)slP0.rpn.execute0());
        map_r2_kernel->setArg(arg++, (float)slP1.rpn.execute0());
        map_r2_kernel->setArg(arg++, (float)slP2.rpn.execute0());
        map_r2_kernel->setArg(arg++, (float)slP3.rpn.execute0());
        map_r2_kernel->setArg(arg++, (float)slP4.rpn.execute0());
        map_r2_kernel->setArg(arg++, (float)slP5.rpn.execute0());
        map_r2_kernel->setArg(arg++, (float)slP6.rpn.execute0());
        map_r2_kernel->setArg(arg++, (float)slP7.rpn.execute0());
        map_r2_kernel->setArg(arg++, dt0);
        map_r2_kernel->setArg(arg++, dt1);
        map_r2_kernel->setArg(arg++, dt2);
        map_r2_kernel->setArg(arg++, dt3);
        map_r2_kernel->setArg(arg++, dt4);
        map_r2_kernel->setArg(arg++, dt5);
        map_r2_kernel->setArg(arg++, dt6);
        map_r2_kernel->setArg(arg++, dt7);
        map_r2_kernel->run1D(count);

        if(count > vertices_capacity) {
            clearVertices();
            reallocateVertices(count);
        }
        if(vertices)
          clResultBuffer.read(vertices, 0, count * 8 * sizeof(float));
        openCL.finish();
        vertices_size = count;
    }
}

void FvFromCl::draw(int li) {
    if(count >= max_resultlen)
        return;

    fbo.begin();
    ofClear(0, 0, 0, 255);

    if(vertices_size > 0) {
        ofPushStyle();
        ofPushMatrix();
        ofTranslate(filterWidth / 2, filterHeight / 2, 0);
        switch((int)round(slMode.rpn.execute0())) {
        case 1:
            ofFill();
            for(size_t i = 0; i < vertices_size; ++i) {
                if(*FVw(i) > 0.0f) {
                    ofSetColor(ofFloatColor(*FVh(i), *FVs(i), *FVv(i), *FVa(i)));
                    ofDrawRectangle(*FVx(i), *FVy(i), *FVw(i), *FVw(i));
                }
            }
            break;
        default:
            ofFill();
            for(size_t i = 0; i < vertices_size; ++i) {
                if(*FVw(i) > 0.0f) {
                    ofSetColor(ofFloatColor::fromHsb(*FVh(i), *FVs(i), *FVv(i), *FVa(i)));
                    ofDrawRectangle(*FVx(i), *FVy(i), *FVw(i), *FVw(i));
                }
            }
            break;
        }
        ofPopMatrix();
        ofPopStyle();
    }

    fbo.end();
}
#endif // WITH_openCL
