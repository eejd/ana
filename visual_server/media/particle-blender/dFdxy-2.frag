#version 330
uniform sampler2DRect tex0;
out vec4 outputColor;

#define scale 5
void main()
{
    vec3 color = texture(tex0, gl_FragCoord.xy).rgb;
    float gray = (color.r + color.g + color.b) / 3.0;
    outputColor = vec4(scale*dFdx(gray), scale*dFdy(gray), 0.0, 1.0);
}
