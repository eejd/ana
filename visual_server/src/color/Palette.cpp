#include "color/Palette.h"

static ofFloatColor default_white[] = {
    ofFloatColor(  1.0f,  1.0f,  1.0f, 1.0f),
};

static ofFloatColor default_colors[] = {
    ofFloatColor( 41/256.0f,  78/256.0f, 104/256.0f, 1.0f),
    ofFloatColor(  6/256.0f, 187/256.0f, 190/256.0f, 1.0f),
    ofFloatColor(247/256.0f, 139/256.0f,   0/256.0f, 1.0f),
    ofFloatColor(255/256.0f, 147/256.0f,  34/256.0f, 1.0f),
};

static ofFloatColor default_transparent[] = {
    ofFloatColor( 41/256.0f,  78/256.0f, 104/256.0f, 0.5f),
    ofFloatColor(  6/256.0f, 187/256.0f, 190/256.0f, 0.5f),
    ofFloatColor(247/256.0f, 139/256.0f,   0/256.0f, 0.5f),
    ofFloatColor(255/256.0f, 147/256.0f,  34/256.0f, 0.5f),
};

Palette::Palette() {
    setDefault();
}

void Palette::setDefault() {
    pal = &default_white[0];
    psize = sizeof(default_white) / sizeof(ofFloatColor);
}

void Palette::setDefaultColors() {
    pal = &default_colors[0];
    psize = sizeof(default_colors) / sizeof(ofFloatColor);
}

void Palette::setDefaultTransparents() {
    pal = &default_transparent[0];
    psize = sizeof(default_transparent) / sizeof(ofFloatColor);
}

Palette::~Palette() {
}
