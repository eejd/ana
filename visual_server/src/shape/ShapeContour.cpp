#include "ofMain.h"
#include "Configuration.h"
#include "globalvars.h"
#include "Filter.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispMng.h"
#include "calc/ColorNames.h"
#include "shape/ShapeContour.h"

ShapeContour::ShapeContour(ofGLWindowSettings settings, OscListener * oscParams) : Filter(settings, oscParams){
}

ShapeContour::~ShapeContour(){
}

void ShapeContour::setup(const std::string id, int numSamples){
	Filter::setup(id, numSamples);
}

void ShapeContour::reload(ofxOscMessage const &msg) {
    auto narg = msg.getNumArgs();
    if(narg > 1) {
        Filter::setInputTexture(msg.getArgAsString(1));
    }
    ScalarLispMng mng;
    mng.compile(slThreshold, "thres", "0.5", msg);
    mng.compile(slConsidered, "count", "50", msg);
    mng.compile(slMinArea, "min", "10", msg);
    mng.compile(slMaxArea, "max", "500", msg);
    mng.compile(slFindHoles, "holes", "-1", msg);
    mng.compile(slFill, "fill", "-1", msg);
    mng.compile(slUpstream, "paint", "-1", msg);
    mng.compile(slNames, "names", "", msg);
    mng.compile(slClear, "clear", "1", msg);
}

void ShapeContour::update() {
	int threshold = (int)(255 * slThreshold.rpn.execute0());
    int considered = slConsidered.rpn.execute0();
    int minArea = slMinArea.rpn.execute0();
    int maxArea = slMaxArea.rpn.execute0();
    bool findHoles = slFindHoles.rpn.execute0() >= 0.0;
//	lineColor = Pool::asInt(id, "lineColor"); TODO
	if(upstream_ids.size() >= 1) {
		auto in_id = upstream_ids[0];
		if(filterDict.find(in_id) != filterDict.end()) {
			auto f = filterDict[in_id];
			if(f) {
				size_t pw = pixels.getWidth();
				size_t ph = pixels.getHeight();
				f->getTexture().readToPixels(pixels);
				if(!colorImg.bAllocated)
					colorImg.allocate(pw, ph);
				if(!grayImage.bAllocated)
					grayImage.allocate(pw, ph);
				pixels.setImageType(OF_IMAGE_COLOR);
				colorImg.setFromPixels(pixels);
				grayImage = colorImg;
				grayImage.threshold(threshold);
				contourFinder.findContours(grayImage, minArea, maxArea*maxArea, considered, findHoles);
			}
		}
	}
}

void ShapeContour::draw(int li){
	if(upstream_ids.size() < 1)
		return;

    fbo.begin();
    if(slClear.rpn.execute0() >= 0.0)
        ofClear(0, 0, 0, 255);

    bool upstream = slUpstream.rpn.execute0() >= 0.0;
	if(upstream)
		colorImg.draw(0, 0);

	ofPushStyle();
    bool fill = slFill.rpn.execute0() >= 0.0;
	if(fill)
		ofFill();
	else
	   ofNoFill();
	if(lineColor < 0) {
		size_t pw = pixels.getWidth();
		size_t ph = pixels.getHeight();
	    for (int ci = 0; ci < contourFinder.nBlobs; ++ci){
	    	auto b = contourFinder.blobs[ci];
	    	if(b.nPts > 0) {
				size_t icl = b.centroid.x-1; if(icl < 0)   icl = 0;
				size_t icr = b.centroid.x+1; if(icr >= pw) icr = pw-1;
				size_t ict = b.centroid.y-1; if(ict < 0)   ict = 0;
				size_t icb = b.centroid.y+1; if(icb >= ph) icb = ph-1;
				auto cl = pixels.getColor(icl,          b.centroid.y);
				auto ct = pixels.getColor(b.centroid.x, ict);
				auto cc = pixels.getColor(b.centroid.x, b.centroid.y);
				auto cb = pixels.getColor(b.centroid.x, icb);
				auto cr = pixels.getColor(icr,          b.centroid.y);
				auto red = (cl.r + ct.r + cc.r + cb.r + cr.r) / 5;
				auto green = (cl.g + ct.g + cc.g + cb.g + cr.g) / 5;
				auto blue = (cl.b + ct.b + cc.b + cb.b + cr.b) / 5;
				ofSetColor(red, green, blue);

				if(slNames.rpn.execute0() > 0.00001) {
                    std::string cname = ColorNames::nearestRGB(red, green, blue);
                    float cn_width = font.stringWidth(cname);
                    font.drawString(cname, b.centroid.x-cn_width/2, b.centroid.y);
				}

                ofBeginShape();
                auto p = b.pts;
                auto n = b.nPts;
                for (int pi = 0; pi < n; ++pi){
                    ofVertex(p[pi].x, p[pi].y);
                }
                ofEndShape(true);
	    	}
	    }
	}
	else {
		ofSetColor(lineColor);
	    for (int ci = 0; ci < contourFinder.nBlobs; ++ci){
	    	auto b = contourFinder.blobs[ci];
			ofBeginShape();
			auto p = b.pts;
			auto n = b.nPts;
			for (int pi = 0; pi < n; ++pi){
				ofVertex(p[pi].x, p[pi].y);
			}
			ofEndShape(true);
		}
	}
    ofPopStyle();

	fbo.end();

	size_t nb = 0;
	vertices_size = 0;
    for (int ci = 0; ci < contourFinder.nBlobs; ++ci){
        if(contourFinder.blobs[ci].nPts > 0)
            ++nb;
    }
    if(nb > vertices_capacity) {
        clearVertices();
        reallocateVertices(nb);
    }
    size_t vi = 0;
    for (int ci = 0; ci < contourFinder.nBlobs; ++ci){
        auto b = contourFinder.blobs[ci];
        if(b.nPts > 0) {
            *FVx(vi) = b.centroid.x;
            *FVy(vi) = b.centroid.y;
            *FVz(vi) = b.length;
            *FVw(vi) = b.area;
            ++vi;
        }
    }
    vertices_size = vi;
}
