(ns lv.demo)
(use 'lv.core)

(layout "grid" [img0 mov0 cline2]
               [mopr1 ras3])
(render)

(image "../../../timelapse/series/20230717-ice-bleeptrack/split/clip_00002.png")
(movie "../../../timelapse/series/20230717-ice-bleeptrack/fc37d73ff764cc7b.mp4")
(hsv :mov0 [c0]
  (sx 2.8) (sy 3) (ty kp7)
  (hsv (* (vec3 1 1.2 1) c0.hsv))
)

(rgb :img0 [c0]
  (sx 2.8) (sy 3) (ty kp7)
  (rgb c0.rgb)
)

; find contours
(expr :cline2 [img0]
  (count 50)
  (max 5000)
  ; (thres kp1)
  (thres (+ 0.3 (* 0.23 (usin (* 0.1 f)))))
  (fill -1)
  (paint -1)
  (holes 1)
)

(expr :mopr1 [cline2] "ddddbbbb")

(rgb :ras3 [mov0 mopr1]
  ; (rgb (mix mov0.rgb mopr1.rgb ks6))
  (rgb (+ mov0.rgb mopr1.rgb))
)
