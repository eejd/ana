#include "ofMain.h"
#include "util/stringhelper.h"
#include "Configuration.h"
#include "globalvars.h"
#include "lsys/LsysMachine.h"
#include "Filter.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispMng.h"
#include "calc/ColorNames.h"
#include "shape/ShapeLsys.h"

ShapeLsys::ShapeLsys(ofGLWindowSettings settings, OscListener * oscParams) : Filter(settings, oscParams){
}

ShapeLsys::~ShapeLsys(){
}

void ShapeLsys::setup(const std::string id, int numSamples){
	Filter::setup(id, numSamples);
}

void ShapeLsys::reload(ofxOscMessage const &msg) {
    auto narg = msg.getNumArgs();
    if(narg > 1) {
        Filter::setInputTexture(msg.getArgAsString(1));
    }
    std::string axiom = "F";
    std::vector<std::string> predecessor = { "F",     "A" };
    std::vector<std::string> successor =   { "[-FA]", "F" };
    std::vector<std::string> leftContext;
    std::vector<std::string> rightContext;
    int depth = 2;

    size_t nargs = msg.getNumArgs();
    for(size_t ax = 1; ax < nargs; ++ax) {
        auto expr = trim(msg.getArgAsString(ax));
        ofLog(OF_LOG_NOTICE) << "Lsys '" << expr << "'";
    }
    l1.setLsys("+-", axiom, predecessor, successor, leftContext, rightContext);
    l1.expand(depth);

    ScalarLispMng mng;
}

void ShapeLsys::update() {
}

void ShapeLsys::draw(int li){
}
