#pragma once

#include "../ana_features.h"
#ifdef WITH_openCL
#include "ofMain.h"
#include "MSAOpenCL.h"

class Inspector {
public:
    Inspector();
    ~Inspector();

    void setup();
    void setInputFilter(std::string descr);
    bool reloadClAnalysisProg(const bool force);
    void osc2channels();
    void draw2Markers();

    size_t filterWidth = 0;
    size_t filterHeight = 0;
    std::shared_ptr<Filter> inspect_this_filter = nullptr;

    std::string clAnalysisProgName;
    long clAnalysisProgModTime = 0;
//    bool fatalClAnalysisProg = false; //TODO
    bool hasClAnalysisProg = false;
    std::filesystem::path pathClAnalysisProg;

    msa::OpenCLKernelPtr analysis_kernel;

    static const size_t resultlen = 64;
    msa::OpenCLBuffer clInParamsBuffer;
    msa::OpenCLBuffer clResultBuffer;
    int32_t inParams[resultlen];
    int32_t zero[resultlen];
    int32_t results[resultlen];

private:
    int channels = 1;

    void setParams(size_t offset, double i0x, double i0y, double i0r);
    ofFloatColor toRGB(size_t offset);
    void drawRectMarker(const int xx, const int yy, const int rr);
};
#endif // WITH_openCL

