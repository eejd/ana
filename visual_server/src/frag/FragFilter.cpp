#include "ofMain.h"
#include "globalvars.h"
#include "ofApp.h"
#include "Configuration.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispMng.h"
#include "frag/FragFilter.h"

FragFilter::FragFilter(ofGLWindowSettings settings, OscListener * oscParams) :
        Filter(settings, oscParams) {
}

FragFilter::~FragFilter() {
}

void FragFilter::reload(ofxOscMessage const &msg) {
    glslProg.fatalFragProg = false;
    auto numArgs = msg.getNumArgs();
    for(size_t n = 0; n < numArgs; ++n) {
//        ofLog(OF_LOG_NOTICE) << n << " " << (char) msg.getArgType(n) << " " << msg.getArgAsString(n) << "\n";
        auto fn = msg.getArgAsString(n);
        switch(n) {
        case 0:
            break;
        case 1:
            Filter::setInputTexture(msg.getArgAsString(n));
            break;
        case 2: {
            auto fn = msg.getArgAsString(n);
            if(!fn.empty()) {
                glslProg.fragProgName = asString(MEDIA_PATH) + "/" + fn;
                glslProg.pathFragProg = glslProg.fragProgName;
                if(!std::filesystem::exists(glslProg.pathFragProg)) {
                    glslProg.fatalFragProg = true;
                    ofLog(OF_LOG_FATAL_ERROR) << "Cannot access '" << glslProg.fragProgName << "'";
                }
                else {
                    reloadFragProg(true, glslProg);
                }
            }
            break;
        }
        default:
            ofLog(OF_LOG_NOTICE) << msg.getArgAsString(n);
            break;
        }
    }

    ScalarLispMng mng;
    for(auto &u : glslProg.uniforms) {
        mng.compile(u.slFloat, u.name, "0", msg);
    }
}

void FragFilter::update() {
    if(glslProg.fragProgName.empty())
        return;

    reloadFragProg(false, glslProg);
}

void FragFilter::draw(int li) {
    fbo.begin();
    if(glslProg.fatalFragProg || glslProg.fragProgName.empty()) {
        ofClear(0, 0, 0, 255);
    } else {
        glslProg.shader.begin();
        sendUniforms(glslProg.shader, li);
        for(auto &u : glslProg.uniforms) {
            glslProg.shader.setUniform1f(u.name, u.slFloat.rpn.execute0());
        }

        auto tx = 0;
        for(std::vector<std::string>::iterator it = upstream_ids.begin(); it != upstream_ids.end(); ++it) {
            std::string in_id = *it;
            if(filterDict.find(in_id) != filterDict.end()) {
                auto f = filterDict[in_id];
                if(f)
                    glslProg.shader.setUniformTexture("tex" + to_string(tx), f->getTexture(), tx);
            }
            ++tx;
        }

        ofDrawRectangle(0, 0, filterWidth, filterHeight);

        glslProg.shader.end();
    }
    fbo.end();
}
