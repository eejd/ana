(ns lv.demo)
(use 'lv.core)

(layout "grid" [img0 tile1])
(image "Mastodon-#ArtReference/mountains-615428/mountains-615428_1920.jpg")

(rgb :img0 [c0]
	(rgb c0.rgb)
)

(expr :tile1 [img0]
	(x (* (+ 1 (usin f)) 30))
	(y 60)
)
(render 1)
