#pragma once

#include "../ana_features.h"
#ifdef WITH_openCL
#include "ofMain.h"
#include "ofxOsc.h"

#include "Filter.h"
#include "OscListener.h"
#include "calc/ScalarLisp.h"
#include "cl/ClFilter.h"

class FvFromCl: public ClFilter {
public:
    FvFromCl(ofGLWindowSettings settings, OscListener * oscParams);
    virtual ~FvFromCl();
    virtual void setup(const std::string id, int numSamples);
    virtual bool reloadClProg(const bool force);
    virtual void reload(ofxOscMessage const &msg);
    virtual void update();
    virtual void draw(int li);


protected:
    virtual bool isSaveForKernel();

    static const size_t max_resultlen = 20 * 500;
    size_t count = 0;
    float results[8 * max_resultlen];

    msa::OpenCLKernelPtr map_r2_kernel;
    msa::OpenCLBuffer clResultBuffer;

    ScalarLisp slMode;
    ScalarLisp slLines;
    ScalarLisp slRows;
    ScalarLisp slP0;
    ScalarLisp slP1;
    ScalarLisp slP2;
    ScalarLisp slP3;
    ScalarLisp slP4;
    ScalarLisp slP5;
    ScalarLisp slP6;
    ScalarLisp slP7;
};
#endif // WITH_openCL

