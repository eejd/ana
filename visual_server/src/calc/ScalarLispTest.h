#pragma once
#include <string>

#include "ana_features.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"


class ScalarLispTest {
public:
    ScalarLispTest();
    virtual ~ScalarLispTest();

#ifdef WITH_testcases
    static void runTestCases();
    static void runProfile();
    static void check(double expected, std::string input, double x, double y, double z, int i);
    static void errcheck(std::string input, double x, double y);
    static bool double_equals(double res1, double expected);
    static void prof(std::string input, double x, double y, double z, int i);

    static int test_exec_failed;
    static int test_syntax_failed;
    static ScalarLisp sl;
#endif
};
