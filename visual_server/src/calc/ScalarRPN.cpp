#include <limits>
#include <math.h>
#include <string>
#include <random>

#include "ofMain.h"

#include "ana_features.h"
#include "globalvars.h"
#include "calc/Noise.h"
#include "calc/ScalarRPN.h"

ScalarRPN::ScalarRPN() {
    dist.param(range);
    mt.seed(228673613753764);
}

ScalarRPN::~ScalarRPN() {
}

#ifdef DEBUG
void ScalarRPN::protectRPN(bool cond, const char *const msg) {
    if(!cond) {
        ofLog(OF_LOG_ERROR) << '\n' << dump();
        ofLog(OF_LOG_ERROR) << msg;
        throw std::runtime_error(msg);
    }
}
#define PROTECTRPN(cond,msg) protectRPN(cond,msg)
#else  // DEBUG
#define PROTECTRPN(cond,msg)
#endif // DEBUG

void ScalarRPN::clear() {
    isConst = true;
    const_result = 0.0;
    tapeLength = 0;
    memset(codeTape, 0, sizeof(codeTape));
    memset(iOper, 0, sizeof(iOper));
    memset(dOper, 0, sizeof(dOper));
    memset(dStack, 0, sizeof(dStack));
    gotos = false;
}

void ScalarRPN::appendOpr(int opcode, double dconst, int iconst) {
    if(tapeLength >= maxTapeLen)
        return;
    codeTape[tapeLength] = opcode;
    dOper[tapeLength] = dconst;
    iOper[tapeLength] = iconst;
    ++tapeLength;
}

/**
 * @param p1
 * @param p2
 * @return
 */
inline double step(double p1, double p2) {
    if(p2 > p1)
        return 1.0;
    else
        return 0.0;
}

/**
 * @param p1
 * @return
 */
inline double clamp(double p1) {
    if(p1 < 0.0)
        return 0.0;
    else if(p1 > 1.0)
        return 1.0;
    else
        return p1;
}

/**
 * @param p1
 * @param p2
 * @return
 */
inline double bias(double p1, double p2) {
    return std::pow(std::fabs(p2), std::log(std::fabs(p1)) / (-0.693147181));
}

void ScalarRPN::seed(long seed) {
    dist.param(range);
    mt.seed(seed);
}

#ifdef DEBUG
#define RPN_TRACE() \
{ \
        std::stringstream linetrace; \
        linetrace.precision(5); \
        linetrace.flags(std::ios::fixed); \
        std::string tracer; \
        std::string mn; \
        dumpOpr(ip, mn); \
        std::stringstream stacktrace; \
        stacktrace.precision(5); \
        stacktrace.flags(std::ios::fixed); \
        for(int si = dSp-1; si >= 0; --si) { \
            stacktrace << dStack[si] << " "; \
        } \
        linetrace << ip << " { " << mn << " } " << dSp << " [" << stacktrace.str() <<  "]"; \
        ofLog(OF_LOG_NOTICE) << linetrace.str(); \
}
#else
#define RPN_TRACE()
#endif

        #define DISPATCH() goto *gotoTape[ip++]

double ScalarRPN::execute4d3i(double x, double y, double z, double w, int i, int j, int k) {
    if(isConst)
        return const_result;

    if(!gotos) {
        int ip = 0;
        while(ip < tapeLength) {
            switch(codeTape[ip]) {
            case oprStop: gotoTape[ip] =        &&labelStop; break;
            case oprPushConst: gotoTape[ip] =   &&labelPushConst; break;
            case oprPushX: gotoTape[ip] =       &&labelPushX; break;
            case oprPushY: gotoTape[ip] =       &&labelPushY; break;
            case oprPushZ: gotoTape[ip] =       &&labelPushZ; break;
            case oprPushW: gotoTape[ip] =       &&labelPushW; break;
            case oprPushI: gotoTape[ip] =       &&labelPushI; break;
            case oprPushJ: gotoTape[ip] =       &&labelPushJ; break;
            case oprPushK: gotoTape[ip] =       &&labelPushK; break;
            case oprPushF: gotoTape[ip] =       &&labelPushF; break;
            case oprPushLx: gotoTape[ip] =      &&labelPushLx; break;
            case oprPushLy: gotoTape[ip] =      &&labelPushLy; break;
            case oprPushRx: gotoTape[ip] =      &&labelPushRx; break;
            case oprPushRy: gotoTape[ip] =      &&labelPushRy; break;
            case oprPushL2: gotoTape[ip] =      &&labelPushL2; break;
            case oprPushR2: gotoTape[ip] =      &&labelPushR2; break;
            case oprPushOnset: gotoTape[ip] =   &&labelPushOnset; break;
            case oprPushVol: gotoTape[ip] =     &&labelPushVol; break;

            case oprPushU0: gotoTape[ip] =      &&labelPushU0; break;
            case oprPushU1: gotoTape[ip] =      &&labelPushU1; break;
            case oprPushU2: gotoTape[ip] =      &&labelPushU2; break;
            case oprPushU3: gotoTape[ip] =      &&labelPushU3; break;
            case oprPushU4: gotoTape[ip] =      &&labelPushU4; break;
            case oprPushU5: gotoTape[ip] =      &&labelPushU5; break;
            case oprPushU6: gotoTape[ip] =      &&labelPushU6; break;
            case oprPushU7: gotoTape[ip] =      &&labelPushU7; break;

            case oprPushT0: gotoTape[ip] =      &&labelPushT0; break;
            case oprPushT1: gotoTape[ip] =      &&labelPushT1; break;
            case oprPushT2: gotoTape[ip] =      &&labelPushT2; break;
            case oprPushT3: gotoTape[ip] =      &&labelPushT3; break;
            case oprPushT4: gotoTape[ip] =      &&labelPushT4; break;
            case oprPushT5: gotoTape[ip] =      &&labelPushT5; break;
            case oprPushT6: gotoTape[ip] =      &&labelPushT6; break;
            case oprPushT7: gotoTape[ip] =      &&labelPushT7; break;

            case oprPushKP0: gotoTape[ip] =     &&labelPushKP0; break;
            case oprPushKP1: gotoTape[ip] =     &&labelPushKP1; break;
            case oprPushKP2: gotoTape[ip] =     &&labelPushKP2; break;
            case oprPushKP3: gotoTape[ip] =     &&labelPushKP3; break;
            case oprPushKP4: gotoTape[ip] =     &&labelPushKP4; break;
            case oprPushKP5: gotoTape[ip] =     &&labelPushKP5; break;
            case oprPushKP6: gotoTape[ip] =     &&labelPushKP6; break;
            case oprPushKP7: gotoTape[ip] =     &&labelPushKP7; break;

            case oprPushKS0: gotoTape[ip] =     &&labelPushKS0; break;
            case oprPushKS1: gotoTape[ip] =     &&labelPushKS1; break;
            case oprPushKS2: gotoTape[ip] =     &&labelPushKS2; break;
            case oprPushKS3: gotoTape[ip] =     &&labelPushKS3; break;
            case oprPushKS4: gotoTape[ip] =     &&labelPushKS4; break;
            case oprPushKS5: gotoTape[ip] =     &&labelPushKS5; break;
            case oprPushKS6: gotoTape[ip] =     &&labelPushKS6; break;
            case oprPushKS7: gotoTape[ip] =     &&labelPushKS7; break;

            case oprPushKB0: gotoTape[ip] =     &&labelPushKB0; break;
            case oprPushKB1: gotoTape[ip] =     &&labelPushKB1; break;
            case oprPushKB2: gotoTape[ip] =     &&labelPushKB2; break;
            case oprPushKB3: gotoTape[ip] =     &&labelPushKB3; break;
            case oprPushKB4: gotoTape[ip] =     &&labelPushKB4; break;
            case oprPushKB5: gotoTape[ip] =     &&labelPushKB5; break;
            case oprPushKB6: gotoTape[ip] =     &&labelPushKB6; break;
            case oprPushKB7: gotoTape[ip] =     &&labelPushKB7; break;

            case oprPushH0: gotoTape[ip] =      &&labelPushH0; break;
            case oprPushS0: gotoTape[ip] =      &&labelPushS0; break;
            case oprPushV0: gotoTape[ip] =      &&labelPushV0; break;
            case oprIfPositive: gotoTape[ip] =  &&labelIfPositive; break;
            case oprIfZero: gotoTape[ip] =      &&labelIfZero; break;
            case oprJmp: gotoTape[ip] =         &&labelJmp; break;
            case oprLabel: gotoTape[ip] =       &&labelLabel; break;
            case oprAdd: gotoTape[ip] =         &&labelAdd; break;
            case oprMult: gotoTape[ip] =        &&labelMult; break;
            case oprSub: gotoTape[ip] =         &&labelSub; break;
            case oprSub1: gotoTape[ip] =        &&labelSub1; break;
            case oprDiv: gotoTape[ip] =         &&labelDiv; break;
            case oprDiv1: gotoTape[ip] =        &&labelDiv1; break;
            case oprUNoise: gotoTape[ip] =      &&labelUNoise; break;
            case oprURandom: gotoTape[ip] =     &&labelURandom; break;
            case oprSNoise: gotoTape[ip] =      &&labelSNoise; break;
            case oprTurbulence: gotoTape[ip] =  &&labelTurbulence; break;
            case oprPow: gotoTape[ip] =         &&labelPow; break;
            case oprLog2: gotoTape[ip] =        &&labelLog2; break;
            case oprSqrt: gotoTape[ip] =        &&labelSqrt; break;
            case oprSqr: gotoTape[ip] =         &&labelSqr; break;
            case oprTan: gotoTape[ip] =         &&labelTan; break;
            case oprSin: gotoTape[ip] =         &&labelSin; break;
            case oprUSin: gotoTape[ip] =        &&labelUSin; break;
            case oprASin: gotoTape[ip] =        &&labelASin; break;
            case oprCos: gotoTape[ip] =         &&labelCos; break;
            case oprUCos: gotoTape[ip] =        &&labelUCos; break;
            case oprACos: gotoTape[ip] =        &&labelACos; break;
            case oprAtan2: gotoTape[ip] =       &&labelAtan2; break;
            case oprLog: gotoTape[ip] =         &&labelLog; break;
            case oprMix: gotoTape[ip] =         &&labelMix; break;
            case oprStep: gotoTape[ip] =        &&labelStep; break;
            case oprPuls: gotoTape[ip] =        &&labelPuls; break;
            case oprClamp: gotoTape[ip] =       &&labelClamp; break;
            case oprMax: gotoTape[ip] =         &&labelMax; break;
            case oprMin: gotoTape[ip] =         &&labelMin; break;
            case oprAbs: gotoTape[ip] =         &&labelAbs; break;
            case oprTrunc: gotoTape[ip] =       &&labelTrunc; break;
            case oprSign: gotoTape[ip] =        &&labelSign; break;
            case oprRound: gotoTape[ip] =       &&labelRound; break;
            case oprSmoothstep: gotoTape[ip] =  &&labelSmoothstep; break;
            case oprBoxstep: gotoTape[ip] =     &&labelBoxstep; break;
            case oprMod: gotoTape[ip] =         &&labelMod; break;
            case oprFloor: gotoTape[ip] =       &&labelFloor; break;
            case oprCeil: gotoTape[ip] =        &&labelCeil; break;
            case oprGamma: gotoTape[ip] =       &&labelGamma; break;
            case oprGain: gotoTape[ip] =        &&labelGain; break;
            case oprBias: gotoTape[ip] =        &&labelBias; break;
            case oprEuclideanDistance: gotoTape[ip] =       &&labelEuclideanDistance; break;
            case oprAbsoluteDistance: gotoTape[ip] =        &&labelAbsoluteDistance; break;
            default:
                PROTECTRPN(false, ("invalid code " + std::to_string(codeTape[ip]) + " on tape position " + std::to_string(ip)).c_str());
                return 0.0;
            }
            ++ip;
        }
        gotoTape[ip] = &&labelStop;
        gotos = true;
    }

    try {
        int dSp = 0;

        int ip = 0;
        DISPATCH();

        labelStop:
            PROTECTRPN(dSp == 1, "remaining elements on stack");
            return dStack[--dSp];
        labelPushConst:
            dStack[dSp++] = dOper[ip-1];
        DISPATCH();
        labelPushX:
            dStack[dSp++] = x;
        DISPATCH();
        labelPushY:
            dStack[dSp++] = y;
        DISPATCH();
        labelPushZ:
            dStack[dSp++] = z;
        DISPATCH();
        labelPushW:
            dStack[dSp++] = w;
        DISPATCH();
        labelPushI:
            dStack[dSp++] = i;
        DISPATCH();
        labelPushJ:
            dStack[dSp++] = j;
        DISPATCH();
        labelPushK:
            dStack[dSp++] = k;
        DISPATCH();
        labelPushF:
            dStack[dSp++] = frame;
        DISPATCH();
        labelPushLx:
            dStack[dSp++] = lx;
        DISPATCH();
        labelPushLy:
            dStack[dSp++] = ly;
        DISPATCH();
        labelPushRx:
            dStack[dSp++] = rx;
        DISPATCH();
        labelPushRy:
            dStack[dSp++] = ry;
        DISPATCH();
        labelPushL2:
            dStack[dSp++] = l2;
        DISPATCH();
        labelPushR2:
            dStack[dSp++] = r2;
        DISPATCH();
        labelPushOnset:
            dStack[dSp++] = onset;
        DISPATCH();
        labelPushVol:
            dStack[dSp++] = smoothedVol;
        DISPATCH();
        labelPushU0:
            dStack[dSp++] = u0;
        DISPATCH();
        labelPushU1:
            dStack[dSp++] = u1;
        DISPATCH();
        labelPushU2:
            dStack[dSp++] = u2;
        DISPATCH();
        labelPushU3:
            dStack[dSp++] = u3;
        DISPATCH();
        labelPushU4:
            dStack[dSp++] = u4;
        DISPATCH();
        labelPushU5:
            dStack[dSp++] = u5;
        DISPATCH();
        labelPushU6:
            dStack[dSp++] = u6;
        DISPATCH();
        labelPushU7:
            dStack[dSp++] = u7;
        DISPATCH();

        labelPushT0:
            dStack[dSp++] = t0;
        DISPATCH();
        labelPushT1:
            dStack[dSp++] = t1;
        DISPATCH();
        labelPushT2:
            dStack[dSp++] = t2;
        DISPATCH();
        labelPushT3:
            dStack[dSp++] = t3;
        DISPATCH();
        labelPushT4:
            dStack[dSp++] = t4;
        DISPATCH();
        labelPushT5:
            dStack[dSp++] = t5;
        DISPATCH();
        labelPushT6:
            dStack[dSp++] = t6;
        DISPATCH();
        labelPushT7:
            dStack[dSp++] = t7;
        DISPATCH();

        labelPushKP0:
            dStack[dSp++] = kp0;
        DISPATCH();
        labelPushKP1:
            dStack[dSp++] = kp1;
        DISPATCH();
        labelPushKP2:
            dStack[dSp++] = kp2;
        DISPATCH();
        labelPushKP3:
            dStack[dSp++] = kp3;
        DISPATCH();
        labelPushKP4:
            dStack[dSp++] = kp4;
        DISPATCH();
        labelPushKP5:
            dStack[dSp++] = kp5;
        DISPATCH();
        labelPushKP6:
            dStack[dSp++] = kp6;
        DISPATCH();
        labelPushKP7:
            dStack[dSp++] = kp7;
        DISPATCH();

        labelPushKS0:
            dStack[dSp++] = ks0;
        DISPATCH();
        labelPushKS1:
            dStack[dSp++] = ks1;
        DISPATCH();
        labelPushKS2:
            dStack[dSp++] = ks2;
        DISPATCH();
        labelPushKS3:
            dStack[dSp++] = ks3;
        DISPATCH();
        labelPushKS4:
            dStack[dSp++] = ks4;
        DISPATCH();
        labelPushKS5:
            dStack[dSp++] = ks5;
        DISPATCH();
        labelPushKS6:
            dStack[dSp++] = ks6;
        DISPATCH();
        labelPushKS7:
            dStack[dSp++] = ks7;
        DISPATCH();

        labelPushKB0:
            dStack[dSp++] = kb0;
        DISPATCH();
        labelPushKB1:
            dStack[dSp++] = kb1;
        DISPATCH();
        labelPushKB2:
            dStack[dSp++] = kb2;
        DISPATCH();
        labelPushKB3:
            dStack[dSp++] = kb3;
        DISPATCH();
        labelPushKB4:
            dStack[dSp++] = kb4;
        DISPATCH();
        labelPushKB5:
            dStack[dSp++] = kb5;
        DISPATCH();
        labelPushKB6:
            dStack[dSp++] = kb6;
        DISPATCH();
        labelPushKB7:
            dStack[dSp++] = kb7;
        DISPATCH();

        labelPushH0:
            dStack[dSp++] = h0;
        DISPATCH();
        labelPushS0:
            dStack[dSp++] = s0;
        DISPATCH();
        labelPushV0:
            dStack[dSp++] = v0;
        DISPATCH();
        labelIfPositive:
            if(dStack[--dSp] < 0.0) {
                ip = iOper[ip-1] - 1;
            }
        DISPATCH();
        labelIfZero:
            if(std::fabs(dStack[--dSp]) > rpn_epsilon) {
                ip = iOper[ip-1] - 1;
            }
        DISPATCH();
        labelJmp:
            ip = iOper[ip-1] - 1;
        DISPATCH();
        labelLabel:
        DISPATCH();
        labelAdd: {
            int argLength = iOper[ip-1];
            double accu0 = dStack[--dSp];
            while(argLength > 1) {
                accu0 += dStack[--dSp];
                --argLength;
            }
            dStack[dSp++] = accu0;
        }
        DISPATCH();
        labelMult: {
            int argLength = iOper[ip-1];
            double accu0 = dStack[--dSp];
            while(argLength > 1) {
                accu0 *= dStack[--dSp];
                --argLength;
            }
            dStack[dSp++] = accu0;
        }
        DISPATCH();
        labelSub: {
            int argLength = iOper[ip-1];
            dSp -= argLength;
            int al = 1;
            while(al < argLength) {
                dStack[dSp] -= dStack[dSp+al];
                ++al;
            }
            ++dSp;
        }
        DISPATCH();
        labelSub1: {
            dStack[dSp-1] = -dStack[dSp-1];
        }
        DISPATCH();
        labelDiv: {
            int argLength = iOper[ip-1];
            dSp -= argLength;
            int al = 1;
            while(al < argLength) {
                dStack[dSp] /= dStack[dSp+al];
                ++al;
            }
            ++dSp;
        }
        DISPATCH();
        labelDiv1: {
            dStack[dSp-1] = 1.0 / dStack[dSp-1];
        }
        DISPATCH();
        labelUNoise:
            PROTECTRPN(iOper[ip-1] == 3, "argument length of operator 'unoise' should be 3");
            {
                double nz = dStack[--dSp];
                double ny = dStack[--dSp];
                double nx = dStack[--dSp];
                dStack[dSp++] = 0.5 + 0.5 * improved_noise.snoise(nx, ny, nz);
            }
        DISPATCH();
        labelURandom:
            PROTECTRPN(iOper[ip-1] == 0, "operator 'rand' has no arguments");
            {
                dStack[dSp++] = dist(mt);
            }
        DISPATCH();
        labelSNoise:
            PROTECTRPN(iOper[ip-1] == 3, "argument length of operator 'snoise' should be 3");
            {
                double nz = dStack[--dSp];
                double ny = dStack[--dSp];
                double nx = dStack[--dSp];
                dStack[dSp++] = improved_noise.snoise(nx, ny, nz);
            }
        DISPATCH();
        labelTurbulence:
            PROTECTRPN(iOper[ip-1] == 4, "argument length of operator 'sturb' should be 4");
            {
                // see: Darwyn Peachey: Building Procedural Textures
                // in: Texturing and Modeling, a Procedural Approach
                double foctaves = dStack[--dSp];
                int octaves = std::max(0.0, std::min(8.0, foctaves));
                double nz = dStack[--dSp];
                double ny = dStack[--dSp];
                double nx = dStack[--dSp];
                double t = 0;
                double f = 1;
                for (int io = 0; io < octaves; io++) {
                    double value = improved_noise.snoise(nx * f, ny * f, nz * f);
                    t += std::fabs(value) / f;
                    f *= 2;
                }
                dStack[dSp++] = t;
            }
        DISPATCH();
        labelPow:
            PROTECTRPN(iOper[ip-1] == 2, "argument length of operator 'pow' should be 2");
            {
                double n2 = dStack[--dSp];
                double n1 = dStack[--dSp];
                dStack[dSp++] = std::pow(std::fabs(n1), n2);
            }
        DISPATCH();
        labelSqrt:
            PROTECTRPN(iOper[ip-1] == 1, "argument length of operator 'sqrt' should be 1");
            dStack[dSp - 1] = std::sqrt(std::fabs(dStack[dSp - 1]));
        DISPATCH();
        labelSqr:
            PROTECTRPN(iOper[ip-1] == 1, "argument length of operator 'sqr' should be 1");
            dStack[dSp - 1] = dStack[dSp - 1] * dStack[dSp - 1];
        DISPATCH();
        labelTan:
            PROTECTRPN(iOper[ip-1] == 1, "argument length of operator 'tan' should be 1");
            dStack[dSp - 1] = std::tan(dStack[dSp - 1]);
        DISPATCH();
        labelSin:
            PROTECTRPN(iOper[ip-1] == 1, "argument length of operator 'sin' should be 1");
            dStack[dSp - 1] = std::sin(dStack[dSp - 1]);
        DISPATCH();
        labelUSin:
            PROTECTRPN(iOper[ip-1] == 1, "argument length of operator 'usin' should be 1");
            dStack[dSp - 1] = 0.5 * (1.0 + std::sin(dStack[dSp - 1]));
        DISPATCH();
        labelASin:
            PROTECTRPN(iOper[ip-1] == 1, "argument length of operator 'asin' should be 1");
            dStack[dSp - 1] = std::asin(dStack[dSp - 1]);
        DISPATCH();
        labelCos:
            PROTECTRPN(iOper[ip-1] == 1, "argument length of operator 'cos' should be 1");
            dStack[dSp - 1] = std::cos(dStack[dSp - 1]);
        DISPATCH();
        labelUCos:
            PROTECTRPN(iOper[ip-1] == 1, "argument length of operator 'ucos' should be 1");
            dStack[dSp - 1] = 0.5 * (1.0 + std::cos(dStack[dSp - 1]));
        DISPATCH();
        labelACos:
            PROTECTRPN(iOper[ip-1] == 1, "argument length of operator 'acos' should be 1");
            dStack[dSp - 1] = std::acos(dStack[dSp - 1]);
        DISPATCH();
        labelAtan2:
            PROTECTRPN(iOper[ip-1] == 2, "argument length of operator 'atan' should be 2");
            {
                double n2 = dStack[--dSp];
                double n1 = dStack[--dSp];
                if((std::fabs(n1) < rpn_epsilon) && (std::fabs(n2) < rpn_epsilon))
                    dStack[dSp++] = 0.0;
                else
                    dStack[dSp++] = std::atan2(n1, n2);
            }
        DISPATCH();
        labelLog:
            PROTECTRPN(iOper[ip-1] == 1, "argument length of operator 'log' should be 1");
            {
                double n1 = dStack[--dSp];
                if(n1 > rpn_epsilon)
                    dStack[dSp++] = std::log(n1);
                else if(n1 < -rpn_epsilon)
                    dStack[dSp++] = std::log(-n1);
                else
                    dStack[dSp++] = -1 * std::numeric_limits<double>::infinity();
            }
        DISPATCH();
        labelLog2:
            PROTECTRPN(iOper[ip-1] == 1, "argument length of operator 'log2' should be 1");
            {
                double n1 = dStack[--dSp];
                if(n1 > rpn_epsilon)
                    dStack[dSp++] = std::log2(n1);
                else if(n1 < -rpn_epsilon)
                    dStack[dSp++] = std::log2(-n1);
                else
                    dStack[dSp++] = -1 * std::numeric_limits<double>::infinity();
            }
        DISPATCH();
        labelMix:
            PROTECTRPN(iOper[ip-1] == 3, "argument length of operator 'mix' should be 3");
            {
                double n3 = dStack[--dSp];
                double n2 = dStack[--dSp];
                double n1 = dStack[--dSp];
                if(n3 < 0.0)
                    n3 = 0.0;
                else if(n3 > 1.0)
                    n3 = 1.0;
                dStack[dSp++] = (1.0 - n3) * n1 + n3 * n2;
            }
        DISPATCH();
        labelStep:
            PROTECTRPN(iOper[ip-1] == 2, "argument length of operator 'step' should be 2");
            {
                double n2 = dStack[--dSp];
                double n1 = dStack[--dSp];
                dStack[dSp++] = step(n1, n2);
            }
        DISPATCH();
        labelPuls:
            PROTECTRPN(iOper[ip-1] == 3, "argument length of operator 'puls' should be 3");
            {
                double n3 = dStack[--dSp];
                double n2 = dStack[--dSp];
                double n1 = dStack[--dSp];
                dStack[dSp++] = step(n1, n3) - step(n2, n3);
            }
        DISPATCH();
        labelClamp:
            PROTECTRPN(iOper[ip-1] == 3, "argument length of operator 'clamp' should be 3");
            {
                double n3 = dStack[--dSp];
                double n2 = dStack[--dSp];
                double n1 = dStack[--dSp];
                if(n2 > n3) {
                    double temp = n2;
                    n2 = n3;
                    n3 = temp;
                }
                if(n1 < n2)
                    dStack[dSp++] = n2;
                else if(n1 > n3)
                    dStack[dSp++] = n3;
                else
                    dStack[dSp++] = n1;
            }
        DISPATCH();
        labelMax: {
            int argLength = iOper[ip-1];
            double accu0 = dStack[--dSp];
            while(argLength > 1) {
                double n1 = dStack[--dSp];
                if(n1 > accu0)
                    accu0 = n1;
                --argLength;
            }
            dStack[dSp++] = accu0;
        }
        DISPATCH();
        labelMin: {
            int argLength = iOper[ip-1];
            double accu0 = dStack[--dSp];
            while(argLength > 1) {
                double n1 = dStack[--dSp];
                if(n1 < accu0)
                    accu0 = n1;
                --argLength;
            }
            dStack[dSp++] = accu0;
        }
        DISPATCH();
        labelAbs:
            PROTECTRPN(iOper[ip-1] == 1, "argument length of operator 'abs' should be 1");
            {
                double n1 = dStack[--dSp];
                if(n1 < 0.0)
                    dStack[dSp++] = -n1;
                else
                    dStack[dSp++] = n1;
            }
        DISPATCH();
        labelSign:
            PROTECTRPN(iOper[ip-1] == 1, "argument length of operator 'sign' should be 1");
            {
                double accu = dStack[dSp - 1];
                dStack[dSp - 1] = (accu > 0.0) - (accu < 0.0);
            }
        DISPATCH();
        labelTrunc:
            PROTECTRPN(iOper[ip-1] == 1, "argument length of operator 'trunc' should be 1");
            dStack[dSp - 1] = std::trunc(dStack[dSp - 1]);
        DISPATCH();
        labelRound:
            PROTECTRPN(iOper[ip-1] == 1, "argument length of operator 'round' should be 1");
            dStack[dSp - 1] = std::round(dStack[dSp - 1]);
        DISPATCH();
        labelSmoothstep:
            PROTECTRPN(iOper[ip-1] == 3, "argument length of operator 'smoothstep' should be 3");
            {
                double n3 = dStack[--dSp];
                double n2 = dStack[--dSp];
                double n1 = dStack[--dSp];
                if(n3 < n1) {
                    dStack[dSp++] = 0.0;
                } else if(n3 >= n2) {
                    dStack[dSp++] = 1.0;
                } else if(std::abs(n2 - n1) > rpn_epsilon) {
                    n3 = (n3 - n1) / (n2 - n1);
                    dStack[dSp++] = n3 * n3 * (3.0 - 2.0 * n3);
                } else {
                    dStack[dSp++] = 1.0;
                }
            }
        DISPATCH();
        labelBoxstep:
            PROTECTRPN(iOper[ip-1] == 3, "argument length of operator 'boxstep' should be 3");
            {
                double n3 = dStack[--dSp];
                double n2 = dStack[--dSp];
                double n1 = dStack[--dSp];
                dStack[dSp++] = clamp((n3 - n1) / (n2 - n1));
            }
        DISPATCH();
        labelMod:
            PROTECTRPN(iOper[ip-1] == 2, "argument length of operator 'mod' should be 2");
            {
                double n2 = dStack[--dSp];
                double n1 = dStack[--dSp];
                if(std::abs(n2) > rpn_epsilon) {
                    int n = (int) (n1 / n2);
                    n1 -= n * n2;
                    if(n1 < 0.0)
                        n1 += n2;
                    dStack[dSp++] = n1;
                } else
                    dStack[dSp++] = std::numeric_limits<double>::infinity();
            }
        DISPATCH();
        labelFloor:
            PROTECTRPN(iOper[ip-1] == 1, "argument length of operator 'floor' should be 1");
            dStack[dSp - 1] = std::floor(dStack[dSp - 1]);
        DISPATCH();
        labelCeil:
            PROTECTRPN(iOper[ip-1] == 1, "argument length of operator 'ceil' should be 1");
            dStack[dSp - 1] = std::ceil(dStack[dSp - 1]);
        DISPATCH();
        labelGamma:
            PROTECTRPN(iOper[ip-1] == 2, "argument length of operator 'gamma' should be 2");
            {
                double n2 = dStack[--dSp];
                double n1 = dStack[--dSp];
                if(std::abs(n1) > rpn_epsilon)
                    dStack[dSp++] = std::pow(std::abs(n2), 1 / n1);
                else
                    dStack[dSp++] = std::numeric_limits<double>::infinity();
            }
        DISPATCH();
        labelGain:
            PROTECTRPN(iOper[ip-1] == 2, "argument length of operator 'gain' should be 2");
            {
                double n2 = dStack[--dSp];
                double n1 = dStack[--dSp];
                if(n2 < 0.5)
                    dStack[dSp++] = bias(1.0 - n1, 2.0 * n2) / 2.0;
                else
                    dStack[dSp++] = 1.0 - bias(1.0 - n1, 2.0 - 2.0 * n2) / 2.0;
            }
        DISPATCH();
        labelBias:
            PROTECTRPN(iOper[ip-1] == 2, "argument length of operator 'bias' should be 2");
            {
                double n2 = dStack[--dSp];
                double n1 = dStack[--dSp];
                dStack[dSp++] = bias(n1, n2);
            }
        DISPATCH();
        labelEuclideanDistance:
            PROTECTRPN(iOper[ip-1] == 4, "argument length of operator 'euclide' should be 4");
            {
                double n4 = dStack[--dSp];
                double n3 = dStack[--dSp];
                double n2 = dStack[--dSp];
                double n1 = dStack[--dSp];
                n1 = n3 - n1;
                n2 = n4 - n2;
                dStack[dSp++] = std::sqrt(n1 * n1 + n2 * n2);
            }
        DISPATCH();
        labelAbsoluteDistance:
            PROTECTRPN(iOper[ip-1] == 4, "argument length of operator 'absolute' should be 4");
            {
                double n4 = dStack[--dSp];
                double n3 = dStack[--dSp];
                double n2 = dStack[--dSp];
                double n1 = dStack[--dSp];
                dStack[dSp++] = std::abs(n3 - n1) + std::abs(n4 - n2);
            }
        DISPATCH();
    } catch(const std::exception &ex) {
        ofLogError() << "ScalarRPN::execute failed with exception '" << ex.what() << "'";
    }
    return 0.0;
}

void ScalarRPN::dumpOpr(int ip, std::string &a) {
    switch(codeTape[ip]) {
    case oprStop:
        a.append("stop");
        break;
    case oprPushConst:
        a.append("pushConst\t");
        a.append(to_string(dOper[ip]));
        break;
    case oprPushX:
        a.append("pushX");
        break;
    case oprPushY:
        a.append("pushY");
        break;
    case oprPushZ:
        a.append("pushZ");
        break;
    case oprPushW:
        a.append("pushW");
        break;
    case oprPushI:
        a.append("pushI");
        break;
    case oprPushJ:
        a.append("pushJ");
        break;
    case oprPushK:
        a.append("pushK");
        break;
    case oprPushF:
        a.append("pushF");
        break;
        
    // nanoKontrol2 potentiometer
    case oprPushKP0:
        a.append("oprPushKP0");
        break;
    case oprPushKP1:
        a.append("oprPushKP1");
        break;
    case oprPushKP2:
        a.append("oprPushKP2");
        break;
    case oprPushKP3:
        a.append("oprPushKP3");
        break;
    case oprPushKP4:
        a.append("oprPushKP4");
        break;
    case oprPushKP5:
        a.append("oprPushKP5");
        break;
    case oprPushKP6:
        a.append("oprPushKP6");
        break;
    case oprPushKP7:
        a.append("oprPushKP7");
        break;

    // nanoKontrol2 slider
    case oprPushKS0:
        a.append("oprPushKS0");
        break;
    case oprPushKS1:
        a.append("oprPushKS1");
        break;
    case oprPushKS2:
        a.append("oprPushKS2");
        break;
    case oprPushKS3:
        a.append("oprPushKS3");
        break;
    case oprPushKS4:
        a.append("oprPushKS4");
        break;
    case oprPushKS5:
        a.append("oprPushKS5");
        break;
    case oprPushKS6:
        a.append("oprPushKS6");
        break;
    case oprPushKS7:
        a.append("oprPushKS7");
        break;

    // nanoKontrol2 button
    case oprPushKB0:
        a.append("oprPushKB0");
        break;
    case oprPushKB1:
        a.append("oprPushKB1");
        break;
    case oprPushKB2:
        a.append("oprPushKB2");
        break;
    case oprPushKB3:
        a.append("oprPushKB3");
        break;
    case oprPushKB4:
        a.append("oprPushKB4");
        break;
    case oprPushKB5:
        a.append("oprPushKB5");
        break;
    case oprPushKB6:
        a.append("oprPushKB6");
        break;
    case oprPushKB7:
        a.append("oprPushKB7");
        break;
        
    case oprPushLx:
        a.append("pushLx");
        break;
    case oprPushLy:
        a.append("pushLy");
        break;
    case oprPushRx:
        a.append("pushRx");
        break;
    case oprPushRy:
        a.append("pushRy");
        break;
    case oprPushL2:
        a.append("pushL2");
        break;
    case oprPushR2:
        a.append("pushR2");
        break;
    case oprPushOnset:
        a.append("pushOnset");
        break;
    case oprPushVol:
        a.append("pushVol");
        break;
    case oprPushU0:
        a.append("pushU0");
        break;
    case oprPushU1:
        a.append("pushU1");
        break;
    case oprPushU2:
        a.append("pushU2");
        break;
    case oprPushU3:
        a.append("pushU3");
        break;
    case oprPushU4:
        a.append("pushU4");
        break;
    case oprPushU5:
        a.append("pushU5");
        break;
    case oprPushU6:
        a.append("pushU6");
        break;
    case oprPushU7:
        a.append("pushU7");
        break;
    case oprIfPositive:
        a.append("pos?\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprIfZero:
        a.append("zero?\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprJmp:
        a.append("jmp\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprLabel:
        a.append("label\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprAdd:
        a.append("add\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprMult:
        a.append("mult\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprSub:
        a.append("sub\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprDiv:
        a.append("div\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprUNoise:
        a.append("unoise\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprURandom:
        a.append("rand\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprSNoise:
        a.append("snoise\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprTurbulence:
        a.append("turbulence\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprPow:
        a.append("pow\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprLog2:
        a.append("log2\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprSqrt:
        a.append("sqrt\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprSqr:
        a.append("sqr\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprTan:
        a.append("tan\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprSin:
        a.append("sin\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprCos:
        a.append("cos\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprAtan2:
        a.append("atanq\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprLog:
        a.append("log\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprMix:
        a.append("mix\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprStep:
        a.append("step\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprPuls:
        a.append("puls\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprClamp:
        a.append("clamp\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprMax:
        a.append("max\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprMin:
        a.append("min\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprAbs:
        a.append("abs\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprTrunc:
        a.append("trunc\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprSign:
        a.append("sign\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprRound:
        a.append("round\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprSmoothstep:
        a.append("smoothstep\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprBoxstep:
        a.append("boxstep\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprMod:
        a.append("mod\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprFloor:
        a.append("floor\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprCeil:
        a.append("ceil\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprGamma:
        a.append("gamma\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprGain:
        a.append("gain\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprBias:
        a.append("bias\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprEuclideanDistance:
        a.append("euclide_distance\t");
        a.append(to_string(iOper[ip]));
        break;
    case oprAbsoluteDistance:
        a.append("absolute_distance\t");
        a.append(to_string(iOper[ip]));
        break;
    default:
        a.append("invalide code\t");
        a.append(to_string(ip));
        a += '\t';
        a.append(to_string(codeTape[ip]));
        a += '\t';
        a.append(to_string(iOper[ip]));
        a += '\t';
        a.append(to_string(dOper[ip]));
        break;
    }
}

std::string ScalarRPN::dump() {
    std::string a;
    int ip = 0;
    while(ip < tapeLength) {
        a.append(to_string(ip) + "\t");
        dumpOpr(ip, a);
        a += '\n';
        ++ip;
    }
    return a;
}

#ifdef WITH_testcases
void ScalarRPN::test1() {
//    (/ x 0.1)
    clear();
    appendOpr(oprPushConst, 0.1);
    appendOpr(oprPushX);
    appendOpr(oprDiv, 0.0, 2);
    appendOpr(oprStop);
    ofLog(OF_LOG_NOTICE) << '\n' << dump();
    auto res1 = execute4d3i(1.23, 0.0, 0.0, 0.0, 0, 0, 0);
    ofLog(OF_LOG_NOTICE) << res1;

//    (/ x (- 1.0 0.1))
    clear();
    appendOpr(oprPushConst, 0.1);
    appendOpr(oprPushConst, 1.0);
    appendOpr(oprSub, 0.0, 2);
    appendOpr(oprPushX);
    appendOpr(oprDiv, 0.0, 2);
    appendOpr(oprStop);
    ofLog(OF_LOG_NOTICE) << '\n' << dump();
    auto res2 = execute4d3i(1.23, 0.0, 0.0, 0.0, 0, 0, 0);
    ofLog(OF_LOG_NOTICE) << res2;
}

void ScalarRPN::runTestCases() {
    ScalarRPN t1;
    t1.test1();
}
#endif
