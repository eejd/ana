# Configure which OpenGL to use

The visualization server is tested with OpenGL version 3.3 and 4.6.
Most aNa Filters are using GLSL shader code from `#version 330`.

If you get aNa running with your drivers providing OpenGL version 3.3 that OK. You are not forced to update your Hardware to an OpenGL 4.x version.

> :warning: aNa needs an "OpenGL **programmable** rendering pipeline". It can not run on a graphics system with a "fixed rendering pipeline". It can not run on systems lacking OpenGL support completely. But a software emulation like Mesa is OK and may be running slower.

> :information_source: Because aNa uses 3.3 shaders it is yet not able to work with OpenGL 3.1 or 3.2. Search the C++ source code for `#version 330` for back porting to an early OpenGL release.
Background information at khronos.org: [OpenGL and GLSL versions](https://www.khronos.org/opengl/wiki/Core_Language_%28GLSL%29#OpenGL_and_GLSL_versions)

## Detect OpenGL via glxinfo
As root user install glxinfo.
```bash
apt install mesa-utils
```
Use `glxinfo` to find out which OpenGL version is supported by your driver and GPU hardware. Use this values in the configuration file `bin/visual_server.config` for `gl_major` and `gl_major`.

First example 4.6.0 NVIDIA 418.152.00.
- -> Looks like a 4.6 hardware acceleration.

```bash
root@nil:~/ana/live_coding glxinfo | grep OpenGL
OpenGL vendor string: NVIDIA Corporation
OpenGL renderer string: GeForce RTX 2080 Ti/PCIe/SSE2
OpenGL core profile version string: 4.6.0 NVIDIA 418.152.00
OpenGL core profile shading language version string: 4.60 NVIDIA
OpenGL core profile context flags: (none)
OpenGL core profile profile mask: core profile
OpenGL core profile extensions:
OpenGL version string: 4.6.0 NVIDIA 418.152.00
OpenGL shading language version string: 4.60 NVIDIA
OpenGL context flags: (none)
OpenGL profile mask: (none)
OpenGL extensions:
OpenGL ES profile version string: OpenGL ES 3.2 NVIDIA 418.152.00
OpenGL ES profile shading language version string: OpenGL ES GLSL ES 3.20Second example 3.3 Mesa
OpenGL ES profile extensions:
```

Second example Mesa.
- -> Looks like a 3.3 Mesa software emulation.

```bash
strom@anaubuntu:~/ana/visual_server$ glxinfo | grep OpenGL
OpenGL vendor string: VMware, Inc.
OpenGL renderer string: llvmpipe (LLVM 10.0.0, 256 bits)
OpenGL core profile version string: 3.3 (Core Profile) Mesa 20.0.8
OpenGL core profile shading language version string: 3.30
OpenGL core profile context flags: (none)
OpenGL core profile profile mask: core profile
OpenGL core profile extensions:
OpenGL version string: 3.1 Mesa 20.0.8
OpenGL shading language version string: 1.40
OpenGL context flags: (none)
OpenGL extensions:
OpenGL ES profile version string: OpenGL ES 3.1 Mesa 20.0.8
OpenGL ES profile shading language version string: OpenGL ES GLSL ES 3.10
OpenGL ES profile extensions:
```

## Or detect OpenGL via a GUI tool
Use the info viewer provided by your desktop to show the OpenGL version supported by your graphics driver.
![](./stuff/doc/kde-infocenter.png)

## Set which OpenGL can be used
Set `gl_major` and `gl_major` in `bin/visual_server.config`.

### First example 4.6.0 NVIDIA 418.152.00

This is OK
```
gl_major = 3
gl_minor = 3
```
Or set it to the maximum your hardware supports
```
gl_major = 4
gl_minor = 6
```
### Second example Mesa 20.0.8
```
gl_major = 3
gl_minor = 3
```
