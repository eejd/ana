# analog Not analog

aNa is a live coding system for visuals. The aNa display server can mix generative code fragments with different inputs channels in real-time: a WEB cam, a microphone, movie files, image sequences, a joystick or Open Sound Control messages.

![](./stuff/doc/horizontal-cover-1.jpg)

The display server is based on openFrameworks and OpenGL to enable hardware acceleration on the graphics card. For live coding Clojure is used.

The main principles are:
- Use code to express your ideas.
- Modify programs while they are running.
- Try to keep all data of a running program even when parts of program are changed on the fly.

Experimental code. Please do not use in production projects.

aNa is inspired by Olivia Jack’s WEB based application Hydra. If you are interested in Hydra try it out. It runs without installation in a WebGL cable browser and gives very interesting results. https://hydra.ojack.xyz/

## The components of aNa
### Visualization server
The visualization server is an openFrameworks application running in full screen mode waiting for rendering jobs. The screen will be updated 25 times per second. So there is a lot of time before updating the next frame to do heavy calculation jobs. There is no UI  on the server side. Instead the server accepts OSC messages.

### Live coding editor.
For live editing Atom and its prot-repl plugin is used.

### Satellite
There can be different small gadgets orbiting around the visualization server. For example a Raspberry Pi can be used as satellite. Its senseHAT has 8x8 LED matrix that can be controlled by OSC messages. In this case these messages are send per frame from the visualization serverto the Raspberry Pi. Other OSC applications can be used too to control a satellite.

### Network
All subsystems that make up an aNa installation can be distributed to different computers. This enables the collaboration of several people.


## Experimental videos
aNa was developed for making short animation sequences. Mixing real word photography, with generative graphic and real-time video input. Some experimental videos can be found here. https://vimeo.com/user119558788

## Concept
### What is special to aNa
- It comes with an simple Lisp-like dialect to express your visual ideas.
- It uses data-flow between frame buffers in your computes graphics card.
- You write Lisp-like expressions to manipulate the data while they flow from one buffer to another.
- Cycles in the in the data-flow are allowed. This can be used for feedback effects. Speaking in mathematical terms aNa handled a directed graph with cycles.
- aNa can communicate with other application over the network with Open Sound Control (OSC).
- There is a build in “pre shot” screen recorder for fast lossless capturing the animation frames.

### What aNa is NOT
To avoid misunderstandings please note these points.
- aNa is not a Browser based application.
- It is not a visual node editor.
- It has no user interface with buttons and sliders.
- It is not a sound synthesizer.
- It has only limited capabilities to analyses an incoming sound stream.
- The `visual_server` component of aNa is not designed for running inside VirtualBox. But it works on Ubuntu 20.04 inside Virtual Box.

## The aNa language
The aNa language is a very small subset of the Clojure programming language. You can treat it as domain specific programming language (DSL) for live coding animations. Parts of your code will be compiled to OpenGL shaders running in the hardware accelerated graphics pipeline. Other parts are compiled to byte code and are executed an the CPU.
But always it looks like Lisp. The same syntax whether its is interpreted in a Clojure REPL, or compiled to as a fragment shader, or interpreted as byte code.

Read more about the [live coding language](./DSL_for_analog_Not_analog.md).

## Filter types
Filter nodes are processing image data at every single frame.

- They are described in the [List of filter types](./filter_nodes_for_analog_Not_analog.md).

- [List of functions for GPU shader filters.](./gpu_executed_functions.md)

- [List of functions for CPU based filters](./cpu_executed_functions.md)

## aNa examples
#### Generative forms and colors.
- [Animation with patterns on a grid](./live_coding/src/demo/pattern/README.md)

- [Generated content](./live_coding/src/demo/raster-fbo-loop/README.md)

#### Processing image files
- [Animation with 4 static images](./live_coding/src/demo/image-sampler/README.md)

- [Convolution filters](./live_coding/src/demo/convolution/README.md)

#### Processing movie files
- [Overlay of four movie files](./live_coding/src/demo/4movies/README.md)

#### Live camera input
- [A feedback loop running in the frame buffer](./live_coding/src/demo/video-fbo-loop/README.md)

- [A monitor to camera feedback loop](./live_coding/src/demo/monitor-camera-feedback/README.md)

#### Sound input
- [Raspberry Pi SenseHAT](./live_coding/src/demo/sound/README.md)

#### Satellites
- [Raspberry Pi SenseHAT](./live_coding/src/demo/satellite/rpi/README.md)

## Satellites
A satellite will listen to OSC messages to start specific effects. For example change the illumination of a scene that is filmed with an WEB cam. Or trigger digital outputs. Satellites are nice to have. But you can use aNa without them.

## System requirements
aNa was implemented on a gaming PC equipped with a high end graphics card. The C++ Code of the visualization server is based on openFrameworks c0.11.0 or v0.11.2. Currently this visualization server is running on Debian 10 and 11.

The editor has lesser hardware requirements. Atom and proto-repl is used. A headless Clojure instance is running in the background to convert the aAa language to OpenGl shader code.

The Raspberry Pi based satellites are implemented in Python. Keep it simple.

Here is a more detailed list of the [system requirements](./system_requirements.md).

# Steps to get aNa running on your own computer
*analog Not analog* is in a early development state. It is not easy to get this mixture of C++ and Clojure to work. Some hints how build the software are documented below.

You DON'T need identical hardware and software equipment as used by me during development.
You can test aNa on a [virtual machine](./aNa_on_virtual_box.md).

Please contact me if you encounter any errors during the installation. Support address: *ana [at] metagrowing.org*

## 1. Compiling the aNa visualization server
The server subsystem of aNa uses openFrameworks release 0.11 and these plugins: ofxOsc, ofxJoystick, ofxNetwork, ofxOpenCv and optionally ofxMSAOpenCL. In short words. Learn how to compile  openFrameworks on your operating system and test some of its OpenGL examples. Then you are able to compile and run the visualization server.

A more detailed description can be found here: [How to build the visualization server](./build_visualization_server.md).

## 2. Preparing the Atom editor for live coding
You need an editor that provides an interactive Clojure REPL. See: [An editor for live coding](./editor_for_live_coding.md).

## 3. Starting a minimal system on a single PC

### 3.1 Optionally configure OpenGL
The aNa server needs at least OpenGL 3.3. You can configure a newer OpenGL version. [Configure which OpenGL to use](./Configure_OpenGL.md)

### 3.2 Starting the aNa visualization server
Use your Desktops terminal program to start a command-line terminal. On the command-line go to the `visual_server` directory. This is the directory containing the `Makefile`.
```bash
cd visual_server
bin/visual_server bin/analog_srv.config
```
A newly started server shows mostly black; in full screen. Don’t panic. Press ALT TAB key and your desktop manager can bring the command-line terminal or the editor back.

## 3.3 Starting the Editor for a live coding session
Here is a detailed description [how to use Atom](./editor_for_live_coding.md) for live coding together with aNa.

## Working with the visualization server
### Stopping the server
- With your mouse click inside the the display area and press the `ESC` key.
- or press the [] button on your joystick.
- or send an `/ana_quit` OSC message to the servers default port 57220.

### Configuration
Have a look at [visual_server/bin/visual_server.config](./visual_server/bin/visual_server.config). You can build variants of this configuration file for different animations.

### Inspecting errors
In the current version error messages produced by the server can only be found inside the terminal where the server was started. They are not send back to the editor yet.

### Capturing image sequences
aNa comes with a lossless screen capturing feature. But this strategy requires a huge amount of CPU RAM. Please read [pre_shot_camera.md](./pre_shot_camera.md).

Of course you can use your own favorite screen recorder.

### Key codes
> :warning: Only when the visualization server has the focus it can receive events from the keyboard. In most situations key press events are send to the editor.

| key | description |
| --- | --- |
| ESC | Quits the visualization server. |
| Alt F4 | The desktop manager will close the visualization server. |
| f<br>F | Toggles the run time on screen information<br> Prints some infos to stdout. |
| m<br>M | Prints mermaid graph to stdout. |
| r<br>R | Export all frames stored in the preshot ring buffer to disk.<br>This will pause the animation for several minutes.<br>see: [pre shot camera](.pre_shot_camera.md) |
| z | Resets the frame counter to 0<br>and rewinds the sound file. |
| Z | Resets the frame counter to 0,<br>rewinds the sound file<br>and clears the preshot ring buffer. |


## Copyright
### Source code GPL 3.0 Copyleft
Copyright © 2020 Thomas Jourdan
This source code is licensed under the [GNU General Public License v3.0](./LICENSE).

### Images and animations
All images and movies in this repository are licensed as creative commons under the following terms: [CC BY-NC 3.0](https://creativecommons.org/licenses/by-nc/3.0/)

## Credits

### aNa is running on top of openFrameworks
openFrameworks is distributed under the [MIT License](https://en.wikipedia.org/wiki/MIT_License). This gives everyone the freedoms to use openFrameworks in any context: commercial or non-commercial, public or private, open or closed source.

### openFrameworks ofxAddons used in aNa
  - ofxOsc is part of the openFrameworks
  - ofxNetwork is part of the openFrameworks
  - ofxOpenCv is part of the openFrameworks
  - ofxMSAOpenCL and ofxMSAPingPong
    - Autor: Memo Akten
    - https://github.com/memo/ofxMSAOpenCL
    - MIT License
  - ofxMidi
    - Autor: Dan Wilcox 2011-2020
    - https://github.com/danomatika/ofxMidi
    - BSD Simplified License.
  - ofxCV
    - Autor: Kyle McDonald
    - https://github.com/kylemcdonald/ofxCv.git
    - MIT License

### Atkinson Hyperlegible Font
This software bundles comes with ATKINSON HYPERLEGIBLE FONT
Copyright ©  2020, Braille  Institute  of  America,  Inc.,  https://www.brailleinstitute.org/freefont with Reserved Typeface Name Atkinson Hyperlegible Font.
