float edist(int2 a, float2 b){
    float2 d = (float2)((a.x - b.x), (a.y - b.y));
//     return d.x * d.x + d.y * d.y;
      return fabs(d.x) + fabs(d.y);
}

float3 hsv2rgb(float3 c)
{
    float4 K = (float4)(1.0f, 2.0f / 3.0f, 1.0f / 3.0f, 3.0f);
    float3 dummy;
    float3 p = fabs(fract(c.xxx + K.xyz, &dummy) * (float3)(6.0, 6.0, 6.0) - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, (float3)(0.0, 0.0, 0.0), (float3)(1.0, 1.0, 1.0)), c.y);
}

__kernel void image_change(const int npoints,
                        __constant float8 * fv,
                        __write_only image2d_t iout){
    const int2 pos = (int2)(get_global_id(0), get_global_id(1));
    float min_dist = edist(pos, fv[0].xy + (float2)(540,540));
    int min0_at_i = 0;
    int min1_at_i = 0;
    int min2_at_i = 0;
    for(int i = 1; i < npoints; ++i) {
        float dist = edist(pos, fv[i].xy + (float2)(540,540));
        if(dist < min_dist) {
            min2_at_i = min1_at_i;
            min1_at_i = min0_at_i;
            min_dist = dist;
            min0_at_i = i;
        }
    }

    float3 rgb0 = hsv2rgb(fv[min0_at_i].s456);
    float3 rgb1 = hsv2rgb(fv[min1_at_i].s456);
    float3 rgb2 = hsv2rgb(fv[min2_at_i].s456);
    write_imagef(iout, pos, (float4)(0.6*rgb0.x + 0.3*rgb1.x + 0.1*rgb2.x,
                                     0.6*rgb0.y + 0.3*rgb1.y + 0.1*rgb2.y,
                                     0.6*rgb0.z + 0.3*rgb1.z + 0.1*rgb2.z,
                                     1));
}

__kernel void image_setup(const int npoints,
                        __constant float8 * fv,
                        __write_only image2d_t iout){
}
