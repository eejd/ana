#include "../ana_features.h"
#ifdef WITH_openCL
#include <random>

#include "ofMain.h"
#include "MSAOpenCL.h"
#include "ofApp.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispMng.h"
#include "cl/ClImageGenerator.h"
#include "globalvars.h"

ClImageGenerator::ClImageGenerator(ofGLWindowSettings settings, OscListener * oscParams) :
ClFilter(settings, oscParams) {
}

ClImageGenerator::~ClImageGenerator() {
}

size_t ClImageGenerator::getKernelWidth() {
    return filterWidth;
}

size_t ClImageGenerator::getKernelHeight() {
    return filterHeight;
}

void ClImageGenerator::setup(const std::string id, int numSamples) {
    Filter::setup(id, numSamples);

    clOutFbo.allocate(getKernelWidth(), getKernelHeight(), GL_RGBA32F_ARB);
    clOutFbo.begin();
    ofClear(0, 0, 0, 255);
    clOutFbo.end();
    clOutImage.initFromTexture(clOutFbo.getTexture());

    const size_t buflen = getKernelWidth() * getKernelHeight();
    clCountersR.initBuffer(buflen * sizeof(int), CL_MEM_READ_WRITE);
    clCountersG.initBuffer(buflen * sizeof(int), CL_MEM_READ_WRITE);
    clCountersB.initBuffer(buflen * sizeof(int), CL_MEM_READ_WRITE);

    clGaussian.initBuffer(2 * buflen * sizeof(float), CL_MEM_READ_ONLY);
    float * ga = floats.gaussian(2 * buflen);
    clGaussian.write(ga, 0, 2 * buflen * sizeof(float));
    delete[] ga;

    clUniform.initBuffer(2 * buflen * sizeof(float), CL_MEM_READ_ONLY);
    ga = floats.uniform(0, 1, 2 * buflen);
    clUniform.write(ga, 0, 2 * buflen * sizeof(float));
    delete[] ga;
}

void ClImageGenerator::reload(ofxOscMessage const &msg) {
    ClFilter::reload(msg);

    ScalarLispMng mng;
    mng.compile(slLoops, "loops", "1", msg);
    mng.compile(slP0, "p0", "1.0", msg);
    mng.compile(slP1, "p1", "1.0", msg);
    mng.compile(slP2, "p2", "1.0", msg);
    mng.compile(slP3, "p3", "1.0", msg);
    mng.compile(slP4, "p4", "1.0", msg);
    mng.compile(slP5, "p5", "1.0", msg);
    mng.compile(slP6, "p6", "1.0", msg);
    mng.compile(slP7, "p7", "1.0", msg);

    hasImageInput = false;
}

bool ClImageGenerator::reloadClProg(const bool force) {
    if(fatalClProg)
        return false;

    bool reloaded = ClFilter::reloadClProg(force);
    if(reloaded) {
        set_zero_kernel = pcl->loadKernel("set_zero");
        map_r2_kernel = pcl->loadKernel("map_r2");
        write_image_kernel = pcl->loadKernel("write_image");
    }
    return reloaded;
}

bool ClImageGenerator::isSaveForKernel() {
    return !fatalClProg && !clProgName.empty() && hasImageInput;
}


void ClImageGenerator::update(){
    if(fatalClProg || clProgName.empty())
        return;

    glFinish();
    if(upstream_ids.size() >= 1) {
        auto in_id = upstream_ids[0];
        if(filterDict.find(in_id) != filterDict.end()) {
            auto f = filterDict[in_id];
            if(f && !hasImageInput) {
                clImageInput.initFromTexture(f->getTexture());
                hasImageInput = true;
            }
        }
    }

    reloadClProg(false);
    if(!fatalClProg) {
        int arg = 0;
        set_zero_kernel->setArg(arg++, clCountersR);
        set_zero_kernel->setArg(arg++, clCountersG);
        set_zero_kernel->setArg(arg++, clCountersB);
        set_zero_kernel->run2D(getKernelWidth(), getKernelHeight());

        arg = 0;
        map_r2_kernel->setArg(arg++, clCountersR);
        map_r2_kernel->setArg(arg++, clCountersG);
        map_r2_kernel->setArg(arg++, clCountersB);
        map_r2_kernel->setArg(arg++, clImageInput);
        map_r2_kernel->setArg(arg++, clGaussian);
        map_r2_kernel->setArg(arg++, clUniform);
        map_r2_kernel->setArg(arg++, (float)slP0.rpn.execute0());
        map_r2_kernel->setArg(arg++, (float)slP1.rpn.execute0());
        map_r2_kernel->setArg(arg++, (float)slP2.rpn.execute0());
        map_r2_kernel->setArg(arg++, (float)slP3.rpn.execute0());
        map_r2_kernel->setArg(arg++, (float)slP4.rpn.execute0());
        map_r2_kernel->setArg(arg++, (float)slP5.rpn.execute0());
        map_r2_kernel->setArg(arg++, (float)slP6.rpn.execute0());
        map_r2_kernel->setArg(arg++, (float)slP7.rpn.execute0());
        map_r2_kernel->setArg(arg++, dt0);
        map_r2_kernel->setArg(arg++, dt1);
        map_r2_kernel->setArg(arg++, dt2);
        map_r2_kernel->setArg(arg++, dt3);
        map_r2_kernel->setArg(arg++, dt4);
        map_r2_kernel->setArg(arg++, dt5);
        map_r2_kernel->setArg(arg++, dt6);
        map_r2_kernel->setArg(arg++, dt7);
        map_r2_kernel->run2D(getKernelWidth(), getKernelHeight());

        arg = 0;
        write_image_kernel->setArg(arg++, clCountersR);
        write_image_kernel->setArg(arg++, clCountersG);
        write_image_kernel->setArg(arg++, clCountersB);
        write_image_kernel->setArg(arg++, clOutImage);
        write_image_kernel->run2D(getKernelWidth(), getKernelHeight());
    }
}

void ClImageGenerator::draw(int li) {
    // make sure all OpenCL kernels have finished executing before drawing
    openCL.finish();

    fbo.begin();
    if(isSaveForKernel()) {
        clOutImage.draw(0, 0);
    } else {
        ofClear(0, 0, 0, 255);
    }
    fbo.end();
}
#endif // WITH_openCL
