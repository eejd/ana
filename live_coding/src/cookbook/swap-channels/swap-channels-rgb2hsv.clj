(ns lv.demo)
(use 'lv.core)

(layout "grid" [img0 img1]
               [img2 ras3])
; assume there are 4 PNG images in the media folder.
(image "flower-sampler/*.png")
(render)

; Red, green and blue channels are assigned to hue, saturation and value.
(hsv :img0 [c0 c1 c2 c3]
  (hsv c0.rgb)
)

; Playing with the equations for HSV output.
; Inputs from two different images and different color models.
(hsv :img1 [c0 c1 c2 c3]
  (h (+ 0.13 c1.r c1.b))
  (s (* 0.64 c1.s))
  (v c1.g)
)

; Keep the hue channel.
; But patch saturation and value.
(hsv :img2 [c0 c1 c2 c3]
  (h c2.h)
  (s c2.b)
  (v c2.r)
)

; Mix the output of node img2 with itself.
; But use different color models.
(hsv :ras3 [img2]
  (hsv (mix img2.hsv img2.rgb 0.78))
)
