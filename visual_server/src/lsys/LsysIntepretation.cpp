#include "ana_features.h"
#include "lsys/LsysIntepretation.h"

LsysIntepretation::LsysIntepretation() {
}

LsysIntepretation::~LsysIntepretation() {
}

/**
 * Initialize transformation from word coordinates to pixel coordinates.
 *
 * @param borderFactor factor for the size of a surrounding area.
 */
void LsysIntepretation::initTransformation() {
    turtle_diffX = turtle_xmax - turtle_xmin;
    turtle_diffY = turtle_ymax - turtle_ymin;
    canvas_xmin = turtle_xmin - turtle_diffX * borderFactor;
    canvas_xmax = turtle_xmax + turtle_diffX * borderFactor;
    canvas_ymin = turtle_ymin - turtle_diffY * borderFactor;
    canvas_ymax = turtle_ymax + turtle_diffY * borderFactor;
    turtle_diffX = std::max(0.1f, canvas_xmax - canvas_xmin);
    turtle_diffY = std::max(0.1f, canvas_ymax - canvas_ymin);

    xscale_to_canvas = canvas_width  / turtle_diffX;
    yscale_to_canvas = canvas_height / turtle_diffY;
    scale_to_canvas = std::min(xscale_to_canvas, yscale_to_canvas);
    center_x = 0.5 * (turtle_xmax + turtle_xmin) * scale_to_canvas;
    center_y = 0.5 * (turtle_ymax + turtle_ymin) * scale_to_canvas;
}

void LsysIntepretation::initCanvas(int canvas_width0, int canvas_height0,
                                   float borderFactor0) {
    canvas_width = canvas_width0;
    canvas_height = canvas_height0;
    borderFactor = borderFactor0;
    turtle_xmax = turtle_ymax = -1 * std::numeric_limits<float>::infinity();
    turtle_xmin = turtle_ymin = std::numeric_limits<float>::infinity();
}

void LsysIntepretation::reset_minmax() {
    if(!expand_only) {
        turtle_xmax = turtle_ymax = -1 * std::numeric_limits<float>::infinity();
        turtle_xmin = turtle_ymin = std::numeric_limits<float>::infinity();
    }
}


