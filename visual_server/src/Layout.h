#pragma once
#include "Filter.h"

class Layout {
public:
    virtual ~Layout();
    virtual int numFilters() = 0;
    virtual void draw(int render, std::vector<std::shared_ptr<Filter>> &filterLine) = 0;
};
