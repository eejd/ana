#pragma once
#include "ana_features.h"
#ifdef WITH_js_dev_input

#include "ofMain.h"

#include <stdexcept>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <linux/input.h>
#include <linux/joystick.h>
#include <iostream>
extern "C" {
#include "axbtnmap.h"
}

class JoystickDevice {

public:
    JoystickDevice();
    ~JoystickDevice();

    void setup(std::string device);
    uint32_t countAxes();
    uint32_t countButtons();
    std::string getName();
    void update();
    bool isButtonPressed(uint32_t n);
    bool isRisingEdge(uint32_t n);
    float readAxis(uint32_t n);
    void logAxes();
    void logButtons();
    void logPressedButtons();

private:
    void clear();
    void reset();

    int version;
    std::string name;
    uint32_t axis_count = 0;
    uint32_t button_count = 0;
    uint16_t btnmap[BTNMAP_SIZE];
    uint8_t axmap[AXMAP_SIZE];
    float *axes = nullptr;
    unsigned char *button_state = nullptr;
    unsigned char *button_last_rising = nullptr;
    struct js_event js;
    int fd;
};
#endif // WITH_js_dev_input
