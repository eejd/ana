(ns lv.demo)
(use 'lv.core)

(layout "grid" [img0 img1]
               [ras2 ras3])
(image "flower-pages/*.png")

(texpr
  ; use nanoKONTROL2 als input
  (t0 (- 1 (* 2 ks0)))
  (t1 (- 1 (* 2 ks1)))
  (t2 (- 1 (* 2 ks2)))
  (t3 (- 1 (* 2 ks3)))
  ; use a game pad als input
  ; (t0 lx)
  ; (t1 ly)
  ; (t2 rx)
  ; (t3 ry)
)

(rgb :img0 [c0 c1 c2]
  (kaleid (+ 3 (sin (* 0.0067 f))))
  (rot (* 0.0071 f))
  (kaleid (+ 2 (sin (* 0.0061 f))))
  (rgb (mix (mix c0.rgb
                 c1.rgb
                 (usin (* 0.051 f)))
            c2.rgb
            (unoise (* 0.007 f) x)))
)

(rgb :img1 [c0 c1 c2]
  (rot (* -0.005 f))
  (rgb (mix (mix c2.rgb
                 c1.rgb
                 (usin (* 0.023 f)))
            c0.rgb
            (unoise (* 0.031 f) y)))
)

(rgb :ras2 [img0 img1]
  (rgb (* 1 (mix img0.rgb
            img1.rgb
            (unoise (* 0.019 f) r))))
)

(inspect [ras2]
  "color2sound/analyse_dualhead_color.cl"
  (x0 t0) (yl t1)
  (r0 (* 0.2 kp0))
  (x1 t2)  (yr t3)
  (r1 (* 0.2 kp1))
)

; (hsv :ras3 []
;   (v (* (distance (vec2 x y)
;                   (vec2 t0 t1))
;         (distance (vec2 x y)
;                   (vec2 t2 t3))))
; )
