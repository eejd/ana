#include <math.h>

#include "ofMain.h"

#include "ana_features.h"
#include "color/Palette.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "lsys/LsysTurtle.h"
#include "lsys/LsysIntepretation.h"

//#define TRACE_MONOTONIC
#undef  TRACE_MONOTONIC

LsysTurtle::LsysTurtle() : LsysIntepretation() {
}

LsysTurtle::~LsysTurtle() {
}

void LsysTurtle::init(size_t length0, char* string0, ScalarLisp** sl0) {
    turtle_length = length0;
    turtle_string = string0;
    turtle_sl = sl0;
    turtle_xmax = turtle_ymax = 0.1f;
    turtle_xmin = turtle_ymin = -0.1f;
    canvas_width = 1;
    canvas_height = 1;
    borderFactor = 0.01;
    initTransformation();
    stackMessageSend = false;
}

void LsysTurtle::draw() {
#ifdef TRACE_MONOTONIC
    timespec time1, time2;
    clock_gettime(CLOCK_MONOTONIC, &time1);
#endif // TRACE_MONOTONIC

    ofClear(0, 255);

    int character = turtle_string[0];
    float turtlePosOldX = 0;
    float turtlePosOldY = 0;
//    float oldX = 0, oldY = 0;
    int nPoints[maxDepthLeafStack];
    branchTOS = 0;
    leafTOS = 0;
    colorIndex = 0;

    turtle_direction = start_direction;
    turtle_x = turtle_y = 0.0;
    nPoints[leafTOS] = 0;

    ofPushMatrix();
    ofTranslate(0.5f*canvas_width - center_x, 0.5f*canvas_height - center_y);
    ofScale(scale_to_canvas);

    size_t ci = 0;
    while(ci < turtle_length) {
        nextState(ci);
        character = turtle_string[ci++];
        if(character == 'F') {
            ofSetColor(lut.at(colorIndex));
            ofDrawLine(turtlePosOldX, turtlePosOldY, turtle_x, turtle_y);
            turtlePosOldX = turtle_x;
            turtlePosOldY = turtle_y;
        } else if(character == 'f' || character == ']') {
            turtlePosOldX = turtle_x;
            turtlePosOldY = turtle_y;
        } else if(character == '{') {
            nPoints[leafTOS] = 0;
            ++leafTOS;
        } else if(character == '}') {
            if(leafTOS > 0 && leafTOS < maxDepthLeafStack) {
                --leafTOS;
                if(nPoints[leafTOS] > 2) {
                    ofSetColor(lut.at(colorIndex));
//                    if(false) {  //TODO
                        ofBeginShape();
                        for(int pi = 0; pi < nPoints[leafTOS]; ++pi) {
                            ofVertex(xPolyPoints[leafTOS][pi], yPolyPoints[leafTOS][pi]);
                        }
                        ofEndShape(true);
//                    }
//                    else {
                        ofPolyline pl;
                        for(int pi = 0; pi < nPoints[leafTOS]; ++pi) {
                            pl.addVertex(xPolyPoints[leafTOS][pi], yPolyPoints[leafTOS][pi]);
                        }
                        pl.addVertex(xPolyPoints[leafTOS][0], yPolyPoints[leafTOS][0]);
                        pl.draw();
//                    }
                    nPoints[leafTOS] = 0;
                }
            }
        } else if(character == '.') {
            if(leafTOS > 0 && leafTOS <= maxDepthLeafStack) {
                size_t lt = leafTOS-1;
                if(nPoints[lt] < maxPolyPoints) {
                    xPolyPoints[lt][nPoints[lt]] = turtle_x;
                    yPolyPoints[lt][nPoints[lt]] = turtle_y;
                    ++nPoints[lt];
                }
            }
        }
    }

    ofPopMatrix();
#ifdef TRACE_MONOTONIC
    clock_gettime(CLOCK_MONOTONIC, &time2);
    ofLog(OF_LOG_NOTICE) << "turtle " << (time2.tv_sec * 1000000000l + time2.tv_nsec) - (time1.tv_sec * 1000000000l + time1.tv_nsec);
#endif // TRACE_MONOTONIC
}

/**
 * Calculates the bounds.
 */
void LsysTurtle::expandBoundigBox() {
#ifdef TRACE_MONOTONIC
    timespec time1, time2;
    clock_gettime(CLOCK_MONOTONIC, &time1);
#endif // TRACE_MONOTONIC
    size_t ci = 0;
    branchTOS = 0;
    leafTOS = 0;
    reset_minmax();
    turtle_direction = start_direction;
    turtle_x = turtle_y = 0.0;
    turtle_xmin = std::min(turtle_x, turtle_xmin);
    turtle_xmax = std::max(turtle_x, turtle_xmax);
    turtle_ymin = std::min(turtle_y, turtle_ymin);
    turtle_ymax = std::max(turtle_y, turtle_ymax);

    while (ci < turtle_length) {
        char character = turtle_string[ci];
        if (character == 'F'
            || character == 'f'
            || character == '+'
            || character == '-'
            || character == '['
            || character == ']'
            || character == '|'
            || character == '{'
            || character == '}'
            || character == '.') {
            nextState(ci);
            if (character == 'F' || character == 'f') {
                turtle_xmin = std::min(turtle_x, turtle_xmin);
                turtle_xmax = std::max(turtle_x, turtle_xmax);
                turtle_ymin = std::min(turtle_y, turtle_ymin);
                turtle_ymax = std::max(turtle_y, turtle_ymax);
            }
        }
        ++ci;
    }
    initTransformation();
#ifdef TRACE_MONOTONIC
    clock_gettime(CLOCK_MONOTONIC, &time2);
    ofLog(OF_LOG_NOTICE) << "expandBoundigBox " << (time2.tv_sec * 1000000000l + time2.tv_nsec) - (time1.tv_sec * 1000000000l + time1.tv_nsec);
#endif // TRACE_MONOTONIC
}


/**
* Next turtle state.
* @param character
*/
void LsysTurtle::nextState(size_t ci) {
    //TODO character == '.'
    switch(turtle_string[ci]) {
    case 'F':
    case 'f': {
        if(turtle_sl[ci] == nullptr) {
            float r = slForward.rpn.execute3d1i(turtle_x, turtle_y, 0.0, branchTOS);
            turtle_x += r * std::cos(turtle_direction);
            turtle_y += r * std::sin(turtle_direction);
        }
        else {
            float r = turtle_sl[ci]->rpn.execute3d1i(turtle_x, turtle_y, 0.0, branchTOS);
            turtle_x += r * std::cos(turtle_direction);
            turtle_y += r * std::sin(turtle_direction);
        }
        return;
    }
    case '+': {
        if(turtle_sl[ci] == nullptr) {
            turtle_direction += slAngel.rpn.execute3d1i(turtle_x, turtle_y, 0.0, branchTOS);;
        }
        else {
            turtle_direction += turtle_sl[ci]->rpn.execute3d1i(turtle_x, turtle_y, 0.0, branchTOS);
        }
        return;
    }
    case '-':
        if(turtle_sl[ci] == nullptr) {
            turtle_direction -= slAngel.rpn.execute3d1i(turtle_x, turtle_y, 0.0, branchTOS);;
        }
        else {
            turtle_direction -= turtle_sl[ci]->rpn.execute3d1i(turtle_x, turtle_y, 0.0, branchTOS);
        }
        return;
    case '|':
        turtle_direction += M_PI;
        return;
    case '[':
        if(branchTOS >= maxDepthBranchStack) {
            if(!stackMessageSend) {
                ofLog(OF_LOG_ERROR) << "stack overflow";
                stackMessageSend = true;
            }
        } else {
            turtleStackX[branchTOS] = turtle_x;
            turtleStackY[branchTOS] = turtle_y;
            turtleStackDir[branchTOS] = turtle_direction;
            turtleStackColor[branchTOS] = colorIndex;
            ++branchTOS;
        }
        return;
    case ']':
        if(branchTOS == 0) {
            if(!stackMessageSend) {
                ofLog(OF_LOG_ERROR) << "stack underflow";
                stackMessageSend = true;
            }
        } else
            --branchTOS;
        turtle_x = turtleStackX[branchTOS];
        turtle_y = turtleStackY[branchTOS];
        turtle_direction = turtleStackDir[branchTOS];
        colorIndex = turtleStackColor[branchTOS];
        return;
    case '\'':
        if(turtle_sl[ci])
            colorIndex += turtle_sl[ci]->rpn.execute3d1i(turtle_x, turtle_y, 0.0, branchTOS);
        else
            ++colorIndex;
        return;
    case ',':
        if(turtle_sl[ci])
            colorIndex -= turtle_sl[ci]->rpn.execute3d1i(turtle_x, turtle_y, 0.0, branchTOS);
        else
            --colorIndex;
        return;
    }
}
