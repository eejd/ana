#include "../ana_features.h"
#ifdef WITH_openCL

#include "ofMain.h"
#include "MSAOpenCL.h"
#include "stringhelper.h"
#include "ofApp.h"
#include "globalvars.h"
#include "calc/FloatArrays.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispMng.h"
#include "cl/ClVerticesPingPong.h"

ClVerticesPingPong::ClVerticesPingPong(ofGLWindowSettings settings, OscListener * oscParams) :
ClFilter(settings, oscParams) {
}

ClVerticesPingPong::~ClVerticesPingPong() {
    buildin_shader.unload();
    releaseBuffers();
}

//static std::string simple2dVert = STRINGIFY(#version 330\n
//in vec4 position;\n
//in vec4 color;\n
//out vec4 colorVarying;\n
//void main()\n
//{\n
//    colorVarying = color;\n
//    gl_Position = vec4(position.xy*2.0-1.0, 0.0, 1.0);\n
//}\n
//);
//
//static std::string simple2dFrag = STRINGIFY(#version 330\n
//in vec4 colorVarying;\n
//out vec4 outColor;\n
//void main()\n
//{\n
//    outColor = colorVarying;\n
//}\n
//);

static std::string phongVert = STRINGIFY(#version 330\n
uniform mat4 modelViewMatrix;\n
uniform mat4 projectionMatrix;\n
uniform mat4 modelViewProjectionMatrix;\n
uniform mat4 normalMatrix;\n
uniform vec3 lightPosition = vec3(-100.0, 100.0, 100.0);\n
in vec4 position;\n
in vec4 normal;\n
in vec4 color;\n
smooth out vec3 lightDirectionVarying;\n
smooth out vec3 normalVarying;\n
smooth out vec3 colorVarying;\n
void main()\n
{\n
    normalVarying = normalize(normalMatrix * normal).xyz;\n
//    normalVarying = normalize(normalMatrix * vec4(0, 0, 1, 1)).xyz;\n
    vec4 position4 = modelViewMatrix * position;\n
    vec3 position3 = position4.xyz / position4.w;\n
    lightDirectionVarying = normalize(lightPosition - position3);\n
    colorVarying = color.rgb;\n
    gl_Position = modelViewProjectionMatrix * position;\n
}\n
);

static std::string phongFrag = STRINGIFY(#version 330\n
uniform vec3 ambientColor  = vec3(0.10, 0.10, 0.15);\n
uniform vec3 specularColor = vec3(0.6, 0.7, 0.5);\n
in vec3 lightDirectionVarying;\n
in vec3 normalVarying;\n
in vec3 colorVarying;\n
out vec4 outputColor;\n
void main()\n
{\n
    vec3 nn = normalize(normalVarying);\n
    float diffuse = dot(nn, normalize(lightDirectionVarying));\n
    if(diffuse > 0.0) {\n
        vec3 reflectionDirection = normalize(reflect(-normalize(lightDirectionVarying), nn));\n
        float specular = dot(nn, reflectionDirection);\n
        if(specular > 0.0) {\n
            outputColor = vec4(ambientColor + diffuse * colorVarying + pow(specular, 128.0) * specularColor, 1);\n
        } else {\n
            outputColor = vec4(ambientColor + diffuse * colorVarying, 1);\n
        }\n
    } else {\n
        /* dark side of the object, only ambient color */\n
        outputColor = vec4(ambientColor, 1);\n
    }\n
}\n
);

void ClVerticesPingPong::allocateBuffers(size_t al, size_t tl) {
    if((agent_length != al || trace_length != tl) && al > 0  && tl > 0) {
        agent_length = al;
        trace_length = tl;
        releaseBuffers();

        glGenVertexArrays(1, &vertexArrayID);
        glBindVertexArray(vertexArrayID);
        glGenBuffers(1, &vertexbufferID);
        glBindBuffer(GL_ARRAY_BUFFER, vertexbufferID);
        glBufferData(GL_ARRAY_BUFFER, agent_length * trace_length * per_vert_stride * sizeof(GLfloat), 0, GL_DYNAMIC_DRAW);
        // Specify the layout of the vertex data
        posAttrib = glGetAttribLocation(progID, "position");
        glEnableVertexAttribArray(posAttrib);
        glVertexAttribPointer(posAttrib,           // attribute
                4,                   // size
                GL_FLOAT,            // type
                GL_FALSE,            // normalized
                per_vert_stride * sizeof(GLfloat), // stride
                0);
        normalAttrib = glGetAttribLocation(progID, "normal");
        glEnableVertexAttribArray(normalAttrib);
        glVertexAttribPointer(normalAttrib,           // attribute
                4,                   // size
                GL_FLOAT,            // type
                GL_FALSE,            // normalized
                per_vert_stride * sizeof(GLfloat), // stride
                (void*) (4 * sizeof(GLfloat)));
        colorAttrib = glGetAttribLocation(progID, "color");
        glEnableVertexAttribArray(colorAttrib);
        glVertexAttribPointer(colorAttrib,           // attribute
                4,                   // size
                GL_FLOAT,            // type
                GL_FALSE,            // normalized
                per_vert_stride * sizeof(GLfloat), // stride
                (void*) (8 * sizeof(GLfloat)));
        glBindVertexArray(0);
        clAgentAttributeBuffer.initBuffer(agent_length * sizeof(AgentAttributes), CL_MEM_READ_WRITE);
        clAgentStateBuffers[0].initBuffer(agent_length * sizeof(AgentState), CL_MEM_READ_WRITE);
        clAgentStateBuffers[1].initBuffer(agent_length * sizeof(AgentState), CL_MEM_READ_WRITE);
        traceBuffer.initFromGLObject(vertexbufferID, agent_length * trace_length * 16);
    }
}

void ClVerticesPingPong::releaseBuffers() {
    clReleaseMemObject(clAgentAttributeBuffer.getCLMem());
    clReleaseMemObject(clAgentStateBuffers[0].getCLMem());
    clReleaseMemObject(clAgentStateBuffers[1].getCLMem());
    clReleaseMemObject(traceBuffer.getCLMem());
    glDeleteBuffers(1, &vertexbufferID);
    glDeleteVertexArrays(1, &vertexArrayID);
}

// see: addons/ofxMSAOpenCL/example-Particles/src/
void ClVerticesPingPong::setup(const std::string id, int numSamples) {
    Filter::setup(id, numSamples);

//    std::string okV = buildin_shader.setupShaderFromSource(GL_VERTEX_SHADER, simple2dVert) ? "OK" : "failed";
//    std::string okF = buildin_shader.setupShaderFromSource(GL_FRAGMENT_SHADER, simple2dFrag) ? "OK" : "failed";
    std::string okV = buildin_shader.setupShaderFromSource(GL_VERTEX_SHADER, phongVert) ? "OK" : "failed";
    std::string okF = buildin_shader.setupShaderFromSource(GL_FRAGMENT_SHADER, phongFrag) ? "OK" : "failed";
    std::string okP = buildin_shader.linkProgram() ? "OK" : "failed";
    ofLog(OF_LOG_NOTICE) << "shader compiler " << id << ": vertex=" << okV << " fragment=" << okF <<  " link=" << okP;
    progID = buildin_shader.getProgram();
//    ofLog(OF_LOG_NOTICE) << buildin_shader.source;
    buildin_shader.printActiveUniforms();
    buildin_shader.printActiveAttributes();

    allocateBuffers(32, 32);
    pingPongIndex = 0;
}

void ClVerticesPingPong::reload(ofxOscMessage const &msg) {
    ClFilter::reload(msg);

    ScalarLispMng mng;
    mng.compile(slAl, "al", "32", msg);
    mng.compile(slTl, "tl", "32", msg);
    mng.compile(slLightPositionX, "lpx", "-100", msg);
    mng.compile(slLightPositionY, "lpy", "100", msg);
    mng.compile(slLightPositionZ, "lpz", "100", msg);
    mng.compile(slRx, "rx", "0", msg);
    mng.compile(slRy, "ry", "0", msg);
    mng.compile(slRz, "rz", "0", msg);
    mng.compile(slP0, "p0", "1.0", msg);
    mng.compile(slP1, "p1", "1.0", msg);
    mng.compile(slP2, "p2", "1.0", msg);
    mng.compile(slP3, "p3", "1.0", msg);
    mng.compile(slP4, "p4", "1.0", msg);
    mng.compile(slP5, "p5", "1.0", msg);
    mng.compile(slP6, "p6", "1.0", msg);
    mng.compile(slP7, "p7", "1.0", msg);

    allocateBuffers(slAl.rpn.execute0(), slTl.rpn.execute0());
    pingPongIndex = 0;
    run_ag_kernel(ag_setup_kernel);
    pingPongIndex = 1;
    openCL.finish();
}

bool ClVerticesPingPong::reloadClProg(const bool force) {
    if(fatalClProg)
        return false;

    bool reloaded = ClFilter::reloadClProg(force);
    if(reloaded) {
        ag_setup_kernel = pcl->loadKernel("ag_setup");
        ag_move_kernel = pcl->loadKernel("ag_move");
    }
    return reloaded;
}

ofTexture& ClVerticesPingPong::getTexture() {
    openCL.finish();
    return fbo.getTexture();
}

void ClVerticesPingPong::run_ag_kernel(msa::OpenCLKernelPtr kernel) {
//    printf("%d run_ag_kernel\n", pingPongIndex);
    int arg = 0;
    kernel->setArg(arg++, (cl_int)filterWidth);
    kernel->setArg(arg++, (cl_int)filterHeight);
    kernel->setArg(arg++, (cl_int)agent_length);
    kernel->setArg(arg++, (cl_int)trace_length);
    kernel->setArg(arg++, (cl_int)trace_index);

    kernel->setArg(arg++, clAgentStateBuffers[pingPongIndex]);
    kernel->setArg(arg++, clAgentStateBuffers[1 - pingPongIndex]);
    kernel->setArg(arg++, clAgentAttributeBuffer);
    kernel->setArg(arg++, traceBuffer);

    kernel->setArg(arg++, (float)p0);
    kernel->setArg(arg++, (float)p1);
    kernel->setArg(arg++, (float)p2);
    kernel->setArg(arg++, (float)p3);
    kernel->setArg(arg++, (float)p4);
    kernel->setArg(arg++, (float)p5);
    kernel->setArg(arg++, (float)p6);
    kernel->setArg(arg++, (float)p7);
    kernel->run1D(agent_length);
}

void ClVerticesPingPong::update(){
    if(fatalClProg || clProgName.empty())
        return;

    p0 = slP0.rpn.execute0();
    p1 = slP1.rpn.execute0();
    p2 = slP2.rpn.execute0();
    p3 = slP3.rpn.execute0();
    p4 = slP4.rpn.execute0();
    p5 = slP5.rpn.execute0();
    p6 = slP6.rpn.execute0();
    p7 = slP7.rpn.execute0();

    if(reloadClProg(false)) {
        pingPongIndex = 0;
        run_ag_kernel(ag_setup_kernel);
        pingPongIndex = 1;
    }
    else {
        glFinish();
    }

    run_ag_kernel(ag_move_kernel);
    trace_index = (trace_index + 1) % trace_length;
    pingPongIndex = 1 - pingPongIndex;
}

void ClVerticesPingPong::draw(int li) {
    // make sure all OpenCL kernels have finished executing before drawing
    openCL.finish();

    fbo.begin();
    if(fatalClProg || clProgName.empty()) {
        ofClear(0, 0, 0, 255);
    } else {
        if(cam) {
            float cam_rx = slRx.rpn.execute0();
            float cam_ry = slRy.rpn.execute0();
            float cam_rz = slRz.rpn.execute0();
            const float r = 10;
            auto gp0 = (glm::rotateX(glm::vec3(r, r, r), cam_rx));
            auto gp1 = (glm::rotateY(gp0, cam_ry));
            auto gp2 = (glm::rotateZ(gp1, cam_rz)); //TODO rotateY twice or rotateZ ?
            cam->setGlobalPosition(gp2);
            cam->lookAt(glm::vec3(0.0));
            cam->begin();

            glEnable(GL_DEPTH_TEST);
            glDepthFunc(GL_LESS);
//            glEnable(GL_CULL_FACE);

            glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            glUseProgram(progID);

            glm::mat4 modelViewMatrix = cam->getModelViewMatrix();
            GLuint modelViewMatrixID = glGetUniformLocation(progID, "modelViewMatrix");
            glUniformMatrix4fv(modelViewMatrixID, 1, GL_FALSE, &modelViewMatrix[0][0]);

            glm::mat4 projectionMatrix = cam->getProjectionMatrix();
            GLuint projectionMatrixID = glGetUniformLocation(progID, "projectionMatrix");
            glUniformMatrix4fv(projectionMatrixID, 1, GL_FALSE, &projectionMatrix[0][0]);

            glm::mat4 modelViewProjectionMatrix = cam->getModelViewProjectionMatrix();
            GLuint modelViewProjectionMatrixID = glGetUniformLocation(progID, "modelViewProjectionMatrix");
            glUniformMatrix4fv(modelViewProjectionMatrixID, 1, GL_FALSE, &modelViewProjectionMatrix[0][0]);

            glm::mat4 normalMatrix = ofGetCurrentNormalMatrix();
            GLuint normalMatrixID = glGetUniformLocation(progID, "normalMatrix");
            glUniformMatrix4fv(normalMatrixID, 1, GL_FALSE, &normalMatrix[0][0]);

            GLuint lightPositionID = glGetUniformLocation(progID, "lightPosition");
            glUniform3f(lightPositionID, slLightPositionX.rpn.execute0(),
                                         slLightPositionY.rpn.execute0(),
                                         slLightPositionZ.rpn.execute0());

            glBindVertexArray(vertexArrayID);
            glBindBuffer(GL_ARRAY_BUFFER, vertexbufferID);
            glEnableVertexAttribArray(posAttrib);
            glEnableVertexAttribArray(normalAttrib);
            glEnableVertexAttribArray(colorAttrib);

            for(size_t px = 0; px < agent_length; ++px) {
                glDrawArrays(GL_LINE_STRIP, px*trace_length, trace_length);
//                glDrawArrays(GL_TRIANGLES, px*trace_length, trace_length);
            }

            glDisableVertexAttribArray(posAttrib);
            glDisableVertexAttribArray(normalAttrib);
            glDisableVertexAttribArray(colorAttrib);
            glBindVertexArray(0);
            glUseProgram(0);

//            glDisable(GL_CULL_FACE);
            glDisable(GL_DEPTH_TEST);

            cam->end();
        }
    }
    fbo.end();
}
#endif // WITH_openCL
