(ns lv.demo)
(use 'lv.core)

(layout "grid" [bill0 ras0 hit2] [ras1 ras3])
(render)

(expr :bill0 []
  (count 19)
  (x (-
    (* 400 (cos (+ i (* 0.013 f))))
    (* 125 (cos (+ i (* 0.11 f))))
    ))
  (y (-
    (* 400 (sin (+ i (* 0.011 f))))
    (* 125 (sin (+ i (* 0.13 f))))
    ))
  (size 120)
)

(hsv :ras0 [ras3]
  (hsv (* (vec3 1 1 3) ras3.hsv))
)

(hsv :ras1 []
  (rot (* 0.31 f))
  (v (* (snoise x (sin (* 0.05 f))) y))
)

(expr :hit2 [bill0 ras1]
  (thres 0.5)
  (paint 1)
)

(rgb :ras3 [hit2 ras3]
  (rgb (mix (* 3 hit2.rgb)
            ras3.rgb
            0.95))
)
