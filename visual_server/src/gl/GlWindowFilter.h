#pragma once

#include "ofMain.h"
#include "Filter.h"
#include "OscListener.h"
#include "calc/ScalarLisp.h"

class GlWindowFilter : public Filter {
	public:
		GlWindowFilter(ofGLWindowSettings settings, OscListener * oscParams);
		virtual ~GlWindowFilter();
		virtual void setup(const std::string id, int numSamples);
        virtual void reload(ofxOscMessage const &msg);
        virtual void update();
        virtual void draw(int li);
		virtual void toMermaid(std::stringstream & info);

private:
        void findForeignWindow_by_num(bool verbose, int window_num);
        void findForeignWindow_by_title(bool verbose);
        void searchFromRoot(bool verbose);

        ScalarLisp slI;

        Display *display = nullptr;
        std::string foreign_title;
        Window foreign_window;
        bool hasWindow = false;
        size_t foreign_width = 0;
        size_t foreign_height = 0;

        bool hasPixelBuffer = false;
		ofPixels pixelBuffer;
		ofImage pixelImage;

    void clearPixelBuffer();
    void allocatePixelBuffer();
};
