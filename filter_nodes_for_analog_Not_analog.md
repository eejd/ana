# Filter nodes

Filter nodes are processing image data at every single frame. They are connected by channels.

[[_TOC_]]

There are different types of filters:
- Filters reading from external image sources: Movie files, camera input or image sequences.
- Filters reading from other filters to modify pixel data.
- Some filters analyses image data and send OSC messages to other programs.  
Input channels are declared in the filters parameter list between brackets `[` ... `]`. If a filter did not need any inputs, a empty parameter list `[]` should be written.

The underlying openFrameworks implementation is selected by combing the operator with the first characters of the filter name.

![](./stuff/doc/filter-name-type.png)

- Combining the operator `rgb` and the name `ras`:
  - This is a filter in the GPUs `ras`ter unit. In OpenGL terms called a fragment shader.
  - Image data can be manipulated in `rgb` color space.
  - `2` is appended to build an unique name.
- `[img0 img1]` The parameter list is denoting the input channels. In this example `img0` and `img1` are the channel names for other filters in the graph delivering inputs.
- Filter names are starting with a colon character `:`.
- Channel names must *not* start with a colon character.
- For reading values from a channel use swizzle notation.
  - `(hsv` filters
    - `img0.hsv` as 3 component vector: hue saturation value
    - `img0.h` as scalar: hue
    - `img0.s` as scalar: saturation
    - `img0.v` as scalar: value
  - `(rgb` filters
    - `img0.rgb` as 3 component vector: red green blue
    - `img0.r` as scalar: red
    - `img0.g` as scalar: green
    - `img0.b` as scalar: blue

```clojure
; a raster / fragment shader
; running in hsv color space
; reading from channel img0 and ras2
(hsv :ras3 [img0 ras2]
  ; operations to deform geometry
  ; do this before reading the color values from upstream
  (kaleid 2)
  (mirror)
  ; copy hue and saturation
  ; without changes to the output.
  (h img0.h)
  (s img0.s)
  ; combine the brightness
  ; of both input channels
  (v (* 1.5 img0.v ras2.v))
)
```

## GPU filter nodes
GPU Filter nodes are processing image data at every single pixel for each frame. Most of this calculation are executed on the GPU as OpenGL fragment shaders. This is an *single instruction multiple data* approach.

More at: [GPU filter nodes](./filter_nodes_GPU.md)

## CPU processing filters
These filter nodes are processed per frame on the CPU. They are calculated before the rasterizer starts.

More at: [CPU filter nodes](./filter_nodes_CPU.md)

## Filter nodes for satellites
These filter are sending data to satellites.

More at: [Filter nodes for satellites](./filter_nodes_satellites.md)
