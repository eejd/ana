(ns lv.core
  (:require [clojure.java.io :as io])
)

(use 'overtone.osc)
(def OSC-WAIT 1)
(def ANALOG_PORT 57220)
; (def to-analog (osc-client "192.168.2.2" ANALOG_PORT))
(def to-analog (osc-client "localhost" ANALOG_PORT))

;--------- glsl source snippets -------------------
(def frag-begin "
uniform ivec2 panel;
uniform int frame;
uniform int sframe = 0;
uniform int slength = 1;
uniform int li;
uniform float b0;
uniform float volume;
uniform float onset;

uniform vec4 fft03;
uniform vec4 fft47;

uniform vec4 u0123;
uniform vec4 u4567;

uniform vec4 t0123;
uniform vec4 t4567;

uniform vec4 kp0123;
uniform vec4 kp4567;
uniform vec4 ks0123;
uniform vec4 ks4567;
uniform vec4 kb0123;
uniform vec4 kb4567;

uniform vec4 js0xy;
uniform vec2 js0lr;

#ifdef TEXTURECHANNELS
uniform sampler2DRect tex0;
uniform sampler2DRect tex1;
uniform sampler2DRect tex2;
uniform sampler2DRect tex3;
uniform sampler2DRect tex4;
uniform sampler2DRect tex5;
uniform sampler2DRect tex6;
uniform sampler2DRect tex7;
#endif /* TEXTURECHANNELS */
#ifdef TEXTUREARRAY
uniform sampler2DArray textures;
#endif /* TEXTUREARRAY */

in vec2 texCoordVarying;
out vec4 outputColor;

const float pi = 3.14159;
const float pi2 = 2 * 3.14159;

float ifif(bool cnd, float val0, float val1) { return cnd ? val0 : val1; }
vec3 ifif(bool cnd, vec3 val0, vec3 val1) { return cnd ? val0 : val1; }

float ifzero(float cnd, float val0, float val1) { return (int(abs(cnd)) == 0) ? val0 : val1; }
vec3 ifzero(float cnd, vec3 val0, vec3 val1) { return (int(abs(cnd)) == 0) ? val0 : val1; }

float iflow(float cnd, float eps, float val0, float val1) { return (abs(cnd) < abs(eps)) ? val0 : val1; }
vec3 iflow(float cnd, float eps, vec3 val0, vec3 val1) { return (abs(cnd) < abs(eps)) ? val0 : val1; }

float ifpos(float cnd, float val0, float val1) { return cnd > 0.0 ? val0 : val1; }
vec3 ifpos(float cnd, vec3 val0, vec3 val1) { return cnd > 0.0 ? val0 : val1; }

float ifeven(float cnd, float val0, float val1) { return (int(round(cnd)) % 2) == 0 ? val0 : val1; }
vec3 ifeven(float cnd, vec3 val0, vec3 val1) { return (int(round(cnd)) % 2) == 0 ? val0 : val1; }

float usin(float val) { return 0.5 + 0.5 * sin(val); }
float ucos(float val) { return 0.5 + 0.5 * cos(val); }

float sqr(float val) { return val * val; }
vec3 sqr(vec3 val) { return val * val; }

// see: https://thebookofshaders.com/08/
mat2 rot(float angle) {
    float cos_a = cos(angle);
    float sin_a = sin(angle);
    return mat2(cos_a, -sin_a,
                sin_a,  cos_a);
}

mat2 scl(float sx, float sy) {
    return mat2(1.0/sx, 0,
                0,      1.0/sy);
}

// --- hsv rgb --------------------------------------------------------
// Fast branchless HSV to RGB conversion in GLSL
// see: http://lolengine.net/blog/2013/07/27/rgb-to-hsv-in-glsl
vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

vec3 rgb2hsv(vec3 c)
{
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));

    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

float hdiff(float h, float a) {
    return min( min(abs(h - a), abs(h - (1.0 + a))),
                    abs((h + 1.0) - a));
}

// --- noise ----------------------------------------------------------
// see: https://thebookofshaders.com/11/
// see: https://github.com/ashima/webgl-noise/blob/master/src/noise2D.glsl
//
// Description : Array and textureless GLSL 2D simplex noise function.
//      Author : Ian McEwan, Ashima Arts.
//  Maintainer : stegu
//     Lastmod : 20110822 (ijm)
//     License : Copyright (C) 2011 Ashima Arts. All rights reserved.
//               Distributed under the MIT License. See LICENSE file.
//               https://github.com/ashima/webgl-noise
//               https://github.com/stegu/webgl-noise
//

vec3 mod289(vec3 x) {
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec2 mod289(vec2 x) {
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec3 permute(vec3 x) {
  return mod289(((x*34.0)+1.0)*x);
}

float snoise(float _s, float _t);
float snoise(vec2 v);

float snoise(float _s, float _t)
{
  return snoise(vec2(_s, _t));
}
float snoise(vec2 v)
{
//  vec2 v = vec2(_s, _t);
  const vec4 C = vec4(0.211324865405187,  // (3.0-sqrt(3.0))/6.0
                      0.366025403784439,  // 0.5*(sqrt(3.0)-1.0)
                     -0.577350269189626,  // -1.0 + 2.0 * C.x
                      0.024390243902439); // 1.0 / 41.0
// First corner
  vec2 i  = floor(v + dot(v, C.yy) );
  vec2 x0 = v -   i + dot(i, C.xx);

// Other corners
  vec2 i1;
  i1 = (x0.x > x0.y) ? vec2(1.0, 0.0) : vec2(0.0, 1.0);
  vec4 x12 = x0.xyxy + C.xxzz;
  x12.xy -= i1;

// Permutations
  i = mod289(i); // Avoid truncation effects in permutation
  vec3 p = permute( permute( i.y + vec3(0.0, i1.y, 1.0 ))
		+ i.x + vec3(0.0, i1.x, 1.0 ));

  vec3 m = max(0.5 - vec3(dot(x0,x0), dot(x12.xy,x12.xy), dot(x12.zw,x12.zw)), 0.0);
  m = m*m ;
  m = m*m ;

// Gradients: 41 points uniformly over a line, mapped onto a diamond.
// The ring size 17*17 = 289 is close to a multiple of 41 (41*7 = 287)

  vec3 x = 2.0 * fract(p * C.www) - 1.0;
  vec3 h = abs(x) - 0.5;
  vec3 ox = floor(x + 0.5);
  vec3 a0 = x - ox;

// Normalise gradients implicitly by scaling m
// Approximation of: m *= inversesqrt( a0*a0 + h*h );
  m *= 1.79284291400159 - 0.85373472095314 * ( a0*a0 + h*h );

// Compute final noise value at P
  vec3 g;
  g.x  = a0.x  * x0.x  + h.x  * x0.y;
  g.yz = a0.yz * x12.xz + h.yz * x12.yw;
  return 130.0 * dot(m, g);
}

float unoise(float _s, float _t) {
  return clamp(0.5 + 0.5 * snoise(_s, _t), 0.0, 1.0);
}

float turb(float _s, float _t, float octaves) {
  float t = 0;
  float f = 1;
  int on = int(abs(octaves));
  for (int io = 0; io < on; io++) {
      float value = snoise(_s, _t);
      t += abs(value) / f;
      f *= 2;
  }
  return t;
}

// --- mapping 2d space -----------------------------------------------
// see: https://github.com/ojack/hydra-synth
vec2 kaleid(vec2 _st, float nSides) {
    vec2 st = _st - panel / 2;
    float r = length(st);
    float a = atan(st.y, st.x);
    float pi = 2.*3.1416;
    a = mod(a,pi/nSides);
    a = abs(a-pi/nSides/2.);
    return r*vec2(cos(a), sin(a));
}

vec2 pixelate(vec2 _st, float pixelX, float pixelY) {
    vec2 xy = vec2(pixelX, pixelY);
    return (floor(_st / xy) + 0.5)  * xy;
}

vec2 repeat(vec2 _st, float repeatX, float repeatY) {
  return panel * fract((_st / panel) * vec2(repeatX, repeatY));
}

vec2 repeat01(vec2 _st, float repeatX, float repeatY) {
    return fract(_st * vec2(repeatX, repeatY));
}

vec2 inv(vec2 _st) {
    vec2 st = _st / panel - 0.5;
    if(abs(st.x) < 0.001)
      st.x = 0.001;
    if(abs(st.y) < 0.001)
      st.y = 0.001;
    st.x = (1.0 / panel.x) / st.x;
    st.y = (1.0 / panel.y) / st.y;
    return (st + 0.5) * panel;
}

vec2 inv01(vec2 _st) {
    vec2 st = _st;
    if(abs(st.x) < 0.001)
      st.x = 0.001;
    if(abs(st.y) < 0.001)
      st.y = 0.001;
    st.x = 1.0 / st.x;
    st.y = 1.0 / st.y;
    return st;
}

vec2 mirror(vec2 _st) {
    vec2 st = _st;
    if(_st.x < panel.x / 2)
      st.x = _st.x * 2;
    else
      st.x = (panel.x -_st.x) * 2;
    if(_st.y < panel.y / 2)
      st.y = _st.y * 2;
    else
      st.y = (panel.y -_st.y) * 2;
    return st;
}

vec2 mirror01(vec2 _st) {
    vec2 st = _st;
    if(_st.x < 0)
      st.x = _st.x * 2;
    else
      st.x = (1.0 -_st.x) * 2;
    if(_st.y < 0)
      st.y = _st.y * 2;
    else
      st.y = (1.0 -_st.y) * 2;
    return st;
}

void main()
{
    float f = float(frame);
    vec2 _xy = 2.0 * gl_FragCoord.xy / panel - 1.0;
    float x = _xy.x;
    float y = _xy.y;
    float r = length(_xy);
    float a = atan(_xy.x, _xy.y);
    vec2 _ij = gl_FragCoord.xy;
    float i = _ij.x;
    float j = _ij.y;
    vec3 hsv = vec3(0.0);
    vec3 rgb = vec3(0.0);

#define vol volume
#define f0 fft03.x
#define f1 fft03.y
#define f2 fft03.z
#define f3 fft03.w
#define f4 fft47.x
#define f5 fft47.y
#define f6 fft47.z
#define f7 fft47.w

#define u0 u0123.x
#define u1 u0123.y
#define u2 u0123.z
#define u3 u0123.w
#define u4 u4567.x
#define u5 u4567.y
#define u6 u4567.z
#define u7 u4567.w

#define t0 t0123.x
#define t1 t0123.y
#define t2 t0123.z
#define t3 t0123.w
#define t4 t4567.x
#define t5 t4567.y
#define t6 t4567.z
#define t7 t4567.w

#define kp0 kp0123.x
#define kp1 kp0123.y
#define kp2 kp0123.z
#define kp3 kp0123.w
#define kp4 kp4567.x
#define kp5 kp4567.y
#define kp6 kp4567.z
#define kp7 kp4567.w

#define ks0 ks0123.x
#define ks1 ks0123.y
#define ks2 ks0123.z
#define ks3 ks0123.w
#define ks4 ks4567.x
#define ks5 ks4567.y
#define ks6 ks4567.z
#define ks7 ks4567.w

#define kb0 kb0123.x
#define kb1 kb0123.y
#define kb2 kb0123.z
#define kb3 kb0123.w
#define kb4 kb4567.x
#define kb5 kb4567.y
#define kb6 kb4567.z
#define kb7 kb4567.w

#define lx js0xy.x
#define ly js0xy.y
#define rx js0xy.z
#define ry js0xy.w
#define l2 js0lr.x
#define r2 js0lr.y
    float k = 0.0;
    {
")

(def frag-hsv-end "

    vec3 _hsv_out = vec3(mod(hsv.x, 1.0), hsv.y, hsv.z);
    outputColor = vec4(hsv2rgb(_hsv_out), 1.0);
    }
}
")

(def frag-rgb-end "

    outputColor = vec4(rgb, 1.0);
    }
}
")

(def frag-def-hsv "    hsv = ")
(def frag-def-h   "    hsv.x = ")
(def frag-def-s   "    hsv.y = ")
(def frag-def-v   "    hsv.z = ")
(def frag-def-rgb "    rgb = ")
(def frag-def-r   "    rgb.r = ")
(def frag-def-g   "    rgb.g = ")
(def frag-def-b   "    rgb.b = ")
(def frag-set-k   "    k = ")


(def frag-def-sx  "#undef _SX\n#define _SX ")
(def frag-call-sx  "\n    _xy.x /= _SX;
    _ij.x /= _SX;
    x = _xy.x;
    y = _xy.y;
    r = length(_xy);
    a = atan(_xy.x, _xy.y);
    i = _ij.x;
    j = _ij.y;
")

(def frag-def-sy  "#undef _SY\n#define _SY ")
(def frag-call-sy  "\n    _xy.y /= _SY;
    _ij.y /= _SY;
    x = _xy.x;
    y = _xy.y;
    r = length(_xy);
    a = atan(_xy.x, _xy.y);
    i = _ij.x;
    j = _ij.y;
")

(def frag-def-rot "#undef _ROT\n#define _ROT ")
(def frag-call-rot "\n    _xy = rot(_ROT) * _xy;
    _ij = _ij - vec2(panel) / 2.0;
    _ij = (rot(_ROT) * _ij);
    _ij += vec2(panel) / 2.0;
    x = _xy.x;
    y = _xy.y;
    r = length(_xy);
    a = atan(_xy.x, _xy.y);
    i = _ij.x;
    j = _ij.y;
")

(def frag-def-tx  "#undef _TX\n#define _TX ")
(def frag-call-tx  "\n    _xy.x += _TX;
    _ij.x += _TX * panel.x;
    x = _xy.x;
    y = _xy.y;
    r = length(_xy);
    a = atan(_xy.x, _xy.y);
    i = _ij.x;
    j = _ij.y;
")

(def frag-def-ty  "#undef _TY\n#define _TY ")
(def frag-call-ty  "\n    _xy.y += _TY;
    _ij.y += _TY * panel.y;
    x = _xy.x;
    y = _xy.y;
    r = length(_xy);
    a = atan(_xy.x, _xy.y);
    i = _ij.x;
    j = _ij.y;
")

(def frag-def-repeat-x  "#undef _REPEAT_X\n#define _REPEAT_X ")
(def frag-call-repeat-x  "\n    _xy = repeat01(_xy, _REPEAT_X, 1.0);
    _ij = repeat(_ij, _REPEAT_X, 1.0);
    x = _xy.x;
    y = _xy.y;
    r = length(_xy);
    a = atan(_xy.x, _xy.y);
    i = _ij.x;
    j = _ij.y;
")

(def frag-def-repeat-y  "#undef _REPEAT_Y\n#define _REPEAT_Y ")
(def frag-call-repeat-y  "\n    _xy = repeat01(_xy, 1.0, _REPEAT_Y);
    _ij = repeat(_ij, 1.0, _REPEAT_Y);
    x = _xy.x;
    y = _xy.y;
    r = length(_xy);
    a = atan(_xy.x, _xy.y);
    i = _ij.x;
    j = _ij.y;
")

(def frag-def-pixelate-x  "#undef _PIXELATE_X\n#define _PIXELATE_X ")
(def frag-call-pixelate-x  "\n    _ij = pixelate(_ij, _PIXELATE_X, 1.0);
    r = length(_xy);
    x = _xy.x;
    y = _xy.y;
    a = atan(_xy.x, _xy.y);
    i = _ij.x;
    j = _ij.y;
")

(def frag-def-pixelate-y  "#undef _PIXELATE_Y \n#define _PIXELATE_Y ")
(def frag-call-pixelate-y  "\n    _ij = pixelate(_ij, 1.0, _PIXELATE_Y);
    x = _xy.x;
    y = _xy.y;
    r = length(_xy);
    a = atan(_xy.x, _xy.y);
    i = _ij.x;
    j = _ij.y;
")

(def frag-def-inv  "")
(def frag-call-inv  "\n    _xy = inv01(_xy);
     _ij = inv(_ij);
     x = _xy.x;
     y = _xy.y;
     r = length(_xy);
     a = atan(_xy.x, _xy.y);
     i = _ij.x;
     j = _ij.y;
")

(def frag-def-kaleid  "#undef _KALEID_N\n#define _KALEID_N ")
(def frag-call-kaleid  "\n    _xy = kaleid((_xy + 0.5) * panel, _KALEID_N) / panel;
    _ij = kaleid(_ij, _KALEID_N);
    x = _xy.x;
    y = _xy.y;
    r = length(_xy);
    a = atan(_xy.x, _xy.y);
    i = _ij.x;
    j = _ij.y;
")

(def frag-call-mirror  "\n    _xy = mirror01(_xy);
     _ij = mirror(_ij);
     x = _xy.x;
     y = _xy.y;
     r = length(_xy);
     a = atan(_xy.x, _xy.y);
     i = _ij.x;
     j = _ij.y;
")
;--------- write fragment shader source -------------------
; debuging only
(def dump-frag-to  "/dev/shm/_l_")
(defn write-shader [shader-type id src]
  (with-open [w (io/writer (str dump-frag-to  id ".frag") :append false)]
    (.write w frag-begin)
    (.write w src)
    (.write w (if shader-type frag-hsv-end frag-rgb-end)))
)

;--------- send fragment shader source -------------------
(defn send-shader [shader-type id src inputs]
;  (write-shader shader-type id src)
  (osc-send to-analog "/ana_frag" (str id)
                              (if (vector? inputs) (str inputs) "[]")
                              "()" ;TODO send lisp source
                              (str frag-begin
                                   src
                                   (if shader-type frag-hsv-end frag-rgb-end)))
 (Thread/sleep OSC-WAIT)
)

;--------- glsl operators -------------------
(def om {
  "+"   :infix
  "-"   :infix
  "*"   :infix
  "/"   :infix
  "%"   :infix
  ">"   :infix
  ">="  :infix
  "=="  :infix
  "!="  :infix
  "<="  :infix
  "<"   :infix
  "&&"   :infix
  "||"   :infix
  "inv"    :prefix0
  "mirror" :prefix0
; Type cast
  "int"    :prefix1
  "float"  :prefix1
; Common Functions
  "abs"    :prefix1
  "sign"   :prefix1
  "mod"    :prefix2
  "floor"  :prefix1
  "ceil"   :prefix1
  "trunc"  :prefix1
  "round"  :prefix1
  "fract"  :prefix1
  "clamp"  :prefix3
; Exponential Functions
  "pow"    :prefix2
  "exp"    :prefix1
  "log"    :prefix1
  "exp2"   :prefix1
  "log2"   :prefix1
  "sqrt"   :prefix1
  "inversesqrt"   :prefix1
  "sqr"    :prefix1
; Angle and Trigonometry Functions
  "cos"    :prefix1
  "ucos"   :prefix1
  "acos"   :prefix1
  "sin"    :prefix1
  "usin"   :prefix1
  "asin"   :prefix1
  "tan"    :prefix1
  "atan"   :prefix2
; hsv color difference
  "hdiff"  :prefix2
; conditional
  "if?"    :prefix3
  "zero?"  :prefix3
  "low?"   :prefix4
  "pos?"   :prefix3
  "even?"  :prefix3
  "step"   :prefix2
  "smoothstep" :prefix3
; merge two channels
  "max"    :prefix2
  "min"    :prefix2
  "mix"    :prefix3
; noise functions
  "snoise"   :prefix2
  "unoise"   :prefix2
  "turb"     :prefix3
; vector
  "vec2"     :prefix2
  "vec3"     :prefix3
  "distance" :prefix2
  "length"   :prefix1
  ; "pixelate"    :prefix2
  ; "_l"   :prefix2
})

(defn infix?  [opr] (= (om (str opr)) :infix))
(defn prefix1? [opr] (= (om (str opr)) :prefix1))
(defn prefix2? [opr] (= (om (str opr)) :prefix2))
(defn prefix3? [opr] (= (om (str opr)) :prefix3))
(defn prefix4? [opr] (= (om (str opr)) :prefix4))

(def fo)

(defn patch-var-const [v]
  (cond
     (or (int? v) (float? v)) (str " " (float v))
     :else (clojure.string/replace (str v) "." "_")))

(defn patch-opr [v]
  (cond
    (= (str v) "if?")   "ifif"
    (= (str v) "zero?") "ifzero"
    (= (str v) "low?")  "iflow"
    (= (str v) "pos?")  "ifpos"
    (= (str v) "even?") "ifeven"
    :else (str v)))

(defn fi [rls opr]
  (str
    (if (list? (first rls))
      (fo (first rls))
      (patch-var-const (first rls)))
    (if (> (count rls) 1)
      (str opr)))
)

(defn fp1 [rls opr]
  (str (patch-opr opr) "("
    (if (list? (first rls))
      (fo (first rls))
      (patch-var-const (first rls)))
  ")")
)

(defn fp2 [rls opr]
  (str (patch-opr opr) "("
    (if (list? (first rls))
      (fo (first rls))
      (patch-var-const (first rls)))
    ", "
    (if (list? (second rls))
      (fo (second rls))
      (patch-var-const (second rls)))
    ")")
)

(defn fp3 [rls opr]
  (str (patch-opr opr) "("
    (if (list? (first rls))
      (fo (first rls))
      (patch-var-const (first rls)))
    ", "
    (if (list? (second rls))
      (fo (second rls))
      (patch-var-const (second rls)))
    ", "
    (if (list? (nth rls 2))
      (fo (nth rls 2))
      (patch-var-const (nth rls 2)))
    ")")
)

(defn fp4 [rls opr]
  (str (patch-opr opr) "("
    (if (list? (first rls))
      (fo (first rls))
      (patch-var-const (first rls)))
    ", "
    (if (list? (second rls))
      (fo (second rls))
      (patch-var-const (second rls)))
    ", "
    (if (list? (nth rls 2))
      (fo (nth rls 2))
      (patch-var-const (nth rls 2)))
    ","
    (if (list? (nth rls 3))
      (fo (nth rls 3))
      (patch-var-const (nth rls 3)))
    ")")
)

(defn fo [tnode]
  (if (list? tnode)
    (let [opr (first tnode)]
      (loop [accu "("
             rls (rest tnode)]
        (if (seq rls)
            (recur (str accu
                        (cond
                          (infix? opr)   (fi  rls opr)
                          (prefix1? opr) (fp1 rls opr)
                          (prefix2? opr) (fp2 rls opr)
                          (prefix3? opr) (fp3 rls opr)
                          (prefix4? opr) (fp4 rls opr)
                          :else          (str "error " opr " !"))
                   )
                   (if (prefix4? opr) (rest (rest (rest (rest rls))))
                     (if (prefix3? opr) (rest (rest (rest rls)))
                                        (if (prefix2? opr) (rest (rest rls))
                                                           (rest rls))))
            )
            (str accu ")"))))))

(defn parse-node [tnode]
    (if (list? tnode)
      (fo tnode)
      (patch-var-const tnode))
)

(defn parse-inputs-channels [tnode]
  (loop [accu (str "#ifdef TEXTURECHANNELS \n")
         tex  0
         rls tnode]
    (if (seq rls)
        (recur (str accu "#define " (first rls) "_hsv rgb2hsv(texture(tex" (int tex) ", _ij).xyz)\n"
                         "#define " (first rls) "_h " (first rls) "_hsv.x\n"
                         "#define " (first rls) "_s " (first rls) "_hsv.y\n"
                         "#define " (first rls) "_v " (first rls) "_hsv.z\n"
                         "#define " (first rls) "_rgb texture(tex" (int tex) ", _ij).rgb\n"
                         "#define " (first rls) "_r   texture(tex" (int tex) ", _ij).r\n"
                         "#define " (first rls) "_g   texture(tex" (int tex) ", _ij).g\n"
                         "#define " (first rls) "_b   texture(tex" (int tex) ", _ij).b\n")
               (inc tex)
               (rest rls))
        (str accu "#endif /* TEXTURECHANNELS */\n"))))

(defn parse-inputs-array [tnode]
  (loop [accu (str "#ifdef TEXTUREARRAY \n")
         tex  0
         rls tnode]
    (if (seq rls)
        (recur (str accu "#define " (first rls) "_hsv rgb2hsv(texture(textures, vec3(_ij / panel, k)).xyz)\n"
                         "#define " (first rls) "_h " (first rls) "_hsv.x\n"
                         "#define " (first rls) "_s " (first rls) "_hsv.y\n"
                         "#define " (first rls) "_v " (first rls) "_hsv.z\n"
                         "#define " (first rls) "_rgb texture(textures, vec3(_ij / panel, k)).rgb\n"
                         "#define " (first rls) "_r   texture(textures, vec3(_ij / panel, k)).r\n"
                         "#define " (first rls) "_g   texture(textures, vec3(_ij / panel, k)).g\n"
                         "#define " (first rls) "_b   texture(textures, vec3(_ij / panel, k)).b\n")
               (inc tex)
               (rest rls))
        (str accu "#endif /* TEXTUREARRAY */\n"))))

(defn ll [& tnode]
    (loop [accu ""
           used #{}
           rls (first tnode)]
       (if (seq rls)
         (let [n (first rls)]
           (recur (str accu (cond
                               (vector? n)
                                 (str (parse-inputs-channels n) (parse-inputs-array n))
                               (= (str (first n)) "set")
                                 (str "    float " (second n) " = " (parse-node (nth n 2)) ";\n")
                               (= (str (first n)) "set!")
                                 (str "    " (second n) " = " (parse-node (nth n 2)) ";\n")
                               (= (str (first n)) "hsv")
                                 (str frag-def-hsv   (parse-node (second n)) ";\n")
                               (= (str (first n)) "h")
                                 (str frag-def-h   (parse-node (second n)) ";\n")
                               (= (str (first n)) "s")
                                 (str frag-def-s   (parse-node (second n)) ";\n")
                               (= (str (first n)) "v")
                                 (str frag-def-v   (parse-node (second n)) ";\n")
                               (= (str (first n)) "rgb")
                                 (str frag-def-rgb   (parse-node (second n)) ";\n")
                               (= (str (first n)) "r")
                                 (str frag-def-r   (parse-node (second n)) ";\n")
                               (= (str (first n)) "g")
                                 (str frag-def-g   (parse-node (second n)) ";\n")
                               (= (str (first n)) "b")
                                 (str frag-def-b   (parse-node (second n)) ";\n")
                               (= (str (first n)) "k")
                                 (str frag-set-k   (parse-node (second n)) ";\n")
                               (= (str (first n)) "discard")
                                 (str "    if(" (parse-node (second n)) ") discard;\n")
                               (= (str (first n)) "sx")
                                 (str frag-def-sx (parse-node (second n)) frag-call-sx)
                               (= (str (first n)) "sy")
                                 (str frag-def-sy (parse-node (second n)) frag-call-sy)
                               (= (str (first n)) "rot")
                                 (str frag-def-rot (parse-node (second n)) frag-call-rot)
                               (= (str (first n)) "tx")
                                 (str frag-def-tx  (parse-node (second n)) frag-call-tx)
                               (= (str (first n)) "ty")
                                 (str frag-def-ty  (parse-node (second n)) frag-call-ty)
                               (= (str (first n)) "kaleid")
                                 (str frag-def-kaleid  (parse-node (second n)) frag-call-kaleid)
                               (= (str (first n)) "repeat-x")
                                 (str frag-def-repeat-x  (parse-node (second n)) frag-call-repeat-x)
                               (= (str (first n)) "repeat-y")
                                 (str frag-def-repeat-y  (parse-node (second n)) frag-call-repeat-y)
                               (= (str (first n)) "pixelate-x")
                                 (str frag-def-pixelate-x  (parse-node (second n)) frag-call-pixelate-x)
                               (= (str (first n)) "pixelate-y")
                                 (str frag-def-pixelate-y  (parse-node (second n)) frag-call-pixelate-y)
                               (= (str (first n)) "inv")
                                 (str frag-def-inv  (parse-node (second n)) frag-call-inv)
                               (= (str (first n)) "mirror")
                                 (str (parse-node (second n)) frag-call-mirror)
                               (= (str (first n)) "dotimes")
                                 (str "{ for(int n=0; n<int(" (parse-node (second (second n))) "); ++n) {\n" (ll (rest (rest n))) "\n}}\n")
                          ))
                  (conj used (str (first n)))
                  ;(rest rls))) (ll-complete accu used)))
                  (rest rls))) accu))
)

;--------- layout -------------------
(defmacro layout [& tnode]
  (osc-send to-analog "/ana_layout" (str (reverse (into '(layout) tnode))))
  (Thread/sleep OSC-WAIT)
)

;--------- hsv fragment shader -------------------
(defn hsv-parse [id tnode]
  (send-shader true id (ll tnode) (first tnode))
)

(defmacro hsv [id & tnode] (hsv-parse id tnode))

;--------- rgb fragment shader -------------------
(defn rgb-parse [id tnode]
  (send-shader false id (ll tnode) (first tnode))
)

(defmacro rgb [id & tnode] (rgb-parse id tnode))

;-------- expr --------------
(defn expr-parse [id tnode]
  (let [argpos (if (vector? (first tnode)) 1 0)]
    (osc-send to-analog "/ana_expr" (str id)
                                (if (= argpos 1) (str (first tnode)) "[]")
                                (str (nth tnode (+ argpos  0) "")) ;TODO var args list
                                (str (nth tnode (+ argpos  1) ""))
                                (str (nth tnode (+ argpos  2) ""))
                                (str (nth tnode (+ argpos  3) ""))
                                (str (nth tnode (+ argpos  4) ""))
                                (str (nth tnode (+ argpos  5) ""))
                                (str (nth tnode (+ argpos  6) ""))
                                (str (nth tnode (+ argpos  7) ""))
                                (str (nth tnode (+ argpos  8) ""))
                                (str (nth tnode (+ argpos  9) ""))
                                (str (nth tnode (+ argpos 10) ""))
                                (str (nth tnode (+ argpos 11) ""))
                                (str (nth tnode (+ argpos 12) ""))
                                (str (nth tnode (+ argpos 13) ""))
                                (str (nth tnode (+ argpos 14) ""))
                                (str (nth tnode (+ argpos 15) ""))))
  (Thread/sleep OSC-WAIT)
)

(defmacro expr [id & tnode] (expr-parse id tnode))

;-------- texpr --------------
(defn texpr-parse [id path tnode]
    (osc-send to-analog path (str id)
                                (str (nth tnode 0 "")) ;TODO var args list
                                (str (nth tnode 1 ""))
                                (str (nth tnode 2 ""))
                                (str (nth tnode 3 ""))
                                (str (nth tnode 4 ""))
                                (str (nth tnode 5 ""))
                                (str (nth tnode 6 ""))
                                (str (nth tnode 7 ""))
                                (str (nth tnode 8 ""))
                                (str (nth tnode 9 ""))
                                (str (nth tnode 10 ""))
                                (str (nth tnode 11 ""))
                                (str (nth tnode 12 ""))
                                (str (nth tnode 13 "")))
  (Thread/sleep OSC-WAIT)
)

(defmacro texpr [id & tnode] (texpr-parse id "/ana_texpr" tnode))

;-------- inspect --------------
(defmacro inspect [id & tnode] (texpr-parse id "/ana_inspect" tnode))

;-------- convolution matrix --------------
(defn send-matrix [id rows inputs]
  (osc-send to-analog "/ana_matrix" (str id)
                              (if (vector? inputs) (str inputs) "[]")
                              (str "(matrix " (into [] rows) ")"))
  (Thread/sleep OSC-WAIT)
)

(defmacro matrix [id & tnode] (send-matrix id (rest tnode) (first tnode)))

;-------- patch --------------
(defn patch-parse [id tnode]
  (osc-send to-analog "/ana_patch" (str id)
                               (if (vector? (first tnode)) (str (first tnode)) "[]")
                               (str (second tnode)))
)

(defmacro patch [id & tnode] (patch-parse id tnode))

;--------- render buffers ------------
(defn render [& ids]
  (osc-send to-analog "/ana_render" (if (nil? ids) "(render [])" (str "(render " (into [] ids) ")")))
  (Thread/sleep OSC-WAIT)
)

;-------- image list ---------------
(defn image [& tnode]
  (osc-send to-analog "/ana_image" (str "(image " (into [] tnode) ")"))
  (Thread/sleep OSC-WAIT)
)

;-------- sequence of images  ---------------
(defn imagesequ [& tnode]
  (osc-send to-analog "/ana_imagesequ" (str "(imagesequ " (into [] tnode) ")"))
  (Thread/sleep OSC-WAIT)
)

; (defmacro imagesequ [& tnode]
;   (osc-send to-analog "/ana_imagesequ" (str "(imagesequ " (into [] tnode) ")"))
;   (Thread/sleep OSC-WAIT)
; )


;-------- movie list ---------------
(defn movie [& tnode]
  (osc-send to-analog "/ana_movie" (str "(movie " (into [] tnode) ")"))
  (Thread/sleep OSC-WAIT)
)

;-------- m-jepg URIs ---------------
(defn ipcam [& tnode]
  (osc-send to-analog "/ana_ipmjpeg" (str "(uri " (into [] tnode) ")"))
  ; (let [argpos (if (vector? (first tnode)) 1 0)]
  ;   (osc-send to-analog "/ana_ipmjpeg" (str id)
  ;                               (if (= argpos 1) (str (first tnode)) "[]")
  ;                               (str (nth tnode (+ argpos  0) "")) ;TODO var args list
  ;                               (str (nth tnode (+ argpos  1) ""))
  ;                               (str (nth tnode (+ argpos  2) ""))
  ;                               (str (nth tnode (+ argpos  3) ""))
  ;                               (str (nth tnode (+ argpos  4) ""))
  ;                               (str (nth tnode (+ argpos  5) ""))
  ;                               (str (nth tnode (+ argpos  6) ""))
  ;                               (str (nth tnode (+ argpos  7) ""))
  ;                               (str (nth tnode (+ argpos  8) ""))
  ;                               (str (nth tnode (+ argpos  9) ""))
  ;                               (str (nth tnode (+ argpos 10) ""))
  ;                               (str (nth tnode (+ argpos 11) ""))
  ;                               (str (nth tnode (+ argpos 12) ""))
  ;                               (str (nth tnode (+ argpos 13) ""))
  ;                               (str (nth tnode (+ argpos 14) ""))
  ;                               (str (nth tnode (+ argpos 15) ""))))
  (Thread/sleep OSC-WAIT)
)

;-------- sound --------------
(defn sound [& tnode]
  (osc-send to-analog "/ana_sound" (str "(sound " (into [] tnode) ")"))
  (Thread/sleep OSC-WAIT)
)

(defn sound-sync []
  (osc-send to-analog "/ana_sync")
)

;-------- send osc message --------------
(defn osend
  ([path]
    (osc-send to-analog path))
  ([path value]
    (osc-send to-analog path value))
)

;-------- send osc message to set an uniform --------------
(defn usend
  ([u0]
    (osc-send to-analog "/ana_uniform" u0))
  ([u0 u1]
    (osc-send to-analog "/ana_uniform" u0 u1))
  ([u0 u1 u2]
    (osc-send to-analog "/ana_uniform" u0 u1 u2))
  ([u0 u1 u2 u3]
    (osc-send to-analog "/ana_uniform" u0 u1 u2 u3))
  ([u0 u1 u2 u3 u4]
    (osc-send to-analog "/ana_uniform" u0 u1 u2 u3 u4))
  ([u0 u1 u2 u3 u4 u5]
    (osc-send to-analog "/ana_uniform" u0 u1 u2 u3 u4 u5))
  ([u0 u1 u2 u3 u4 u5 u6]
    (osc-send to-analog "/ana_uniform" u0 u1 u2 u3 u4 u5 u6))
  ([u0 u1 u2 u3 u4 u5 u6 u7]
    (osc-send to-analog "/ana_uniform" u0 u1 u2 u3 u4 u5 u6 u7))
)

(defn u0 [uvalue] (osc-send to-analog "/ana_u0" uvalue))
(defn u1 [uvalue] (osc-send to-analog "/ana_u1" uvalue))
(defn u2 [uvalue] (osc-send to-analog "/ana_u2" uvalue))
(defn u3 [uvalue] (osc-send to-analog "/ana_u3" uvalue))
(defn u4 [uvalue] (osc-send to-analog "/ana_u4" uvalue))
(defn u5 [uvalue] (osc-send to-analog "/ana_u5" uvalue))
(defn u6 [uvalue] (osc-send to-analog "/ana_u6" uvalue))
(defn u7 [uvalue] (osc-send to-analog "/ana_u7" uvalue))
