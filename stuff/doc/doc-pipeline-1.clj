(ns lv.demo)
(use 'lv.core)

(layout "grid" [ras0 ras1] [ras2 ras3])
; show all filters
(render)

; generate animated rings
(hsv :ras0 []
  (v (sin (+ (* 0.05 f)
             (* -50 r))))
)

; generate a rotating star
(hsv :ras1 []
  (rot (* 0.002 f))
  (v (sin (* 50 a)))
)

; mix both upstream filters
(hsv :ras2 [ras0 ras1]
  ; (mirror)
  (kaleid 13)
  (v (* ras0.v ras1.v))
)

; mix ras0 and ras2 controlled by ras1
(hsv :ras3 [ras0 ras1 ras2]
  (v (mix ras0.v ras2.v ras1.v))
)

; only show filter 3
(render 3)
