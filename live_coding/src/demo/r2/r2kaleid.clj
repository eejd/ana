(ns lv.demo)
(use 'lv.core)

(layout "grid" [img0 igen1]
               [ras2 ras3])

(image "flower-sampler/*.png")

(rgb :img0 [c0 c1 c2 c3]
  (rgb (mix c3.rgb c2.rgb (usin (* 0.031 f))))
)

(expr :igen1 [img0]
  "r2/r2kaleido-4samples.cl"
  (p0 (* ks0 3))
  (p1 (* ks1 3))
  (p2 (+ 0.1 (sin (* 0.01 f))))
  ; (p2 ks5)
  (p3 (* ks3 3))
  (p4 (* ks4 3))
  (p5 (sqrt ks5))
  (p7 (* ks7 1))
)

(rgb :ras2 [img0]
  (kaleid 5)
  (mirror)
  (rot (* 0.01 f))
  (rgb img0.rgb)
)

(rgb :ras3 [igen1 ras2]
  (kaleid 5)
  (rgb (+ ras2.rgb igen1.rgb))
)

(render)
