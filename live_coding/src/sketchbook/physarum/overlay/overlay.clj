(ns lv.demo)
(use 'lv.core)

; (layout "grid" [ras0 ras3]
;                [clag1 clag2])
(layout "horizontal" [clag1 ras3 clag2])
(render)
(hsv :ras0 []
  ; (v (- 1 (/ (floor (* 5 r)) 5)))
  (v 0)
)

(expr :clag1 [ras0]
  "physarum/overlay/overlay1.cl"
  (paint 1)

  (p0 (+ (* ks0 0.005) 0.00005))
  (p1 (* ks1 0.01))
  (p2 (* ks2 0.2))
  (p3 (* ks3 0.5))
  (p6 (sqrt (/ kp0 16)))
  (p7 (+ kp1 0.1))
)

(expr :clag2 [ras0]
  "physarum/overlay/overlay2.cl"
  (paint 1)

  (p0 (+ (* ks4 0.005) 0.00005))
  (p1 (* ks5 0.01))
  (p2 (* ks6 0.2))
  (p3 (* ks7 0.5))
  (p6 (sqrt (/ kp4 16)))
  (p7 (+ kp5 0.1))
)

(rgb :ras3 [clag1 clag2]
  (rgb (mix clag1.rgb clag2.rgb 0.5))
)
