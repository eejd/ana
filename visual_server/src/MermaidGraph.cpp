#include <string>

#include "ofMain.h"
#include "globalvars.h"
#include "Filter.h"
#include "MermaidGraph.h"

MermaidGraph::MermaidGraph() {
}

std::string MermaidGraph::toGraphString(std::vector<std::shared_ptr<Filter>> & filterLine) {
    std::stringstream info;
    info << "\ngraph LR\n";
    for(auto s : filterLine) {
        s->toMermaid(info);
    }
    return info.str();
}
