#pragma once
#include "ofMain.h"
#include "OscListener.h"
#include "Filter.h"
#include "calc/ScalarLisp.h"
#include "glslhelper.h"

class FragFilter: public Filter {
public:
    FragFilter(ofGLWindowSettings settings, OscListener * oscParams);
    virtual ~FragFilter();
    virtual void reload(ofxOscMessage const &msg);
    virtual void update();
    virtual void draw(int li);

    GlslProgMng glslProg;
};
