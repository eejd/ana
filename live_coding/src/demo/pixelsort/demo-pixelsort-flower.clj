(ns lv.demo)
(use 'lv.core)

(layout "grid" [img0 cline1]
               [ras2 pix1])
; https://pixabay.com/photos/flower-dewdrops-petals-6028389/
(image "Mastodon-#ArtReference/flower-6028389/flower-6028389_1920.jpg")

(rgb :img0 [c0]
	(ty 0.2) (tx 0.51)
	(rgb c0.rgb)
)

; find contours
(expr :cline1 [img0]
  (count 200)
  (max 5000)
  (thres 0.23)
  ; (thres u3)
  (fill -1)
  (paint -1)
  (holes 1)
)

(rgb :ras2 [img0 cline1]
	(rgb (max (* 1 img0.rgb)
	          (* 4 cline1.rgb)))
)

(expr :pix1 [ras2]
  ; (b u0)
	; (w u1)
  (b (+ 0.6 (* 0.2 (snoise 0.1 (* 0.11 f) (* 0.13 f)))))
  (w 1)
	(comp (- v))
)
(render)
