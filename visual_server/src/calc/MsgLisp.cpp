#include <string>

#include "ofMain.h"

#include "ana_features.h"
#include "calc/MsgLisp.h"

#define TRACE_MONOTONIC

MsgLisp::MsgLisp() {
}

MsgLisp::~MsgLisp() {
}

void MsgLisp::skip() {
    while(ix < len && isspace(inp[ix])) {
        ++ix;
    }
}

void MsgLisp::collect(ast_m* r) {
    while(ix < len) {
        skip();
        switch(inp[ix]) {
        case '(': {
            ++ix;
            ast_m* c = new ast_m("(");
            collect(c);
            r->addChild(c);
            break;
        }
        case '[': {
            ++ix;
            ast_m* c = new ast_m("[");
            collect(c);
            r->addChild(c);
            break;
        }
        case ')':
        case ']':
            ++ix;
            return;
        default:
            tokBuffer.clear();
            while(ix < len && !(isspace(inp[ix])
                    || inp[ix] == '(' || inp[ix] == ')'
                    || inp[ix] == '[' || inp[ix] == ']')) {
                tokBuffer += inp[ix];
                ++ix;
            }
            ast_m* n = new ast_m(tokBuffer);
            r->addSibling(n);
            break;
        }
    }
}

ast_m* MsgLisp::compile(std::string src) {
    ast_m* r = nullptr;
    err = 0;
    ix = 0;
    len = src.size();
    if(len > 0) {
        inp = src.c_str();
        skip();
        if(inp[ix] == '(') {
            ++ix;
            r = new ast_m("(");
            collect(r);
        }
        else {
            tokBuffer.clear();
            while(ix < len && !(isspace(inp[ix])
                    || inp[ix] == '(' || inp[ix] == ')'
                    || inp[ix] == '[' || inp[ix] == ']')) {
                tokBuffer += inp[ix];
                ++ix;
            }
            r = new ast_m(tokBuffer);
        }
    }
    return r;
}

std::string MsgLisp::dump_ast(ast_m* node, size_t depth) {
    std::string dump;
    if(node != nullptr) {
        for(auto i=0u; i<depth; ++i) {
            dump.append("  ");
        }
        dump.append("|");
        dump.append(to_string(depth));
        dump.append("|e");
        dump.append(to_string(node->count()));
        dump.append(":");
        dump.append(node->tok);
        dump.append("\n");

        for(int i=0; i<node->count(); ++i) {
            ast_m* c = node->at(i);
            if(node->isChild(i)) {
                dump.append(dump_ast(c, depth+1));
            }
            else {
                dump.append("|");
                for(auto i=0u; i<depth; ++i) {
                    dump.append("  ");
                }
                dump.append(c->tok);
                dump.append("\n");
            }
        }
    }
    return dump;
}

std::string MsgLisp::ast2frag(ast_m* node, size_t depth) {
    std::string term_src;
    if(node == nullptr)
        return term_src;

    if(node->count() > 0) {
        term_src.append("(");

        ast_m* opr = node->at(0);
        bool first = true;
        for(int i=1; i<node->count(); ++i) {
            bool last = i+1>=node->count();
            ast_m* c = node->at(i);
            if(!first) {
                term_src.append(opr->tok);
                term_src.append(" ");
            }
            if(node->isChild(i)) {
                term_src.append(ast2frag(c, depth+1));
            }
            else {
                term_src.append(c->tok);
                if(!last)
                    term_src.append(" ");
            }
            first = false;
        }
        term_src.append(")");
    }
    else if(node->count() == 0) {
        term_src = node->tok;
    }
    return term_src;
}

