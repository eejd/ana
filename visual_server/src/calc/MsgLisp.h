#pragma once
#include <string>
#include "ana_features.h"

class ast_m {
public:
    ast_m(std::string t) {
        tok = t;
        child_count = 0;
    }

    void addChild(ast_m *a) {
        entry.push_back(a);
        type_flag.push_back(true);
        ++child_count;
    }

    void addSibling(ast_m *a) {
        entry.push_back(a);
        type_flag.push_back(false);
    }

    int count() {
        return entry.size();
    }

    int childCount() const {
        return child_count;
    }

    bool isChild(size_t index) {
        return index >= type_flag.size() ? 0 : type_flag[index];
    }

    ast_m* at(size_t index) {
        return index >= entry.size() ? nullptr : entry[index];
    }

    std::string tok;
    int child_count = 0;
    std::vector<ast_m *> entry;
    std::vector<bool> type_flag;
};

class MsgLisp {
public:
    MsgLisp();
    virtual ~MsgLisp();
    ast_m* compile(std::string src);

private:
    void skip();
    void collect(ast_m* r);

    int len = 0;
    std::string tokBuffer;
    int ix = 0;
    int err = 0;
    const char *inp = nullptr;

public:
    std::string dump_ast(ast_m* a, size_t depth);
    std::string ast2frag(ast_m* node, size_t depth);
};
