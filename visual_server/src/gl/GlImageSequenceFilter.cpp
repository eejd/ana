#include <filesystem>
#include <algorithm>

#include "ofMain.h"
#include "globalvars.h"
#include "pathhelper.h"
#include "Filter.h"
#include <gl/GlImageSequenceFilter.h>

GlImageSequenceFilter::GlImageSequenceFilter(ofGLWindowSettings settings, OscListener * oscParams) : Filter(settings, oscParams){
}

GlImageSequenceFilter::~GlImageSequenceFilter(){
}

void GlImageSequenceFilter::setup(const std::string id, int numSamples){
	Filter::setup(id, numSamples);
	usedImages = 0;
}

void GlImageSequenceFilter::reload(ofxOscMessage const &msg) {
    auto narg = msg.getNumArgs();
    if(narg > 2) {
        Filter::compileShader(msg.getArgAsString(0), msg.getArgAsString(3));
    }
    sframe = 0;
}

const std::string& GlImageSequenceFilter::differentiation() const {
	static std::string defs = "#version 330\n#undef TEXTURECHANNELS\n#define TEXTUREARRAY\n";
	return defs;
}

void GlImageSequenceFilter::draw(int li){
	fbo.begin();

	if(sizeImageSequ > 0) {
		shader.begin();
		sendUniforms(shader, li);
		shader.setUniformTexture("textures", GL_TEXTURE_2D_ARRAY, imageSequ_textureID, 0);
		shader.setUniform1i("sframe", sframe);
		shader.setUniform1i("slength", sizeImageSequ);

		ofDrawRectangle(0, 0, filterWidth, filterHeight);

		shader.end();
	}

	fbo.end();
}

void GlImageSequenceFilter::finish() {
    if(sframe >= sizeImageSequ) {
        if(sizeImageSequ > 0 && apptarget) {
//            apptarget->startPreshotExport(); TODO
        }
        sframe = 0;
    }
    else {
        ++sframe;
    }
}

void GlImageSequenceFilter::toMermaid(std::stringstream & info) {
    info << "c0>" << imagesequ0 << "]" << " -- c0 --> " << id << "\n"; //TODO quote [ ]
    Filter::toMermaid(info);
}
