#pragma OPENCL EXTENSION all : enable

#define TRAIL(xy) (clamp((int)(img_y_max*xy.y), 0, img_y_max) * img_x_max \
                 + clamp((int)(img_x_max*xy.x), 0, img_x_max))

const sampler_t SAMPLER = CLK_NORMALIZED_COORDS_TRUE | CLK_ADDRESS_REPEAT | CLK_FILTER_LINEAR;

__kernel void image_setup() {}
__kernel void image_change() {}

// ---------------------------------------------------------
__kernel void ag_setup(
__read_only image2d_t image_in,
const int img_x_max,
const int img_y_max,
__global float2* pos,
__global float2* pos_out,
__global float* vel,
__global float* vel_out,
__global float* head,
__global float* head_out,
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7
) 
{
    const int ai = get_global_id(0);
    pos_out[ai] = pos[ai];
    vel_out[ai] = p0;
    head_out[ai] = head[ai];
}

// ---------------------------------------------------------
__kernel void trail_setup(
__read_only image2d_t image_in,
const int img_x_max,
const int img_y_max,
__global int* trail,
__global int* trail_out,
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7
) 
{
    const int ti = get_global_id(0);
    trail_out[ti] = 0;
}


// ---------------------------------------------------------
__kernel void ag_move(
__read_only image2d_t image_in,
const int img_x_max,
const int img_y_max,
__global float2* pos,
__global float2* pos_out,
__global float* vel,
__global float* vel_out,
__global float* head,
__global float* head_out,
__global int* trail,
volatile __global int* trail_out,
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7
)
{
    const int ai = get_global_id(0);
    
    const float sense_dist   = p1+0.0001f;
    const float sense_angle  = p2+0.0001f;
    
    const int px = ai % img_x_max;
    const int py = ai / img_x_max;
    const float4 color = read_imagef(image_in, SAMPLER, (float2)(px/(float)img_x_max, py/(float)img_y_max));
//     const float rotate_angle = (color.x*p3)+0.0001f;
    const float rotate_angle = p3-0.5*color.x+0.0001f;
//     const float rotate_angle = p3+0.0001f;
    
    float2 old_pos = pos[ai];
    while(old_pos.x <  0.0f) old_pos.x += 1.0f;
    while(old_pos.x >= 1.0f) old_pos.x -= 1.0f;
    while(old_pos.y <  0.0f) old_pos.y += 1.0f;
    while(old_pos.y >= 1.0f) old_pos.y -= 1.0f;
    
    float2 ph = pos[ai] + sense_dist * (float2)(cos(head[ai]), sin(head[ai]));
    while(ph.x <  0.0f) ph.x += 1.0f;
    while(ph.x >= 1.0f) ph.x -= 1.0f;
    while(ph.y <  0.0f) ph.y += 1.0f;
    while(ph.y >= 1.0f) ph.y -= 1.0f;
    int sense_head = trail[TRAIL(ph)];
    
    float2 pl = pos[ai] + sense_dist * (float2)(cos(head[ai]+sense_angle), sin(head[ai]+sense_angle));
    while(pl.x <  0.0f) pl.x += 1.0f;
    while(pl.x >= 1.0f) pl.x -= 1.0f;
    while(pl.y <  0.0f) pl.y += 1.0f;
    while(pl.y >= 1.0f) pl.y -= 1.0f;
    int sense_left = trail[TRAIL(pl)];
    
    float2 pr = pos[ai] + sense_dist * (float2)(cos(head[ai]-sense_angle), sin(head[ai]-sense_angle));
    while(pr.x <  0.0f) pr.x += 1.0f;
    while(pr.x >= 1.0f) pr.x -= 1.0f;
    while(pr.y <  0.0f) pr.y += 1.0f;
    while(pr.y >= 1.0f) pr.y -= 1.0f;
    int sense_right = trail[TRAIL(pr)];
    
    float add_head = 0.0f;
    if(sense_head > sense_left && sense_head > sense_right)
        add_head = 0.0f;
    else if(sense_head < sense_left && sense_head < sense_right) {
        switch(ai % 4) {
            case 0:
                add_head = 1.02*rotate_angle;
                break;
            case 1:
                add_head = -1.06*rotate_angle;
                break;
            case 2:
                add_head = 0.53*rotate_angle;
                break;
            case 3:
                add_head = -0.51*rotate_angle;
                break;
        }
    }
    else if(sense_left < sense_right)
        add_head = -rotate_angle;
    else if(sense_left > sense_right)
        add_head = rotate_angle;
    const float new_head = head[ai] + add_head;
    
    float2 new_pos = pos[ai] + vel[ai] * (float2)(cos(new_head), sin(new_head));
    while(new_pos.x <  0.0f) new_pos.x += 1.0f;
    while(new_pos.x >= 1.0f) new_pos.x -= 1.0f;
    while(new_pos.y <  0.0f) new_pos.y += 1.0f;
    while(new_pos.y >= 1.0f) new_pos.y -= 1.0f;
    pos_out[ai] = new_pos;
    
    
    vel_out[ai] = p0;
    head_out[ai] = new_head;
    
    int ti = TRAIL(old_pos);
    atomic_add(&trail_out[ti], (int)1024);
}


// ---------------------------------------------------------
__kernel void trail_disipate(
__read_only image2d_t image_in,
const int img_x_max,
const int img_y_max,
__global int* trail,
__global int* trail_out,
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7
)
{
    const int ti = get_global_id(0);
    const int tmax = img_x_max * img_y_max;
    
    int W = ti-1;         if(W<0)          W += img_x_max;
    int O = ti+1;         if(O>=img_x_max) O -= img_x_max;
    int S = ti+img_x_max; if(S>=tmax)      S -= tmax;
    int N = ti-img_x_max; if(N<0)          N += tmax;
    trail_out[ti] = (float)(trail[W]
                            +trail[O]
                            +trail[S]
                            +trail[N]
                            + 2*trail[ti]
                            ) / 7.0f;
}


// ---------------------------------------------------------
__kernel void ag_clear(
__read_only image2d_t image_in,
const int img_x_max,
const int img_y_max,
__write_only image2d_t image_out
)
{
    const int px = get_global_id(0);
    const int py = get_global_id(1);
    write_imagef(image_out, (int2)(px, py), (float4)(0, 0, 0, 1));
}


// ---------------------------------------------------------
__kernel void ag_draw(
__read_only image2d_t image_in,
const int img_x_max,
const int img_y_max,
__global int* trail,
// __global int* trail_out,
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7,
__write_only image2d_t image_out
)
{
    const int px = get_global_id(0);
    const int py = get_global_id(1);
    const int ti = py * img_x_max + px;
    
    float gray = pow((float)trail[ti] / (float)1024, p7);
    write_imagef(image_out, (int2)(px, py), (float4)(gray, gray, gray, 1));
}
