(ns lv.demo)
(use 'lv.core)

(layout "grid" [bill0 bill1] [ras2 hit3] [pihat4 vid5])
(render)

(expr :bill0 []
  (count 21)
  (x (* 1000 (sin (/ (- f 0) 200)) 0.400 (cos (* 0.3 i))))
  (y (* 1000 (sin (/ (- f 0) 200)) 0.400 (sin (* 0.3 i))))
  (size 360)
)
(expr :bill1 []
  (count 21)
  (x (+ 1 (* 1000 (sin (/ f 200)) 0.450 (cos (* 0.3 i)))))
  (y (+ 1 (* 1000 (sin (/ f 200)) 0.450 (sin (* 0.3 i)))))
  (size 120)
)

(rgb :ras2 [bill0 bill1]
  (tx (* 0.03 (snoise (* 0.011 f) (* 0.01 y))))
  (ty (* 0.03 (snoise (* 0.013 f) (* 0.01 x))))
  (rgb (* 3 (max bill0.rgb bill1.rgb)))
)

(expr :hit3 [bill1 bill0]
  (thres 0.2)
  (paint 2)
)

(render 2)

(expr :pihat4 [ras2]
  (mode 0) ; draw mode 0:
           ; use the color from the upstream filter node
)

(rgb :vid5 [c0]
  (rgb c0.rgb)
)
