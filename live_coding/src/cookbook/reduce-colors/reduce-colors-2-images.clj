(ns lv.demo)
(use 'lv.core)

(layout "grid" [img0 img1]
               [img2 img3])
; assume there are 4 PNG images in the media folder.
(image "flower-sampler/*.png")
(render)

; multiply hsv channels of two images
(hsv :img0 [c0 c1 c2 c3]
  (hsv (* c0.hsv c2.hsv))
)

; mix hsv channels of two images
; use the value of a third image as weight fro mixing.
(hsv :img1 [c0 c1 c2 c3]
  (hsv (mix c0.hsv c2.hsv c1.v))
)

; mix one hsv channel with some constant values
; use the value of an additional image as weight fro mixing.
(hsv :img2 [c0 c1 c2 c3]
  (hsv (mix c0.hsv
            (vec3 0.795 0.354 0.181)
            c1.v))
)

; add rgb values of 4 input images
; every image has its own weight
; Use cosine function to invert and remap to 0.0 - 1.0 range
(rgb :img3 [c0 c1 c2 c3]
  (rgb (cos (+ (* 0.740 c0.rgb)
               (* 0.622 c1.rgb)
               (* 0.535 c2.rgb)
               (* 0.425 c3.rgb))))
)
