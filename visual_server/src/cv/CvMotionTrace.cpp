#include "ofMain.h"
#include "ofxCv.h"

#include "globalvars.h"
#include "util/p3circle.h"
#include "OscListener.h"
#include "Filter.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispMng.h"
#include "cv/CvMotionTrace.h"

void LineTrace::append(ofFloatColor c, ofPoint p0, ofPoint p1) {
    colors.push_back(c);
    from.push_back(p0);
    to.push_back(p1);
}

void LineTrace::clear() {
    colors.clear();
    from.clear();
    to.clear();
}

void LineTrace::draw() {
    for(size_t lx = 0u; lx < colors.size(); ++lx) {
        ofSetColor(colors[lx]);
        ofDrawLine(from[lx], to[lx]);
    }
}

void LineTracer::append(LineTrace *line, size_t traceLenght) {
    while(frameQueue.size() > traceLenght) {
        auto f = frameQueue.front();
        delete f;
        frameQueue.pop_front();
    }
    frameQueue.push_back(line);
}

void LineTracer::draw() {
    for(size_t fx = 0; fx < frameQueue.size(); ++fx) {
        frameQueue[fx]->draw();
    }
}

CvMotionTrace::CvMotionTrace(ofGLWindowSettings settings, OscListener *oscParams) :
        Filter(settings, oscParams) {
}

CvMotionTrace::~CvMotionTrace() {
    frame_captured.release();
    frame_gray.release();
    frame0.release();
    frame1.release();
    frame0Gray.release();
    frame1Gray.release();
    pts.clear();
    nextPts.clear();
    status.clear();
    err.clear();
}

void CvMotionTrace::setup(const std::string id, int numSamples) {
    Filter::setup(id, numSamples);
}

void CvMotionTrace::reload(ofxOscMessage const &msg) {
    auto narg = msg.getNumArgs();
    if(narg > 1) {
        Filter::setInputTexture(msg.getArgAsString(1));
    }
    ScalarLispMng mng;
    mng.compile(slFeaturePoints, "count", "32", msg);
    mng.compile(slMode, "mode", "", msg);
    mng.compile(slClear, "clear", "1", msg);
    mng.compile(slMinDistance, "min", "50", msg);
    mng.compile(slQuality, "qual", "0.1", msg);
    mng.compile(slTraceLenght, "trace", "32", msg);
    mng.compile(slUpstream, "paint", "-1", msg);

    featurepoints = slFeaturePoints.rpn.execute0();
    pts.resize(featurepoints);
    nextPts.resize(featurepoints);
    status.resize(featurepoints);
    reallocateVertices(featurepoints);
}

void CvMotionTrace::update() {
    hasData = false;
    mode = slMode.rpn.execute0();
    traceLenght = slTraceLenght.rpn.execute0();

    if(upstream_ids.size() >= 1) {
        auto in_id = upstream_ids[0];
        if(filterDict.find(in_id) != filterDict.end()) {
            auto f = filterDict[in_id];
            if(f) {
                f->getTexture().readToPixels(grabbedPixels);
                frame_captured = ofxCv::toCv(grabbedPixels);
                video_width = grabbedPixels.getWidth();
                video_height = grabbedPixels.getHeight();
                hasData = true;
            }
        }
    }
    if(!hasData)
        return;

    cv::Mat ptr0, ptr1;
    if(frameIndex == 0) {
        frame_captured.copyTo(frame0);
        cv::cvtColor(frame0, frame0Gray, cv::COLOR_RGBA2GRAY);
    } else {
        if(frameIndex % 2 == 1) {
            frame_captured.copyTo(frame1);
            cv::cvtColor(frame1, frame1Gray, cv::COLOR_RGBA2GRAY);
            ptr0 = frame0Gray;
            ptr1 = frame1Gray;
        } else {
            frame_captured.copyTo(frame0);
            cv::cvtColor(frame0, frame0Gray, cv::COLOR_RGBA2GRAY);
            ptr0 = frame1Gray;
            ptr1 = frame0Gray;
        }

        pts.clear();
        qualityLevel = slQuality.rpn.execute0();
        minDistance = slMinDistance.rpn.execute0();
        cv::goodFeaturesToTrack(ptr0, pts, featurepoints, std::max(0.0, qualityLevel), std::max(0.0, minDistance));
        if(pts.size() > 0) {
            cv::calcOpticalFlowPyrLK(ptr0, ptr1, pts, nextPts, status, err);
            { // MotionVertexList
                float sx = (float) filterWidth / (float) video_width;
                float sy = (float) filterHeight / (float) video_height;
                vertices_size = 0;
                for(size_t px = 0; px < nextPts.size(); ++px) {
                    if(status[px] && vertices_size < vertices_capacity) {
                        auto np1 = nextPts[px];
                        *FVx(vertices_size) = sx * np1.x;
                        *FVy(vertices_size) = sy * np1.y;
                        cv::Vec4i ic = frame_captured.at<cv::Vec4b>(np1);
                        ofFloatColor(ic[0] / 256.0f, ic[1] / 256.0f, ic[2] / 256.0f).getHsb(*FVh(vertices_size), *FVs(vertices_size), *FVv(vertices_size));
                        *FVa(vertices_size) = 1;
                        ++vertices_size;
                    }
                }
            }
            switch(mode) {
            case 1: { // MotionTraceLine
                LineTrace *line = new LineTrace();
                for(size_t px = 0; px < nextPts.size(); ++px) {
                    if(status[px]) {
                        auto np0 = pts[px];
                        auto np1 = nextPts[px];
                        float dx = np0.x - np1.x;
                        float dy = np0.y - np1.y;
                        float square_distance = dx * dx + dy * dy;
                        if(square_distance < 100) {
                            cv::Vec4i ic = frame_captured.at<cv::Vec4b>(np1);
                            line->append(ofFloatColor(ic[0] / 256.0f, ic[1] / 256.0f, ic[2] / 256.0f), ofPoint(np0.x, np0.y), ofPoint(np1.x, np1.y));
                        }
                    }
                }
                linetraces.append(line, traceLenght);
            }
                break;
            case 2: // FeaturePointCircles
                break;
            default: { // MotionFeature
            }
            }
        }
    }
}

void CvMotionTrace::drawFeaturePoints() {
    int tx = 0;
    for(auto p : nextPts) {
        if(status[tx]) {
            float dx = p.x - pts[tx].x;
            float dy = p.y - pts[tx].y;
            float square_distance = dx * dx + dy * dy;
            if(square_distance < 100) {
                cv::Vec4i ic = frame_captured.at<cv::Vec4b>(p);
                ofSetColor(ic[0], ic[1], ic[2]);
                ofDrawCircle(p.x, p.y, 10);
            }
        }
        ++tx;
    }
}

void CvMotionTrace::draw(int li) {
    if(!hasData)
        return;

    fbo.begin();
    if(slClear.rpn.execute0() >= 0.0)
        ofClear(0, 0, 0, 255);

    bool upstream = slUpstream.rpn.execute0() >= 0.0;
    if(upstream) {
        if(upstream_ids.size() >= 1) {
            auto in_id = upstream_ids[0];
            if(filterDict.find(in_id) != filterDict.end()) {
                auto f = filterDict[in_id];
                if(f) {
                    ofPushMatrix();
                    ofScale((float) filterWidth / (float) video_width, (float) filterHeight / (float) video_height, 1);
                    f->getTexture().draw(0, 0);
                    ofPopMatrix();
                }
            }
        }
    }

    switch(mode) {
    case 1: { // MotionTraceLine
        ofPushMatrix();
        ofScale((float) filterWidth / (float) video_width, (float) filterHeight / (float) video_height, 1);
        ofNoFill();
        linetraces.draw();
        drawFeaturePoints();
        ofPopMatrix();
    }
        break;
    case 2: { // FeaturePointCircles
        ofNoFill();
        int collected = 0;
        cv::Point2f p0, p1, p2;
        for(size_t px = 0; px < nextPts.size(); ++px) {
            if(status[px]) {
                switch(collected) {
                case 0:
                    p0 = pts[px];
                    ++collected;
                    break;
                case 1:
                    p1 = pts[px];
                    ++collected;
                    break;
                case 2: {
                    p2 = pts[px];
                    float cx, cy, r;
                    findCircle(p0.x, p0.y, p1.x, p1.y, p2.x, p2.y, cx, cy, r);
                    ofPushMatrix();
                    ofScale((float) filterWidth / (float) video_width, (float) filterHeight / (float) video_height, 1);
                    cv::Vec4i ic = frame_captured.at<cv::Vec4b>(cv::Point2i((int) p0.x, (int) p0.y));
                    ofSetColor(ic[0], ic[1], ic[2]);
                    ofSetCircleResolution(std::sqrt(r));
                    ofDrawCircle(cy, cy, r);
                    ofPopMatrix();
                    p0 = p1;
                    p1 = p2;
                    }
                    break;
                }
            }
        }
    }
        break;
    default: { // MotionFeature
        ofPushMatrix();
        ofScale((float) filterWidth / (float) video_width, (float) filterHeight / (float) video_height, 1);
        drawFeaturePoints();
        ofPopMatrix();
    }
        break;
    }

    fbo.end();
}

void CvMotionTrace::finish() {
    ++frameIndex;
}
