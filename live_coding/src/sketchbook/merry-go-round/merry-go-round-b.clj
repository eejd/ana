(ns lv.demo)
(use 'lv.core)
; video: merry-go-round
(layout "horizontal" [img0 imgs1 cline3 ras2])
(imagesequ "20191217-Karussell-4/*.png")
(image "20191217-Karussell-4/*99.png")
(render)

(rgb :img0 [c0 c1 c2]
  (rgb (* (- 1.5 y) (+
    (* 0.7 (usin (* 0.11 f)) c0.rgb)
    (* 0.7 (ucos (* 0.13 f)) c1.rgb)
    (* 0.7 (usin (* 0.17 f)) c2.rgb)
       )))
)
(render 0)
(hsv :imgs1 [c0]
  (rot (/ pi 2))
  ; (k (* slength (unoise (* 0.001 sframe) 0.1)))
  (k (- slength sframe))
  ; (k 954)
  (hsv c0.hsv)
)
(render 1)

(rgb :ras2 [img0 imgs1 cline3]
  (rgb (+ (* 2 cline3.rgb)
          (* img0.rgb imgs1.rgb)))
)
(render 2)

(expr :cline3 [imgs1]
  (count 250)
  (max 500)
  (thres 0.35)
  (paint -1)
  (fill -1)
  (holes 1)
)
(render 3)
