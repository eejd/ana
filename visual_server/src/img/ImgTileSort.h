#pragma once

#include "ofMain.h"
#include "Filter.h"
#include "calc/ScalarLisp.h"

struct Tpix {
    Tpix(size_t x, size_t y, float c) {
        comp = c;
        tile_ix = x;
        tile_iy = y;
    };
    float comp;
    size_t tile_ix, tile_iy;
};

class ImgTileSort: public Filter {
public:
    ImgTileSort(ofGLWindowSettings settings, OscListener *oscParams);
    virtual ~ImgTileSort();
    virtual void setup(const std::string id, int numSamples);
    virtual void reload(ofxOscMessage const &msg);
    virtual void update();
    virtual void draw(int li);

    ScalarLisp slCompare;
    ScalarLisp slTileSizeX;
    ScalarLisp slTileSizeY;

private:
    ScalarLisp slMode;
    int mode = 0;
    ScalarLisp slClutterLevel;
    size_t tile_size_x = 60;
    size_t tile_size_y = 60;
    ofFloatPixels pixels;
    std::vector<Tpix> hpix;
};
