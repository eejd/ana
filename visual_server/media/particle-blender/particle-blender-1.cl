#pragma OPENCL EXTENSION all : enable

#define TRAIL(xy) (clamp((int)(img_y_max*xy.y), 0, img_y_max) * img_x_max \
                 + clamp((int)(img_x_max*xy.x), 0, img_x_max))

// const sampler_t SAMPLER = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_LINEAR;
const sampler_t SAMPLER = CLK_NORMALIZED_COORDS_TRUE | CLK_ADDRESS_REPEAT | CLK_FILTER_LINEAR;

__kernel void image_setup() {}
__kernel void image_change() {}

// ---------------------------------------------------------
__kernel void ag_setup(
__read_only image2d_t image_in,
const int img_x_max,
const int img_y_max,
__global float2* pos,
__global float2* pos_out,
__global float* vel,
__global float* vel_out,
__global float* head,
__global float* head_out,
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7
) 
{
    const int ai = get_global_id(0);
    pos_out[ai] = pos[ai];
    vel_out[ai] = vel[ai];
    head_out[ai] = head[ai];
}

// ---------------------------------------------------------
__kernel void trail_setup(
__read_only image2d_t image_in,
const int img_x_max,
const int img_y_max,
__global int* trail,
__global int* trail_out,
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7
) 
{
    const int ti = get_global_id(0);
    trail_out[ti] = 0;
}


// ---------------------------------------------------------
__kernel void ag_move(
__read_only image2d_t image_in,
const int img_x_max,
const int img_y_max,
__global float2* pos,
__global float2* pos_out,
__global float* vel,
__global float* vel_out,
__global float* head,
__global float* head_out,
__global int* trail,
volatile __global int* trail_out,
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7
)
{
    const int ai = get_global_id(0);
    
    float2 old_pos = pos[ai];
    while(old_pos.x <  0.0f) old_pos.x += 1.0f;
    while(old_pos.x >= 1.0f) old_pos.x -= 1.0f;
    while(old_pos.y <  0.0f) old_pos.y += 1.0f;
    while(old_pos.y >= 1.0f) old_pos.y -= 1.0f;
    float4 color = read_imagef(image_in, SAMPLER, old_pos);
//     float rotate_angle = atan2(color.y-0.5f, color.x-0.5f);
    float rotate_angle = atan2(color.y-0.5, color.x-0.5);
    
    const float new_head = mix(head[ai], rotate_angle, p3);
    const float new_vel  = mix(vel[ai], 0.1f*(color.y+color.x), p0);
    
    float2 new_pos = pos[ai] + new_vel * (float2)(cos(new_head), sin(new_head));
    while(new_pos.x <  0.0f) new_pos.x += 1.0f;
    while(new_pos.x >= 1.0f) new_pos.x -= 1.0f;
    while(new_pos.y <  0.0f) new_pos.y += 1.0f;
    while(new_pos.y >= 1.0f) new_pos.y -= 1.0f;
    pos_out[ai] = new_pos;
    vel_out[ai] = new_vel;
    head_out[ai] = new_head;

    int ti = TRAIL(old_pos);
    atomic_add(&trail_out[ti], (int)1024);
}


// ---------------------------------------------------------
__kernel void trail_disipate(
__read_only image2d_t image_in,
const int img_x_max,
const int img_y_max,
__global int* trail,
__global int* trail_out,
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7
)
{
    const int ti = get_global_id(0);
    const int tmax = img_x_max * img_y_max;
    
    const int px = ti % img_x_max;
    const int py = ti / img_x_max;
    float4 color = read_imagef(image_in, SAMPLER, (float2)(px/(float)img_x_max, py/(float)img_y_max));
   
    int W = ti-1;         if(W<0)                    W += img_x_max;
    int O = ti+1;         if(O>=img_x_max)           O -= img_x_max;
    int S = ti+img_x_max; if(S>=tmax)                S -= tmax;
    int N = ti-img_x_max; if(N<0)                    N += tmax;
    trail_out[ti] = (float)(trail[W]
                    +trail[O]
                    +trail[S]
                    +trail[N]
                    + 2*trail[ti]
//                     + (color.x * p5 * 1024)
                    ) / 8;
}


// ---------------------------------------------------------
__kernel void ag_clear(
__read_only image2d_t image_in,
const int img_x_max,
const int img_y_max,
__write_only image2d_t image_out
)
{
    const int px = get_global_id(0);
    const int py = get_global_id(1);
    write_imagef(image_out, (int2)(px, py), (float4)(0, 0, 0, 1));
}


// ---------------------------------------------------------
__kernel void ag_draw(
__read_only image2d_t image_in,
const int img_x_max,
const int img_y_max,
__global int* trail,
// __global int* trail_out,
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7,
__write_only image2d_t image_out
)
{
    const int px = get_global_id(0);
    const int py = get_global_id(1);
    const int ti = py * img_x_max + px;
    
    float gray = pow((float)trail[ti] / (float)1024, p7);
    write_imagef(image_out, (int2)(px, py), (float4)(gray, gray, gray, 1));
}
