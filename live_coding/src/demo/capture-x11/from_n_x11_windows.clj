(ns lv.demo)
(use 'lv.core)

(layout "grid" [win0 win1]
               [ras1 ras2])
(render 2)

; read from a visible X11 window
(expr :win0 []
 ; "analog Not analog"
 "System Monitor"
 ; (i -1)
 ; (i (* 0.05 f))
)

(expr :win1 []
 "ana : top"
 ; (i -1)
 ; (i (* 0.05 f))
)


(rgb :ras1 [win0 win1]
  ; (inv)
  (kaleid 13)
  (rot (* 0.01 f))
  (mirror)
  ; (inv)
  ; (mirror)
  (rgb (max win0.rgb win1.rgb))
)

; mix the upstream video with the current frame buffer content.
; reads from buffer ras2 and writes the result back to buffer ras2.
(rgb :ras2 [win0 ras2]
  (kaleid 5)
  (mirror)
  (rot (* 0.005 f))
  (rgb (mix win0.rgb ras2.rgb 0.3))
)
