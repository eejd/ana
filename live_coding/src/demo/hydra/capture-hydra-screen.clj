(ns lv.demo)
(use 'lv.core)

(layout "grid"
 [screen0 mopr3 mt5]
 [ras0 cline2 ras3 ])
(render)

; Requires that Hydra's window is above aNa's window.
; Because aNa actually copies its own window
; and as a side effect all windows that are above it.
(expr :screen0 [c0]
  (x 300)
  (y 0)
  (w 512)
  (h 512)
)

(expr :cline2 [screen0]
  (count 250)
  (max 2000)
  (thres 0.2)
  (fill -1)
  (names -1)
  (clear (zero? (% f 30) 1 -1))
  (paint -1)
  (holes 1)
)

(expr :mopr3 [cline2] "dbb")

(rgb :ras3 [screen0 mopr3 ras3]
  (kaleid 3)
  (rot (* f ks2 0.005))
  (rgb (mix ras3.rgb
            (+ screen0.rgb mopr3.rgb)
            ks1))
)

(expr :mt5 [screen0]
  (mode 0)
  (count 100)
  (trace 32)
  (qual 0.02)
  (paint -1)
)
