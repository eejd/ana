#pragma once

#include <opencv2/opencv.hpp>
#include "ofxCv.h"

#include "OscListener.h"
#include "Filter.h"
#include "calc/ScalarLisp.h"

class CvFlowFarneback : public Filter {
	public:
		CvFlowFarneback(ofGLWindowSettings settings, OscListener * oscParams);
		virtual ~CvFlowFarneback();
		virtual void setup(const std::string id, int numSamples);
		void reload(ofxOscMessage const &msg);
		virtual void update();
		virtual void draw(int li);

	private:
		std::shared_ptr<Filter> f = nullptr;
		int reduced_size = 12;
        size_t reduced_filterHeight = 1;
        size_t reduced_filterWidth = 1;
        bool need_realloc = true;

        bool hasData = false;
        ofPixels grabbedPixels;
        cv::Mat frame_captured;
        cv::Mat frame_distored;
        cv::Mat frame_scaled;
        cv::Mat prev_scaled;
        cv::Mat flow;
        cv::Mat distortion;

        ScalarLisp slReduce;
        ScalarLisp slPyrScale;
        ScalarLisp slLevels;
        ScalarLisp slWinSize;
        ScalarLisp slIterations;
        ScalarLisp slPolyN;
        ScalarLisp slPolySigma;
        ScalarLisp slUseGaussian;
        ScalarLisp slMode;
        int mode = 0;
        ScalarLisp slStepSize;
        int stepSize = 4;
        ScalarLisp slClear;
        ScalarLisp slSaturation;
        ScalarLisp slCircleAlpha;
        ScalarLisp slCircleRadius;
        ScalarLisp slUpstream;
};
