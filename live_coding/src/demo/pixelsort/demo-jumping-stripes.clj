(ns lv.demo)
(use 'lv.core)

(layout "grid" [img0 tile1]
               [tile2 ras3])
; original image
; Chris Roth
; CC-BY-SA-NC 4.0
; https://chrisroth.art/produkt/art-piece-crypto/
(image "CC-BY-SA-NC/46a5fb84b7243e67.jpeg")

(rgb :img0 [c0] (rgb c0.rgb))

(expr :tile1 [img0]
	(mode 1)
	(clutter (* 0.0001 (usin (* 0.0031 f))
		          (unoise x y (* 0.0013 f))
		          (sqrt (+ (sqr (- x 600))
							         (sqr (- y 150))))))
	(x 6)
	(y 270)
	; (comp (+ (unoise h (* 0.03 f) v)))
)

(expr :tile2 [img0]
	(mode 1)
	(clutter (* 0.0001 (usin (* 0.0061 f))
		          (unoise x y (* 0.0031 f))
		          (sqrt (+ (sqr (- x 600))
							         (sqr (- y 150))))))
	(x 120)
	(y 6)
	; (comp (+ (unoise h (* 0.03 f) v)))
)

(rgb :ras3 [tile1 tile2]
	(rgb (mix tile1.rgb
		        (mix tile1.rgb tile2.rgb 0.5)
						(usin (/ f 300))))
)
(render 3)
