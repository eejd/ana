(ns lv.demo)
(use 'lv.core)

(layout "grid" [mov0 clipp1])
(movie "bkgrd/*.MOV")

; two movies, but useing only the first
(hsv :mov0 [c0 c1]
  (sy 2.5)
  (ty 0.1)
  (hsv c0.hsv)
)

; read from nanoKONTROL2
; kp0    0.29134
;
; ks0    1
; ks1    0
; ks2    0.41732
; ks3    0
;
; ks7    0.54331
(expr :clipp1 [mov0]
  "cl/image-color-drift.cl"
  (paint 1)
  ; kp0 = mix upstren image
  (p0 (* kp0 0.1))
  ; ks0 .. ks3 = direction of the drift
  ; ks7 = damping the feedback loop
  (p1 (* ks7 (- ks0 0.5) 8))
  (p2 (* ks7 (- ks1 0.5) 8))
  (p3 (* ks7 (- ks2 0.5) 8))
  (p4 (* ks7 (- ks3 0.5) 8))
)
(render 1)
