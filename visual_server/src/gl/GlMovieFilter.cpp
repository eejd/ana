#include "ofMain.h"

#include "stringhelper.h"
#include "globalvars.h"
#include "OscListener.h"
#include "gl/GlMovieFilter.h"

GlMovieFilter::GlMovieFilter(ofGLWindowSettings settings, OscListener * oscParams) :
        Filter(settings, oscParams) {
}


GlMovieFilter::~GlMovieFilter(){
}

void GlMovieFilter::setInputTexture(std::string inputs) {
	split(trim(inputs, '[', ']'), ' ', upstream_ids);
	for (std::vector<std::string>::iterator it = upstream_ids.begin() ; it != upstream_ids.end(); ++it) {
		std::string idx = *it;
		findAndReplaceAll(idx, "c", "");
		if(idx.length() > 0) {
			int movieIndex = std::stoi(idx);
			channel_ids.push_back(movieIndex % maxMovies);
		}
	}
}

void GlMovieFilter::reload(ofxOscMessage const &msg) {
    upstream_ids.clear();
    channel_ids.clear();
    auto narg = msg.getNumArgs();
    if(narg > 1) {
        setInputTexture(msg.getArgAsString(1));
        if(narg > 2) {
            Filter::compileShader(msg.getArgAsString(0), msg.getArgAsString(3));
        }
    }
}

void GlMovieFilter::draw(int li){
	fbo.begin();
	ofClear(0, 0, 0, 255);

	shader.begin();
	sendUniforms(shader, li);

	auto tx = 0;
	for (std::vector<int>::iterator it = channel_ids.begin() ; it != channel_ids.end(); ++it) {
		auto texture = movies[*it].getTexture();
		if(texture.isAllocated())
			shader.setUniformTexture("tex" + to_string(tx), texture, tx);
		++tx;
	}

	ofDrawRectangle(0, 0, filterWidth, filterHeight);

	shader.end();
	fbo.end();
}
