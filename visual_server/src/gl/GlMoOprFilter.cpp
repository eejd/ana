#include <filesystem>
#include <chrono>

#include "ofMain.h"

#include "stringhelper.h"
#include "globalvars.h"
#include "OscListener.h"
#include "Filter.h"
#include "gl/GlMoOprFilter.h"

GlMoOprFilter::GlMoOprFilter(ofGLWindowSettings settings, OscListener * oscParams) : Filter(settings, oscParams){
}

GlMoOprFilter::~GlMoOprFilter(){
	opr_sequ.clear();
	dilateShader.unload();
	erodeShader.unload();
	blurShader.unload();
	tmpFbo0.clear();
	tmpFbo1.clear();
}

static std::string builtinVert = STRINGIFY(#version 330\n
uniform mat4 modelViewProjectionMatrix;\n
in vec4 position;\n
void main()\n
{\n
	gl_Position = modelViewProjectionMatrix * position;\n
}\n
);

static std::string builtinDilateFrag = STRINGIFY(#version 330\n
uniform sampler2DRect tex0;\n
out vec4 outputColor;\n
void main()\n
{\n
	outputColor = vec4(0.0);\n
\n
	outputColor = max(outputColor, texture(tex0, gl_FragCoord.xy + vec2(-1,  1)));\n
	outputColor = max(outputColor, texture(tex0, gl_FragCoord.xy + vec2( 0,  1)));\n
	outputColor = max(outputColor, texture(tex0, gl_FragCoord.xy + vec2( 1,  1)));\n
\n
	outputColor = max(outputColor, texture(tex0, gl_FragCoord.xy + vec2(-1,  0)));\n
	outputColor = max(outputColor, texture(tex0, gl_FragCoord.xy));\n
	outputColor = max(outputColor, texture(tex0, gl_FragCoord.xy + vec2( 1,  0)));\n
\n
	outputColor = max(outputColor, texture(tex0, gl_FragCoord.xy + vec2(-1, -1)));\n
	outputColor = max(outputColor, texture(tex0, gl_FragCoord.xy + vec2( 0, -1)));\n
	outputColor = max(outputColor, texture(tex0, gl_FragCoord.xy + vec2( 1, -1)));\n
\n
	outputColor.a = 1.0;\n
}\n
);

static std::string builtinErodeFrag = STRINGIFY(#version 330\n
uniform sampler2DRect tex0;\n
out vec4 outputColor;\n
void main()\n
{\n
	outputColor = vec4(1000.0);\n
\n
	outputColor = min(outputColor, texture(tex0, gl_FragCoord.xy + vec2(-1,  1)));\n
	outputColor = min(outputColor, texture(tex0, gl_FragCoord.xy + vec2( 0,  1)));\n
	outputColor = min(outputColor, texture(tex0, gl_FragCoord.xy + vec2( 1,  1)));\n
\n
	outputColor = min(outputColor, texture(tex0, gl_FragCoord.xy + vec2(-1,  0)));\n
	outputColor = min(outputColor, texture(tex0, gl_FragCoord.xy));\n
	outputColor = min(outputColor, texture(tex0, gl_FragCoord.xy + vec2( 1,  0)));\n
\n
	outputColor = min(outputColor, texture(tex0, gl_FragCoord.xy + vec2(-1, -1)));\n
	outputColor = min(outputColor, texture(tex0, gl_FragCoord.xy + vec2( 0, -1)));\n
	outputColor = min(outputColor, texture(tex0, gl_FragCoord.xy + vec2( 1, -1)));\n
\n
	outputColor.a = 1.0;\n
}\n
);

static std::string builtinBlurFrag = STRINGIFY(#version 330\n
uniform sampler2DRect tex0;\n
out vec4 outputColor;\n
void main()\n
{\n
	outputColor  = 0.077847 * texture(tex0, gl_FragCoord.xy + vec2(-1,  1));\n
	outputColor += 0.123317 * texture(tex0, gl_FragCoord.xy + vec2( 0,  1));\n
	outputColor += 0.077847 * texture(tex0, gl_FragCoord.xy + vec2( 1,  1));\n
\n
	outputColor += 0.123317 * texture(tex0, gl_FragCoord.xy + vec2(-1,  0));\n
	outputColor += 0.195346 * texture(tex0, gl_FragCoord.xy);\n
	outputColor += 0.123317 * texture(tex0, gl_FragCoord.xy + vec2( 1,  0));\n
\n
	outputColor += 0.077847 * texture(tex0, gl_FragCoord.xy + vec2(-1, -1));\n
	outputColor += 0.123317 * texture(tex0, gl_FragCoord.xy + vec2( 0, -1));\n
	outputColor += 0.077847 * texture(tex0, gl_FragCoord.xy + vec2( 1, -1));\n
\n
	outputColor.a = 1.0;\n
}\n
);

void GlMoOprFilter::setup(const std::string id, int numSamples) {
	Filter::setup(id, numSamples);

	tmpFbo0.allocate(filterWidth, filterHeight, GL_RGBA32F_ARB, numSamples);
	tmpFbo1.allocate(filterWidth, filterHeight, GL_RGBA32F_ARB, numSamples);

	ofLog(OF_LOG_NOTICE) << "compile build in shaders " << id;
	dilateShader.setupShaderFromSource(GL_VERTEX_SHADER, builtinVert);
	dilateShader.setupShaderFromSource(GL_FRAGMENT_SHADER, builtinDilateFrag);
	dilateShader.linkProgram();
	erodeShader.setupShaderFromSource(GL_VERTEX_SHADER, builtinVert);
	erodeShader.setupShaderFromSource(GL_FRAGMENT_SHADER, builtinErodeFrag);
	erodeShader.linkProgram();
	blurShader.setupShaderFromSource(GL_VERTEX_SHADER, builtinVert);
	blurShader.setupShaderFromSource(GL_FRAGMENT_SHADER, builtinBlurFrag);
	blurShader.linkProgram();
}

void GlMoOprFilter::reload(ofxOscMessage const &msg){
    auto narg = msg.getNumArgs();
    if(narg > 2) {
        Filter::setInputTexture(msg.getArgAsString(1));
        if(narg > 2) {
            opr_sequ.clear();
            for(auto r : msg.getArgAsString(2)){
                switch(r) {
                case 'b':
                    opr_sequ.push_back(&blurShader);
                    break;
                case 'd':
                    opr_sequ.push_back(&dilateShader);
                    break;
                case 'e':
                    opr_sequ.push_back(&erodeShader);
                    break;
                }
            }
        }
    }
}

void GlMoOprFilter::draw(int li) {
	size_t len = opr_sequ.size();
	size_t uix = 0;
	bool toggle = false;
	for(auto useShader : opr_sequ){
		bool last = uix+1 >= len;
		bool first = uix == 0;
		if(last) fbo.begin(); else (toggle ? tmpFbo0 : tmpFbo1).begin();
		ofClear(0, 0, 0, 255);

		useShader->begin();

		if(first) {
			if(upstream_ids.size() >= 1) {
				auto in_id = upstream_ids[0];
				if(filterDict.find(in_id) != filterDict.end()) {
					auto f = filterDict[in_id];
					if(f) {
						useShader->setUniformTexture("tex0", f->getTexture(), 0);
					}
				}
			}
		}
		else {
			useShader->setUniformTexture("tex0", (toggle ? tmpFbo1 : tmpFbo0).getTexture(), 0);
		}

		ofDrawRectangle(0, 0, filterWidth, filterHeight);

		useShader->end();
		if(last) fbo.end(); else (toggle ? tmpFbo0 : tmpFbo1).end();
		++uix;
		toggle = !toggle;
	}
}
