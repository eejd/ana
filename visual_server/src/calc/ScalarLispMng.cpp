#include <limits>
#include <math.h>
#include <string>

#include "ofMain.h"

#include "ana_features.h"
#include "util/stringhelper.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispMng.h"

#define TRACE_MONOTONIC

ScalarLispMng::ScalarLispMng() {
}

ScalarLispMng::~ScalarLispMng() {
}

int ScalarLispMng::compile(ScalarLisp &sl, std::string key, std::string fallback, ofxOscMessage const &msg, size_t first) {
    if(key.empty())
        ofLog(OF_LOG_FATAL_ERROR) << "ScalarLispMng::compile Key to search for is empty";
    std::string pattern1 = "(" + key + " ";//TODO
    std::string pattern2 = "(" + key + "(";
    std::string pattern3 = "(" + key + "\t";
    std::string pattern4 = "(" + key + "\n";
    size_t nargs = msg.getNumArgs();
    for(size_t ax = first; ax < nargs; ++ax) {
        auto expr = trim(msg.getArgAsString(ax));
        if(expr.find(pattern1) == 0)
            return sl.compile(trim(expr, pattern1, ")"));
        if(expr.find(pattern2) == 0)
            return sl.compile(trim(expr, pattern2, ")") + "(");
        if(expr.find(pattern3) == 0)
            return sl.compile(trim(expr, pattern3, ")"));
        if(expr.find(pattern4) == 0)
            return sl.compile(trim(expr, pattern4, ")"));
    }
    return sl.compile(fallback);
}

std::string ScalarLispMng::scanForString(std::string key, ofxOscMessage const &msg, size_t first) {
    size_t nargs = msg.getNumArgs();
    std::string pattern1 = "(" + key + " ";//TODO
    for(size_t ax = first; ax < nargs; ++ax) {
        auto expr = trim(msg.getArgAsString(ax));
        if(expr.find(pattern1) == 0) {
            //TODO
            std::string v = trim(trim(expr.substr(pattern1.size()), ' ', ')'), '"', '"');
            return v;
        }
    }
    return "";
}

