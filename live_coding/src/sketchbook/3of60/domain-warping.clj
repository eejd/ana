(ns lv.demo)
(use 'lv.core)

(layout "grid" [img0 img1]
               [ras3])
(image "DistortImage/a*.png")
(render)

(rgb :img0 [c0 c1]
  ; (tx (usin (* 0.1 f)))
  (ty 0.6)
  (sx 0.7)
  (sy 0.7)
  (rgb c0.rgb)
)

; (rgb :img1 [c0 c1]
;   (tx (usin (* 0.01 f)))
;   (ty (ucos (* 0.02 f)))
;   (tx (* 0.1   (unoise (* 0.017 f) y)))
;   (ty (* 0.1   (unoise (* 0.011 f) x)))
;   (tx (* 0.05  (snoise (* 0.033 f) y)))
;   (ty (* 0.05  (snoise (* 0.031 f) x)))
;   (tx (* 0.025 (snoise (* 0.037 f) y)))
;   (ty (* 0.025 (snoise (* 0.043 f) x)))
;   (rgb c1.rgb)
; )

(rgb :img1 [c0 c1]
  (tx (usin (* 0.01 f)))
  (ty (ucos (* 0.02 f)))
  (set vv (* 0.0005 f))
  (dotimes [n 5]
    (tx (* vv (snoise (* vv n 0.017 f) y)))
    (ty (* vv (snoise (* vv n 0.011 f) x)))
    (set! vv (/ vv 2))
  )
  (rgb c1.rgb)
)

(rgb :ras3 [img0 img1]
  (rgb (mix img1.rgb img0.rgb
            (+ (* 0.005 f)
               (* (usin (* 0.01 f))
                  (snoise x y)))
       )
  )
)
