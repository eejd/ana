; Karl Sims Reaction-Diffusion Tutorial
; http://www.karlsims.com/rd.html

; width and height must be power of 2
(ns lv.demo)
(use 'lv.core)

(layout "grid" [clpp0 ras1]
               [ras2])

(expr :clpp0 []
  "cl/Karl_Sims-Reaction_Diffusion.cl"
  (loops 30)
  (p0 0.95)  ; timestep
  (p1 1.0)   ; D_a
  (p2 0.5)   ; D_b
  (p3 0.055) ; f
  (p4 (+ 0.06 (* 0.004 (usin (* 0.1 f))))) ; k
)

(hsv :ras1 [clpp0]
  (v (sqrt clpp0.r))
)
(hsv :ras2 [clpp0]
  ; (rot (* 0.005 f))
  ; (kaleid (+ 1 (* 0.01 f)))
  ; (h (* 0.0001 x f)) (s (sqrt (* x y)) 0.4)
  (v (* 1.1 (atan clpp0.g clpp0.r)))
)
(render 2)
