#pragma once
#include "../ana_features.h"
#ifdef WITH_openCL
#include "ofMain.h"
#include "MSAOpenCL.h"
#include "OscListener.h"
#include "ClFilter.h"

class ClUpstreamPoints : public ClFilter {
	public:
		ClUpstreamPoints(ofGLWindowSettings settings, OscListener * oscParams);
	    virtual ~ClUpstreamPoints();
		virtual void setup(const std::string id, int numSamples);
		virtual void update();
		virtual void runKernel(msa::OpenCLKernelPtr kernel, int vsize);

	private:
		msa::OpenCLBuffer fv_buffer;
	    msa::OpenCLImage clOutImage;
};
#endif // WITH_openCL
