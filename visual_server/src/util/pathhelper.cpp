#include <glob.h>
#include <sys/stat.h>

#include "ofMain.h"
#include "util/stringhelper.h"
#include "Configuration.h"
#include "calc/MsgLisp.h"
#include "util/pathhelper.h"

bool file_exists(const std::string &name) {
    struct stat buffer;
    return (stat(name.c_str(), &buffer) == 0);
}

// see: https://stackoverflow.com/questions/8401777/simple-glob-in-c-on-unix-system
std::vector<std::string> file_names(const std::string pattern, bool verbose) {
    vector<string> filenames;
    glob_t glob_result;
    memset(&glob_result, 0, sizeof(glob_result));

    // do the glob operation
    int return_value = glob(pattern.c_str(), GLOB_TILDE, NULL, &glob_result);
    if(return_value != 0) {
        if(verbose) {
            ofLogError() << "collecting file names '" << pattern << "' failed with return value " << return_value;
        }
    } else {
        // collect all the filenames into a std::list<std::string>
        for(size_t i = 0; i < glob_result.gl_pathc; ++i) {
            filenames.push_back(string(glob_result.gl_pathv[i]));
        }
    }
    globfree(&glob_result);

    std::sort(filenames.begin(), filenames.end());
    return filenames;
}

bool makeDirectory(string epath) {
    if(!epath.empty()) {
        ofDirectory dir(epath);
        if(!dir.exists()) {
            dir.create(true);
        }
        return dir.exists();
    }
    return false;
}

std::vector<std::filesystem::path> buildFileList(std::string descr) {
    std::vector<std::filesystem::path> files;
    MsgLisp l1;
    ast_m* res = l1.compile(descr);
    bool ok = res
            && res->count() == 2
            && (res->at(0)->tok == "image" || res->at(0)->tok == "movie" || res->at(0)->tok == "imagesequ");
    if(ok) {
        ast_m* r1 = res->at(1);
        if(r1->tok == "[") {
            for(int pi=0; pi < r1->count(); ++pi) {
                std::string pattern = trim(r1->at(pi)->tok, '"', '"');
                std::vector<std::string> fns = file_names(asString(MEDIA_PATH) + "/" + pattern);
                for(std::vector<std::string>::iterator fit = fns.begin(); fit != fns.end(); ++fit) {
                    std::string fn = *fit;
                    files.push_back(fn);
                }
            }
        }
    }
    return files;
}


