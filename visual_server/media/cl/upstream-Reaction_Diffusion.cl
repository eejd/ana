// Karl Sims Reaction-Diffusion Tutorial
// http://www.karlsims.com/rd.html

const sampler_t SAMPLER = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_LINEAR;

#define XY(x,y) ((x) & ((X)-1)) * (Y) + ((y) & ((Y)-1))
// #define XY(x,y) ((x) % ((X))) * (Y) + ((y) % ((Y)))
// int XY(int x, int y) { return ((x % 1024) * 1024) + (y % 1024); }
bool inside(float x, float y, float mx, float my) {
    float dx = mx-x;
    float dy = my-y;
    return (dx * dx + dy * dy) < 0.0005;
}

__kernel void image_setup(
__global float* a_in,
__global float* a_out,
__global float* b_in,
__global float* b_out,
__global float* c_in,
__global float* c_out,
__global float* d_in,
__global float* d_out,
__global float* e_in,
__global float* e_out,
__global float* f_in,
__global float* f_out,
__global float* g_in,
__global float* g_out,
__global float* h_in,
__global float* h_out,
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7,
__read_only image2d_t image_in,
__write_only image2d_t image2
) 
{

    const int x = get_global_id(0);
    const int y = get_global_id(1);
    const int X = get_global_size(0);
    const int Y = get_global_size(1);
    const int ixy = XY(x, y);
    
    a_out[ixy] = 1.0;
    if(        inside((float)x/(float)X, (float)y/(float)Y, 0.50, 0.50)
            || inside((float)x/(float)X, (float)y/(float)Y, 0.40, 0.60)
            || inside((float)x/(float)X, (float)y/(float)Y, 0.50, 0.82)
            || inside((float)x/(float)X, (float)y/(float)Y, 0.50, 0.20)
            || inside((float)x/(float)X, (float)y/(float)Y, 0.30, 0.40)
            || inside((float)x/(float)X, (float)y/(float)Y, 0.80, 0.80)
            || inside((float)x/(float)X, (float)y/(float)Y, 0.82, 0.30)) {
        b_out[ixy] = 1.0f;
    } else {
        b_out[ixy] = 0.0f;
    }
    
    const int2 pixelcoord = (int2)(x, y);
    write_imagef(image2, pixelcoord, (float4)(a_out[ixy], b_out[ixy], 0, 1));
}

__kernel void image_change(
__global float *a_in,
__global float *a_out,
__global float *b_in,
__global float *b_out,
__global float *c_in,
__global float *c_out,
__global float* d_in,
__global float* d_out,
__global float* e_in,
__global float* e_out,
__global float* f_in,
__global float* f_out,
__global float* g_in,
__global float* g_out,
__global float* h_in,
__global float* h_out,
const float p0,
const float p1,
const float p2,
const float p3,
const float p4,
const float p5,
const float p6,
const float p7,
__read_only image2d_t image_in,
__write_only image2d_t image2)
{

    const int x = get_global_id(0);
    const int y = get_global_id(1);
    const int X = get_global_size(0);
    const int Y = get_global_size(1);
    const int ixy = XY(x, y);
    const int i_n  = XY(x  , y+1);
    const int i_ne = XY(x+1, y+1);
    const int i_e  = XY(x+1, y  );
    const int i_se = XY(x+1, y-1);
    const int i_s  = XY(x  , y-1);
    const int i_sw = XY(x-1, y-1);
    const int i_w  = XY(x-1, y  );
    const int i_nw = XY(x-1, y+1);

    const float a = a_in[ixy];
    const float b = b_in[ixy];
    const float c = c_in[ixy];

    const float a_n  = a_in[i_n];
    const float a_ne = a_in[i_ne];
    const float a_e  = a_in[i_e];
    const float a_se = a_in[i_se];
    const float a_s  = a_in[i_s];
    const float a_sw = a_in[i_sw];
    const float a_w  = a_in[i_w];
    const float a_nw = a_in[i_nw];
    const float b_n  = b_in[i_n];
    const float b_ne = b_in[i_ne];
    const float b_e  = b_in[i_e];
    const float b_se = b_in[i_se];
    const float b_s  = b_in[i_s];
    const float b_sw = b_in[i_sw];
    const float b_w  = b_in[i_w];
    const float b_nw = b_in[i_nw];

    const float _K0 = -1.00f; // center weight
    const float _K1 =  0.20f; // edge-neighbors
    const float _K2 =  0.05f; // vertex-neighbors

    // compute the Laplacians of each chemical
    float laplacian_a = a_n*_K1 + a_s*_K1 + a_w*_K1 + a_e*_K1
                    + a_ne*_K2 + a_nw*_K2 + a_se*_K2 + a_sw*_K2
                    + a*_K0;
    float laplacian_b = b_n*_K1 + b_s*_K1 + b_w*_K1 + b_e*_K1
                    + b_ne*_K2 + b_nw*_K2 + b_se*_K2 + b_sw*_K2
                    + b*_K0;

//     const float timestep = 0.95f;
//     const float D_a = 1.0f;
//     const float D_b = 0.5f;
//     const float f   = 0.055f;
//     const float k   = 0.062f;
    const int2 pixelcoord = (int2)(x, y);
    float4 rgba =  read_imagef(image_in, SAMPLER, pixelcoord);
    #define timestep p0
    #define D_a      p1
    #define D_b      p2
    #define f        rgba.x
    #define k        rgba.y

    const float abb = a*b*b;
    const float delta_a = D_a * laplacian_a - abb + f*(1.0f-a);
    const float delta_b = D_b * laplacian_b + abb - (k + f)*b;
    

    a_out[ixy] = clamp(a + timestep * delta_a, 0.0f, 1.0f);
    b_out[ixy] = clamp(b + timestep * delta_b, 0.0f, 1.0f);

    write_imagef(image2, pixelcoord, (float4)(a_out[ixy], b_out[ixy], 0, 1));
}

