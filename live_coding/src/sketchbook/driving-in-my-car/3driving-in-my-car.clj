(ns lv.demo)
(use 'lv.core)

(layout "grid"
  [imgs0 dist1] [ras3 mopr2])

(imagesequ "../../../timelapse/series/20201231-1-plattenspieler/split/*.png")
(render)

(hsv :imgs0 [c0]
  (k sframe)
  (hsv c0.hsv)
)
(expr :dist1 [imgs0]
  (mode 0)
  (clear -1)
  (levels 6)
  (iter 4)
  (win 11)
  (gauss 1)
  (step 15)
  (dist 4)
)
(render 1)
; (expr :mt1 [imgs0]
;   (mode (* (usin (* 0.011 f)) 2.1))
;   (clear (- (sin (* 0.023 f)) 0.5))
;   (trace 75)
;   (paint -1)
;   (count 90)
;   (qual 0.01)
;   (min 50)
; )

(expr :mopr2 [ras3]
  "deb"
)
(rgb :ras3 [imgs0 dist1]
  (rgb (* 8 dist1.b
          (* 2.5 imgs0.rgb)
          (* 0.5 dist1.rgb)))
)
(render 3)
