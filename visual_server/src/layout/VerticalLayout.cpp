#include "globalvars.h"
#include "Configuration.h"
#include "Filter.h"
#include "ofApp.h"
#include "layout/VerticalLayout.h"

VerticalLayout::VerticalLayout(int c) {
    cols = c > 0 ? c : 1;
}

VerticalLayout::~VerticalLayout() {
}

int VerticalLayout::numFilters() {
    return cols;
}

void VerticalLayout::draw(int render, std::vector<std::shared_ptr<Filter>> &filterLine) {
    int w = ofGetWidth();
    int h = ofGetHeight();
    bool badrange = false;
    if(render >= 0) {
        if((int) filterLine.size() > render) {
            auto s = filterLine[render];
            ofFbo &fbo = s->getFbo();
            int top_border  = std::max(0, (h - asInt(FILTER_HEIGHT)) / 2);
            int left_border = std::max(0, (w - asInt(FILTER_WIDTH)) / 2);
            fbo.draw(left_border, top_border);
        } else {
            badrange = true;
        }
    }
    if(badrange || render < 0) {
        int col_width = asInt(CANVAS_WIDTH) / cols;
        int top_border  = std::max(0, (h - asInt(FILTER_HEIGHT)) / 2);
        int left_border = std::max(0, w - col_width * cols) / 2;
        int u = 0;
        for(auto s : filterLine) {
            ofFbo &fbo = s->getFbo();
            fbo.draw(left_border + col_width * u, top_border,
                     col_width, asInt(FILTER_HEIGHT));
            ++u;
        }
    }
}
