#include "ofMain.h"

#include "ana_features.h"
#include "color/ColorQuantization.h"
#include "color/ColorQuantizationTest.h"

#define TRACE_MONOTONIC

ColorQuantizationTest::ColorQuantizationTest() {
}

ColorQuantizationTest::~ColorQuantizationTest() {
}

#ifdef WITH_testcases
int ColorQuantizationTest::test_exec_failed = 0;
int ColorQuantizationTest::test_syntax_failed = 0;

void ColorQuantizationTest::runTestCases() {
    ofLog(OF_LOG_NOTICE) << "--- begin ColorQuantizationTest::runTestCases ---";
    if(test_exec_failed || test_syntax_failed) {
        ofLog(OF_LOG_ERROR) << "TESTERROR " << test_syntax_failed << " " << test_exec_failed << " some test cases failed";
    }

    {
    image_t * im = read_ppm("media/Mastodon-#ArtReference/waterford-5915361/waterford-5915361_1920.ppm");
#ifdef TRACE_MONOTONIC
        timespec time1, time2;
        clock_gettime(CLOCK_MONOTONIC, &time1);
#endif // TRACE_MONOTONIC
    color_quant_image(im, 16, 0);
#ifdef TRACE_MONOTONIC
        clock_gettime(CLOCK_MONOTONIC, &time2);
        ofLog(OF_LOG_NOTICE)  << "w16\t" << (time2.tv_sec * 1000000000l + time2.tv_nsec) - (time1.tv_sec * 1000000000l + time1.tv_nsec);
#endif // TRACE_MONOTONIC
        write_ppm(im, "out-w16.pnm");
        std::free(im);
    }

    {
    image_t * im = read_ppm("media/Mastodon-#ArtReference/waterford-5915361/waterford-5915361_1920.ppm");
#ifdef TRACE_MONOTONIC
        timespec time1, time2;
        clock_gettime(CLOCK_MONOTONIC, &time1);
#endif // TRACE_MONOTONIC
    color_quant_image(im, 4, 0);
#ifdef TRACE_MONOTONIC
        clock_gettime(CLOCK_MONOTONIC, &time2);
        ofLog(OF_LOG_NOTICE)  << "w4\t" << (time2.tv_sec * 1000000000l + time2.tv_nsec) - (time1.tv_sec * 1000000000l + time1.tv_nsec);
#endif // TRACE_MONOTONIC
        write_ppm(im, "out-w4.pnm");
        std::free(im);
    }

    {
    image_t * im = read_ppm("media/Mastodon-#ArtReference/roses-5335743/roses-5335743_1920.ppm");
#ifdef TRACE_MONOTONIC
        timespec time1, time2;
        clock_gettime(CLOCK_MONOTONIC, &time1);
#endif // TRACE_MONOTONIC
    color_quant_image(im, 16, 0);
#ifdef TRACE_MONOTONIC
        clock_gettime(CLOCK_MONOTONIC, &time2);
        ofLog(OF_LOG_NOTICE)  << "r16\t" << (time2.tv_sec * 1000000000l + time2.tv_nsec) - (time1.tv_sec * 1000000000l + time1.tv_nsec);
#endif // TRACE_MONOTONIC
        write_ppm(im, "out-r16.pnm");
        std::free(im);
    }

    {
    image_t * im = read_ppm("media/Mastodon-#ArtReference/roses-5335743/roses-5335743_1920.ppm");
#ifdef TRACE_MONOTONIC
        timespec time1, time2;
        clock_gettime(CLOCK_MONOTONIC, &time1);
#endif // TRACE_MONOTONIC
    color_quant_image(im, 4, 0);
#ifdef TRACE_MONOTONIC
        clock_gettime(CLOCK_MONOTONIC, &time2);
        ofLog(OF_LOG_NOTICE)  << "r4\t" << (time2.tv_sec * 1000000000l + time2.tv_nsec) - (time1.tv_sec * 1000000000l + time1.tv_nsec);
#endif // TRACE_MONOTONIC
        write_ppm(im, "out-r4.pnm");
        std::free(im);
    }

    {
    image_t * im = read_ppm("media/Mastodon-#ArtReference/woman-6004282/face6004282_640_640.ppm");
#ifdef TRACE_MONOTONIC
        timespec time1, time2;
        clock_gettime(CLOCK_MONOTONIC, &time1);
#endif // TRACE_MONOTONIC
    color_quant_image(im, 16, 1);
#ifdef TRACE_MONOTONIC
        clock_gettime(CLOCK_MONOTONIC, &time2);
        ofLog(OF_LOG_NOTICE)  << "y16d\t" << (time2.tv_sec * 1000000000l + time2.tv_nsec) - (time1.tv_sec * 1000000000l + time1.tv_nsec);
#endif // TRACE_MONOTONIC
        write_ppm(im, "out-y16d.pnm");
        std::free(im);
    }

    {
    image_t * im = read_ppm("media/Mastodon-#ArtReference/woman-6004282/face6004282_640_640.ppm");
#ifdef TRACE_MONOTONIC
        timespec time1, time2;
        clock_gettime(CLOCK_MONOTONIC, &time1);
#endif // TRACE_MONOTONIC
    color_quant_image(im, 4, 1);
#ifdef TRACE_MONOTONIC
        clock_gettime(CLOCK_MONOTONIC, &time2);
        ofLog(OF_LOG_NOTICE)  << "y4d\t" << (time2.tv_sec * 1000000000l + time2.tv_nsec) - (time1.tv_sec * 1000000000l + time1.tv_nsec);
#endif // TRACE_MONOTONIC
        write_ppm(im, "out-y04d.pnm");
        std::free(im);
    }

    {
    image_t * im = read_ppm("media/Mastodon-#ArtReference/woman-6004282/face6004282_640_640.ppm");
#ifdef TRACE_MONOTONIC
        timespec time1, time2;
        clock_gettime(CLOCK_MONOTONIC, &time1);
#endif // TRACE_MONOTONIC
    color_quant_image(im, 3, 1);
#ifdef TRACE_MONOTONIC
        clock_gettime(CLOCK_MONOTONIC, &time2);
        ofLog(OF_LOG_NOTICE)  << "y3d\t" << (time2.tv_sec * 1000000000l + time2.tv_nsec) - (time1.tv_sec * 1000000000l + time1.tv_nsec);
#endif // TRACE_MONOTONIC
        write_ppm(im, "out-y03d.pnm");
        std::free(im);
    }

    {
    image_t * im = read_ppm("media/Mastodon-#ArtReference/woman-6004282/face6004282_640_640.ppm");
#ifdef TRACE_MONOTONIC
        timespec time1, time2;
        clock_gettime(CLOCK_MONOTONIC, &time1);
#endif // TRACE_MONOTONIC
    color_quant_image(im, 2, 1);
#ifdef TRACE_MONOTONIC
        clock_gettime(CLOCK_MONOTONIC, &time2);
        ofLog(OF_LOG_NOTICE)  << "y02d\t" << (time2.tv_sec * 1000000000l + time2.tv_nsec) - (time1.tv_sec * 1000000000l + time1.tv_nsec);
#endif // TRACE_MONOTONIC
        write_ppm(im, "out-y02d.pnm");
        std::free(im);
    }

    {
    image_t * im = read_ppm("media/Mastodon-#ArtReference/waterford-5915361/waterford-5915361_1920.ppm");
#ifdef TRACE_MONOTONIC
        timespec time1, time2;
        clock_gettime(CLOCK_MONOTONIC, &time1);
#endif // TRACE_MONOTONIC
        std::vector<ofColor> pal;
        color_quant_palette(im, 16, pal);
        size_t plen = pal.size();
        for(auto pi=0u; pi<plen; ++pi) {
            ofLog(OF_LOG_NOTICE) << "r=" << (int)pal[pi].r << "\tg=" << (int)pal[pi].g << "\tb=" << (int)pal[pi].b << "\ta=" << (int)pal[pi].a;
        }
#ifdef TRACE_MONOTONIC
        clock_gettime(CLOCK_MONOTONIC, &time2);
        ofLog(OF_LOG_NOTICE)  << "w16\t" << (time2.tv_sec * 1000000000l + time2.tv_nsec) - (time1.tv_sec * 1000000000l + time1.tv_nsec);
#endif // TRACE_MONOTONIC
        write_ppm(im, "out-w16.pnm");
        std::free(im);
    }

    ofLog(OF_LOG_NOTICE) << "--- end ColorQuantizationTest::runTestCases ---";
}
#endif
