; width and height must be power of 2
(ns lv.demo)
(use 'lv.core)

(layout "grid" [clpp0 ras1])

(expr :clpp0 []
  "cl/Froese2014.cl"
  (loops 25)
)

(hsv :ras1 [clpp0]
  ; (kaleid 7)
  (v (sqr (log clpp0.b)))
)
(render 1)
