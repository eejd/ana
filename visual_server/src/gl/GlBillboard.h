#pragma once

#include "ofMain.h"
#include "Filter.h"
#include "OscListener.h"
#include "calc/ScalarLisp.h"

#define MAX_BILLBOARDS (size_t)10000

class GlBillboard : public Filter {
	public:
		GlBillboard(ofGLWindowSettings settings, OscListener * oscParams);
		virtual ~GlBillboard();
		virtual void setup(const std::string id, int numSamples);
		virtual void reload(ofxOscMessage const & msg);
		virtual void update();
		virtual void draw(int li);
		virtual void exportCsv(int li, std::ofstream & ostrm);

		ScalarLisp slCount;
        ScalarLisp slRotX;
        ScalarLisp slRotY;
        ScalarLisp slRotZ;
		ScalarLisp slX;
		ScalarLisp slY;
		ScalarLisp slZ;
        ScalarLisp slSize;
        ScalarLisp slClear;

        glm::vec3 cameraRotation;

		size_t num_billboards = 0;
		ofShader billboardShader;
        ofImage textureImg;
        bool texture_loaded = false;

		ofVboMesh billboards;

private:
	virtual void fill(size_t num);
	std::string alpha_billboard = "billboard/ring-alpha.png";  //TODO
};
